package model.request;

import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.Utils;
import application.utils.MD5;

public class Login {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strPhoneNo = "";
	private String m_strPassword = "";
	private String m_strVCode = "";

	public Login(){

	}

	/*
	 * {
	 * "DEVICEID":"83e462f770cc34eb",
	 * "Psw":"da907a1b8f74e6922d93b025eecfb852",
	 * "SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 * "VCode":"",
	 * "MOBILE":"+8615806131326"
	 * }
	 */
	public void setPhoneNo(String strPhoneNo){
		m_strPhoneNo = strPhoneNo;
	}

	public void setPassword(String strPassword){
		m_strPassword = strPassword;
	}

	public void setVerificationCode(String strVericationCode)
	{
		m_strVCode = strVericationCode;
	}

	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();

		String strEncodedPassword = MD5.getHashString(m_strPassword);

		//Get IMEI
		String strIMEI = Utils.getIMEI();
		
		try {
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("Mobile", m_strPhoneNo);
			jsonResult.put("Psw", strEncodedPassword);
			jsonResult.put("VCode", m_strVCode);
			jsonResult.put("DEVICEID", strIMEI);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonResult;
	}
}
