package application.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.io.http.HttpDateParser;

public class DateUtils {
	public static boolean acceptableDate(String strCurrentDate, String strPickupDate){
		long currentTime = HttpDateParser.parse(strCurrentDate);
		long pickupTime = HttpDateParser.parse(strPickupDate);
		
		if (currentTime > pickupTime) return true;
		
		return false;
	}
	
	public static String getCurrentTimeString(){
		String strCurrentTime = "";
		
		SimpleDateFormat formDateNow = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	    strCurrentTime = formDateNow.format(new Date());
		    
		return strCurrentTime;    
	}
	
	public static boolean acceptableDate(String strPickupDate)
	{
	    SimpleDateFormat formDateNow = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	    
	    String strCurrentDate = formDateNow.format(new Date());
	    
	    long currentTime = HttpDateParser.parse(strCurrentDate);
		long pickupTime = HttpDateParser.parse(strPickupDate);
		
		if (currentTime < pickupTime) return true;
		
		return false;
	}
	
	public static String StringToDate(String dateToParse) 
	{

//		Date formatter = new Date(HttpDateParser.parse(dateToParse));
//		SimpleDateFormat dateFormat = new SimpleDateFormat(
//				"yyyy-MM-dd HH:mm");
//		Calendar cal = Calendar.getInstance();
//		int offset = cal.getTimeZone().getRawOffset();
//		formatter.setTime(formatter.getTime() + offset);
//		String strCustomDateTime = dateFormat.format(formatter);
		String strCustomDateTime = dateToParse.substring(0, dateToParse.length() - 3);
		
		return strCustomDateTime;
	}
}
