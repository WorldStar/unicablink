package application.setting;

import org.json.me.JSONArray;
import net.rim.device.api.util.Persistable;
import application.ResouceConstant;

public class AppSetting implements Persistable {
	/**
	 * For User information
	 */
	private String strName;
	private String strPhone;
	private String strEmail;
	private String strPassword;
	private String strCustomerID;

	/**
	 * For Log in and Verification
	 */
	private boolean bLoggedIn;
	private boolean bVerified;
	private int nAllOrderCount;
	
	/**
	 * For Booking History
	 */
	private int nOrderId[];

	public AppSetting(){
		//initialize setting
		strName = "";
		strPhone = "";
		strEmail = "";
		strPassword = "";
		strCustomerID = "";
		
		nOrderId = new int[ResouceConstant.MAX_SAVE_ORDER_COUNT];
		
		bLoggedIn = false;
		bVerified = false;
		
		nAllOrderCount = 0;
		for (int i = 0; i < ResouceConstant.MAX_SAVE_ORDER_COUNT; i++)
		{
			nOrderId[i] = 0;
		}
	}
	
	/**
	 * Private functions
	 */
	private void increaseOrderCount()
	{
		nAllOrderCount++;
	}
	
	private void refreshOrders()
	{
		//shift orders
		for (int i = 0; i < ResouceConstant.MAX_SAVE_ORDER_COUNT - 1; i++){
			nOrderId[i] = nOrderId[i + 1];
		}
		
		nOrderId[ResouceConstant.MAX_SAVE_ORDER_COUNT - 1] = 0;
	}
	
	/**
	 * Set values for Setting
	 * params: Account Name, Phone, Email, Password, CustomerID, 
	 * 			Logged Info, Verification Info, Order IDs
	 */
	public void setLogged(boolean bLogged)
	{
		bLoggedIn = bLogged;
	}
	
	public void setVerified(boolean bVerified)
	{
		this.bVerified = bVerified;
	}
	
	public void setName(String strCustomerName)
	{
		strName = strCustomerName;
	}
	
	public void setPhoneNo(String strCustomerPhoneNo)
	{
		strPhone = strCustomerPhoneNo;
	}
	
	public void setEmailAddress(String strCustomerEmail)
	{
		strEmail = strCustomerEmail;
	}
	
	public void setPassword(String strCustomerPassword)
	{
		strPassword = strCustomerPassword;
	}
	
	public void setCustomerID(String strCustomerIdentifier)
	{
		strCustomerID = strCustomerIdentifier;
	}
	
	public void addOrder(int nOrderID)
	{
		if (nAllOrderCount != ResouceConstant.MAX_SAVE_ORDER_COUNT){
			increaseOrderCount();
		}
		else{
			refreshOrders();
		}

		nOrderId[nAllOrderCount - 1] = nOrderID;
	}
	
	/**
	 * Get values from Setting
	 * params: Account Name, Phone, Email, Password, CustomerID, 
	 * 			Logged Info, Verification Info, Order IDs
	 */
	public String getAccountName()
	{
		return strName;
	}
	
	public String getAccountPhone()
	{
		return strPhone;
	}
	
	public String getAccountEmail()
	{
		return strEmail;
	}
	
	public String getAccountPassword()
	{
		return strPassword;
	}
	
	public String getAccountCustomerID()
	{
		return strCustomerID;
	}
	
	public boolean getLoggedInfo()
	{
		return bLoggedIn;
	}
	
	public boolean getVerificationInfo()
	{
		return bVerified;
	}
	
	public int[] getOrders()
	{
		return nOrderId;
	}
	
	public int getOrderCount()
	{
		return nAllOrderCount;
	}
}
