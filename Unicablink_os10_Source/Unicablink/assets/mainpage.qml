import bb.cascades 1.0

Page {
    Container {
        id: mainPageZ10

        background: Color.create("#f5f5f5")
        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }

        horizontalAlignment: HorizontalAlignment.Center

        animations: [
            FadeTransition {
                id: showMainPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        Container {
            id: topMainBar

            preferredHeight: 112.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            ImageView {
                id: topMainBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 112.0
                preferredWidth: 768.0
            }
            Label {
                text: "Booking"
                textStyle.fontWeight: FontWeight.Normal
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 24.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
            }

        }//end TopBar
        
        Container{
            id: bookingInfo
            horizontalAlignment: HorizontalAlignment.Center

            preferredWidth: 768.0
            Container {
            	id: pickupInfo

                layout: DockLayout {

                }

                preferredWidth: 768.0
                maxHeight: 100.0

                
                
                ImageView {
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    maxHeight: 102.0
                    minHeight: 102.0
                    
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    preferredWidth: 768.0

                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 30.0
                    rightPadding: 46.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Pick up"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 20.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        preferredWidth: 224.0
                    }

                    Label {
                        id: pickupInfoLabel
                        text: ""
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 20.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        preferredWidth: 394.0
                    }
                    
                    ImageButton {
                        preferredWidth: 58.0
                        preferredHeight: 78.0
                        
                        defaultImageSource: "asset:///Image/icon_pickup.png"
                        pressedImageSource: "asset:///Image/icon_pickup.png"
                        
                        onTouch: {
                            if(!event.isUp()) return;
                            
                            
                        }

                    }

                }
            } //Pickup Info

            Container {
                id: pickupAtInfo

                layout: DockLayout {

                }

                preferredWidth: 768.0
                maxHeight: 100.0

                ImageView {
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    maxHeight: 102.0
                    minHeight: 102.0
                    preferredWidth: 768.0
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 30.0
                    rightPadding: 46.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Pick up at"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 20.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        preferredWidth: 224.0
                    }

                    TextField {
                        id: pickupAtInfoText
                        text: ""
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 20.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        hintText: "Building/House No/Lob..."
                        backgroundVisible: false
                    }
                }
            } //Pickup at Info

            Container {
                id: dropOffInfo

                layout: DockLayout {

                }

                preferredWidth: 768.0
                maxHeight: 100.0

                ImageView {
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    
                    maxHeight: 102.0
                    minHeight: 102.0
                    preferredWidth: 768.0
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 30.0
                    rightPadding: 46.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Drop off"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 20.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        preferredWidth: 224.0
                    }

                    Label {
                        id: dropoffInfoLabel
                        text: ""
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 20.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        preferredWidth: 394.0
                    }

                    ImageButton {
                        preferredWidth: 58.0
                        preferredHeight: 78.0

                        defaultImageSource: "asset:///Image/icon_dropoff.png"
                        pressedImageSource: "asset:///Image/icon_dropoff.png"

                        onTouch: {
                            if (! event.isUp()) return;

                        }

                    }

                }
            } //dropoff Info
            
            Container {
                id: timeBudgetInfo

                preferredHeight: 100.0
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight

                }
                Container {
                    id: timeInfo
                    preferredWidth: 366.0
                    preferredHeight: 100.0

                    layout: DockLayout {

                    }

                    ImageView {
                        imageSource: "asset:///Image/bar_insert_middle_profile.png"
                        scalingMethod: ScalingMethod.Fill

                        minHeight: 100.0
                        maxHeight: 100.0
                    }

                    Container {
                        leftPadding: 36.0
                        topPadding: 10.0
                        bottomPadding: 10.0
                        rightPadding: 46.0

                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }

                        verticalAlignment: VerticalAlignment.Center

                        ImageButton {
                            id: timeButton
                            preferredWidth: 58.0
                            preferredHeight: 58.0
                            defaultImageSource: "asset:///Image/icon_now.png"

                            onTouch: {
                                if (! event.isUp()) return;
                            }
                            pressedImageSource: "asset:///Image/icon_now.png"
                        }

                        Label {
                            id: timeLabel

                            leftMargin: 24.0
                            text: "Now"
                            textStyle.fontWeight: FontWeight.Default
                            textStyle.color: Color.Black
                            textStyle.textAlign: TextAlign.Left
                            textStyle.fontFamily: "Consolas"
                            textStyle.fontSizeValue: 20.0
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                        }
                    }

                } //Time Info
                
                Container {
                    id: budgetInfo
                    preferredWidth: 402.0
                    preferredHeight: 100.0

                    layout: DockLayout {

                    }

                    ImageView {
                        
                        imageSource: "asset:///Image/bar_insert_middle_profile.png"
                        scalingMethod: ScalingMethod.Fill

                        
                        
                        horizontalAlignment: HorizontalAlignment.Right
                        minWidth: 404.0
                        minHeight: 100.0
                        maxHeight: 100.0
                    }

                    Container {
                        leftPadding: 100.0
                        topPadding: 10.0
                        bottomPadding: 10.0
                        rightPadding: 46.0

                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }

                        verticalAlignment: VerticalAlignment.Center

                        ImageButton {
                            id: carButton
                            preferredWidth: 84.0
                            preferredHeight: 46.0
                            defaultImageSource: "asset:///Image/icon_budget.png"

                            onTouch: {
                                if (! event.isUp()) return;
                            }
                            pressedImageSource: "asset:///Image/icon_budget.png"
                        }

                        Label {
                            id: budgetLabel

                            leftMargin: 24.0
                            text: "Budget"
                            textStyle.fontWeight: FontWeight.Default
                            textStyle.color: Color.Black
                            textStyle.textAlign: TextAlign.Left
                            textStyle.fontFamily: "Consolas"
                            textStyle.fontSizeValue: 20.0
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Left
                        }
                    }

                }
            }//Time and Budget Info


        }
        

        //Map and Book now button
        Container {
            preferredWidth: 768.0

        }

    }
}
