package view.screens;

import model.data.DataConstant;
import net.rim.blackberry.api.invoke.Invoke;
import net.rim.blackberry.api.invoke.MessageArguments;
import net.rim.blackberry.api.invoke.PhoneArguments;
import net.rim.blackberry.api.mail.Address;
import net.rim.blackberry.api.mail.Folder;
import net.rim.blackberry.api.mail.Message;
import net.rim.blackberry.api.mail.Session;
import net.rim.blackberry.api.mail.SupportedAttachmentPart;
import net.rim.blackberry.api.mail.Transport;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import view.components.ImageButtonField;
import view.fieldmanager.InfoManager;
import application.ResouceConstant;
import application.Utils;

public class ContactScreen extends MainBaseScreen {

	/**
	 * Private members
	 */
	
	public ContactScreen() {
		super();

		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.CONTACT_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));

		/**
		 * Logo Bmp Manager
		 */
		InfoManager logoManager = new InfoManager(0, ResouceConstant.CONTACT_TOP_PADDING, 0, 0);

		Bitmap bmpLogo = Utils.getBitmapResource(ResouceConstant.CONTACT_LOGO);

		logoManager.add(new BitmapField(bmpLogo, Field.FIELD_HCENTER));

		/**
		 * Content Manager
		 */
		InfoManager contentManager = new InfoManager(0, ResouceConstant.CONTACT_CONTENT_PADDING, 0, 0)
		{
			public void sublayout(int nWidth, int nHeight)
			{
				int nNumFields = getFieldCount();
				int nYPos = this.getTopPadding(); 

				if (nNumFields == 2)
				{
					Field btnCall = this.getField(0);
					Field btnEmail = this.getField(1);

					setPositionChild(btnCall, (Utils.getDisplayWidth() - btnCall.getPreferredWidth()), nYPos);
					layoutChild(btnCall, this.getPreferredWidth(), Integer.MAX_VALUE);
					nYPos = nYPos + btnCall.getPreferredHeight() + getVPadding();

					setPositionChild(btnEmail, 0, nYPos);
					layoutChild(btnEmail, this.getPreferredWidth(), Integer.MAX_VALUE);
					nYPos = nYPos + btnEmail.getPreferredHeight() + getVPadding();
				}

				this.setExtent(Utils.getDisplayWidth(), nYPos - getVPadding());
			}
		};

		ImageButtonField btnCall = new ImageButtonField(ResouceConstant.CONTACT_BTN_CALL, "", 0, "")
		{
			public void clickButton()
			{
				callCenter();
			}
		};

		ImageButtonField btnEmail = new ImageButtonField(ResouceConstant.CONTACT_BTN_EMAIL, "", 0, "")
		{
			public void clickButton()
			{
				emailCenter();
			}
		};

		contentManager.add(btnCall);
		contentManager.add(btnEmail);

		/**
		 * Add Managers to MainManager
		 */
		add(logoManager);
		add(contentManager);
	}

	/**
	 * User Interaction Process
	 */
	public void callCenter()
	{
		// get phone number
        String strServerCallNumber = DataConstant.SERVICE_VOICE_CALL;  // assume some method here depending on your solution

        // make the call
        PhoneArguments callArgs = new PhoneArguments(PhoneArguments.ARG_CALL, strServerCallNumber);
        
        //Invoke
        Invoke.invokeApplication(Invoke.APP_TYPE_PHONE, callArgs);
	}

	public void emailCenter()
	{
		try
		{
			//make email
			Address[] address = new Address[1];
			address[0] = new Address(DataConstant.SERVICE_EMAIL_ADDRESS,"Unicablink Service Center");
			Folder folders[] = Session.getDefaultInstance().getStore().list(Folder.SENT);
			Message msg=new Message(folders[0]);
			msg.addRecipients(Message.RecipientType.TO, address);
			msg.setSubject("");
			
			//Invoke
			Invoke.invokeApplication(Invoke.APP_TYPE_MESSAGES,new MessageArguments(msg));
		}
		catch (Exception e){
			Dialog.alert("Failed send Email.");
		}
	}
}
