import bb.cascades 1.0

NavigationPane{
    id: navigationPane
    objectName: "navigationPane"

    Page {
    	id: loginPage
        property real screenWidth: 720
        property real screenHeight: 720

        Container {
            id: loginScreenQ10

            //q10

			
            background: Color.create("#f5f5f5")
            layout: StackLayout {

            }

            horizontalAlignment: HorizontalAlignment.Center

            animations: [
                FadeTransition {
                    id: showPageEffect
                    duration: 500
                    toOpacity: 1.0
                    fromOpacity: 0.0
                }
            ]

            preferredWidth: 720.0
            preferredHeight: 720.0
            
            //Main Contents
            Container {
                id: mainContents

                horizontalAlignment: HorizontalAlignment.Center
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }

                    //logo bar
                    Container {
                        layout: DockLayout {

                        }

                        horizontalAlignment: HorizontalAlignment.Center

                        topPadding: 20.0
                        ImageView {
                            preferredWidth: 162.0
                            preferredHeight: 172.0
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: "asset:///Image/icon_logo.png"
                            scalingMethod: ScalingMethod.Fill
                        }
                    }

                    //Log In info
                    Container {
                        id: logInInfo

                        horizontalAlignment: HorizontalAlignment.Center

                        preferredWidth: 580.0

                        topPadding: 46.0
                        layoutProperties: AbsoluteLayoutProperties {

                        }
                        Container {
                            id: mainInfo

                            layout: StackLayout {

                            }

                            //Phone No Info
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }

                                verticalAlignment: VerticalAlignment.Center

                                preferredHeight: 74.0

                                background: phoneInfoBackground.imagePaint

                                attachedObjects: [
                                    ImagePaintDefinition {
                                        id: phoneInfoBackground
                                        imageSource: "asset:///Image/bar_insert_big.png"
                                    }
                                ]

                                leftPadding: 28.0

                                rightPadding: 14.0
                                topPadding: 10.0
                                bottomPadding: 10.0
                                Label {
                                    preferredWidth: 140.0
                                    preferredHeight: 68.0

                                    text: "Phone"
                                    textStyle.fontWeight: FontWeight.Default
                                    textStyle.color: Color.Black
                                    textStyle.textAlign: TextAlign.Left
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 7.0
                                    verticalAlignment: VerticalAlignment.Center
                                    textStyle.fontSize: FontSize.PointValue

                                }

                                TextField {
                                    id: phoneText
                                    objectName: "phoneTextField"
                                    
                                    clearButtonVisible: false
                                    input.submitKey: SubmitKey.Done
                                    textFormat: TextFormat.Plain
                                    textStyle.textAlign: TextAlign.Left
                                    input.masking: TextInputMasking.Masked
                                    verticalAlignment: VerticalAlignment.Center
                                    inputMode: TextFieldInputMode.PhoneNumber
                                    backgroundVisible: false
                                    textStyle.color: Color.Black
                                    text: ""
                                    focusHighlightEnabled: false
                                    hintText: "e.g.+60123456789"
                                    textStyle.fontSize: FontSize.PointValue
                                    textStyle.fontSizeValue: 7.0
                                    
                                    onTextChanging: {
                                        eventHandler.correctTextField(phoneText, text);
                                    }
                                }

                            }

                            //Password Info
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }

                                verticalAlignment: VerticalAlignment.Center

                                preferredHeight: 74.0

                                background: passwordInfoBackground.imagePaint

                                attachedObjects: [
                                    ImagePaintDefinition {
                                        id: passwordInfoBackground
                                        imageSource: "asset:///Image/bar_insert_big.png"
                                    }
                                ]

                                leftPadding: 28.0

                                rightPadding: 14.0
                                topPadding: 10.0
                                bottomPadding: 10.0
                                topMargin: 20.0
                                Label {
                                    preferredWidth: 140.0
                                    preferredHeight: 68.0

                                    text: "Password"
                                    textStyle.fontWeight: FontWeight.Default
                                    textStyle.color: Color.Black
                                    textStyle.textAlign: TextAlign.Left
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 7.0
                                    verticalAlignment: VerticalAlignment.Center
                                    textStyle.fontSize: FontSize.PointValue
                                }

                                TextField {
                                    id: passwordText
                                    objectName: "passwordTextField"

                                    clearButtonVisible: false
                                    input.submitKey: SubmitKey.Done
                                    textFormat: TextFormat.Plain
                                    textStyle.textAlign: TextAlign.Left
                                    input.masking: TextInputMasking.Masked
                                    verticalAlignment: VerticalAlignment.Center
                                    inputMode: TextFieldInputMode.Password
                                    backgroundVisible: false
                                    textStyle.color: Color.Black
                                    text: ""
                                    focusHighlightEnabled: false
                                    hintText: "min.6 (alphanumeric)"
                                    textStyle.fontSizeValue: 7.0
                                    textStyle.fontSize: FontSize.PointValue
                                }

                            }

                            //Sign In Button
                            Container {
                                topMargin: 20.0

                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center

                                layout: DockLayout {

                                }

                                ImageButton {
                                    id: signInButton
                                    defaultImageSource: "asset:///Image/button_greenbar.png"
                                    pressedImageSource: "asset:///Image/button_greenbar.png"

                                    horizontalAlignment: HorizontalAlignment.Center

                                    preferredWidth:580.0
                                    preferredHeight: 64.0

                                }

                                Label {
                                    id: signInLabel
                                    textStyle.color: Color.Black
                                    text: "Sign In"

                                    textStyle.textAlign: TextAlign.Center
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 7.0
                                    horizontalAlignment: HorizontalAlignment.Center
                                    verticalAlignment: VerticalAlignment.Center
                                    textStyle.fontSize: FontSize.PointValue

                                }

                                onTouch: {
                                	if (event.isDown()){
                                    	validateUser();
                                    	//requestDisplayTabedMainPage();
                                    }
                                }
                            }

                            Container {
                                id: signUpContainer

                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }
                                topMargin: 54.0

                                horizontalAlignment: HorizontalAlignment.Center
                                Label {
                                    text: "Don't have an account ?"
                                    textStyle.textAlign: TextAlign.Center
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 7.0

                                    verticalAlignment: VerticalAlignment.Top
                                    textStyle.color: Color.Black
                                    textStyle.fontSize: FontSize.PointValue

                                }

                                ImageButton {
                                    id: signUpButton
                                    defaultImageSource: "asset:///Image/signup_z.png"
                                    pressedImageSource: "asset:///Image/signup_z.png"

                                    onClicked: {
                                        var signUpPage = signUpDefinition.createObject();
                                        navigationPane.push(signUpPage);
                                    }

                                    attachedObjects: [
                                        // Create the ComponentDefinition that represents the custom
                                        // component in myPage.qml
                                        ComponentDefinition {
                                            id: signUpDefinition
                                            source: "register.qml"
                                        }
                                    ]
                                    preferredWidth: 106.0
                                    preferredHeight: 32.0
                                    verticalAlignment: VerticalAlignment.Bottom
                                }
                                
                                
                                }

                            }

                            Container {
                                id: forgotContainer

                                layout: StackLayout {

                                }

                                topMargin: 14.0

                                horizontalAlignment: HorizontalAlignment.Center
                            preferredWidth: 236.0
                            preferredHeight: 32.0
                            ImageButton {
                                    id: forgotButton
                                    defaultImageSource: "asset:///Image/forgotpassword_z.png"
                                    pressedImageSource: "asset:///Image/forgotpassword_z.png"

	                                onClicked: {
	                                    var forgotPage = forgotPageDefinition.createObject();
	                                    navigationPane.push(forgotPage);
	
	                                }
	
	                                attachedObjects: [
		                                // Create the ComponentDefinition that represents the custom
		                                // component in myPage.qml
		                                ComponentDefinition {
		                                    id: forgotPageDefinition
		                                    source: "forgotpassword.qml"
		                                }
		                            ]
                            }

                            }
                        }
                    }
                }
            }

    }
    onPopTransitionEnded: {
        page.destroy();
    }

    //Validate User

    function validateUser() {
        eventHandler.ValidateUser(phoneText.text, passwordText.text);
    }

    //display Tabed Main Page

    function requestDisplayTabedMainPage() {
        eventHandler.ShowTabPage();
    }

}
