import bb.cascades 1.0

Container {
    id: root

    property alias itemWidth: root.preferredWidth
    property alias itemHeight: root.preferredHeight

    property alias labelImageSource: labelBackground.imageSource
    property alias textImageSource: textBackground.imageSource

    property alias nameLabelWidth: itemNameContainer.preferredWidth

    property int bStatus: 0
    property alias labelText: itemNameLabel.text
    property alias valueText: itemValueLabel.text
    property alias valueMultiline: itemValueLabel.multiline
    property bool bPopupable: false
    property bool bAnimate: false

    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }

    Container {
        id: itemNameContainer

        background: labelBackground.imagePaint
        minHeight: itemHeight

        leftPadding: 30.0
        rightPadding: 30.0

        layout: DockLayout {

        }
        Label {
            id: itemNameLabel

            preferredWidth: itemNameContainer.preferredWidth - 60

            verticalAlignment: VerticalAlignment.Center
            
            
            textStyle.color: Color.create("#ff606060")
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 7.0
        }
    }

    Container {
        id: itemValueContainer
        minHeight: itemHeight
        
        background: textBackground.imagePaint

        horizontalAlignment: HorizontalAlignment.Left
        verticalAlignment: VerticalAlignment.Center
        
        leftPadding: 30.0
        rightPadding: 10.0

        layout: DockLayout {

        }
        Label {
            id: itemValueLabel

			preferredWidth: root.preferredWidth - itemNameContainer.preferredWidth

            verticalAlignment: VerticalAlignment.Center

            
            textStyle.color: (bStatus == 0)? Color.create("#ff606060"): Color.White
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 7.0

            animations: [
                TranslateTransition {
                    id: translateAnimation
                    fromX: 0
                    toX: -400
                    duration: 6000
                    repeatCount: AnimationRepeatCount.Forever
                }
            ]
        }
        
        onTouch: {
            if (event.isUp()) {
                if (bPopupable) {
                    eventHandler.showFullLocation(itemNameLabel.text, itemValueLabel.text);
                }
            }
        }

        onCreationCompleted: {
            if(bAnimate == true){
                translateAnimation.play();
            }
        }
    }
    
    attachedObjects: [
        ImagePaintDefinition {
            id: labelBackground 
        },
        
        ImagePaintDefinition {
            id: textBackground
        }
    ]
}
