package application.utils;

import javax.microedition.global.Formatter;

import net.rim.device.api.ui.component.Dialog;

public class StringUtils {
	public static boolean checkPassword(String strPassword, String strRetype)
	{
		if (strPassword.length() != 6)
		{
			Dialog.alert("Password must be 6 digits.");
			return false;
		}
		
		if (!strPassword.equals(strRetype))
		{
			Dialog.alert("Please retype password correctly.");
			return false;
		}
		
		return true;
	}
	
	public static boolean isEmailFormat(String strText){
		boolean bCheck = true;
		//check "*.com"
		if (!strText.endsWith(".com")) 
			bCheck = false;
		
		boolean bCheckAmp = false;
		for (int i = 0; i < strText.length(); i++){
			String strTemp = String.valueOf(strText.charAt(i));
			 if (strTemp.equals("@")) {
				 bCheckAmp = true;
				 break;
			 }
		}
		
		//check containing of "@"
		if (!bCheckAmp) 
			bCheck = false;
		
		if ((strText.indexOf("@") == 0) || (strText.indexOf("@") == strText.length() - 5)) 
			bCheck = false;
		
		if (!bCheck)
		{
			Dialog.alert("Please input valid Email Address.");
			return false;
		}
		
		return true;
	}
	
	public static boolean isPhoneNoFormat(String strText){
		//check containing only number ('0' ~ '9')
		if (!strText.startsWith("+")){
			Dialog.alert("Phone No should start with +.");
			return false;
		}
		
		if ((strText.length() < 11) || (strText.length() > 15)) {
			Dialog.alert("Please input valid phone number.");
			return false;
		}
		
		for(int i = 1; i < strText.length(); i++){
			if ((strText.charAt(i) < '0') || (strText.charAt(i) > '9')){
				Dialog.alert("Please input valid phone number.");
				return false;
			}
		}
		
		return true;
	}
	
	public static String replace(String _text, String _searchStr, String _replacementStr){
		// String buffer to store str
		StringBuffer sb = new StringBuffer();
	
		// Search for search
		int searchStringPos = _text.indexOf(_searchStr);
		int startPos = 0;
		int searchStringLength = _searchStr.length();
	
		// Iterate to add string
		while (searchStringPos != -1) {
			sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
			startPos = searchStringPos + searchStringLength;
			searchStringPos = _text.indexOf(_searchStr, startPos);
		}
	
		// Create string
		sb.append(_text.substring(startPos,_text.length()));

		return sb.toString();
	}
	
	public static double formatDouble(double dNum, int nCut){
		Formatter formatter = new Formatter();
		String formatted = formatter.formatNumber(dNum, nCut);
		return Double.parseDouble(formatted);
	}

	
}
