package view.screens;

import application.Unicablink;
import application.ResouceConstant;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.FullScreen;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class SplashScreen extends FullScreen {

	/**
	 * Create Splash Screen Object
	 */
	public SplashScreen() {
		super();
		showSplashImage();
	}

	void showSplashImage() {
		Bitmap bootup_bmp = Utils.getBitmapResource(ResouceConstant.UNCAB_SPLASH_URL);
		Background bg = BackgroundFactory.createBitmapBackground(bootup_bmp);
		setBackground(bg);

		//run Splash thread and after 2s push PartnerScreen
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				Unicablink theApp = (Unicablink) UiApplication.getUiApplication();
				theApp.goStartScreen();
			}
		}, Utils.LOGO_SCREEN_DURATION, false);
	}
}
