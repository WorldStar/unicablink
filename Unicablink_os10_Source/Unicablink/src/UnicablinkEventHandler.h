/*
 * UnicablinkEventHandler.h
 *
 *  Created on: Jul 5, 2013
 *      Author: Passion
 */

#ifndef UNICABLINKEVENTHANDLER_H_
#define UNICABLINKEVENTHANDLER_H_

#include <QOBJECT>
#include <bb/cascades/Control>
#include <bb/cascades/maps/MapView>
#include <bb/platform/geo/GeoLocation>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/Dialog>
#include <QtLocationSubset/QGeoServiceProvider>
#include <QtLocationSubset/QGeoSearchManager>
#include <QtLocationSubset/QGeoSearchReply>

#include "UnicablinkApp.h"
#include "JSONConnect.hpp"
#include "SOAPRequest.h"

using namespace bb::cascades;
using namespace bb::cascades::maps;
using namespace bb::platform::geo;
using namespace bb::data;

class UnicablinkEventHandler: public QObject {
	Q_OBJECT
public:
	UnicablinkEventHandler();
	UnicablinkEventHandler(QObject *);
	virtual ~UnicablinkEventHandler();

public:
	//For showing pin, on z10
	Q_SIGNAL void addPintoContainer(int i, double lat, double lon);
    Q_SIGNAL void removePins();
    Q_SIGNAL void updatePins();
    Q_SIGNAL void selectCurrentTab();

    Q_INVOKABLE QVariantList worldToPixelInvokable(QObject* mapObject, double latitude, double longitude) const;
    Q_INVOKABLE void updateMarkers(QObject* mapObject, QObject* containerObject) const;

	//login page
	Q_INVOKABLE void ShowLogIn();
	Q_SLOT void onShowLogIn();

	//Register page
	Q_INVOKABLE void ShowRegister();
	Q_SLOT void onShowRegister();

	//My Booking Page
	Q_INVOKABLE void changeTime();

	//MainTabed page
	Q_INVOKABLE void ShowTabPage();
	Q_SLOT void onShowTabPage();

	//Book Again
	Q_INVOKABLE void BookAgain();

	//Validate User
	Q_INVOKABLE void ValidateUser(QString strPhoneNo, QString strPassword);
	Q_SLOT void CheckUser();

	//Signal
	Q_SIGNAL void updatePosition();

	//Register
	Q_INVOKABLE void RegisterUser(QString strName, QString strEmail, QString strPhone, QString strPassword, QString strRetype);
	Q_SLOT void CheckRegisterResult();

	//Send Email
	Q_INVOKABLE void SendEmail();

	//Activate User Account
	Q_INVOKABLE void activateUser(QString strActivationCode);

	//Apply Location
	Q_INVOKABLE void applyLocation(QString strLocation,double dLat, double dLon);
	Q_SLOT void setTargetLocation();

	//Hide and show above Container
	Q_INVOKABLE void showAboveContainer();
	Q_INVOKABLE void hideAboveContainer();

	//Voice Call
	Q_INVOKABLE void VoiceCall();

	//Pin at current position
	void setProvider();

	//Set correct time
	Q_INVOKABLE void setCorrectTime(QDateTime changedTime);

	//Resend Activation Code
	Q_INVOKABLE void resendActivationCode();
	Q_SLOT void checkResendStatus();

	//Initialize Profile Page
	Q_INVOKABLE void initializeProfilePage();

	//Initialize My Booking Page
	Q_INVOKABLE void initializeMyBookingPage();
	Q_SLOT void resetBookingPage();

	//Update Profile
	Q_INVOKABLE void updateProfile(QString strName, QString strEmail, QString strPhone, QString strCurrentPassword, QString strNewPassword, QString strRetype);
	Q_SLOT void checkUpdateResult();

	//Cancel Order
	Q_INVOKABLE void cancelOrder(QString strOrderID);
	Q_SLOT void checkCancelResult();

	//Submit Password
	Q_INVOKABLE void submitPassword(QString strPhone);
	Q_SLOT void checkSubmitResult();

	//Set Pickup Flag
	Q_INVOKABLE void setPickupFlag(QString strPickupLocation);

	//Set DropOff Flag
	Q_INVOKABLE void setDropOffFlag(QString strDropoffLocation);

	//Find Location
	Q_INVOKABLE void findLocation(QString strAddress);
	Q_SLOT void checkFindResult();

	//Set Map Position
	Q_INVOKABLE void setMapPosition(QString strLat, QString strLon, QString strAddress);
	Q_SLOT void setBubbleDescription();

	//Book Now
	Q_INVOKABLE void bookNow(QString strPickupLocation, QString strPickupAtLocation, QString strDropoffLocation, QString strTime, QString strType);
	Q_SLOT void checkOrderResult();

	//Reset Current Booking Page
	Q_SLOT void resetCurrentBookingPage();

	//Show details for order
	Q_INVOKABLE void showDetails(QString strOrderID);

	//back to My Booking
	Q_INVOKABLE void back2MyBooking();

	//initialize main booking page
	Q_INVOKABLE void initializeMainBookingPage();

	//show register page for creating new profile
	Q_INVOKABLE void showRegisterWithData();

	//addPinToPostion
	Q_INVOKABLE void addPinToDevicePosition(QObject* mapObject, double latitude, double longitude);

	//Set pickup marker
	Q_INVOKABLE void setPickupMarker(double dLat, double dLon);

	//Get Address for coordinate
	Q_INVOKABLE void getAddress(double dLat, double dLon);

	//Reverse geocoding slot
	Q_SLOT void readReverseGeocode();

	//Show Main Booking Places in Main Booking Tab
	Q_INVOKABLE void showMainPlaces();

	//Show Homepage
	Q_INVOKABLE void showHomepage();

	//Show Order Details after Confirm Dialog OK Button Clicked
	Q_INVOKABLE void showOrderDetails();

	//Show Full location
	Q_INVOKABLE void showFullLocation(QString strLableName, QString strLocation);

	//correct phone number
	Q_INVOKABLE void correctTextField(QObject*, QString);


	//auto refresh of order info
	Q_SLOT void refreshOrderInfo();

	Q_INVOKABLE void Message(int w);
	Q_INVOKABLE void Message(QString s);

	//start added by my team at 20130928
	//Show mapView after specified time in millisecond
	Q_INVOKABLE void showMapViewWithDelay(int msec, int sender);
	Q_SLOT void setMapViewVisible();
	Q_SIGNALS: void ShowMapView();
	//  end added by my team at 20130928

private:
	//Get IMEI of Phone
	QString getIMEI();

	//get MD5 password
	QString getMD5Password(QString strPassword);

	//get coordinate
	QPoint worldToPixel(QObject* mapObject, double latitude, double longitude) const;

	//get Response
	bool getResponse();

	//convert double to String
	QString double2String(double dNum);
	double string2double(QString strNumber);

	//Get Log in
	//For initialize pane
	void initializePane();

	QString getResponseType(int nType);

	QString getJSONString(QXmlStreamReader& xml);

	void setAddress(int nType, double dLat, double dLon);

	//Utility functions
	bool checkName(QString strName);
	bool checkEmailAddress(QString strEmail);
	bool checkPhoneNo(QString strPhone);
	bool checkPassword(QString strPassword, QString strRetype);
	bool checkPassword(QString strPassword);

	//Start Activity
	void startActivity();

	//Stop Activity
	void stopActivity();

	//Show Message
	void showMessage(QString strMessage);

	void showMessage(QString strTitle, QString strMessage);

	//Create Activity Dialog
	void createActivityDialog();

	//show detail page
	void showCurrentBookingPage();

private:
	UnicablinkApp* m_pMainInstance;

	//window size
	float m_nWinWidth;
	float m_nWinHeight;

	//Reverse geocode
	QtMobilitySubset::QGeoServiceProvider* m_geoServiceProvider;
	QtMobilitySubset::QGeoSearchManager* m_geoSearchManager;

	//all panes
	AbstractPane* m_paneLogIn;
	AbstractPane* m_paneRegister;
	AbstractPane* m_paneBooking;
	AbstractPane* m_paneOrder;
	AbstractPane* m_paneStatus;
	AbstractPane* m_paneCheckOrder;
	AbstractPane* m_paneProfile;
	AbstractPane* m_paneNewNumber;
	AbstractPane* m_paneInfo;
	AbstractPane* m_paneTrackTaxi;
	AbstractPane* m_panePickupLocation;
	AbstractPane* m_paneTabedMain;

	AbstractPane* m_panePrev;

	Page* m_pageActivation;
	Page* m_pageCurrentBooking;


	QString m_strPickupLocation;
	QString m_strDropoffLocation;
	QString m_strAddressLocation;

	//Connections
	JSONConnect* m_jsonConnect;
	SOAPRequest* m_soapRequest;

	//Flag
	int m_nFlag;
	int m_nOrderID[100];
	int m_nOrderCount;


	bool m_bFindLocation;
	bool m_bFlagCurrentBooking;
	bool m_bFlagNeedVerification;
	bool m_bLoggedIn;
	bool m_bFirstPickup;

	bool m_bDropOffSet;
	bool m_bPickupSet;

	//Order IDs
	QList<QVariant> m_varOrderList;

	//Map View
    bb::platform::geo::GeoLocation* deviceLocation;

    //Responses
    QMap<QString,QString> responseMap;
    QVariantMap m_jsonMap;
    QVariantMap m_jsonLocationMap;
    QVariantList m_listLocation;
    QByteArray responseArray;

    //Activity Dialog
    Dialog* m_activityDialog;

    //Pickup and drop off location
    double m_dPickupLat;
    double m_dPickupLon;
    double m_dDropOffLat;
    double m_dDropOffLon;

    double m_dTempLat;
    double m_dTempLon;

    //type of setting address
    int m_nType;

    //Json Parser
    JsonDataAccess m_jsonDataAccess;

    //User Informations
    QString m_strName;
    QString m_strEmail;
    QString m_strPhoneNo;
    QString m_strPassword;

    QString m_strStatusMessage[11];
    QString m_strDescription[11];

    //Current Order ID
    int m_nCurrentOrderID;

    //Order information list
    QVariantList m_orderInfoList;

    //Timer for auto-refresh order info
    QTimer* m_orderTimer;
};

#endif /* UNICABLINKEVENTHANDLER_H_ */
