package model.request;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.utils.DateUtils;

public class ForgotPassword {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strPhoneNo = "";
	private String m_strEmail = "";
	
	public ForgotPassword(){
		
	}
	
	public void setPhoneNo(String strPhoneNo)
	{
		m_strPhoneNo = strPhoneNo;
	}
	
	public void setEmail(String strEmail)
	{
		m_strEmail = strEmail;
	}
	
	public JSONObject toJSONObject()
	{
		JSONObject jsonResult = new JSONObject();
		
		//Get Current time
		String strCurrentTime = DateUtils.getCurrentTimeString();
		
		try 
		{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("Mobile", m_strPhoneNo);
			jsonResult.put("Email", m_strEmail);
			jsonResult.put("REQUESTTIME", strCurrentTime);
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		return jsonResult;
	}
}
