/*
 * UnicablinkApp.h
 *
 *  Created on: Jul 5, 2013
 *      Author: Passion
 */
#include <QObject>
#include <bb/cascades/AbstractPane>

#ifndef UNICABLINKAPP_H_
#define UNICABLINKAPP_H_

namespace bb { namespace cascades { class Application; }}

using namespace bb::cascades;

class UnicablinkApp : public QObject{
	Q_OBJECT
public:
	UnicablinkApp(Application *app);
	virtual ~UnicablinkApp() {}

private:
};

#endif /* UNICABLINKAPP_H_ */
