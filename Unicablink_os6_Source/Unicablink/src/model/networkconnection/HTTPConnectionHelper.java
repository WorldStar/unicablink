package model.networkconnection;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import application.Utils;
import application.utils.StringUtils;
import model.data.DataConstant;
import model.xml.XMLParser;

import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.WLANInfo;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class HTTPConnectionHelper 
{
	public static JSONObject requestToServer(String strRequestJSON, String strSeverUrl, String strRequestType)
	{
		/**
		 * initialize
		 */
		HttpConnection httpConnect = null;
		InputStream inputSteam = null;
		
		StringBuffer stringBuffer = new StringBuffer();
		
		/**
		 * Create encrypted post data
		 */
		URLEncodedPostData encPostData = new URLEncodedPostData("UTF-8", false);

		String requestSoap = getSoapHeader(strRequestType) + strRequestJSON + getSoapFooter(strRequestType);
		encPostData.setData(requestSoap);//set your json string

		byte[] postData;
		try 
		{
			postData = encPostData.toString().getBytes("UTF-8");
		}
		catch (UnsupportedEncodingException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
			Dialog.alert("Failed in encoding.");
			return null;
		}   
		
		/**
		 * Create HttpRequest (Post Method)
		 */
		try
		{
			//Establish http connection
			httpConnect = (HttpConnection)Connector.open(strSeverUrl + getConnectionString());
			
			// Set the request method and headers
			httpConnect.setRequestMethod(HttpConnection.POST);

			httpConnect.setRequestProperty("Content-type", "text/xml; charset=utf-8");
			httpConnect.setRequestProperty("SOAPAction", "http://CabOrdering.WS.App");
			httpConnect.setRequestProperty("Content-Length", String.valueOf(postData.length)); 

			httpConnect.openOutputStream().write(encPostData.getBytes()); 
		}
		catch (IOException ex) 
		{
			// TODO: handle exception
			ex.printStackTrace();
			
			Dialog.alert("Please check network connection. Please try again later.");
			return null;
		}

		int nResponse;
		try 
		{
			nResponse = httpConnect.getResponseCode();
			if (nResponse != HttpConnection.HTTP_OK) 
			{
				Dialog.alert("Service is currently not available, Please try again later.");
				return null;
			}
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Dialog.alert("Service is currently not available, Please try again later.");
			return null;
		}


		try 
		{
			inputSteam = httpConnect.openInputStream();
			int i;
			while((i= inputSteam.read())!=-1)
			{
				stringBuffer.append((char)i);
			}
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
			Dialog.alert("Failed to get response.");
			return null;
		}

		String strResponse = stringBuffer.toString();
//		strResponse = StringUtils.replace(strResponse, "&amp;", "&");
		strResponse = StringUtils.replace(strResponse, "&amp;", "");//modified by KO
		String strXML = StringUtils.replace(strResponse, "&quot;", "\"");//modified by KO
		
		if (strRequestType.equals(DataConstant.API_CAB_ORDER))
		{
			//PreProcessing
			strXML = StringUtils.replace(strXML, "&gt;", ")");
			strXML = StringUtils.replace(strXML, "&lt;", "(");
		}

		XMLParser parser = new XMLParser();
		Document doc = parser.getDomElement(strXML); // getting DOM element
		NodeList nl = doc.getElementsByTagName(getMainTag(strRequestType));
		Element e = (Element) nl.item(0);
		String strResponseJSON = parser.getValue(e, getSubTag(strRequestType));

		if (strRequestType.equals(DataConstant.API_CAB_ORDER))
		{
			//PreProcessing
			int nIndex = strResponseJSON.indexOf("OrderID");
			//{'253547","TIP":"","DEVICEID":"123456.78.364813.8"}
			strResponseJSON = "{\"" + strResponseJSON.substring(nIndex);//modified by KO
		}
		
		JSONObject object = null;
		
		try 
		{
			object = new JSONObject(strResponseJSON);
		}
		catch (JSONException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Dialog.alert("Failed to get response.");
			return null;
		}

		if (inputSteam != null)
		{
			try 
			{
				inputSteam.close();
			}
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if (httpConnect != null)
		{
			try 
			{
				httpConnect.close();
			}
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}

		return object;
	}

	
	public static String getConnectionString() {

	    String connectionString = null;

	    // Simulator behaviour is controlled by the USE_MDS_IN_SIMULATOR
	    // variable.
	    if (DeviceInfo.isSimulator()) {

	        connectionString = ";deviceside=true";
	    }

	    // Wifi is the preferred transmission method
	    else if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {

	        connectionString = ";interface=wifi";
	    }

	    // Is the carrier network the only way to connect?
	    else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {

	        String carrierUid = getCarrierBIBSUid();

	        if (carrierUid == null) {
	            // Has carrier coverage, but not BIBS. So use the carrier's TCP
	            // network

	            connectionString = ";deviceside=false";
	        } else {
	            // otherwise, use the Uid to construct a valid carrier BIBS
	            // request

	            connectionString = ";deviceside=false;connectionUID="+carrierUid + ";ConnectionType=mds-public";
	        }
	    }

	    // Check for an MDS connection instead (BlackBerry Enterprise Server)
	    else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {

	        connectionString = ";deviceside=false";
	    }

	    // If there is no connection available abort to avoid hassling the user
	    // unnecssarily.
	    else if (CoverageInfo.getCoverageStatus() == CoverageInfo.COVERAGE_NONE) {
	        connectionString = "none";

	    }

	    // In theory, all bases are covered by now so this shouldn't be reachable.But hey, just in case ...
	    else {
	        connectionString = ";deviceside=true";
	    }

	    return connectionString;
	}

	/**
	 * Looks through the phone's service book for a carrier provided BIBS
	 * network
	 * 
	 * @return The uid used to connect to that network.
	 */
	private synchronized static String getCarrierBIBSUid() {
	    ServiceRecord[] records = ServiceBook.getSB().getRecords();
	    int currentRecord;

	    for (currentRecord = 0; currentRecord < records.length; currentRecord++) {
	        if (records[currentRecord].getCid().toLowerCase().equals("ippp")) {
	            if (records[currentRecord].getName().toLowerCase()
	                    .indexOf("bibs") >= 0) {
	                return records[currentRecord].getUid();
	            }
	        }
	    }

	    return null;
	}
	
	public static String getSoapHeader(String strRequestType){
		return "<?xml version='1.0' encoding='UTF-8'?>" +
				"<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:" +
				"soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns='http://CabOrderingV2.WS.App'><soap:Body><" +
				strRequestType +
				"><content>";
	}

	public static String getSoapFooter(String strRequestType){
		return "</content><version>AND2.2.0</version></" +
				strRequestType +
				"></soap:Body></soap:Envelope>";
	}

	public static String getMainTag(String strRequestType){
		return "ns1:" +
				strRequestType +
				"Response";
	}

	public static String getSubTag(String strRequestType){
		return strRequestType +
				"Return";
	}
	
	public static JSONObject requestAddress(double dLat, double dLon)
	{
		/**
		 * Creating Server URL (http://maps.googleapis.com/maps/api/geocode/json?latlng=41.3,102.1&sensor=true)
		 */
		String strServerUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
		
		strServerUrl = strServerUrl + String.valueOf(dLat) + "," + String.valueOf(dLon) + "&sensor=true";
		
		
		/**
		 * initialize
		 */
		HttpConnection httpConnect = null;
		InputStream inputSteam = null;
		
		StringBuffer stringBuffer = new StringBuffer();
		
		/**
		 * Create HttpRequest (Get Method)
		 */
		try
		{
			httpConnect = (HttpConnection)Connector.open(strServerUrl + getConnectionString());
			// Set the request method and headers
			httpConnect.setRequestMethod(HttpConnection.GET);
		}
		catch (IOException ex) 
		{
			// TODO: handle exception
			ex.printStackTrace();
			
			Dialog.alert("Please check network connection. Please try again later.");
			return null;
		}

		int nResponse;
		try 
		{
			nResponse = httpConnect.getResponseCode();
			if (nResponse != HttpConnection.HTTP_OK) 
			{
				Dialog.alert("Location service failed. Can't get address from current location.");
				return null;
			}
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Dialog.alert("Please check network connection. Please try again later.");
			return null;
		}


		try 
		{
			inputSteam = httpConnect.openInputStream();
			int i;
			while((i= inputSteam.read())!=-1)
			{
				stringBuffer.append((char)i);
			}
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
//			Dialog.alert("Failed to get response.");
			return null;
		}

		String strResponse = stringBuffer.toString();

		JSONObject object = null;
		
		try 
		{
			object = new JSONObject(strResponse);
		}
		catch (JSONException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Dialog.alert("Location service failed. Can't get address from current location.");
			return null;
		}

		if (inputSteam != null)
		{
			try 
			{
				inputSteam.close();
			}
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		if (httpConnect != null)
		{
			try 
			{
				httpConnect.close();
			}
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}

		return object;
	}

}

