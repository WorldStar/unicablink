package view.screens;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.components.AccountInfoPasswordField;
import view.components.AccountInfoTextField;
import view.components.ImageButtonField;
import view.fieldmanager.InfoManager;
import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.ForgotPassword;
import model.request.Registeration;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import application.setting.AppSetting;
import application.setting.SettingStore;
import application.utils.StringUtils;

public class RegisterScreen extends BaseScreen {

	/**
	 * private members
	 */
	AccountInfoTextField nameInfo;
	AccountInfoTextField emailInfo;
	AccountInfoTextField phoneInfo;
	AccountInfoPasswordField passwordInfo;
	AccountInfoPasswordField retypeInfo;
	
	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;

	public RegisterScreen() {
		super();

		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.REGISTER_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));
		
		/**
		 * Register Info Manager
		 */
		InfoManager registerInfoManager = new InfoManager(ResouceConstant.REGISTER_GUTTER_MAIN, 
				ResouceConstant.REGISTER_TOP_PADDING, 0, 0);
		
		InfoManager signUpInfoManager = new InfoManager(ResouceConstant.REGISTER_GUTTER_DESCRIPTION, 
				ResouceConstant.REGISTER_GUTTER_DESCRIPTION, ResouceConstant.REGISTER_BTM_PADDING, 0);
		
		/**
		 * Get Modified Info
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
		
		String strInitialName = "";
		String strInitialEmail = "";
		
		AppSetting appSetting = mAppSettingStore.getAppSetting();
		
		if (theApp.bModified)
		{
			strInitialName = appSetting.getAccountName();
			strInitialEmail = appSetting.getAccountEmail();
		}
		
		/**
		 * create information fields
		 */
		//Name Info
		nameInfo = new AccountInfoTextField(ResouceConstant.REGISTER_BG_TOP, null, 0, 0, 30, 30, 
				"Name", "", strInitialName, 160, EditField.FILTER_DEFAULT, 20, 34, false);
		emailInfo = new AccountInfoTextField(ResouceConstant.REGISTER_BG_MIDDLE, null, 0, 0, 30, 30, 
				"Email", "", strInitialEmail, 160, EditField.FILTER_EMAIL, 25, 34, false);
		phoneInfo = new AccountInfoTextField(ResouceConstant.REGISTER_BG_MIDDLE, null, 0, 0, 30, 30, 
				"Phone", "e.g. +60123456789", "", 160, EditField.FILTER_PHONE, 15, 34, false);
		passwordInfo = new AccountInfoPasswordField(ResouceConstant.REGISTER_BG_MIDDLE, null, 0, 0, 30, 30, 
				"Password", "min. 6 (alphanumeric)", "", 160, EditField.FILTER_DEFAULT, 6, 34, false);
		retypeInfo = new AccountInfoPasswordField(ResouceConstant.REGISTER_BG_BTM, null, 0, 0, 30, 30, 
				"Retype", "min. 6 (alphanumeric)", "", 160, EditField.FILTER_DEFAULT, 6, 34, false);
		
		Bitmap bmpDescription = Utils.getBitmapResource(ResouceConstant.REGISTER_DESCRIPTION);
		ImageButtonField btnSignUp = new ImageButtonField(ResouceConstant.REGISTER_BTN_SIGNUP, "", 0, "")
		{
			public void clickButton()
			{
				onClickSignUp();
			}
		};
		
		/**
		 * Add fields to manager
		 */
		registerInfoManager.add(nameInfo);
		registerInfoManager.add(emailInfo);
		registerInfoManager.add(phoneInfo);
		registerInfoManager.add(passwordInfo);
		registerInfoManager.add(retypeInfo);
		
		signUpInfoManager.add(new BitmapField(bmpDescription, Field.FIELD_HCENTER));
		signUpInfoManager.add(btnSignUp);
		
		/**
		 * Add managers to screen
		 */
		add(registerInfoManager);
		add(signUpInfoManager);
		
	}
	
	/**
	 * User interaction process
	 */
	public void onClickSignUp()
	{
		String strPhoneNo = phoneInfo.getText();
		String strEmail = emailInfo.getText();
		String strName = nameInfo.getText();
		String strPassword = passwordInfo.getText();
		String strRetype = retypeInfo.getText();
		
		//check phone no
		if (!StringUtils.isPhoneNoFormat(strPhoneNo))
			return;
		
		//check name
		if (strName.length() == 0)
		{
			Dialog.alert("Please fill name.");
			return;
		}
		
		//check Email Address
		if (!StringUtils.isEmailFormat(strEmail))
		{
			return;
		}
		
		//check Password
		if (!StringUtils.checkPassword(strPassword, strRetype))
		{
			return;
		}
		
		
		//Create JSON Request
		Registeration jsonRegister = new Registeration();
		
		jsonRegister.setEmailAddress(strEmail);
		jsonRegister.setName(strName);
		jsonRegister.setPassword(strPassword);
		jsonRegister.setPhoneNo(strPhoneNo);
		
		/**
		 * Get Unicablink App and save members temporally
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
		
		theApp.mStrName = strName;
		theApp.mStrPhoneNo = strPhoneNo;
		theApp.mStrEmail = strEmail;
		theApp.mStrPassword = strPassword;
		
		//Get the response
		JSONObject jsonResult = null;
		
		String strJSONRegister = jsonRegister.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONRegister, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_REGISTER);
		
		if (jsonResult == null)
		{
			return;
		}
		
		/*
		 * Response type:
		 * 
		 * {
		 *		ResponseTime:2013-08-12 10:24:36,
		 *		SecurityKey:e92684cb325f1e4d4cce27c0207562bf,
		 *		Status:1
		 * }
		 *
		 */
		
		int nStatus;
		
		try 
		{
			nStatus = jsonResult.getInt("Status");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			Dialog.alert("Failed recognition of response.");
			return;
		}
		
		if (nStatus == 1)
		{
			//in case of Success
			Dialog.alert("Successfully registered.");
			
			AppSetting appSetting = mAppSettingStore.getAppSetting();
			
			appSetting.setName(theApp.mStrName);
			appSetting.setEmailAddress(theApp.mStrEmail);
			appSetting.setPhoneNo(theApp.mStrPhoneNo);
			appSetting.setPassword(theApp.mStrPassword);
			appSetting.setVerified(true);
			appSetting.setLogged(false);
			
			mAppSettingStore.saveAppSetting(appSetting);
			
			/**
			 * Pop log in and register screens and show activation screen.
			 */
			if (!theApp.bModified)
			{
				theApp.popOldScreen();
				theApp.goActivateScreen();
			}
			else
			{
				theApp.bModified = false;
				theApp.reActivate();
			}
		}
		else 
		{
			Dialog.alert("Failed registeration, please try again later.");
		}
		
		
	}

}
