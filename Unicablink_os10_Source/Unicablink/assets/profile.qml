import bb.cascades 1.0

Page {
    Container { //profile main container
    id: profileContainer
    preferredWidth: 768.0
    preferredHeight: 1280.0
    
    layout: StackLayout {
    
    }
    
    background: Color.create("#f5f5f5")
    animations: [
        FadeTransition {
            id: showProfilePageEffect
            duration: 500
            toOpacity: 1.0
            fromOpacity: 0.0
        }
    ]
    
    //Top Contents
    Container {
        id: topProfileBar
        
        preferredHeight: 112.0
        
        horizontalAlignment: HorizontalAlignment.Center
        
        layout: DockLayout {
        
        }
        
        minHeight: 112.0
        
        ImageView {
            id: topBarBackground
            imageSource: "asset:///Image/topbar_gradient.png"
            scalingMethod: ScalingMethod.Fill
            preferredHeight: 112.0
            preferredWidth: 768.0
        }
        Label {
            text: "Profile"
            textStyle.fontWeight: FontWeight.Bold
            textStyle.color: Color.White
            textStyle.textAlign: TextAlign.Center
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            textStyle.fontSize: FontSize.Default
        }
    
    } //end of topbar
    
    //Old Profile
    Container {
        id: oldProfile
        
        topMargin: 40.0
        
        horizontalAlignment: HorizontalAlignment.Center
        
        //name field
        ImageTextBox {
            objectName: "nameTextField"
            
            backgroundSource: "asset:///Image/bar_insert_top_profile.png"
            boxWidth: 684
            boxHeight: 112
            
            paddingLeft: 40
            paddingRight: 40
            paddingTop: 21
            paddingBottom: 15
            
            labelWidth: 152
            labelText: "Name"
            
            textInputMode: TextFieldInputMode.Default
            textHintText: ""
            textEnabled: true
            textValue: "Nicole"
        }
        
        //email field
        ImageTextBox {
            objectName: "emailTextField"
            
            backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
            boxWidth: 684
            boxHeight: 106
            
            paddingLeft: 40
            paddingRight: 40
            paddingTop: 15
            paddingBottom: 15
            
            labelWidth: 152
            labelText: "Email"
            
            textInputMode: TextFieldInputMode.EmailAddress
            textHintText: ""
            textEnabled: true
            textValue: "Nicole@gmail.com"
        }
        
        //phone Field
        ImageTextBox {
            objectName: "phoneTextField"
            
            backgroundSource: "asset:///Image/bar_insert_btm_profile.png"
            boxWidth: 684
            boxHeight: 112
            
            paddingLeft: 40
            paddingRight: 40
            paddingTop: 15
            paddingBottom: 21
            
            labelWidth: 152
            labelText: "Phone"
            
            textInputMode: TextFieldInputMode.Default
            textHintText: ""
            textEnabled: false
            textValue: "+601234567890"
            
            onTouch: {
                if (event.isDown()){
                    var registerPage = registerPageDefinition.createObject();
                    profileNavigationPane.push(registerPage);
                }
            
            }
        
        
        }
    } //end of old profile
    
    //New Password Setting
    Container {
        id: newPasswordContainer
        horizontalAlignment: HorizontalAlignment.Center
        topMargin: 26.0
        
        //Change Password Label
        ImageTextBox {
            backgroundSource: "asset:///Image/bar_insert_top_grey_profile.png"
            boxWidth: 684
            boxHeight: 108
            
            paddingLeft: 40
            paddingRight: 40
            paddingTop: 15
            paddingBottom: 15
            
            labelWidth: 348
            labelText: "Change Password:"
            
            textInputMode: TextFieldInputMode.Default
            textHintText: ""
            textEnabled: false
            textValue: ""
        }
        
        //Current Password Field
        ImageTextBox {
            backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
            boxWidth: 684
            boxHeight: 108
            
            paddingLeft: 40
            paddingRight: 40
            paddingTop: 15
            paddingBottom: 15
            
            labelWidth: 338
            labelText: "Current Password"
            
            textInputMode: TextFieldInputMode.Password
            textHintText: "must be 6 digits"
            textEnabled: true
            textValue: ""
        }
        
        //New Password Field
        ImageTextBox {
            backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
            boxWidth: 684
            boxHeight: 108
            
            paddingLeft: 40
            paddingRight: 40
            paddingTop: 15
            paddingBottom: 15
            
            labelWidth: 338
            labelText: "New Password"
            
            textInputMode: TextFieldInputMode.Password
            textHintText: "must be 6 digits"
            textEnabled: true
            textValue: ""
        }
        
        //Retype Password Field
        ImageTextBox {
            backgroundSource: "asset:///Image/bar_insert_btm_profile.png"
            boxWidth: 684
            boxHeight: 108
            
            paddingLeft: 40
            paddingRight: 40
            paddingTop: 15
            paddingBottom: 15
            
            labelWidth: 338
                labelText: "Retype"
            
            textInputMode: TextFieldInputMode.Password
            textHintText: "must be 6 digits"
            textEnabled: true
            textValue: ""
        }
    }
    
    ImageButton {
        id: saveButton
        
        defaultImageSource: "asset:///Image/button_greenbar_small_save.png"
        pressedImageSource: "asset:///Image/button_greenbar_small_save.png"
        preferredWidth: 298.0
        preferredHeight: 92.0
        horizontalAlignment: HorizontalAlignment.Center
        topMargin: 30.0
        
        onClicked: {
        
        }
    }

    } //end profile main container
    
    
    attachedObjects: [
        // Create the ComponentDefinition that represents the custom
        // component in myPage.qml
        ComponentDefinition {
            id: registerPageDefinition
            source: "register.qml"
        }
    ]

}