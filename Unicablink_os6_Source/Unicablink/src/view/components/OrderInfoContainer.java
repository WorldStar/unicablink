package view.components;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.NullField;
import application.ResouceConstant;
import application.Unicablink;
import view.fieldmanager.InfoManager;

/**
 * This class is used when display summary of order
 * @author Passion
 *
 */
public class OrderInfoContainer extends InfoManager{
	/**
	 * Create OrderInfoContainer
	 * @param strOrderID: orderID
	 * @param strPickup: Pickup Location
	 * @param strDropOff: DropOff Location
	 * @param strStatus: Status String
	 */
	
	/*
	 * Private Members
	 */
	private String strOrderId;
	
	public OrderInfoContainer(String strOrderID, String strPickup, String strDropOff, String strSummary, String strStatus){
		super(0, 40, 20, 0);
		
		this.strOrderId = strOrderID;
		
		OrderTopInfoManager topInfo = new OrderTopInfoManager(strOrderID, strSummary)
		{
			//Overrides
			public void showOrderDetails()
			{
				goDetailsPage();
			}
		};
		
		AccountInfoTextField pickupInfo = new AccountInfoTextField(ResouceConstant.HISTORY_CONTENT_MIDDLE, null, 0, 0, 36, 20, "Pick up", "", strPickup, 120, 0, 200, 22, true);
		pickupInfo.setEditable(false);
		
		AccountInfoTextField dropOffInfo = new AccountInfoTextField(ResouceConstant.HISTORY_CONTENT_MIDDLE, null, 0, 0, 36, 20, "Drop off", "", strDropOff, 120, 0, 200, 22, true);
		dropOffInfo.setEditable(false);
		
		AccountInfoTextField statusInfo = new AccountInfoTextField(ResouceConstant.HISTORY_CONTENT_BTM, null, 0, 0, 36, 20, "Status", "", strStatus, 120, 0, 200, 22, true);
		statusInfo.setEditable(false);
		
		try
		{
			add(topInfo);
			add(pickupInfo);
			add(dropOffInfo);
			add(statusInfo);
			add(new NullField(NullField.FOCUSABLE));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Dialog.alert("Failed add of UI components");
		}
	}
	
	/**
	 * User Interaction Process
	 */
	public void goDetailsPage()
	{
		//Save Selected Order Id
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
		theApp.strSelectedOrderId = getOrderID();
		
		//Show Details Screen
		theApp.goDetailsScreen(false);
		
	}
	
	public String getOrderID()
	{
		return this.strOrderId;
	}
	
	protected boolean navigationClick(int status, int time) 
	{
		if (status != 0){
			fieldChangeNotify(0);
			goDetailsPage();
		}
		return true;
	}

	public boolean keyChar(char key, int status, int time) 
	{
		if (key == Characters.ENTER) {
			fieldChangeNotify(0);
			goDetailsPage();
			return true;
		}
		return false;
	}
	protected boolean trackwheelClick(int status, int time)
	{        
		if (status != 0) goDetailsPage();    
		return true;
	}

	protected boolean invokeAction(int action) 
	{
		switch( action ) {
		case ACTION_INVOKE: {
			goDetailsPage(); 
			return true;
		}
		}
		return super.invokeAction(action);
	}    


	protected boolean touchEvent(TouchEvent message)
	{
		int x = message.getX( 1 );
		int y = message.getY( 1 );
		if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
			// Outside the field
			return false;
		}
		switch( message.getEvent() ) {
		case TouchEvent.UNCLICK:
			goDetailsPage();
			return true;
		}
		return super.touchEvent( message );
	}
}
