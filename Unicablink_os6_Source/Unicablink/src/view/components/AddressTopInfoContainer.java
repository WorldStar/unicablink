package view.components;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResouceConstant;
import application.Utils;

public class AddressTopInfoContainer extends HorizontalFieldManager{
	/**
	 * private members
	 */
	//left: 60, text: 440

	private int nLeftMargin = 0;
	private int nWidthText = 0;
	
	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private float fScale = Utils.getVRatio();
	
	private Font mFontText;
	private int nFontSize = 0;
	
	int nMaxLen = 0;

	EditField mTextAddress;
	
	public AddressTopInfoContainer(String strAddress)
	{
		super(USE_ALL_WIDTH | Manager.HORIZONTAL_SCROLL | Manager.HORIZONTAL_SCROLLBAR);

		/**
		 * initialize
		 */
		nLeftMargin = (int)(60 * fScale);
		nWidthText = (int)(440 * fScale);
		
		nMaxLen = ResouceConstant.ADDRESS_MAX_LENGTH;

		/**
		 * set Background
		 */
		Bitmap bgBmp = Utils.getBitmapResource(ResouceConstant.ADDRESS_BAR_SEARCH);
		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));

		//initialize
		nTotalWidth = bgBmp.getWidth();
		nTotalHeight = bgBmp.getHeight();
		
		if (strAddress.length() > nMaxLen)
		{
			strAddress = strAddress.substring(0, nMaxLen - 1);
		}
		
		//Image Button
//		mTextAddress = new EditField("", strAddress, nMaxLen, FIELD_VCENTER | EditField.NO_NEWLINE)
		mTextAddress = new InsideLabelEditField("Input Address", "", 100, FIELD_VCENTER | EditField.NO_NEWLINE)//modified by KO
		{
			public boolean keyChar(char key, int status, int time) 
			{
				boolean bResult = super.keyChar(key, status, time);
				if (key == Characters.ENTER) {
					fieldChangeNotify(0);
					searchAddress();
				}
				return bResult;
			}
		};
		
		mFontText = Utils.getFont(Font.PLAIN, 22);
		mTextAddress.setFont(mFontText);
		ImageButtonField btnSearch = new ImageButtonField(ResouceConstant.ADDRESS_ICON_SEARCH, "", 0, "")
		{
			public void clickButton()
			{
				searchAddress();
			}
		};
		/**
		 * Add fields to Manager
		 */
		add(mTextAddress);
		add(btnSearch);
	}

	/**
	 * overrides
	 */
	public void sublayout(int nWidth, int nHeight)
	{
		int nNumFields = getFieldCount();

		if (nNumFields == 2)
		{
			int nXPos = nLeftMargin;

			Field textField = this.getField(0);
			Field button = this.getField(1);
			
			layoutChild(textField, nWidthText, nTotalHeight / 2);
			setPositionChild(textField, nXPos, (nTotalHeight - textField.getHeight()) / 2);
			
			layoutChild(button, button.getPreferredWidth(), Integer.MAX_VALUE);
			setPositionChild(button, (int)(550 * fScale) , (nTotalHeight - button.getHeight()) / 2);
		}

		this.setExtent(nTotalWidth, nTotalHeight);
	}

	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}
	
	
	/**
	 * User Interaction Process
	 */
	public void setAddress(String strAddress)
	{
		int aLength = strAddress.length();
		int twidth = mFontText.getAdvance(strAddress, 0, aLength);
		while(twidth > nWidthText)
		{
			aLength--;
			twidth = mFontText.getAdvance(strAddress, 0, aLength);
		}
		strAddress = strAddress.substring(0, aLength);

		mTextAddress.setText(strAddress);
	}
	
	public String getAddress()
	{
		return mTextAddress.getText();
	}
	
	public void searchAddress()
	{
		
	}
}
