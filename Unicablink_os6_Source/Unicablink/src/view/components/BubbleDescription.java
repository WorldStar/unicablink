package view.components;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResouceConstant;
import application.Utils;

public class BubbleDescription extends HorizontalFieldManager{
	/**
	 * Private members
	 */
	private int nLeftPadding = 0;
	private int nRightPadding = 0;
	private int nTopPadding = 0;
	private int nBottomPadding = 0;

	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private float fScale = Utils.getVRatio();

	private Font mFontText;
	private int nFontSize = 0;

	//Map Information
	private String strLocationString = "";

	//Location Label
	LabelField mLabelAddress;

	/**
	 * Create Object: (Location Candidate Button)
	 * @param strEnableBGFile: Background File when enable
	 * @param strDisableBGFile: Background File when disable
	 * @param leftPadding: Left Padding
	 * @param rightPadding: Right Padding
	 * @param strLabelText: Location String
	 */
	public BubbleDescription(String strLabelText)
	{
		super(USE_ALL_WIDTH);

		/**
		 * initialize
		 */
		nLeftPadding = (int)(10 * fScale);
		nRightPadding = (int)(10 * fScale);
		nTopPadding = (int)(8 * fScale);
		nBottomPadding = (int)(24 * fScale);

		nFontSize = 24;


		this.strLocationString = strLabelText;

		/**
		 * set Background
		 */
		Bitmap bgBmp = Utils.getBitmapResource(ResouceConstant.ADDRESS_BUBBLE);
		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));

		//initialize
		nTotalWidth = bgBmp.getWidth();
		nTotalHeight = bgBmp.getHeight();

		//set fonts
		mFontText = Utils.getFont(Font.PLAIN, nFontSize);
		
		//Location Label
		mLabelAddress = new LabelField(strLocationString);
		mLabelAddress.setFont(mFontText);

		/**
		 * Add fields to Manager
		 */
		add(mLabelAddress);
	}
	
	/**
	 * Get and Set
	 */
	public void setLocation(String strLocation)
	{
		mLabelAddress.setText(strLocation);
	}

	/**
	 * overrides
	 */
	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}
	
	public void sublayout(int nWidth, int nHeight)
	{
		int nNumFields = getFieldCount();

		if (nNumFields == 1)
		{
			int nXPos = nLeftPadding;

			Field label = this.getField(0);
			
			layoutChild(label, nTotalWidth - nLeftPadding - nRightPadding, nTotalHeight - nTopPadding - nBottomPadding);
			setPositionChild(label, nLeftPadding,  nTopPadding + ((nTotalHeight - nBottomPadding - nTopPadding) - label.getHeight()) / 2);
		}

		this.setExtent(nTotalWidth, nTotalHeight);
	}
	
}