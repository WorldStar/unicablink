import bb.cascades 1.0

Container {
    id: root
    property alias backgroundSource: background.imageSource

    property alias boxWidth: root.preferredWidth
    property alias boxHeight: root.preferredHeight

    property alias paddingLeft: root.leftPadding
    property alias paddingRight: root.rightPadding
    property alias paddingTop: root.topPadding
    property alias paddingBottom: root.bottomPadding

    property alias labelWidth: label.maxWidth
    property alias labelHeight: label.maxHeight
    property alias labelText: label.text

    property int index: 0
    property variant strLon: 0
    property variant strLat: 0

    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight

    }

    verticalAlignment: VerticalAlignment.Center

    background: background.imagePaint

    attachedObjects: [
        ImagePaintDefinition {
            id: background
        }
    ]

    Label {
        id: label

        text: ""
        textStyle.fontWeight: FontWeight.Default
        textStyle.color: Color.Black
        textStyle.textAlign: TextAlign.Left
        verticalAlignment: VerticalAlignment.Center
        multiline: true
        textStyle.fontSize: FontSize.XSmall
        maxHeight: boxHeight - 5 
        
    }

    onTouch: {
        if (event.isUp()){
            eventHandler.setMapPosition(strLat, strLon, labelText);
        }
    }
}