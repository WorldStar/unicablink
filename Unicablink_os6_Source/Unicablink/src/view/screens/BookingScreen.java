package view.screens;

import java.util.Calendar;
import java.util.Date;

import javax.microedition.location.AddressInfo;
import javax.microedition.location.Criteria;
import javax.microedition.location.Landmark;
import javax.microedition.location.Location;
import javax.microedition.location.LocationException;
import javax.microedition.location.LocationProvider;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.components.AccountInfoTextField;
import view.components.ImageButtonField;
import view.components.LocationContainer;
import view.components.SpecifiedButton;
import view.fieldmanager.InfoManager;
import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.OrderCab;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.lbs.Locator;
import net.rim.device.api.lbs.LocatorException;
import net.rim.device.api.lbs.maps.model.MapDataModel;
import net.rim.device.api.lbs.maps.model.MapLocation;
import net.rim.device.api.lbs.maps.model.MapPoint;
import net.rim.device.api.lbs.maps.model.Mappable;
import net.rim.device.api.lbs.maps.ui.MapAction;
import net.rim.device.api.lbs.maps.ui.MapField;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.AbsoluteFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.picker.DateTimePicker;
import net.rim.device.api.ui.toolbar.ShowKeyboardToolbarButtonField;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import application.setting.AppSetting;
import application.setting.SettingStore;
import application.utils.DateUtils;


/**
 * @author Passion
 *
 */
public class BookingScreen extends MainBaseScreen {

	/**
	 * Private members
	 */
	LocationContainer pickupContainer;
	AccountInfoTextField pickupAtContainer;
	LocationContainer dropOffContainer;
	SpecifiedButton nowButton;
	SpecifiedButton budgetButton;

	ImageButtonField bookNowButton;

	private AbsoluteFieldManager mapButtonManager;
//	private VerticalFieldManager mapButtonManager;
	private MapField mMapField;
	private MapDataModel mModel;

	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;
	private MapLocation pickupLocation;
	private MapLocation dropOffLocation;
	
	private ImageButtonField btnBookNow;//modified by KO

	public BookingScreen() {
		super();

		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.BOOKING_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));

		/**
		 * book info manager
		 */
		InfoManager bookInfoManager = new InfoManager(0, 0, 0, 0);
		HorizontalFieldManager timeTypeManager = new HorizontalFieldManager(USE_ALL_WIDTH);

		/**
		 * Get Saved Information
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
		String strPickup = theApp.strPickupLocation;
		String strDropOff = theApp.strDropOffLocation;

		pickupContainer = new LocationContainer("Pick up", strPickup, ResouceConstant.BOOKING_BTN_PICKUP, "Tap to select pick up")
		{
			public void showLocation()
			{
				pickupLocation();
			}
		};

		pickupAtContainer = new AccountInfoTextField(ResouceConstant.BOOKING_BAR_LONG, null, 0, 0, 30, 20, "Pick up at", "E.g. house no/building/lobby/note to driver", "", 120, EditField.NO_NEWLINE, 100, 26, false);
		pickupAtContainer.setEditable(true);

		dropOffContainer = new LocationContainer("Drop off", strDropOff, ResouceConstant.BOOKING_BTN_DROPOFF, "Tap to select drop off")
		{
			public void showLocation()
			{
				dropOffLocation();
			}
		};

		nowButton = new SpecifiedButton("Now", ResouceConstant.BOOKING_BTN_NOW, ResouceConstant.BOOKING_BAR_NOW)
		{
			public void doAction()
			{
				getTime();
			}
		};

		budgetButton = new SpecifiedButton("Budget", ResouceConstant.BOOKING_BTN_TYPE, ResouceConstant.BOOKING_BAR_TYPE)
		{
			public void doAction()
			{
				changeType();
			}
		};

		/**
		 * Add fields
		 */
		bookInfoManager.add(pickupContainer);
		bookInfoManager.add(pickupAtContainer);
		bookInfoManager.add(dropOffContainer);

		timeTypeManager.add(nowButton);
		timeTypeManager.add(budgetButton);

		/**
		 * Add managers to Main Manager
		 */
		add(bookInfoManager);
		add(timeTypeManager);


		/**
		 * Map and book now button
		 */

		mapButtonManager = new AbsoluteFieldManager()
		{
			/**
			 * overrides
			 */
			public void sublayout(int nWidth, int nHeight)
			{
				int nNumFields = getFieldCount();

				if (nNumFields == 2)
				{
					Field mapField = this.getField(0);
					Field button = this.getField(1);

					layoutChild(mapField, mapField.getPreferredWidth(), mapField.getPreferredHeight());
					setPositionChild(mapField, 0, 0);

					layoutChild(button, button.getPreferredWidth(), button.getPreferredHeight());
					setPositionChild(button, (Utils.getDisplayWidth() - button.getPreferredWidth()) / 2, (int)(Utils.getVRatio() * ResouceConstant.ADDRESS_BTN_PADDING));
				}

				this.setExtent(Utils.getDisplayWidth(), (int)(ResouceConstant.ADDRESS_MAP_HEIGHT * Utils.getVRatio()));
			}
		};

		//MapField
		mMapField = new MapField(Utils.getDisplayWidth(), (int)(ResouceConstant.ADDRESS_MAP_HEIGHT * Utils.getVRatio()));
		mMapField.setAutoUpdate(true);
		MapAction action = mMapField.getAction();
		action.setCentreAndZoom(new MapPoint(3.1581084, 101.7118345), 3);   

		//Apply Location Button
		btnBookNow = new ImageButtonField(ResouceConstant.BOOKING_BTN_BOOK_NOW, "", 0, "")
		{
			public void clickButton()
			{
				bookNow();
			}
		};

		/**
		 * Add fields and add Main Manager
		 */
		mapButtonManager.add(mMapField);
		mapButtonManager.add(btnBookNow);

		add(mapButtonManager);

		/**
		 * Initialize values
		 */
		getCurrentAddress();
	}

	/**
	 * User Interaction Process
	 */
	public void setPickupLocation(String strPickupLocation)
	{
		pickupContainer.setLocation(strPickupLocation);
	}

	public void setDropOffLocation(String strDropOffLocation)
	{
		dropOffContainer.setLocation(strDropOffLocation);
	}

	public void bookNow()
	{
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		/**
		 * Check fields and get values
		 */
		String strPickupLocation = pickupContainer.getLocation();
		String strDropOffLocation = dropOffContainer.getLocation();
		String strPikcupAt = pickupAtContainer.getText();
		String strPickupTime = nowButton.getLabelText();
		String strType = budgetButton.getLabelText();
		String strCabType = (strType.equals("Budget")) ? "1": "2";

		if (strPickupLocation.length() == 0)
		{
			Dialog.alert("Please select pick up location.");
			return;
		}

		if (strDropOffLocation.length() == 0)
		{
			Dialog.alert("Please select drop off location.");
			return;
		}

		if (strPikcupAt.length() == 0)
		{
			Dialog.alert("Please fill 'Pick up at' field.");
			return;
		}

		if (strPickupTime.equals("Now"))
		{
			strPickupTime = DateUtils.getCurrentTimeString();
		}

		//Get Account Info
		AppSetting appSetting = mAppSettingStore.getAppSetting();

		String strName = appSetting.getAccountName();
		String strEmail = appSetting.getAccountEmail();
		String strPhone = appSetting.getAccountPhone();
		String strCustomerID = appSetting.getAccountCustomerID();

		/**
		 * Create Request
		 */
		OrderCab jsonOrder = new OrderCab();

		jsonOrder.setCarType(strCabType);
		jsonOrder.setCEmail(strEmail);
		jsonOrder.setCName(strName);
		jsonOrder.setCustomerID(strCustomerID);
		jsonOrder.setDatePickup(strPickupTime);
		jsonOrder.setDlat(theApp.dDropOffLat);
		jsonOrder.setDlon(theApp.dDropOffLon);
		jsonOrder.setDropOff(strDropOffLocation);
		jsonOrder.setOrderDate(DateUtils.getCurrentTimeString());
		jsonOrder.setPickupLocation(strPickupLocation);
		jsonOrder.setPlat(theApp.dPickupLat);
		jsonOrder.setPlon(theApp.dPickupLon);
		jsonOrder.setRemark(strPikcupAt);
		jsonOrder.setSPhoneNo(strPhone);

		//Get the response
		JSONObject jsonResult = null;

		String strJSONOrder = jsonOrder.toJSONObject().toString();


		jsonResult = HTTPConnectionHelper.requestToServer(strJSONOrder, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_CAB_ORDER);

		if (jsonResult == null)
		{
			return;
		}


		/*
		 * Response type:
		 * 
		 * {
		 *		"Dispatching":"1",
		 *		"FareUrl":"http://www.sunlighttaxi.com/thing-to-note-before-making-a-online-taxi-booking",
		 *		"Message":"&lt;html&gt;&lt;body style='font-size:18px;color:white;'&gt;TQ for using Unicablink App. Fare will be based on taxi meter (On call = RM2, Starting = RM3, Distance/Time charge, Midnight charge = Additional 50%) + Car Park Fee / Toll, if incurred. For details, please refer to &lt;a style='color:#33CCFF' href='http://www.sunlighttaxi.com/thing-to-note-before-making-a-online-taxi-booking'&gt;here&lt;/a&gt;&lt;/body&gt;&lt;/html&gt;",
		 *		"OrderID":"3874546",
		 *		"Remarks":"asdfasdf",
		 *		"ResponseTime":"2013-08-13 08:52:31",
		 *		"SecurityKey":"e92684cb325f1e4d4cce27c0207562bf",
		 *		"Status":"1"
		 *	}
		 *
		 */

		int nStatus;

		try 
		{
			nStatus = jsonResult.getInt("Status");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("We are sorry, Order failed to add. Please try again.");
			return;
		}
		
		if (nStatus == 2) {
			Dialog.alert("We are sorry, Order failed to add. Please try again.");
			return;
		}
		
		int nOrderID;
		try 
		{
			nOrderID = jsonResult.getInt("OrderID");

		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("We are sorry, Order failed to add. Please try again.");
			return;
		}

		if (nOrderID == 0)
		{
			Dialog.alert("We are sorry, Order failed to add. Please try again.");
			return;
		}

		//Save Order ID
		appSetting.addOrder(nOrderID);

		mAppSettingStore.saveAppSetting(appSetting);

		theApp.strSelectedOrderId = String.valueOf(nOrderID);

		ConfirmDialog confirmationDialog = new ConfirmDialog();
		if (confirmationDialog.doModal() == Dialog.D_OK) {
			//Show Details Screen
			theApp.goDetailsScreen(true);
		}
	}

	public void pickupLocation()
	{
		/*
		 * Save temp Data
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		theApp.nFlagScreen = DataConstant.FROM_PICKUP_CONTROL;
		theApp.mStrTempLocation = pickupContainer.getLocation();

		/**
		 * push Address screen
		 */
		theApp.goAddressScreen();
	}

	public void dropOffLocation()
	{
		/*
		 * Save temp Data
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		theApp.nFlagScreen = DataConstant.FROM_DROPOFF_CONTROL;
		theApp.mStrTempLocation = dropOffContainer.getLocation();

		/**
		 * push Address screen
		 */
		theApp.goAddressScreen();
	}

	/**
	 * Utility
	 */
	public void getTime()
	{
		DateTimePicker datePickerDate = DateTimePicker.createInstance(Calendar.getInstance(), "yyyy-MM-dd", "HH:mm");

		if (datePickerDate.doModal() == true){
			//get date
			Calendar calendarDate = datePickerDate.getDateTime();
			Date dateDate = calendarDate.getTime();


			SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");


			String strDate = formatDate.format(dateDate);

			if (DateUtils.acceptableDate(strDate))
			{
				nowButton.setLableText(strDate);
			}
			else
			{
				Dialog.alert("Please select correct time.");
			}
		}
	}

	public void changeType()
	{
		//Change Type
		if (budgetButton.getLabelText().equals("Budget"))
		{
			budgetButton.setLableText("Premium/Executive");
		}
		else
		{
			budgetButton.setLableText("Budget");
		}
	}


	/**
	 * Reverse Geocoding
	 */
	public void getCurrentAddress()
	{
		//Get Current Location
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		boolean bFinished = false;
		
		if (theApp.mHandleGPS != null)
		{
			bFinished = theApp.mHandleGPS.bFinished;
		}
		
		if (theApp.bNotFoundLocation || bFinished){
			return;
		}
		
		//Get Current Location using HandleGPS
		double currentLat = theApp.mHandleGPS.latitude;
		double currentLon = theApp.mHandleGPS.longitude;
		
//		//Get Current Location using HandleGPS
//		double currentLat = theApp.mHandleGPSNew.latitude;
//		double currentLon = theApp.mHandleGPSNew.longitude;
//		
		if ((currentLat == 0) && (currentLon == 0)) 
		{
			theApp.bNotFoundLocation = true;
			return;
		}

		
		//set pickup location
		theApp.dPickupLat = currentLat;
		theApp.dPickupLon = currentLon;

		//Get Address and set pickup field text
		JSONObject jsonResult = null;

		jsonResult = HTTPConnectionHelper.requestAddress(currentLat, currentLon);

		if (jsonResult == null)
		{
			return;
		}


		/*
		 {
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "Ejin",
               "short_name" : "Ejin",
               "types" : [ "sublocality", "political" ]
            },
            {
               "long_name" : "Alxa",
               "short_name" : "Alxa",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Nei Mongol",
               "short_name" : "Nei Mongol",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "China",
               "short_name" : "CN",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Ejin, Alxa, Inner Mongolia, China",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 42.7951629,
                  "lng" : 103.103721
               },
               "southwest" : {
                  "lat" : 40.4054075,
                  "lng" : 97.1727628
               }
            },
            "location" : {
               "lat" : 41.958542,
               "lng" : 101.068934
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 42.7951629,
                  "lng" : 103.103721
               },
               "southwest" : {
                  "lat" : 40.4054075,
                  "lng" : 97.1727628
               }
            }
         },
         "types" : [ "sublocality", "political" ]
      }],
   "status" : "OK"
		 */

		String strStatus;
		String strResults;

		try 
		{
			strStatus = jsonResult.getString("status");
			strResults = jsonResult.getString("results");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		if (!strStatus.equals("OK")){
			/*AddressInfo addrInfo = null;

            int latitude  = (int)(currentLat * 100000);
            int longitude = (int)(currentLon * 100000);

            try
            {
                Landmark[] results = Locator.reverseGeocode
                 (latitude, longitude, Locator.ADDRESS );

                if ( results != null && results.length > 0 )
                    addrInfo = results[0].getAddressInfo();
            }
            catch ( LocatorException lex )
            {
            }*/
			return;
		}

		JSONArray aJSONArrayResults;
		try {
			aJSONArrayResults = new JSONArray(strResults);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		int nCount = aJSONArrayResults.length();

		if (nCount > 0)
		{
			try {
				JSONObject jsonAddress = aJSONArrayResults.getJSONObject(0);
				String strAddress = jsonAddress.getString("formatted_address");

				pickupContainer.setLocation(strAddress);

				MapAction action = mMapField.getAction();
				action.setCentreAndZoom(new MapPoint(currentLat, currentLon), 3);   

				pickupLocation = new MapLocation( currentLat, currentLon, "", null );

				//Set Pickup Pin
				mModel = mMapField.getModel();
				mModel.add( (Mappable) pickupLocation, "PICKUP");
				mModel.setVisibleNone();
				mModel.setVisible( "PICKUP" );

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}

		}
	}

	public void setAllPin()
	{
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();


		//Set Pickup Pin
		mModel = mMapField.getModel();
		mModel.removeAll();
		
		mModel.add( (Mappable) pickupLocation, "PICKUP");
		mModel.setVisibleNone();
		mModel.setVisible( "PICKUP" );
		
		if ((theApp.dPickupLat != 0) || (theApp.dPickupLon != 0))
		{
			//Add Pickup Pin
			pickupLocation = new MapLocation( theApp.dPickupLat, theApp.dPickupLon, "", null );
			mModel.add( (Mappable) pickupLocation, "PICKUP");
			mModel.setVisible("PICKUP");
		}
		
		if ((theApp.dDropOffLat != 0) || (theApp.dDropOffLon != 0))
		{
			//Add Pickup Pin
			pickupLocation = new MapLocation( theApp.dDropOffLat, theApp.dDropOffLon, "", null );
			mModel.add( (Mappable) pickupLocation, "PICKUP");
			mModel.setVisible("PICKUP");
		}
		
		if (theApp.nFlagScreen == DataConstant.FROM_PICKUP_CONTROL)
		{
			MapAction action = mMapField.getAction();
			action.setCentreAndZoom(new MapPoint(theApp.dPickupLat, theApp.dPickupLon), 3);   
		}
		else
		{
			MapAction action = mMapField.getAction();
			action.setCentreAndZoom(new MapPoint(theApp.dDropOffLat, theApp.dDropOffLon), 3);   
		}
	}
	
	//start added by KO
	public void initBookingScreen()
	{
//		pickupContainer.setLocation("");
		pickupContainer.setFocus();
//		pickupAtContainer.setText("");
		pickupAtContainer.setFocus();
//		dropOffContainer.setLocation("");
		dropOffContainer.setFocus();
//		nowButton.setLableText("Now");
//		budgetButton.setLableText("Budget");
		btnBookNow.setFocus();
	}
	//  end added by KO
}
