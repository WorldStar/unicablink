import bb.cascades 1.0

Page {
    Container { //Current Booking Container
        id: currentBookingContainer

        preferredWidth: 768.0
        preferredHeight: 1280.0

        layout: StackLayout {

        }

        background: Color.create("#f5f5f5")
        animations: [
            FadeTransition {
                id: showCurrentBookingPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        //Top Contents
        Container {
            id: topCurrentBookingBar

            preferredHeight: 112.0
            preferredWidth: 768.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

			leftPadding: 20.0
            minHeight: 112.0
            
            background: currentBookingBackground.imagePaint
            
			attachedObjects: [
			    ImagePaintDefinition {
           			id: currentBookingBackground
                    imageSource: "asset:///Image/topbar_gradient.png"
                }
			]
            
            Label {
                text: "Details"
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 9.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
            }
            
            Container {
                id: cancelButtonContainer
                objectName: "cancelButtonContainer"
                
                layout: DockLayout {
                
                }
                
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                
                ImageButton {
                    objectName: "cancelButton"
                    preferredWidth: 154.0
                    preferredHeight: 70.0
                    defaultImageSource: "asset:///Image/button_greybar_cancel.png"
                    pressedImageSource: "asset:///Image/button_greybar_cancel.png"

                    
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    disabledImageSource: "asset:///Image/button_greybar_cancel.png"
                    
                    onClicked: {
                        cancelOrder();
                    }
                }
            
            }
        } //end of topbar
        
        ScrollContainer {
            horizontalAlignment: HorizontalAlignment.Center

            Container { //Order Info Container
                id: bookingDetailContainer
                preferredWidth: 686.0
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center

                topPadding: 30.0
                bottomPadding: 30.0
                layout: StackLayout {

                }
                Container {
                    background: detailTopBackground.imagePaint

                    attachedObjects: [
                        ImagePaintDefinition {
                            id: detailTopBackground
                            imageSource: "asset:///Image/bar_top_grey_order.png"
                        }
                    ]
                    preferredWidth: 686.0
                    minHeight: 98.0
                    leftPadding: 30.0
                    rightPadding: 30.0

                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    Label {
                        preferredWidth: 176.0
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        text: "Order ID: "
                        
                        textStyle.color: Color.create("#ff505050")
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 8.0
                    }

                    Label {
                        id: orderIdLabel
                        objectName: "orderIdLabel"
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        
                        textStyle.color: Color.create("#ff505050")

                        text: "56542"
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 8.0
                    }
                } //end of top order info Label

                Container {
                    id: orderInfoContainer

                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }

                    horizontalAlignment: HorizontalAlignment.Center
                    preferredWidth: 686.0

                    OrderInfoItem {
                        objectName: "dateTimeContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Date/ Time"
                        valueText: "24/4/2013 11:40 AM"
                    } //end of Date/ Time

                    OrderInfoItem {
                        objectName: "pickupContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Pick up"
                        valueText: "Guan Qian Street"
                        bPopupable: true
                    } //end of Pickup

                    OrderInfoItem {
                        objectName: "pickupatContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Pick up at"
                        valueText: "Guan Qian Street East"
                    } //end of Pickup at

                    OrderInfoItem {
                        objectName: "dropoffContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Drop Off"
                        valueText: "JinJi Lake"
                        bPopupable: true
                    } //end of Drop Off

                    OrderInfoItem {
                        objectName: "carNoContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Car No"
                        valueText: "HDA5654"
                    } //end of Car No

                    OrderInfoItem {
                        objectName: "carTypeContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Car Type"
                        valueText: "Budget"
                    } //end of Car Type

                    OrderInfoItem {
                        objectName: "driverContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Driver"
                        valueText: "Rock"
                    } //end of Driver

                    OrderInfoItem {
                        objectName: "etaContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "ETA/EDA"
                        valueText: "2 min, 2 Km"
                    } //end of ETA/EDA

                    OrderInfoItem {
                        objectName: "estContainer"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 98

                        labelImageSource: "asset:///Image/bar_middle_grey_order.png"
                        textImageSource: "asset:///Image/bar_middle_white_order.png"

                        nameLabelWidth: 246

                        labelText: "Est. Fare"
                        valueText: "RM30.00"
                    } //end of Est. Fare

                    StatusInfoItem {
                        id: statusItem
                        objectName: "statusItem"
                        itemWidth: orderInfoContainer.preferredWidth
                        itemHeight: 130

                        labelImageSource: "asset:///Image/bar_down_left.png"
                        textImageSource: "asset:///Image/bar_btm_purple_order.png"
                        bAnimate: true

                        nameLabelWidth: 246
                        bStatus: 1
                        labelText: "Status"
                        valueText: "There is a Taxi for you, TQ."
                    } //end of Pickup
                } //end of value for order
            } //end of Order Info Container
        }
    } //end of Current Booking Container

    function cancelOrder()
    {
        eventHandler.cancelOrder(orderIdLabel.text);
    }
 
    function back2MyBooking()
 	{
         eventHandler.back2MyBooking();
 	}
 	
 	function bookAgain()
 	{
         eventHandler.BookAgain();
 	}

    actions: [
        ActionItem {
            title: "My Booking"
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///Image/icon_mybooking.png"
            
            onTriggered: {
                back2MyBooking();
            }
        },
        ActionItem {
            title: "Book Again"
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///Image/icon_bookacab.png"

            onTriggered: {
                bookAgain();
            }
        }
    ]
}
