package view.screens;

import application.ResouceConstant;
import application.Unicablink;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.image.Image;
import net.rim.device.api.ui.image.ImageFactory;
import net.rim.device.api.util.StringProvider;

public class MainBaseScreen extends MainScreen {

	/**
	 * 
	 */
	public MainBaseScreen() {
		super(MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR | USE_ALL_HEIGHT | USE_ALL_WIDTH);
		// TODO Auto-generated constructor stub
		
		//set Manager's background color
		getMainManager().setBackground(BackgroundFactory.createSolidBackground(0xf5f5f5));
	}
	
	/**
	 * Make Menu
	 * 4 menus: Book A Cab, My Booking, Profile, Contact
	 * Each Screen has this menu, so they can move each other
	 */
	protected void makeMenu(Menu menu, int instance) { 
		super.makeMenu(menu, instance);
		
		//Book A Cab Menu
		MenuItem menuItemBook = new MenuItem(new StringProvider("Book A Cab"), 10, 10) { 
			public void run() { 
				Unicablink theApp = (Unicablink) UiApplication.getUiApplication();
				theApp.switchBookingScreen();
			} 
		};
		
		//Set icon to menu item (Book a cab)
		Image menuIconBook = ImageFactory.createImage(Bitmap.getBitmapResource(ResouceConstant.UNICAB_MENU_ICON_BOOK));
		menuItemBook.setIcon(menuIconBook);
		
		menu.add(menuItemBook); 

		//My Booking Menu
		MenuItem menuItemHistory = new MenuItem(new StringProvider("My Booking"), 10, 10) { 
			public void run() { 
				Unicablink theApp = (Unicablink) UiApplication.getUiApplication();
				theApp.switchHistoryScreen();
			} 
		};
		
		//Set icon to menu item (my booking)
		Image menuIconHistory = ImageFactory.createImage(Bitmap.getBitmapResource(ResouceConstant.UNICAB_MENU_ICON_HISTORY));
		menuItemHistory.setIcon(menuIconHistory);
		
		menu.add(menuItemHistory);

		//Profile Menu
		MenuItem menuItemProfile = new MenuItem(new StringProvider("Profile"), 10, 10) { 
			public void run() { 
				Unicablink theApp = (Unicablink) UiApplication.getUiApplication();
				theApp.switchProfileScreen();
			} 
		};
		
		//Set icon to menu item (Profile)
		Image menuIconProfile = ImageFactory.createImage(Bitmap.getBitmapResource(ResouceConstant.UNICAB_MENU_ICON_PROFILE));
		menuItemProfile.setIcon(menuIconProfile);
		
		menu.add(menuItemProfile);
		
		//Contact Menu
		MenuItem menuItemContact = new MenuItem(new StringProvider("Contact"), 10, 10) { 
			public void run() { 
				Unicablink theApp = (Unicablink) UiApplication.getUiApplication();
				theApp.switchContactScreen();
			} 
		};
		
		//Set icon to menu item (Contact)
		Image menuIconContact = ImageFactory.createImage(Bitmap.getBitmapResource(ResouceConstant.UNICAB_MENU_ICON_CONTACT));
		menuItemContact.setIcon(menuIconContact);
		
		menu.add(menuItemContact);
	} 
}
