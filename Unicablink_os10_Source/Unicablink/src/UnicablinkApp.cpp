/*
 * UnicablinkApp.cpp
 *
 *  Created on: Jul 5, 2013
 *      Author: Passion
 */

#include "UnicablinkApp.h"
#include "UnicablinkEventHandler.h"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/TabbedPane>
#include <bb/data/DataSource>


using namespace bb::cascades;

UnicablinkApp::UnicablinkApp(bb::cascades::Application *app)
: QObject(app)
{

	//add a QTimer class as a qml type
	qmlRegisterType<QTimer>("my.library", 1, 0, "QTimer");

	// create scene document from main.qml asset
	// set parent to created document to ensure it exists for the whole application lifetime
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

	if(!qml->hasErrors()) {
		// create a event handler object
		UnicablinkEventHandler *pHandler = new UnicablinkEventHandler(this);
		qml->setContextProperty("eventHandler", pHandler);

		// create root object for the UI
		AbstractPane *root = qml->createRootObject<AbstractPane>();

		if(root){
			// set created root object as a scene
			Application::instance()->setScene(root);
		}
	}
}
