package view.screens;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.Login;
import model.request.ResendActivationCode;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import view.components.AccountInfoTextField;
import view.components.ImageButtonField;
import view.fieldmanager.InfoManager;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import application.setting.AppSetting;
import application.setting.SettingStore;
import application.utils.StringUtils;


public class ActivateScreen extends BaseScreen {
	/**
	 * Private Members
	 */
	AccountInfoTextField activationInfo;
	ImageButtonField resendButton;

	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;

	public ActivateScreen() {
		super();

		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.FORGOT_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));

		/**
		 * Managers
		 */
		InfoManager activationInfoManager = new InfoManager(ResouceConstant.ACTIVATE_MAIN_GUTTER, 
				ResouceConstant.ACTIVATE_TOP_PADDING, 0, 0);

		InfoManager resendManager = new InfoManager(ResouceConstant.ACTIVATE_MIDDLE_GUTTER, 
				ResouceConstant.ACTIVATE_MIDDLE_PADDING, ResouceConstant.ACTIVATE_BTM_PADDING, 0);

		/**
		 * Create fields
		 */
		Bitmap bmpDescription = Utils.getBitmapResource(ResouceConstant.ACTIVATE_DESCRIPTION);
		Bitmap bmpResendDescription = Utils.getBitmapResource(ResouceConstant.ACTIVATE_DESCRIPTION_RESEND);

		activationInfo = new AccountInfoTextField(ResouceConstant.ACTIVATE_BG_TEXT, null, 0, 0, 26, 26, "", "Enter Activation code here", "", 0, 
				EditField.FILTER_DEFAULT, 10, 34, true);

		ImageButtonField activateButton = new ImageButtonField(ResouceConstant.ACTIVATE_BTN_ACTIVATE, "", 0, "")
		{
			public void clickButton()
			{
				onClickActivate();
			}
		};

		resendButton = new ImageButtonField(ResouceConstant.ACTIVATE_BTN_RESEND_ENABLE, "", 0, 
				ResouceConstant.ACTIVATE_BTN_RESEND_DISABLE)
		{
			public void clickButton()
			{
				boolean bEnabled = this.getActionEnabled();
				if (bEnabled)
				{
					this.setActionEnabled(false);
					onClickResend();
				}
			}
		};

		/**
		 * Add Fields to Manager
		 */
		activationInfoManager.add(new BitmapField(bmpDescription));
		activationInfoManager.add(activationInfo);
		activationInfoManager.add(activateButton);

		resendManager.add(new BitmapField(bmpResendDescription));
		resendManager.add(resendButton);

		/**
		 * Add info managers to Main Manager
		 */
		add(activationInfoManager);
		add(resendManager);
	}

	/**
	 * User Interaction Process
	 */
	public void onClickActivate()
	{
		/**
		 * Get Unicablink App and get saved data
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		AppSetting appSetting = mAppSettingStore.getAppSetting();

		String strPhoneNo = appSetting.getAccountPhone();
		String strPassword = appSetting.getAccountPassword();

		//Get verification code
		String strVerificationCode = activationInfo.getText();

		if (strVerificationCode.length() == 0){
			Dialog.alert("Please fill all fields.");
			return;
		}		

		//Create JSON Request
		Login jsonLogin = new Login();

		jsonLogin.setPassword(strPassword);
		jsonLogin.setPhoneNo(strPhoneNo);
		jsonLogin.setVerificationCode(strVerificationCode);



		//Get the response
		JSONObject jsonResult = null;

		String strJSONLogin = jsonLogin.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONLogin, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_LOG_IN);

		if (jsonResult == null)
		{
			return;
		}

		/*
		 * Response type:
		 * 
		 * {
		 * CustomerID:"6364C141-9C27-ED59-1F15A0A7BB8B8FA2",
		 * Email:"yuan@obama.com",
		 * Name:"Micheal",
		 * ResponseTime:"2013-08-12 10:52:43",
		 * SecurityKey:"e92684cb325f1e4d4cce27c0207562bf",
		 * Status:4
		 * }
		 *
		 */

		int nStatus;

		try 
		{
			nStatus = jsonResult.getInt("Status");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("Failed recognition of response.");
			return;
		}

		switch (nStatus) 
		{
		case 1:
		{
			try 
			{
				String strCustomerID = jsonResult.getString("CustomerID");
				String strEmail = jsonResult.getString("Email");
				String strName = jsonResult.getString("Name");

				appSetting.setCustomerID(strCustomerID);
				appSetting.setName(strName);
				appSetting.setEmailAddress(strEmail);
				appSetting.setPhoneNo(strPhoneNo);
				appSetting.setPassword(strPassword);
				appSetting.setLogged(true);
				appSetting.setVerified(false);

				mAppSettingStore.saveAppSetting(appSetting);

				
				/**
				 * Pop log in and register screens and show activation screen.
				 */
				theApp.popLoginScreen();
				theApp.goBookingScreen();
			}
			catch (JSONException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();

				Dialog.alert("Failed to get response.");
			}
			break;
		}

		case 2:
		{
			Dialog.alert("Invalid User ID / Password.");
			break;
		}

		case 3:
		{
			Dialog.alert("Account Inactive.");
			break;
		}

		case 4:
		{
			Dialog.alert("You are not verified.");
			break;
		}
		default:
			break;
		}
	}

	public void onClickResend()
	{
		//run Splash thread and after 2s push PartnerScreen
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				resendButton.setActionEnabled(true);
			}
		}, ResouceConstant.ACTIVATE_DURATION, false);
		
		
		/**
		 * Get Unicablink App and get saved data
		 */
		AppSetting appSetting = mAppSettingStore.getAppSetting();

		String strPhoneNo = appSetting.getAccountPhone();
		String strEmail = appSetting.getAccountEmail();

		/*
		 * Request type:
		 * 
		 * {
		 *			"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
		 *			"MOBILE":"+1126253547",
		 *			"EMAIL":"wang198904@gmail.com", 
		 *			"REQUESTTIME": "2013-08-15 12:05:00"			
         * }
         * 
		 */
		
		//Create JSON Request
		ResendActivationCode jsonResend = new ResendActivationCode();

		jsonResend.setEmail(strEmail);
		jsonResend.setPhoneNo(strPhoneNo);
		
		//Get the response
		JSONObject jsonResult = null;

		String strJSONResend = jsonResend.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONResend, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_RESEND_CODE);

		if (jsonResult == null)
		{
			return;
		}

		/*
		 * {
		 *  	�SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
		 *		�STATUS�:1,
		 *		�MESSAGE�: ��,
		 *		�RESPONSETIME�:�2012-03-14 14:32:55�
		 * }
		 */
		
		int nStatus;
		String strMessage;
		
		try 
		{
			nStatus = jsonResult.getInt("STATUS");
			strMessage = jsonResult.getString("MESSAGE");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("Failed recognition of response.");
			return;
		}
		
		if (nStatus == 1)
		{
			if (strMessage.length() != 0) 
			{
				Dialog.alert(strMessage);
			}
			else 
			{
				Dialog.alert("Sent successfully.");
			}
		}
		else
		{
			if (strMessage.length() != 0) 
			{
				Dialog.alert(strMessage);
			} 
			else 
			{
				Dialog.alert("Request Failed, Please try again later.");
			}
		}

		
	}
}
