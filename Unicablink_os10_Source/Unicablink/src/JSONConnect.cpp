/* Copyright (c) 2012 Research In Motion Limited.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#include <bb/data/JsonDataAccess>
#include "JSONConnect.hpp"
using namespace bb::cascades;
using namespace bb::data;
using namespace bb::pim::contacts;

//! [0]
JSONConnect::JSONConnect(QObject *parent)
	: QObject(parent)
{
    //if(NetworkCheck()){
    	//RequestServer(QString("http://223.25.247.242/api/1/venues/areas/"));
    //}
	networkAccessManager = new QNetworkAccessManager(this);


	SITE_URL = "http://202.9.98.143/App/WS/CabOrderingV2/ws.cfc?wsdl";
	m_pParent = parent;
//! [0]
}

JSONConnect::~JSONConnect()
{
	networkAccessManager->deleteLater();
}

//! [1]
//network connect check function
bool JSONConnect::NetworkCheck(){
	QNetworkConfigurationManager m;
	if(m.isOnline()){
		return true;
	}else{
		bool available = false;
		foreach(QNetworkInterface interface,QNetworkInterface::allInterfaces())
		{
			if(interface.flags().testFlag(QNetworkInterface::IsUp))
			{
				available = true;
			}
		}
		return available;
	}
	return true;
}
//Retrieve list of all areas

//Retrieve Carlsberg Promos

bool JSONConnect::SendGetRequest(QMap<QString, QString> &params,const char *slot)
{
	if(NetworkCheck())
	{
		//Request type: http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&sensor=true

		//Make request url;
		QString url = "http://maps.googleapis.com/maps/api/geocode/json";

		url.append("?");

		QMap<QString,QString>::iterator i = params.begin();
		while(i != params.end())
		{
			url.append(i.key());
			url.append("=");
			url.append(i.value());
			url.append("&");
			i++;
		}

		QNetworkRequest request = QNetworkRequest();
		request.setUrl( QUrl(url) );

		// Sends the HTTP request.
		QNetworkReply * reply = networkAccessManager->get(request);
		connect(reply, SIGNAL(finished()), m_pParent, slot);
		return true;
	}
	return false;
}

const QString& JSONConnect::GetJsonUrl()
{
	return JSON_URL;
}
const QString& JSONConnect::GetSiteUrl()
{
	return SITE_URL;
}

//Finish SIGNAL Function
void JSONConnect::requestFinished(QNetworkReply *reply)
{
	if (!reply->error()) {
		// Let's get ALL the data

	} else {

	}

}

bool JSONConnect::SendSoapRequest(QString strXml, const char *slot)
{
	if(NetworkCheck())
	{
		//Make request url;
		QString url = "http://202.9.98.42/App/WS/CabOrderingV2/ws.cfc?wsdl";

	    QNetworkRequest request = QNetworkRequest();
	    request.setHeader(QNetworkRequest::ContentTypeHeader, QLatin1String("text/xml; charset=utf-8"));
	    request.setRawHeader("SOAPAction", "http://202.9.98.42/App/WS/CabOrderingV2/ws.cfc?wsdl#user_login");

		request.setUrl(QUrl(url));

		int nSize = strXml.toUtf8().count();
		Q_UNUSED(nSize);

		// Sends the HTTP request.
		QNetworkReply * reply = networkAccessManager->post(request, strXml.toUtf8());
		connect(reply, SIGNAL(finished()), m_pParent, slot);
		return true;
	}

	return false;
}



//! [7]
