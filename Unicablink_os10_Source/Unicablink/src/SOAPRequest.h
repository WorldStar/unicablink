/*
 * SOAPRequest.h
 *
 *  Created on: Aug 7, 2013
 *      Author: Passion
 */

#ifndef SOAPREQUEST_H_
#define SOAPREQUEST_H_

#include <QtCore/QObject>
#include <QString>
#include <bb/cascades/GroupDataModel>

#include "JSONReqeust.h"

using namespace bb::cascades;

class SOAPRequest: public QObject {
	Q_OBJECT
public:
	SOAPRequest(QObject* parent = 0);
	~SOAPRequest();

public:
	QString getSOAPMessage(QMap<QString, QVariant> params, int nType);
	QString getSOAPHeader(int nType);
	QString getSOAPFooter(int nType);

private:
	QObject* m_parent;
	JSONReqeust* m_jsonRequest;
};

#endif /* SOAPREQUEST_H_ */
