import bb.cascades 1.0

Container {
    id: root
    
    property alias textValue: textField.text
    property alias backgroundSource: background.imageSource
    property alias boxWidth: root.preferredWidth
    
    property alias boxHeight: root.preferredHeight
    
    property alias paddingLeft: root.leftPadding
    property alias paddingRight: root.rightPadding
    property alias paddingTop: root.topPadding
    property alias paddingBottom: root.bottomPadding
    
    
    property alias labelWidth: label.preferredWidth
    property alias labelHeight: label.preferredHeight
    property alias labelText: label.text
    property alias labelFontStyle: label.textStyle
    
    property alias textInputMode: textField.inputMode
    property alias textHintText: textField.hintText
    property alias textEnabled: textField.enabled
    property alias textFontStyle: textField.textStyle
    
    property int bSearchBox: 0
    property int index: 0
    property double dLon: 0
    property double dLat: 0
    property bool bNewProfile: false
    
    
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight

    }

    verticalAlignment: VerticalAlignment.Center

    background: background.imagePaint

    attachedObjects: [
        ImagePaintDefinition {
            id: background
        }
    ]

    
    Label {
        id: label

        text: ""
        textStyle.fontWeight: FontWeight.Default
        textStyle.color: ((bSearchBox == 0)? Color.Black: Color.create("#7c7c7c"))
        textStyle.textAlign: TextAlign.Left
        verticalAlignment: VerticalAlignment.Center
        textStyle.fontSize: FontSize.PointValue
        textStyle.fontSizeValue: 7.0
    }

    TextField {
        id: textField
        objectName: "textField"

        clearButtonVisible: true
        input.submitKey: SubmitKey.Done
        textFormat: TextFormat.Plain
        textStyle.textAlign: TextAlign.Left
        input.masking: TextInputMasking.Masked
        verticalAlignment: VerticalAlignment.Center
        inputMode: TextFieldInputMode.Default
        backgroundVisible: false
        textStyle.color: ((bSearchBox == 0) ? Color.Black : Color.create("#7c7c7c"))
        text: ""
        focusHighlightEnabled: false
        hintText: ""
        enabled: true

        onFocusedChanged: {
            if (bNewProfile)
            {
                if (focused == true) {
                    eventHandler.hideAboveContainer();
                }
                if (focused == false) {
                    eventHandler.showAboveContainer();
                }

            }
        }
        textStyle.fontSize: FontSize.PointValue
        textStyle.fontSizeValue: 7.0

    }
    
    onTouch: {
        if (event.isUp()){
            if (bSearchBox == 1) {
                eventHandler.setMapPosition(dLat, dLon);
            }
        }
    }
}