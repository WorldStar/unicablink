package view.components;

import view.screens.LocationDialog;
import application.ResouceConstant;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * This class is used when fieldmanager has label and textfield with background
 * @author Passion
 *
 * label text, hint text and text of textfield are initialized at first time
 */

public class AccountInfoTextField extends HorizontalFieldManager{
	/**
	 * members
	 */
	private InsideLabelEditField mEditField;
	private LabelField mLabelField;

	private int nTopPadding = 0;
	private int nBottomPadding = 0;
	private int nLeftPadding = 0;
	private int nRightPadding = 0;

	private int nLabelWidth = 0;
	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private Font mFontLabel;
	private Font mFontText;
	
	//start modified by KO at 20131128
	private String strFullText;
	//  end modified by KO at 20131128
	
	private Bitmap bgBmp = null;
	private Bitmap bgHighlightBmp = null;
	
	private boolean bPopupable = false;
	
	private int nFontSize = 0;

	private float fScale = Utils.getVRatio();


	/**
	 * create object
	 * @param strBGFile: background file name
	 * @param topPadding: top padding in field manager
	 * @param bottomPadding: bottom padding in field manager
	 * @param leftPadding: left padding in field manager
	 * @param rightPadding: right padding in field manager
	 * @param totalWidth: width of field manager
	 * @param totalHeight: height of field manager
	 * @param strLabelText: Text of Label
	 * @param strHintText: Hint Text of Text field
	 * @param strInitialText: Initial text of Text field
	 * @param labelWidth: width of Label
	 * @param textStyle: text style of Text field
	 * @param maxLen: max length of Text field
	 * @param fontSize: font size of Label and Text field
	 */
	public AccountInfoTextField(String strBGFile, String strHBGFile, int topPadding, int bottomPadding, int leftPadding, 
			int rightPadding, String strLabelText, String strHintText,
			String strInitialText, int labelWidth, long textStyle, int maxLen, int fontSize, boolean bBold){

		super(HorizontalFieldManager.NO_HORIZONTAL_SCROLL | HorizontalFieldManager.NO_HORIZONTAL_SCROLLBAR);

		//initialize
		nTopPadding = (int)(topPadding * fScale);
		nBottomPadding = (int)(bottomPadding * fScale);
		nLeftPadding = (int)(leftPadding * fScale);
		nRightPadding = (int)(rightPadding * fScale);
		nLabelWidth = (int)(labelWidth * fScale);

		//set background
		bgBmp = Utils.getBitmapResource(strBGFile);

		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));
		if (strHBGFile != null)
		{
			bgHighlightBmp = Utils.getBitmapResource(strHBGFile);
		}
		
		//initialize
		nTotalWidth = bgBmp.getWidth();
		nTotalHeight = bgBmp.getHeight();

		//set fonts
		nFontSize = fontSize;
		
		if (bBold)
		{
			mFontLabel = Utils.getFont(Font.BOLD, fontSize);	
		}
		else
		{
			mFontLabel = Utils.getFont(Font.PLAIN, fontSize);
		}
		
		

		//create Label
		mLabelField = new LabelField(strLabelText, FIELD_VCENTER);
		mLabelField.setFont(mFontLabel);

		//create textfield
		//start modified by KO at 20131127
		strFullText = strInitialText;
		if(strInitialText.length() > 90)
		{
			strInitialText = strInitialText.substring(0,  90) + " ...";
		}
		//  end modified by KO at 20131127
		
		mEditField = new InsideLabelEditField(strHintText, strInitialText, maxLen, FIELD_VCENTER | EditField.NO_NEWLINE | textStyle);
		mEditField.setChangeListener(new FieldChangeListener() {
		    public void fieldChanged(Field field, int context) {
				if((field.getStyle() & EditField.FILTER_PHONE) != 0) {
					String text = getText();
					int nLength = text.length();
					if(nLength == 1){
						if(!text.equals("+")){
							setText("");
						}
					}else if(nLength > 1){
						int i; boolean bchanged = false;
						for(i = nLength - 1; i > 0; i--)
						{
							char ch = text.charAt(i);
							String remain1 = text.substring(0, i);
							String remain;
							if(i + 1 == nLength){
								remain = remain1;// + remain2;
							}else{
								String remain2 = text.substring(i + 1, nLength);
								remain = remain1 + remain2;
							}

							if(ch != Characters.DIGIT_ZERO
								&& ch != Characters.DIGIT_ONE
								&& ch != Characters.DIGIT_TWO
								&& ch != Characters.DIGIT_THREE
								&& ch != Characters.DIGIT_FOUR
								&& ch != Characters.DIGIT_FIVE
								&& ch != Characters.DIGIT_SIX
								&& ch != Characters.DIGIT_SEVEN
								&& ch != Characters.DIGIT_EIGHT
								&& ch != Characters.DIGIT_NINE){
								text = remain;
								nLength = text.length();
								bchanged = true;
							}
						}
						if(bchanged)
							setText(text);
					}
				}
		    }
		});
//start modified by KO at 20131127
//		if (strInitialText.length() > 80)
//		{
//			mFontText = Utils.getFont(Font.PLAIN, fontSize - 4);
//		}
//		else
//  end modified by KO at 20131127
		{
			mFontText = Utils.getFont(Font.PLAIN, fontSize);
		}
		mEditField.setFont(mFontText);

		//add Fields
		add(mLabelField);
		add(mEditField);
	}
	
	public String getFullText(){
		return strFullText;
	}

	public String getText(){
		return mEditField.getText();
	}

	public void setText(String strText){
		strFullText = strText;
		mEditField.setText(strText);
	}
	
	public String getLabelText(){
		return mLabelField.getText();
	}
	
	public void setEditable(boolean editable) 
	{
		mEditField.setEditable(editable);
	}

	public void setMaxChar(int maxChar)
	{
		mEditField.setMaxSize(maxChar);
	}

	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}

	public void setFocus() 
	{
		mEditField.setFocus();
	}

	public void focusChangeNotify(int arg0) 
	{
		super.focusChangeNotify(arg0);
	}

	protected void paint(Graphics graph) {
		super.paint(graph);
		if (isFocus()){
			if (bgHighlightBmp != null)
			{
				setBackground(BackgroundFactory.createBitmapBackground(bgHighlightBmp));	
			}
			
		}
		else{
			setBackground(BackgroundFactory.createBitmapBackground(bgBmp));
		}

	}
	
	public void sublayout(int nWidth, int nHeight){
		Field labelField = getField(0); //Label Field
		Field textField = getField(1); //Text Field

		int nYPosLabel = (nTotalHeight - mFontLabel.getHeight()) / 2;
		
		//Label field
		setPositionChild(labelField, nLeftPadding, nYPosLabel);
		layoutChild(labelField, nLabelWidth, labelField.getHeight());

		int nXPosText = nLeftPadding + nLabelWidth;
		
		
		
		//Text Field
		layoutChild(textField, nTotalWidth - nLeftPadding - nRightPadding - nLabelWidth, Integer.MAX_VALUE);
		setPositionChild(textField, nXPosText, (nTotalHeight - textField.getHeight()) / 2);
		

		this.setExtent(nTotalWidth, nTotalHeight);
	}
}
