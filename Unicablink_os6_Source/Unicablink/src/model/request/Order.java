package model.request;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class Order {
	private int m_nOrderId;
	private int m_nRequestCount;

	public Order(){
	}

	public void setOrderId(int nOrderId){
		m_nOrderId = nOrderId;
	}

	public void setRequestCount(int nRequestCount)
	{
		m_nRequestCount = nRequestCount;
	}

	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();

		try{
			jsonResult.put("ORDERID", String.valueOf(m_nOrderId));
			jsonResult.put("REQUESTCOUNT", m_nRequestCount);
		}
		catch(JSONException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonResult;
	}

}
