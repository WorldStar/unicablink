package view.components;

import net.rim.device.api.i18n.Locale;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public class InsideLabelPasswordField extends PasswordEditField {


	private boolean _drawFocus;
	private boolean _isDrawingLabel = true;
	private int _levelColor = Color.DARKGRAY;
	private int _backColor = Color.FLORALWHITE;
	private int _paddingSpace = 10;
	private String _label;
	private int _fontSize = 24;
	private int _foreColor = Color.BLACK;

	public InsideLabelPasswordField(String label) {
		super("", "");
		_label = label;
		setFont(Font.getDefault().derive(Font.PLAIN, _fontSize, Ui.UNITS_px));
	}

	protected void drawFocus(Graphics graphics, boolean on) {
		_drawFocus = on;
		super.drawFocus(graphics, on);
		_drawFocus = false;
	}

	protected void onUnfocus() {
		if (getText().length() == 0) {
			setText("");
			setCursorPosition(0);
			_isDrawingLabel = true;
		}
	}

	protected boolean keyDown(int keycode, int time) {
		String r = getText();

		if (r.length() == 0) {
			setText("");
			_isDrawingLabel = false;
		} else if (r.length() == 1 && keycode == 524288/* backspace */) {
			setCursorPosition(0);
			_isDrawingLabel = true;
		}

		return super.keyDown(keycode, time);
	}

	protected void paint(Graphics g) {

		int oldForecolor = g.getColor();
		int oldBackcolor = g.getBackgroundColor();

		if(!_drawFocus) {
			g.clear();
			g.setBackgroundColor(_backColor);

			if (_isDrawingLabel) {
				g.setColor(_levelColor);
				g.drawText(_label, 0, 0, (int) DrawStyle.LEFT, getWidth()
						- _paddingSpace);
			}
			else {
				g.setColor(_foreColor);

			}
		}

		super.paint(g);

		g.setColor(oldForecolor);
		g.setBackgroundColor(oldBackcolor);
	}

	public Locale getPreferredInputLocale() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getTextInputStyle() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isUnicodeInputAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void updateInputStyle() {
		// TODO Auto-generated method stub

	}

}
