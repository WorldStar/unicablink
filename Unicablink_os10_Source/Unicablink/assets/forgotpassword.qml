import bb.cascades 1.0

Page {
    //Z10
    id: forgotPasswordPage
    
    Container {
        id: forgotPasswordContainer

        background: Color.create("#f5f5f5")
        layout: StackLayout {

        }

        horizontalAlignment: HorizontalAlignment.Center

        animations: [
            FadeTransition {
                id: showPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        Container {
            id: topBar

            minHeight: 112.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            ImageView {
                id: topBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 112.0
                preferredWidth: 768.0
            }
            Label {
                text: "Forgot Password"
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 9
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
            }

        }

        Container {
            id: forgotInfoContainer
            topMargin: 24.0
            preferredWidth: 662.0

            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }

            horizontalAlignment: HorizontalAlignment.Center

            //Phone No Info
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight

                }

                verticalAlignment: VerticalAlignment.Center

                preferredHeight: 110.0

                background: phoneNoInfoBackground.imagePaint

                attachedObjects: [
                    ImagePaintDefinition {
                        id: phoneNoInfoBackground
                        imageSource: "asset:///Image/bar_insert_big.png"
                    }
                ]

                leftPadding: 30.0

                rightPadding: 20.0
                topPadding: 10.0
                bottomPadding: 10.0
                topMargin: 20.0
                Label {
                    preferredWidth: 176.0
                    preferredHeight: 68.0

                    text: "Phone No"
                    textStyle.fontWeight: FontWeight.Default
                    textStyle.color: Color.Black
                    textStyle.textAlign: TextAlign.Left
                    textStyle.fontFamily: "Consolas"
                    textStyle.fontSizeValue: 8.0
                    verticalAlignment: VerticalAlignment.Center
                    textStyle.fontSize: FontSize.PointValue

                }

                TextField {
                    id: phoneText

                    clearButtonVisible: false
                    input.submitKey: SubmitKey.Done
                    textFormat: TextFormat.Plain
                    textStyle.textAlign: TextAlign.Left
                    input.masking: TextInputMasking.Masked
                    verticalAlignment: VerticalAlignment.Center
                    inputMode: TextFieldInputMode.PhoneNumber
                    backgroundVisible: false
                    textStyle.color: Color.Black
                    text: ""
                    focusHighlightEnabled: false
                    hintText: "e.g +60123456789"
                    textStyle.fontSize: FontSize.PointValue
                    textStyle.fontSizeValue: 8.0

                    onTextChanging: {
                        eventHandler.correctTextField(phoneText, text);
                    }
                }

            }
            TextArea {
                id: descriptionArea
                text: "Password will be sent either via SMS to the above phone number or via email(for non Malaysian mobile number) upon successful submission."
                textStyle.fontWeight: FontWeight.Default
                textStyle.color: Color.Black
                textStyle.textAlign: TextAlign.Left
                textStyle.fontFamily: "Consolas"
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                preferredWidth: 662.0
                topMargin: 12.0
                backgroundVisible: false
                enabled: false
                opacity: 0.6
                editable: false
                textStyle.fontSize: FontSize.PointValue
                textStyle.fontSizeValue: 7.0
                minHeight: 240.0
                preferredHeight: 240.0
                input {
                    flags: TextInputFlag.SpellCheckOff
                }
            }

            //Submit Button
            Container {
                topMargin: 524.0

                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center

                layout: DockLayout {

                }

                ImageButton {
                    id: submitButton
                    defaultImageSource: "asset:///Image/button_greenbar.png"
                    pressedImageSource: "asset:///Image/button_greenbar.png"

                    horizontalAlignment: HorizontalAlignment.Center

                    preferredWidth: 626.0
                    preferredHeight: 90.0

                }

                Label {
                    id: submitLabel
                    textStyle.color: Color.Black
                    text: "Submit"

                    textStyle.textAlign: TextAlign.Center
                    textStyle.fontFamily: "Consolas"
                    textStyle.fontSizeValue: 20.0
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center

                }

                onTouch: {
                    if (event.isDown()) {
                        submitPassword();
                    }
                }

                attachedObjects: [
                    // Create the ComponentDefinition that represents the custom
                    // component in myPage.qml
                    ComponentDefinition {
                        id: pageDefinition
                        source: "activation.qml"
                    }
                ]
            }

        }
    }
    
    function submitPassword(){
        eventHandler.submitPassword(phoneText.text);
    }
}
