import bb.cascades 1.0
//import my.library 1.0

Page {
    id: activationPage

    Container {
        id: activationScreenZ10

        background: Color.create("#f5f5f5")
        layout: StackLayout {

        }

        horizontalAlignment: HorizontalAlignment.Center

        animations: [
            FadeTransition {
                id: showPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        Container {
            id: topBar

            minHeight: 112.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            ImageView {
                id: topBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 112.0
                preferredWidth: 768.0
            }
            Label {
                text: "Activation"
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 9.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
            }

        }

        TextArea {
            text: "Please enter the verification code send to you via SMS or email (for non Malaysian Mobile No)."
            textStyle.fontWeight: FontWeight.Default
            textStyle.color: Color.Black
            textStyle.textAlign: TextAlign.Left
            textStyle.fontFamily: "Consolas"
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            preferredWidth: 662.0
            topMargin: 24.0
            backgroundVisible: true
            opacity: 0.6
            textFormat: TextFormat.Plain
            editable: false
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 7.0
            minHeight: 170.0
            preferredHeight: 170.0
            input {
                flags: TextInputFlag.SpellCheckOff
            }
        }
        
        Container {
            
            preferredWidth: 662.0
            preferredHeight: 108.0
            topMargin: 130.0

            background: activationInfoBackground.imagePaint

            attachedObjects: [
                ImagePaintDefinition {
                    id: activationInfoBackground
                    imageSource: "asset:///Image/bar_insert_big.png"
                }
            ]
            leftPadding: 30.0
            rightPadding: 30.0
            topPadding: 10.0
            bottomPadding: 10.0

            horizontalAlignment: HorizontalAlignment.Center
            
            TextField {
                id: activationText

                clearButtonVisible: false
                input.submitKey: SubmitKey.Done
                textFormat: TextFormat.Plain
                textStyle.textAlign: TextAlign.Left
                input.masking: TextInputMasking.Masked
                verticalAlignment: VerticalAlignment.Center
                inputMode: TextFieldInputMode.NumbersAndPunctuation
                backgroundVisible: false
                textStyle.color: Color.Black
                text: ""
                focusHighlightEnabled: false
                hintText: "Enter activation code here"
                textStyle.fontSize: FontSize.PointValue
                textStyle.fontSizeValue: 7.0
            }
        }
        
        Container {
            topMargin: 196.0

            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            ImageButton {
                id: activateButton
                defaultImageSource: "asset:///Image/button_greenbar.png"
                pressedImageSource: "asset:///Image/button_greenbar.png"

                horizontalAlignment: HorizontalAlignment.Center

                preferredWidth: 624.0
                minHeight: 90.0

            }

            Label {
                id: activateLabel
                textStyle.color: Color.Black
                text: "Activate Now"

                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 7.0
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
            }

            onTouch: {
            	if (event.isDown()){
            	    activate();
            	}
            }

        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Center

            topMargin: 96.0
            Label {
                text: "Didn't receive verification code?"
                textStyle.fontWeight: FontWeight.Default
                textStyle.color: Color.Black
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 7.0

                horizontalAlignment: HorizontalAlignment.Center
                preferredWidth: 662.0
                topMargin: 90.0
                
                enabled: false
                opacity: 0.6
                textStyle.fontSize: FontSize.PointValue
            }

            Container {
                id: resendContainer
                topMargin: 18.0
                enabled: true

                horizontalAlignment: HorizontalAlignment.Center

                layout: DockLayout {

                }

                ImageButton {
                    id: resendButton
                    defaultImageSource: "asset:///Image/button_greenbar_activation.png"
                    disabledImageSource: "asset:///Image/button_greybar_activation.png"
                    pressedImageSource: "asset:///Image/button_greenbar_activation.png"

                    horizontalAlignment: HorizontalAlignment.Center
					
                    preferredWidth: 624.0
                    preferredHeight: 90.0
                    enabled: true
                    
                    onClicked: {
                        resendButton.enabled = false;
                        goResendTimer.start();
                        resendActivationCode();
                    }
                }
            }

        }

    }
    
//    attachedObjects: [
//        QTimer {
//            id: goResendTimer
//            interval: 120000
//            onTimeout: {
//                //display login screen after 2 seconds
//                goResendTimer.stop();
//                resendButton.enabled = true;
//            }
//        }
//    ]
    
    function activate(){
        eventHandler.activateUser(activationText.text);
    }
    
    function resendActivationCode(){
    	eventHandler.resendActivationCode();
    }
}
