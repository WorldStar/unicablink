package application;

public class ResouceConstant {
	/**
	 * Constants and Strings for Unicablink
	 */
	public static int MAX_SAVE_ORDER_COUNT = 1000;
	
	
	/**
	 * Resouce Url
	 */
	
	/**
	 * Splash Screen
	 */
	public static final String UNCAB_SPLASH_URL = "img/splash/splash.png";
	
	/**
	 * MenuItem Icons
	 */
	public static final String UNICAB_MENU_ICON_BOOK = "img/main/icon_bookacab.png";
	public static final String UNICAB_MENU_ICON_HISTORY = "img/main/icon_mybooking.png";
	public static final String UNICAB_MENU_ICON_PROFILE = "img/main/icon_profile.png";
	public static final String UNICAB_MENU_ICON_CONTACT = "img/main/icon_contact.png";
	
	/**
	 * LogIn Screen
	 */
	//Resources
	public static final String UNICAB_LOGO = "img/login/icon_logo.png";
	public static final String LOGIN_BG_TEXT = "img/login/bar_insert.png";
	public static final String LOGIN_BTN_SIGNIN = "img/login/button_signin.png";
	public static final String LOGIN_BTN_SIGNUP = "img/login/button_signup.png";
	public static final String LOGIN_BTN_FORGOT = "img/login/button_forgot.png";
	
	//Strings
	public static final String LOGIN_STR_FORGOT = "Don't have an account?";
	
	
	//Constants
	public static int LOGIN_PADDING_VERTICAL = 12;
	public static int LOGIN_FORGOT_LABEL_HEIGHT = 28;
	public static int LOGIN_VERTICAL_GUTTER = 18;
	public static int LOGIN_TOP_PADDING = 20;
	
	/**
	 * Forgot Screen
	 */
	//Resources
	public static final String FORGOT_TOP_BAR = "img/forgot/forgot_topbar.png";
	public static final String FORGOT_BG_TEXT = "img/forgot/bar_insert_big.png";
	public static final String FORGOT_BTN_FORGOT = "img/forgot/submission_button.png";
	public static final String FORGOT_DESCRIPTION = "img/forgot/description.png";
	
	//Strings
	public static final String FORGOT_MESSAGE = "Password will be sent either via SMS to the above phone number or via email (for non Malaysian mobile number) upon successful submit.";
	
	//Constants
	public static int FORGOT_GUTTER_DESCRIPTION = 30;
	public static int FORGOT_TOP_PADDING = 30;
	public static int FORGOT_GUTTER_BUTTON = 70;
	
	/**
	 * Register Screen
	 */
	//Resources
	public static final String REGISTER_TOP_BAR = "img/register/register_topbar.png";
	public static final String REGISTER_DESCRIPTION = "img/register/description.png";
	public static final String REGISTER_BG_TOP = "img/register/bar_insert_top.png";
	public static final String REGISTER_BG_MIDDLE = "img/register/bar_insert_middle.png";
	public static final String REGISTER_BG_BTM = "img/register/bar_insert_btm.png";
	public static final String REGISTER_BTN_SIGNUP = "img/register/button_signup.png";
	
	//Constants
	public static int REGISTER_TOP_PADDING = 14;
	public static int REGISTER_GUTTER_MAIN = 0;
	public static int REGISTER_GUTTER_DESCRIPTION = 14;
	public static int REGISTER_BTM_PADDING = 40;
	
	/**
	 * Activation Screen
	 */
	//Resources
	public static final String ACTIVATE_TOP_BAR = "img/activate/activate_topbar.png";
	public static final String ACTIVATE_DESCRIPTION = "img/activate/activate_description.png";
	public static final String ACTIVATE_BG_TEXT = "img/activate/bar_insert_big.png";
	public static final String ACTIVATE_BTN_ACTIVATE = "img/activate/button_activate.png";
	public static final String ACTIVATE_BTN_RESEND_DISABLE = "img/activate/button_resend_disable.png";
	public static final String ACTIVATE_BTN_RESEND_ENABLE = "img/activate/button_resend_enable.png";
	public static final String ACTIVATE_DESCRIPTION_RESEND = "img/activate/resend_description.png";
	
	//Constants
	public static int ACTIVATE_TOP_PADDING = 30;
	public static int ACTIVATE_MAIN_GUTTER = 20;
	public static int ACTIVATE_MIDDLE_PADDING = 40;
	public static int ACTIVATE_MIDDLE_GUTTER = 10;
	public static int ACTIVATE_BTM_PADDING = 30;
	
	public static int ACTIVATE_DURATION = 30000;
	
	
	/**
	 * Contact Screen
	 */
	//Resources
	public static final String CONTACT_TOP_BAR = "img/contact/contact_topbar.png";
	public static final String CONTACT_BTN_EMAIL = "img/contact/btn_email.png";
	public static final String CONTACT_BTN_CALL = "img/contact/btn_call.png";
	public static final String CONTACT_LOGO = "img/contact/icon_logo2.png";
	
	//Constants
	public static int CONTACT_TOP_PADDING = 22;
	public static int CONTACT_CONTENT_PADDING = 6;
	
	/**
	 * Profile Screen
	 */
	public static final String PROFILE_TOP_BAR = "img/profile/profile_topbar.png";
	public static final String PROFILE_BG_TOP = "img/profile/bar_insert_top_profile.png";
	public static final String PROFILE_BG_MIDDLE = "img/profile/bar_insert_middle_profile.png";
	public static final String PROFILE_BG_BTM = "img/profile/bar_insert_btm_profile.png";
	public static final String PROFILE_BG_TOP_H = "img/profile/bar_insert_top_profile_h.png";
	public static final String PROFILE_BG_MIDDLE_H = "img/profile/bar_insert_middle_profile_h.png";
	public static final String PROFILE_BG_BTM_H = "img/profile/bar_insert_btm_profile_h.png";
	public static final String PROFLIE_BTN_SAVE = "img/profile/button_save.png";
	public static final String PROFILE_BG_GREY_TOP = "img/profile/bar_insert_top_grey_profile.png";
	
	//Constants
	public static int PROFILE_TOP_PADDING = 30;
	public static int PROFILE_CONTENT_PADDING = 24;
	public static int PROFILE_BUTTON_PADDING = 40;
	public static int PROFILE_BTM_PADDING = 50;
	
	/**
	 * History Screen
	 */
	//Resouce
	public static final String HISTORY_TOP_BAR = "img/history/history_topbar.png";
	public static final String HISTORY_CONTENT_BTM = "img/history/bar_insert_btm_my-booking.png";
	public static final String HISTORY_CONTENT_TOP = "img/history/bar_insert_top_grey_my-booking.png";
	public static final String HISTORY_CONTENT_TOP_SEL = "img/history/bar_insert_top_grey_my-booking_sel.png";
	public static final String HISTORY_CONTENT_MIDDLE = "img/history/bar_insert_middle_my-booking.png";
	public static final String HISTORY_BTN_ARROW = "img/history/icon_arrow.png";
	public static final String HISTORY_TAB_CURRENT_SEL = "img/history/tab_current_sel.png";
	public static final String HISTORY_TAB_CURRENT_UNSEL = "img/history/tab_current_unsel.png";
	public static final String HISTORY_TAB_ADVANCE_SEL = "img/history/tab_advanced_sel.png";
	public static final String HISTORY_TAB_ADVANCE_UNSEL = "img/history/tab_advanced_unsel.png";
	public static final String HISTORY_TAB_HISTORY_SEL = "img/history/tab_history_sel.png";
	public static final String HISTORY_TAB_HISTORY_UNSEL = "img/history/tab_history_unsel.png";
	public static final String HISTORY_MENU_ITEM_REFRESH = "img/history/icon_refresh_main.png";
	public static final String HISTORY_NO_RECORDS = "img/history/no-record@2x.png";
	
	//Constants
	public static final int HISTORY_TAB_PADDING = 26;
	public static final int HISTORY_CONTENT_PADDING = 42;
	
	/**
	 * Current Booking Screen
	 */
	//Resouce
	public static final String DETAILS_TOP_BAR = "img/details/detail_topbar.png";
	public static final String DETAILS_BTN_CANCEL_NORMAL = "img/details/btn_cancel_enable.png";
	public static final String DETAILS_BTN_CANCEL_DISABLE = "img/details/btn_cancel_disable.png";
	public static final String DETAILS_BTN_HISTORY = "img/details/btn_history.png";
	public static final String DETAILS_BTN_BOOK_AGAIN = "img/details/btn_bookagain.png";
	public static final String DETAILS_BAR_MIDDLE = "img/details/bar_middle.png";
	public static final String DETAILS_BAR_MIDDLE_SEL = "img/details/bar_middle_sel.png";
	public static final String DETAILS_BAR_TOP = "img/details/bar_top_grey_order.png";
	public static final String DETAILS_BAR_DOWN_CANCEL = "img/details/bar_down_cancel.png";
	public static final String DETAILS_BAR_DOWN_NORMAL = "img/details/bar_down_normal.png";
	
	//Constants
	public static final int DETAILS_TOP_PADDING = 40;
	public static final int DETAILS_BTN_PADDING = 52;
	
	/**
	 * Booking Screen
	 */
	//Resource
	public static final String BOOKING_TOP_BAR = "img/booking/booking_topbar.png";
	public static final String BOOKING_BAR_LONG = "img/booking/bar_long.png";
	public static final String BOOKING_BAR_NOW = "img/booking/bar_now.png";
	public static final String BOOKING_BAR_TYPE = "img/booking/bar_type.png";
	public static final String BOOKING_BTN_BOOK_NOW = "img/booking/btn_book_now.png";
	public static final String BOOKING_BTN_NOW = "img/booking/icon_now.png";
	public static final String BOOKING_BTN_PICKUP = "img/booking/icon_pickup.png";
	public static final String BOOKING_BTN_DROPOFF = "img/booking/icon_dropoff.png";
	public static final String BOOKING_BTN_TYPE = "img/booking/icon_budget.png";
	public static final String BOOKING_DIALOG_BTN_HERE = "img/booking/icon_here.png";
	
	/**
	 * Address Screen
	 */
	public static final String ADDRESS_TOP_BAR = "img/address/address_topbar.png";
	public static final String ADDRESS_BAR_SEARCH = "img/address/bar_search_address.png";
	public static final String ADDRESS_BTN_APPLY = "img/address/btn_apply_location.png";
	public static final String ADDRESS_ICON_DROPOFF = "img/address/icon_dropoff.png";
	public static final String ADDRESS_ICON_PICKUP = "img/address/icon_pickup.png";
	public static final String ADDRESS_ICON_SEARCH = "img/address/icon_search.png";
	public static final String ADDRESS_ICON_TARGET = "img/address/icon_target.png";
	public static final String ADDRESS_BAR_SEL = "img/address/bar_long_sel.png";
	public static final String ADDRESS_BAR_UNSEL = "img/address/bar_long_unsel.png";
	public static final String ADDRESS_BUBBLE = "img/address/bubble.png";
	
	//Constants
	public static final int ADDRESS_MAX_LENGTH = 50;
	public static final int ADDRESS_MAP_HEIGHT = 400;
	public static final int ADDRESS_BTN_PADDING = 320;
	
	
	/**
	 * Status Message String
	 */
	public static final String STATUS_PROCESS = "We are now processing your order, please wait, the process may take up to 5 min!";
	public static final String STATUS_PROCESS_AGAIN = "We are still trying to locate a taxi for you, please wait...";
	public static final String STATUS_TAXI_EXIST = "There is taxi for you. TQ";
	public static final String STATUS_PROCESS_PICKUP = "Your taxi has arrived. Please proceed to pick up point immediately. TQ";
	public static final String STATUS_JOURNEY = "Journey start. Have a nice trip. TQ";
	public static final String STATUS_PICKUP = "Successfully pick up. TQ for using Unicablink";
	public static final String STATUS_PICKUP_COMPLETE = "Successfully pick up. TQ for using Unicablink";
	public static final String STATUS_JOURNEY_COMPLETE = "Journey completed. TQ for using Unicablink";
	public static final String STATUS_PASSENGER_CANCEL = "Passenger canceled. TQ for using Unicablink";
	public static final String STATUS_DRIVER_CANCEL = "Opp, We are sorry, for some reasons driver cancel the job. Please try again / call our customer support.";
	public static final String STATUS_NO_TAXI = "We are sorry, there is no taxi nearby available at the moment, please try again!, TQ for using Unicablink";
	public static final String STATUS_ADVANCE_JOB = "You are making an advance job, we will proceed your order later.";
	public static final String STATUS_ACCEPTED = "Your job has been accepted"; 
	
	/**
	 * Status Description String
	 */
	public static final String DESCRIPTION_PROCESSING = "Processing";
	public static final String DESCRIPTION_CONFIRMED = "Confirmed";
	public static final String DESCRIPTION_ARRIVED = "Arrived";
	public static final String DESCRIPTION_PICKUP = "Pick up";
	public static final String DESCRIPTION_COMPLETE = "Completed";
	public static final String DESCRIPTION_PASSENGER_CANCEL = "Passenger Canceled";
	public static final String DESCRIPTION_DRIVER_CANCEL = "Driver Canceled";
	public static final String DESCRIPTION_NO_TAXI = "No Taxi";
	public static final String DESCRIPTION_NEW = "New";
	public static final String DESCRIPTION_ACCEPT = "Accepted";
	
	
}
