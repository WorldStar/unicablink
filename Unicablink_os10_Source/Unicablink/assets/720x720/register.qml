import bb.cascades 1.0

Page {
    id: registerPage
    Container {
        id: registerScreenQ10

        background: Color.create("#f5f5f5")
        layout: StackLayout {
            
        }

        horizontalAlignment: HorizontalAlignment.Center

        animations: [
            FadeTransition {
                id: showPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        preferredWidth: 720.0
        preferredHeight: 720.0
        Container {
            id: topBar

            preferredHeight: 64.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }
            
            ImageView {
                id: topBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 64.0
                preferredWidth: 720.0
            }
            Label {
                text: "Register"
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 8.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
            }

        }
        
        ScrollContainer {
            
            Container
            {
                id: scrollContainer
                topMargin: 0.0
                horizontalAlignment: HorizontalAlignment.Center

                topPadding: 16.0
                Container {
                    id: registerInfoContainer
                    topMargin: 0.0
                    preferredWidth: 558.0

                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }

                    horizontalAlignment: HorizontalAlignment.Center
                    Container {
                        id: nameInfo

                        layout: DockLayout {

                        }

                        preferredWidth: 558.0
                        maxHeight: 78.0

                        ImageView {
                            imageSource: "asset:///Image/bar_insert_top.png"
                            scalingMethod: ScalingMethod.Fill
                            maxHeight: 82.0
                            minHeight: 82.0
                            preferredWidth: 558.0
                        }

                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }

                            leftPadding: 14.0
                            rightPadding: 0.0
                            topPadding: 10.0
                            bottomPadding: 10.0

                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: "Name"
                                textStyle.fontWeight: FontWeight.Default
                                textStyle.color: Color.Black
                                textStyle.textAlign: TextAlign.Left
                                textStyle.fontFamily: "Consolas"
                                textStyle.fontSizeValue: 7.0
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredWidth: 144.0
                                textStyle.fontSize: FontSize.PointValue
                            }

                            TextField {
                                id: nameText
                                objectName: "nameText"
                                
                                clearButtonVisible: true
                                input.submitKey: SubmitKey.Done
                                textFormat: TextFormat.Plain
                                textStyle.textAlign: TextAlign.Left
                                input.masking: TextInputMasking.Masked
                                verticalAlignment: VerticalAlignment.Center
                                inputMode: TextFieldInputMode.Default
                                backgroundVisible: false
                                textStyle.color: Color.Black
                                text: ""
                                focusHighlightEnabled: false
                                hintText: ""
                                textStyle.fontSizeValue: 7.0
                                textStyle.fontSize: FontSize.PointValue

                            }

                        }
                    }

                    Container {
                        id: emailInfo

                        layout: DockLayout {

                        }

                        preferredWidth: 558.0
                        maxHeight: 78.0

                        ImageView {
                            imageSource: "asset:///Image/bar_insert_middle.png"
                            scalingMethod: ScalingMethod.Fill
                            maxHeight: 82.0
                            minHeight: 82.0
                            preferredWidth: 558.0
                        }

                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }

                            leftPadding: 14
                            rightPadding: 0.0
                            topPadding: 10.0
                            bottomPadding: 10.0

                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: "Email"
                                textStyle.fontWeight: FontWeight.Default
                                textStyle.color: Color.Black
                                textStyle.textAlign: TextAlign.Left
                                textStyle.fontFamily: "Consolas"
                                textStyle.fontSizeValue: 7.0
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredWidth: 144.0
                                textStyle.fontSize: FontSize.PointValue
                            }

                            TextField {
                                id: emailText
                                objectName: "emailText"
                                
                                clearButtonVisible: true
                                input.submitKey: SubmitKey.Done
                                textFormat: TextFormat.Plain
                                textStyle.textAlign: TextAlign.Left
                                input.masking: TextInputMasking.Masked
                                verticalAlignment: VerticalAlignment.Center
                                inputMode: TextFieldInputMode.EmailAddress
                                backgroundVisible: false
                                textStyle.color: Color.Black
                                text: ""
                                focusHighlightEnabled: false
                                hintText: ""
                                textStyle.fontSizeValue: 7.0
                                textStyle.fontSize: FontSize.PointValue

                            }

                        }
                    }

                    Container {
                        id: phoneInfo

                        layout: DockLayout {

                        }

                        preferredWidth: 558.0
                        maxHeight: 78.0

                        ImageView {
                            imageSource: "asset:///Image/bar_insert_middle.png"
                            scalingMethod: ScalingMethod.Fill
                            maxHeight: 82.0
                            minHeight: 82.0
                            preferredWidth: 558.0
                        }

                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }

                            leftPadding: 14.0
                            rightPadding: 0.0
                            topPadding: 10.0
                            bottomPadding: 10.0

                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: "Phone"
                                textStyle.fontWeight: FontWeight.Default
                                textStyle.color: Color.Black
                                textStyle.textAlign: TextAlign.Left
                                textStyle.fontFamily: "Consolas"
                                textStyle.fontSizeValue: 7.0
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredWidth: 144.0
                                textStyle.fontSize: FontSize.PointValue
                            }

                            TextField {
                                id: phoneText

                                clearButtonVisible: true
                                input.submitKey: SubmitKey.Done
                                textFormat: TextFormat.Plain
                                textStyle.textAlign: TextAlign.Left
                                input.masking: TextInputMasking.Masked
                                verticalAlignment: VerticalAlignment.Center
                                inputMode: TextFieldInputMode.PhoneNumber
                                backgroundVisible: false
                                textStyle.color: Color.Black
                                text: ""
                                focusHighlightEnabled: false
                                hintText: "e.g.+60123456789"
                                textStyle.fontSizeValue: 7.0
                                textStyle.fontSize: FontSize.PointValue

                                onTextChanging: {
                                    eventHandler.correctTextField(phoneText, text);
                                }
                            }
                        }
                    }

                    Container {
                        id: passwordInfo

                        layout: DockLayout {

                        }

                        preferredWidth: 558.0
                        maxHeight: 78.0

                        ImageView {
                            imageSource: "asset:///Image/bar_insert_middle.png"
                            scalingMethod: ScalingMethod.Fill
                            maxHeight: 82.0
                            minHeight: 82.0
                            preferredWidth: 558.0
                        }

                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }

                            leftPadding: 14.0
                            rightPadding: 0.0
                            topPadding: 10.0
                            bottomPadding: 10.0

                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: "Password"
                                textStyle.fontWeight: FontWeight.Default
                                textStyle.color: Color.Black
                                textStyle.textAlign: TextAlign.Left
                                textStyle.fontFamily: "Consolas"
                                textStyle.fontSizeValue: 7.0
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredWidth: 144.0
                                textStyle.fontSize: FontSize.PointValue
                            }

                            TextField {
                                id: passwordText

                                clearButtonVisible: false
                                input.submitKey: SubmitKey.Done
                                textFormat: TextFormat.Plain
                                textStyle.textAlign: TextAlign.Left
                                input.masking: TextInputMasking.Masked
                                verticalAlignment: VerticalAlignment.Center
                                inputMode: TextFieldInputMode.Password
                                backgroundVisible: false
                                textStyle.color: Color.Black
                                text: ""
                                focusHighlightEnabled: false
                                hintText: "min. 6 (alphanumeric)"
                                textStyle.fontSizeValue: 7.0
                                textStyle.fontSize: FontSize.PointValue

                            }

                        }
                    }

                    Container {
                        id: retypeInfo

                        layout: DockLayout {

                        }

                        preferredWidth: 558.0
                        maxHeight: 78.0

                        ImageView {
                            imageSource: "asset:///Image/bar_insert_btm.png"
                            scalingMethod: ScalingMethod.Fill
                            maxHeight: 78.0
                            minHeight: 78.0
                            preferredWidth: 558.0
                        }

                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }

                            leftPadding: 14.0
                            rightPadding: 0.0
                            topPadding: 10.0
                            bottomPadding: 10.0

                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: "Retype"
                                textStyle.fontWeight: FontWeight.Default
                                textStyle.color: Color.Black
                                textStyle.textAlign: TextAlign.Left
                                textStyle.fontFamily: "Consolas"
                                textStyle.fontSizeValue: 7.0
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredWidth: 144.0
                                textStyle.fontSize: FontSize.PointValue
                            }

                            TextField {
                                id: retypeText

                                clearButtonVisible: false
                                input.submitKey: SubmitKey.Done
                                textFormat: TextFormat.Plain
                                textStyle.textAlign: TextAlign.Left
                                input.masking: TextInputMasking.Masked
                                verticalAlignment: VerticalAlignment.Center
                                inputMode: TextFieldInputMode.Password
                                backgroundVisible: false
                                textStyle.color: Color.Black
                                text: ""
                                focusHighlightEnabled: false
                                hintText: "min. 6 (alphanumeric)"
                                textStyle.fontSize: FontSize.PointValue
                                textStyle.fontSizeValue: 7.0

                            }

                        }
                    }
                }
                Container {
                    id: descriptionContainer
                    topMargin: 12.0
                    preferredWidth: 710.0

                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 60.0
                    TextArea {
                        text: "Activation code will be send via SMS to the above phone number or email (for non Malaysia mobile number) upon signing up"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        verticalAlignment: VerticalAlignment.Top

                        preferredWidth: 640.0

                        backgroundVisible: false
                        enabled: false
                        opacity: 0.6
                        textStyle.fontSize: FontSize.PointValue
                        horizontalAlignment: HorizontalAlignment.Left
                        leftPadding: 20.0
                        textStyle.fontSizeValue: 6.0
                        minHeight: 150.0
                        input {
                            flags: TextInputFlag.SpellCheckOff
                        }
                    }
                }

                //Sign Up Button
                Container {
                    topMargin: 12.0
                    bottomMargin: 30.0

                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center

                    layout: DockLayout {

                    }

                    ImageButton {
                        id: signUpButton
                        defaultImageSource: "asset:///Image/button_greenbar.png"
                        pressedImageSource: "asset:///Image/button_greenbar.png"

                        horizontalAlignment: HorizontalAlignment.Center

                        preferredWidth: 546.0
                        preferredHeight: 56.0
                    }

                    Label {
                        id: signUpLabel
                        textStyle.color: Color.Black
                        text: "Sign Up"

                        textStyle.textAlign: TextAlign.Center
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 7.0
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.PointValue

                    }

                    onTouch: {
                        if (event.isDown()) {
                            requestSignUp();
                        }
                    }

                    function requestSignUp() {
                        //Register
                        eventHandler.RegisterUser(nameText.text, emailText.text, phoneText.text, passwordText.text, retypeText.text);
                    }

                    attachedObjects: [
                        // Create the ComponentDefinition that represents the custom
                        // component in myPage.qml
                        ComponentDefinition {
                            id: pageDefinition
                            source: "activation.qml"
                        }
                    ]

                }

            }
        }
    }
}
