package model.request;

import org.json.me.JSONObject;

public class TrackTaxi {
	private String m_strSecurityKey = "a5ab307e0cec2e0d17e523e62298f0c1";
	private long m_nOrderID = 0;
	private String m_strAssistTime = "";
	private String m_strRequestTime = "";
	
	public void setOrderID(long nOrderID){
		m_nOrderID = nOrderID;
	}
	
	public void setAssistTime(String strAssistTime){
		m_strAssistTime = strAssistTime;
	}
	
	public void setRequestTime(String strRequestTime){
		m_strRequestTime = strRequestTime;
	}
	
	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();
		
		try{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("Orderid", m_nOrderID);
			jsonResult.put("AssistTime", m_strAssistTime);
			jsonResult.put("REQUESTTIME", m_strRequestTime);
		}
		catch(Exception e){
			
		}
		return jsonResult;
	}
	
}
