package view.components;

import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * This class is used when fieldmanager has label and textfield with background
 * @author Passion
 *
 * label text, hint text and text of textfield are initialized at first time
 */

public class StatusInfoField extends HorizontalFieldManager{
	/**
	 * members
	 */
	private TranslateAnimateEditField mEditField;
	private LabelField mLabelField;

	private int nTopPadding = 0;
	private int nBottomPadding = 0;
	private int nLeftPadding = 0;
	private int nRightPadding = 0;

	private int nLabelWidth = 0;
	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private Font mFontLabel;
	private Font mFontText;
	
	private int nFontSize = 0;

	private float fScale = Utils.getVRatio();


	/**
	 * create object
	 * @param strBGFile: background file name
	 * @param topPadding: top padding in field manager
	 * @param bottomPadding: bottom padding in field manager
	 * @param leftPadding: left padding in field manager
	 * @param rightPadding: right padding in field manager
	 * @param totalWidth: width of field manager
	 * @param totalHeight: height of field manager
	 * @param strLabelText: Text of Label
	 * @param strHintText: Hint Text of Text field
	 * @param strInitialText: Initial text of Text field
	 * @param labelWidth: width of Label
	 * @param textStyle: text style of Text field
	 * @param maxLen: max length of Text field
	 * @param fontSize: font size of Label and Text field
	 */
	public StatusInfoField(String strBGFile, int topPadding, int bottomPadding, int leftPadding, 
			int rightPadding, String strLabelText, String strHintText,
			String strInitialText, int labelWidth, long textStyle, int maxLen, int fontSize){

		super(HorizontalFieldManager.NO_HORIZONTAL_SCROLL | HorizontalFieldManager.NO_HORIZONTAL_SCROLLBAR);

		//initialize
		nTopPadding = (int)(topPadding * fScale);
		nBottomPadding = (int)(bottomPadding * fScale);
		nLeftPadding = (int)(leftPadding * fScale);
		nRightPadding = (int)(rightPadding * fScale);
		nLabelWidth = (int)(labelWidth * fScale);

		//set background
		Bitmap bgBmp = Utils.getBitmapResource(strBGFile);
		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));

		//initialize
		nTotalWidth = bgBmp.getWidth();
		nTotalHeight = bgBmp.getHeight();

		//set fonts
		nFontSize = fontSize;
		mFontLabel = Utils.getFont(Font.BOLD, fontSize);

		//create Label
		mLabelField = new LabelField(strLabelText, FIELD_VCENTER);
		mLabelField.setFont(mFontLabel);

		//create TextField
		mEditField = new TranslateAnimateEditField(strInitialText, FIELD_VCENTER | USE_ALL_WIDTH);

		mFontText = Utils.getFont(Font.PLAIN, fontSize);
		mEditField.setFont(mFontText);
		mEditField.setFontSize(fontSize);

		//add Fields
		add(mLabelField);
		add(mEditField);
	}

	public String getText(){
		return mEditField.getText();
	}

	public void setText(String strText){
		mEditField.setText(strText);
	}

	public void setEditable(boolean editable) 
	{
		mEditField.setEditable(editable);
	}

	public void setMaxChar(int maxChar)
	{
	}

	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}

	public void setFocus() 
	{
		mEditField.setFocus();
	}

	public void focusChangeNotify(int arg0) 
	{
		super.focusChangeNotify(arg0);
	}

	public void setBackground(String strBGFileName)
	{
		//set background
		Bitmap bgBmp = Utils.getBitmapResource(strBGFileName);
		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));
		
		invalidate();
	}

	public void sublayout(int nWidth, int nHeight){
		Field labelField = getField(0); //Label Field
		Field textField = getField(1); //Text Field

		int nYPosLabel = (nTotalHeight - mFontLabel.getHeight()) / 2;

		//Label field
		setPositionChild(labelField, nLeftPadding, nYPosLabel);
		layoutChild(labelField, nLabelWidth, labelField.getHeight());

		int nXPosText = nLeftPadding + nLabelWidth;

		//Text Field
		layoutChild(textField, nTotalWidth - nLeftPadding - nRightPadding - nLabelWidth, Integer.MAX_VALUE);
		setPositionChild(textField, nXPosText, (nTotalHeight - textField.getHeight()) / 2);

		this.setExtent(nTotalWidth, nTotalHeight);
	}
}
