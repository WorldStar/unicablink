package view.fieldmanager;

import application.Utils;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class TabMenuManager extends HorizontalFieldManager{
	/**
	 * private members
	 */
	private int nLeftMargin = 0;
	private int nRightMargin = 0;
	private int nTopMargin = 0;
	private int nBottomMargin = 0;
	
	private int nPadding = 0;
	
	private float fScale = Utils.getVRatio();
	
	public TabMenuManager(int leftMargin, int rightMargin, int topMargin, int bottomMargin, int padding, int backColor)
	{
		super(USE_ALL_WIDTH | Manager.HORIZONTAL_SCROLL | Manager.HORIZONTAL_SCROLLBAR);
		
		/**
		 * initialize
		 */
		nLeftMargin = (int)(leftMargin * fScale);
		nRightMargin = (int)(rightMargin * fScale);
		nTopMargin = (int)(topMargin * fScale);
		nBottomMargin = (int)(bottomMargin * fScale);
		
		nPadding = (int)(padding * fScale);
		
		/**
		 * set Background
		 */
		if (backColor != 0)
		{
			setBackground(BackgroundFactory.createSolidBackground(backColor));
		}
	}
	
	/**
	 * get and set
	 */
	public int getPadding()
	{
		return nPadding;
	}
	
	public void setPadding(int padding)
	{
		nPadding = (int)(padding * fScale);
	}
	
	/**
	 * overrides
	 */
	public void sublayout(int nWidth, int nHeight)
    {
        int nNumFields = getFieldCount();
        int nYPos = nTopMargin;
        int nXPos = nLeftMargin;
        
        int nContentHeight = 0;
        
        for (int i = 0; i < nNumFields; i++)
        {
            Field fField = this.getField(i);
            setPositionChild(fField, nXPos, nYPos);
            layoutChild(fField, this.getPreferredWidth(), Integer.MAX_VALUE);
            
            nXPos = nXPos + fField.getPreferredWidth() + nPadding;
            if (nContentHeight < fField.getPreferredHeight())
            {
            	nContentHeight = fField.getPreferredHeight();
            }
        }

    	this.setExtent(Utils.getDisplayWidth(), nYPos + nContentHeight + nBottomMargin);
    }
	
}
