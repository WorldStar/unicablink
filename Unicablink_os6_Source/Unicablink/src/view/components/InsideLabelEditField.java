package view.components;

import java.util.Calendar;
import java.util.Date;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.KeypadListener;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.ContextMenu;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.TextField;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;
import net.rim.device.api.ui.picker.DateTimePicker;
import net.rim.device.api.ui.text.TextFilter;

/**
 * EditField having the label inside itself The label appear or disappear
 * automatically
 * 
 * @author Simon. Just Mobile Sdn Bhd.
 * @created 23/10/2012
 */
public class InsideLabelEditField extends EditField {
	private static final int EDIT_FIELD = 0;
	private static final int DATE_FIELD = 1;
	private static final int PASS_FIELD = 2;
	
	private String _label = "";
	private boolean _isDrawingLabel = false;
	private int _foreColor = Color.BLACK;

	private int _fontSize = 24;
	private int _paddingSpace = 6;
	private int _fieldType;
	private Date _date;
	private int _levelColor = Color.DARKGRAY;
	private int _backColor = Color.FLORALWHITE;
	private boolean _drawFocus = false;

	public InsideLabelEditField(String label) {
		super("", "", 255, TextField.NO_LEARNING | TextField.NO_NEWLINE | TextField.JUMP_FOCUS_AT_END
				| TextField.NON_SPELLCHECKABLE);

		if (label.length() > 0) {
			_label = label;
			_isDrawingLabel = true;
		}

		initialize();
	}

	public InsideLabelEditField(String label, int maxLength, int foreColor) {
		super("", "", maxLength, TextField.NO_LEARNING | TextField.NO_NEWLINE | TextField.JUMP_FOCUS_AT_END
				| TextField.NON_SPELLCHECKABLE);

		if (label.length() > 0) {
			_label = label;
			_isDrawingLabel = true;
		}

		_foreColor = foreColor;

		initialize();
	}
	
	public InsideLabelEditField(String label, String initialText, int maxLength, long style)
	{
		super("", initialText, maxLength, TextField.NO_LEARNING | TextField.NO_NEWLINE | TextField.JUMP_FOCUS_AT_END
				| TextField.NON_SPELLCHECKABLE | style);

		if (label.length() > 0) {
			_label = label;
			_isDrawingLabel = true;
		}

		initialize();
	}

	public InsideLabelEditField(String label, long style) {
		super("", "", 255, style);

		if (label.length() > 0) {
			_label = label;
			_isDrawingLabel = true;
		}

		initialize();
	}

	private void initialize() {
		setFont(Font.getDefault().derive(Font.PLAIN, _fontSize, Ui.UNITS_px));

		_fieldType = EDIT_FIELD;
	}

	protected boolean keyDown(int keycode, int time) {
		if(this.isEditable()){
			String r = getText();
	
			if (r.equals(_label) || r.length() == 0) {
				setText("");
				_isDrawingLabel = false;
			} else if (r.length() == 1 && keycode == 524288/* backspace */) {
				setCursorPosition(0);
				_isDrawingLabel = true;
			}
			
			if(_fieldType != DATE_FIELD) {
			}else {
				int key = Keypad.key(keycode);
				if(key != Keypad.KEY_MENU && key != Keypad.KEY_ESCAPE){
					String date = PickUpDate();
					setText(date);
					
					return true;
				}
				return false;
			}
		}
		return super.keyDown(keycode, time);
	}

	protected void paint(Graphics g) {
		int oldForecolor = g.getColor();
		int oldBackcolor = g.getBackgroundColor();
//		String t = getInsideLabel();
		if(!_drawFocus) {
			g.clear();
			g.setBackgroundColor(_backColor);
	
			if (_isDrawingLabel) {
				g.setColor(_levelColor);
				g.drawText(_label, 0, 0, (int) DrawStyle.LEFT, getWidth()
						- _paddingSpace);
			} else {
				g.setColor(_foreColor);
				
				if(_fieldType == PASS_FIELD){
					String stars = "";
					
					for(int i = 0; i < this.getTextLength(); i++)
						stars += "*";
					
					g.drawText(stars, 0, 0, (int) DrawStyle.LEFT, getWidth()
							- _paddingSpace);
					return;
				}
			}
		}	
	
		super.paint(g);
		g.setColor(oldForecolor);
		g.setBackgroundColor(oldBackcolor);
			
	}

	protected void onUnfocus() {
		if (getText().length() == 0) {
			setText("");
			setCursorPosition(0);
			_isDrawingLabel = true;
		}
	}

	public void setNumeric() {
		setFilter(TextFilter.get(TextFilter.NUMERIC));
		drawLabel(true);
	}

	public void setPhone() {
		setFilter(TextFilter.get(TextFilter.PHONE));
		drawLabel(true);
	}

	public void setEmail() {
		setFilter(TextFilter.get(TextFilter.EMAIL));
		drawLabel(true);
	}

	public void setDateTime() {
		_fieldType = DATE_FIELD;
//		setEditable(false);
	}

	protected void makeContextMenu(ContextMenu contextMenu) {
		super.makeContextMenu(contextMenu);
		if(_fieldType == DATE_FIELD)
			contextMenu.addItem(new DatePickMenu(this, "Date", 10, 10));
	}

	private class DatePickMenu extends MenuItem {

		private EditField _parent;

		public DatePickMenu(InsideLabelEditField parent, String text, int ordinal, int priority) {
			super(text, ordinal, priority);
			_parent = parent;
		}

		public void run() {
			setText("");
			_isDrawingLabel = false;
			
			_parent.setText(PickUpDate());
		}

	}
	
	private String PickUpDate(){
		if (_fieldType == DATE_FIELD) {
			DateTimePicker dtPicker = DateTimePicker.createInstance(
					Calendar.getInstance(), "MM-dd-yyyy", null);
			dtPicker.doModal();
			
			_date = dtPicker.getDateTime().getTime();
			String strDate = new SimpleDateFormat("dd MMM yyyy").format(_date);
			return strDate;
		}
		
		return null;
	}

	public Date getDate() {
		return _date;
	}

	public void setPassword() {
		_fieldType = PASS_FIELD;
	}

	public void setInsideLabel(String label) {
		_label = label;
		_isDrawingLabel = true;
		setText("");
	}

	public String getInsideLabel() {
		return _label;
	}

	public int getFontSize() {
		return _fontSize;
	}
	
	public void setText(String text) {
		super.setText(text);
		_isDrawingLabel = false;
	}
	
    protected void drawFocus(Graphics graphics, boolean on) {
        _drawFocus = on;
        super.drawFocus(graphics, on);
        _drawFocus = false;
    }

    /**
     * Must be called with "true" value after setNumeric() was called.
     * @param flag
     */
    private void drawLabel(boolean flag) {
		_isDrawingLabel = flag;
    }
}
