import bb.cascades 1.0
import QtMobility.sensors 1.2
import bb.cascades.maps 1.0
import QtMobilitySubset.location 1.1

TabbedPane {
	id: tabbedPane
	showTabsOnActionBar: false
	peekEnabled: false
	objectName: "tabbedPane"

	//start added by my team at 20130928
	onSidebarStateChanged: {
		if (sidebarState != 0) {
			console.log("====tab sider bar is shown===" + sidebarState);
			mapViewContainer.visible = false;
		} else {
			eventHandler.showMapViewWithDelay(500, 3);
		}
	}
	//  end added by my team at 20130928

	Tab {
		id: mainbookingTab
		objectName: "mainBookingTab"
		title: "Book A Cab"

		property int nType: 1
		property bool bInitMainBooking: true

		content: NavigationPane {
			id: navigationPane
			objectName: "navigationPane"
			Page {
				Container {
					id: mainPageZ10

					background: Color.create("#f5f5f5")
					layout: StackLayout {
						orientation: LayoutOrientation.TopToBottom
					}

					horizontalAlignment: HorizontalAlignment.Center

					animations: [
						FadeTransition {
							id: showMainPageEffect
							duration: 500
							toOpacity: 1.0
							fromOpacity: 0.0
						}
					]

					Container {
						id: topMainPageBar

						preferredHeight: 112.0
						minHeight: 112.0

						horizontalAlignment: HorizontalAlignment.Center

						layout: DockLayout {

						}

						ImageView {
							id: topMainPageBarBackground
							imageSource: "asset:///Image/topbar_gradient.png"
							scalingMethod: ScalingMethod.Fill
							preferredHeight: 112.0
							preferredWidth: 768.0
						}
						Label {
							text: (mainbookingTab.nType == 1) ? "Booking" : "Address"
							textStyle.fontWeight: FontWeight.Bold
							textStyle.color: Color.White
							textStyle.textAlign: TextAlign.Center
							textStyle.fontFamily: "Consolas"
							textStyle.fontSizeValue: 9.0
							verticalAlignment: VerticalAlignment.Center
							horizontalAlignment: HorizontalAlignment.Center
							textStyle.fontSize: FontSize.PointValue
						}

						Container {
							leftPadding: 20.0

							horizontalAlignment: HorizontalAlignment.Left
							verticalAlignment: VerticalAlignment.Center
							ImageButton {
								defaultImageSource: "asset:///Image/button_greybar_back.png"
								pressedImageSource: "asset:///Image/button_greybar_back.png"
								preferredWidth: 154.0
								preferredHeight: 70.0

								visible: (mainbookingTab.nType == 1) ? false: true
								onClicked: {
									showMainPlaces();
								}
							}
						}
					} //end TopBar

					Container {
						id: topInformationContainer

						layout: DockLayout {

						}

						horizontalAlignment: HorizontalAlignment.Fill
						//Map and Book now button
						Container {
							id: mapViewContainer
							objectName: "mapViewContainer"
							property alias dLat: mapview.latitude
							property alias dLon: mapview.longitude

							layout: DockLayout {

							}

							horizontalAlignment: HorizontalAlignment.Center
							verticalAlignment: VerticalAlignment.Top

							preferredWidth: 768
							minHeight: 1040
							preferredHeight: 1040

							//Map View
							MapView {
								id: mapview
								objectName: "mapView"

								altitude: 2000

								latitude: 43.449488
								longitude: -80.406777

								preferredWidth: 768
								preferredHeight: 1040
								minHeight: 1040

//								onTouch: {
//									if (event.isMove())
//									{
//										if (mainbookingTab.nType != 1)
//										{
//											candListScrollView.visible = false;
//										}
//									}
//									if (event.isUp()){
//										if (mainbookingTab.nType != 1)
//										{
//											getAddress();
//										}
//									}
//                                    eventHandler.Message("yyyyy");
//								}

								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Center
								minWidth: 768.0
							} // end of Map view
                            
                            onTouch: {
                                if (event.isMove()) {
                                    if (mainbookingTab.nType != 1) {
                                        candListScrollView.visible = false;
                                    }
                                }
                                if (event.isUp()) {
                                    if (mainbookingTab.nType != 1) {
                                        getAddress();
                                    }
                                }
                            }

							Container {
								id: pinContainer

								visible: (mainbookingTab.nType == 1) ? false : true
								// Must match the mapview width and height and position
								preferredWidth: 768
								preferredHeight: 1040
								minHeight: 1040

								overlapTouchPolicy: OverlapTouchPolicy.Allow

								layout: AbsoluteLayout {
								}

								minWidth: 768.0
								Container {
									id: descriptionContainer
									preferredWidth: 768.0
									preferredHeight: 112.0
									layoutProperties: AbsoluteLayoutProperties {
										positionY: 338
									}

									objectName: "descriptionContainer"

									visible: false

									background: descriptionBackground.imagePaint
									overlapTouchPolicy: OverlapTouchPolicy.Allow

									leftPadding: 10.0
									rightPadding: 10.0

									topPadding: 10.0
									minWidth: 768.0
									Label {
										id: descriptionLabel
										objectName: "descriptionLabel"

										text: ""
										textStyle.fontWeight: FontWeight.Default
										textStyle.color: Color.Black
										textStyle.textAlign: TextAlign.Left
										verticalAlignment: VerticalAlignment.Center
										multiline: true
										textStyle.fontSize: FontSize.XSmall
										maxHeight: 90.0
										overlapTouchPolicy: OverlapTouchPolicy.Allow
									}
								}

								ImageView {
									id: pinButton

									visible: (mainbookingTab.nType == 1) ? false : true
									imageSource: (mainbookingTab.nType == 3) ? "asset:///Image/icon_dropoff.png" : "asset:///Image/icon_pickup.png"

									layoutProperties: AbsoluteLayoutProperties {
										positionX: 358
										positionY: 450
									}

									preferredWidth: 52.0
									preferredHeight: 70.0
									overlapTouchPolicy: OverlapTouchPolicy.Allow
								}
							}

							Container {
								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Bottom

								bottomPadding: 30.0

								//Book Now Button
								ImageButton {
									defaultImageSource: (mainbookingTab.nType == 1) ? "asset:///Image/button_greenbar_booknow.png" : "asset:///Image/button_greenbar_apply_location.png"
									pressedImageSource: (mainbookingTab.nType == 1) ? "asset:///Image/button_greenbar_booknow.png" : "asset:///Image/button_greenbar_apply_location.png"

									onClicked: {
										if (mainbookingTab.nType == 1) {
											bookNow();
										} else {
											applyLocation();
										}
									}

									preferredWidth: 620.0
									preferredHeight: 90.0
									horizontalAlignment: HorizontalAlignment.Center
								}
							} //end of image button

							onCreationCompleted: {
								positionSource.update();
							}
							minWidth: 768.0
						}

						Container {
							id: bookingTabContainer

							horizontalAlignment: HorizontalAlignment.Center
							verticalAlignment: VerticalAlignment.Top
							layout: DockLayout {

							}

							minWidth: 768.0
							preferredWidth: 768.0
							Container {
								id: bookingInfo
								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Top

								visible: (mainbookingTab.nType == 1) ? true : false

								preferredWidth: 768.0
								minWidth: 768.0
								Container {
									id: pickupInfo

									layout: DockLayout {

									}


									preferredWidth: 768.0

									minWidth: 768.0
									background: infoBackground.imagePaint

									preferredHeight: 102.0
									minHeight: 102.0
									Container {

										layout: StackLayout {
											orientation: LayoutOrientation.LeftToRight
										}
										preferredWidth: 768.0

										leftPadding: 30.0
										rightPadding: 30.0

										verticalAlignment: VerticalAlignment.Center

										minWidth: 768.0
										Label {
											text: "Pick up"
											textStyle.fontWeight: FontWeight.Default
											textStyle.color: Color.Black
											textStyle.textAlign: TextAlign.Left
											textStyle.fontFamily: "Consolas"
											textStyle.fontSizeValue: 7
											verticalAlignment: VerticalAlignment.Center
											horizontalAlignment: HorizontalAlignment.Left
											minWidth: 164.0
											maxWidth: 164.0
											textStyle.fontSize: FontSize.PointValue
											preferredWidth: 164.0
										}

										Container {
											id: pickInfoContainer

											layout: DockLayout {

											}

											preferredWidth: 544.0
											minWidth: 544.0
											maxWidth: 544.0

											Label {
												id: pickupInfoLabel
												objectName: "pickupInfoLabel"

												property variant strPickupLocation: ""
												property bool bSetPickup: false

												text: (bSetPickup) ? strPickupLocation : "Tap to select nearest pick up."

												textStyle.color: (bSetPickup) ? Color.Black : Color.create("#7f7f7f")

												textStyle.textAlign: TextAlign.Left
												textStyle.fontFamily: "Consolas"
												textStyle.fontSizeValue: 7
												verticalAlignment: VerticalAlignment.Center

												textStyle.fontSize: FontSize.PointValue
												textStyle.fontStyle: (bSetPickup) ? FontStyle.Default : FontStyle.Italic
												minWidth: 486.0
												preferredWidth: 486.0
												maxWidth: 486.0
												horizontalAlignment: HorizontalAlignment.Left
											}

											ImageButton {
												preferredWidth: 58.0
												preferredHeight: 78.0

												defaultImageSource: "asset:///Image/icon_pickup.png"
												pressedImageSource: "asset:///Image/icon_pickup.png"

												onClicked: {
													dateTimeContainer.visible = false;
													mainbookingTab.nType = 2;
													//Go AddressPage
													setPickupFlag();
												}

												verticalAlignment: VerticalAlignment.Center
												minWidth: 58.0
												minHeight: 78.0
												horizontalAlignment: HorizontalAlignment.Right

											}




											onTouch: {
												if (event.isUp()) {
													dateTimeContainer.visible = false;
													mainbookingTab.nType = 2;
													//Go AddressPage
													setPickupFlag();
												}
											}
										}
									}
								} //Pickup Info

								Container {
									id: pickupAtInfo

									layout: DockLayout {

									}

									preferredWidth: 768.0
									minWidth: 768.0
									minHeight: 102.0
									preferredHeight: 102.0
									background: infoBackground.imagePaint

									Container {
										layout: StackLayout {
											orientation: LayoutOrientation.LeftToRight
										}

										leftPadding: 30.0
										rightPadding: 30.0

										verticalAlignment: VerticalAlignment.Center
										preferredWidth: 768.0
										minWidth: 768.0
										Label {
											text: "Pick up at"
											textStyle.fontWeight: FontWeight.Default
											textStyle.color: Color.Black
											textStyle.textAlign: TextAlign.Left
											textStyle.fontFamily: "Consolas"
											textStyle.fontSizeValue: 7.0
											verticalAlignment: VerticalAlignment.Center
											horizontalAlignment: HorizontalAlignment.Left
											minWidth: 144.0
											textStyle.fontSize: FontSize.PointValue
											preferredWidth: 144.0
											maxWidth: 144.0
										}

										Container {
											verticalAlignment: VerticalAlignment.Center

											minWidth: 564.0
											preferredWidth: 564.0
											maxWidth: 564.0
											layout: StackLayout {

											}
											TextField {
												id: pickupAtInfoText
												text: ""
												textStyle.fontWeight: FontWeight.Default
												textStyle.color: Color.Black
												textStyle.textAlign: TextAlign.Left
												textStyle.fontFamily: "Consolas"
												textStyle.fontSizeValue: 7.0
												verticalAlignment: VerticalAlignment.Center

												hintText: "E.g. house no/building/lobby/note to driver"
												backgroundVisible: false
												textStyle.fontSize: FontSize.PointValue
											}
										}

									}
								} //Pickup at Info

								Container {
									id: dropOffInfo

									layout: DockLayout {

									}

									preferredWidth: 768.0
									maxHeight: 0.0

									minWidth: 768.0
									background: infoBackground.imagePaint

									minHeight: 102.0
									preferredHeight: 102.0
									Container {
										layout: StackLayout {
											orientation: LayoutOrientation.LeftToRight
										}
										preferredWidth: 768.0

										leftPadding: 30.0
										rightPadding: 30.0

										verticalAlignment: VerticalAlignment.Center
										minWidth: 768.0
										Label {
											text: "Drop off"
											textStyle.fontWeight: FontWeight.Default
											textStyle.color: Color.Black
											textStyle.textAlign: TextAlign.Left
											textStyle.fontFamily: "Consolas"
											textStyle.fontSizeValue: 7.0
											verticalAlignment: VerticalAlignment.Center
											minWidth: 164.0
											textStyle.fontSize: FontSize.PointValue
											preferredWidth: 164.0
											maxWidth: 164.0
										}

										Container {
											id: dropInfoContainer

											layout: DockLayout {

											}

											preferredWidth: 544.0
											maxWidth: 544.0
											minWidth: 544.0

											ImageButton {
												defaultImageSource: "asset:///Image/icon_dropoff.png"
												pressedImageSource: "asset:///Image/icon_dropoff.png"

												verticalAlignment: VerticalAlignment.Center
												minWidth: 58.0
												minHeight: 78.0

												onClicked: {
													dateTimeContainer.visible = false;
													mainbookingTab.nType = 3;
													//Go Dropoff Page
													setDropOffFlag()
												}
												horizontalAlignment: HorizontalAlignment.Right
											}

											Label {
												id: dropoffInfoLabel
												objectName: "dropoffInfoLabel"

												property variant strDropOffLocation: ""
												property bool bSetDropOff: false

												text: (bSetDropOff) ? strDropOffLocation : "Tap to select drop off."

												textStyle.color: (bSetDropOff) ? Color.Black : Color.create("#7f7f7f")

												textStyle.textAlign: TextAlign.Left
												textStyle.fontFamily: "Consolas"
												textStyle.fontSizeValue: 7
												verticalAlignment: VerticalAlignment.Center

												textStyle.fontSize: FontSize.PointValue
												textStyle.fontStyle: (bSetDropOff) ? FontStyle.Default : FontStyle.Italic
												minWidth: 486.0
												preferredWidth: 486.0
												maxWidth: 486.0
												horizontalAlignment: HorizontalAlignment.Left
											}

											onTouch: {
												if (event.isUp()) {
													dateTimeContainer.visible = false;
													mainbookingTab.nType = 3;
													//Go Dropoff Page
													setDropOffFlag()
												}
											}

										}
									}
								} //dropoff Info

								Container {
									id: timeBudgetInfo

									preferredHeight: 102.0
									layout: StackLayout {
										orientation: LayoutOrientation.LeftToRight

									}

									preferredWidth: 768.0
									minWidth: 768.0
									minHeight: 102.0
									Container {
										id: timeInfo
										preferredWidth: 366.0
										preferredHeight: 102.0

										layout: DockLayout {

										}

										minWidth: 366.0

										minHeight: 102.0
										background: infoBackground.imagePaint

										Container {
											leftPadding: 30.0
											topPadding: 10.0

											layout: StackLayout {
												orientation: LayoutOrientation.LeftToRight
											}

											verticalAlignment: VerticalAlignment.Center

											ImageButton {
												id: timeButton
												defaultImageSource: "asset:///Image/icon_now.png"
												pressedImageSource: "asset:///Image/icon_now.png"
												minWidth: 46.0
												minHeight: 46.0

												onClicked: {
													dateTimeContainer.visible = true;
												}
											}

											Label {
												id: timeLabel
												objectName: "timeLabel"
												leftMargin: 6.0
												text: "Now"
												textStyle.fontWeight: FontWeight.Default
												textStyle.color: Color.Black
												textStyle.textAlign: TextAlign.Left
												verticalAlignment: VerticalAlignment.Center
												horizontalAlignment: HorizontalAlignment.Left
												textStyle.fontFamily: "Consolas"
												textStyle.fontSizeValue: 6.0
												textStyle.fontSize: FontSize.PointValue
											}

											onTouch: {
												if (event.isUp()) {
													dateTimeContainer.visible = true;
												}
											}
										}

									} //Time Info

									Container {
										id: budgetInfo
										preferredWidth: 422.0
										preferredHeight: 102.0

										layout: DockLayout {

										}

										minWidth: 422.0
										minHeight: 102.0
										background: infoBackground.imagePaint

										Container {
											leftPadding: 24.0
											topPadding: 10.0

											layout: StackLayout {
												orientation: LayoutOrientation.LeftToRight
											}

											verticalAlignment: VerticalAlignment.Center

											ImageButton {
												id: carButton
												minWidth: 66.0
												minHeight: 46.0
												defaultImageSource: "asset:///Image/icon_budget.png"

												onClicked: {
													if (budgetLabel.text == "Budget") {
														budgetLabel.text = "Premium / Executive"
													} else {
														budgetLabel.text = "Budget"
													}
												}

												pressedImageSource: "asset:///Image/icon_budget.png"
											}

											Label {
												id: budgetLabel

												leftMargin: 6.0
												text: "Budget"
												textStyle.fontWeight: FontWeight.Default
												textStyle.color: Color.Black
												textStyle.textAlign: TextAlign.Left
												textStyle.fontFamily: "Consolas"
												textStyle.fontSizeValue: 6.0
												verticalAlignment: VerticalAlignment.Center
												horizontalAlignment: HorizontalAlignment.Left
												textStyle.fontSize: FontSize.PointValue
											}

											onTouch: {
												if (event.isUp()) {
													if (budgetLabel.text == "Budget") {
														budgetLabel.text = "Premium / Executive"
													} else {
														budgetLabel.text = "Budget"
													}
												}
											}
										}

									}
								} //end of Time and Budget Info

							}

							Container {
								id: addressPageContainer

								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Top

								visible: (mainbookingTab.nType == 1) ? false : true

								preferredWidth: 768.0

								minWidth: 768.0
								Container {
									preferredWidth: 768.0

									layout: DockLayout {

									}

									background: searchBarBackground.imagePaint

									attachedObjects: [
										ImagePaintDefinition {
											id: searchBarBackground
											imageSource: "asset:///Image/bar_search_address.png"
											repeatPattern: RepeatPattern.Fill
										}
									]

									leftPadding: 74.0
									rightPadding: 22.0

									minHeight: 132.0
									maxHeight: 132.0
									minWidth: 768.0
									TextField {
										id: addressField
										objectName: "addressField"

										preferredWidth: 550.0
										preferredHeight: 48.0

										horizontalAlignment: HorizontalAlignment.Left
										verticalAlignment: VerticalAlignment.Center
										textStyle.fontSize: FontSize.Small

										hintText: "Input Address"
										textStyle.color: Color.create("#cb000000")
										backgroundVisible: false
										clearButtonVisible: true
										input {
											submitKey: SubmitKey.Done

											onSubmitted: {
												findLocation();
											}
										}
									}

									ImageButton {
										preferredWidth: 72.0
										preferredHeight: 72.0

										verticalAlignment: VerticalAlignment.Center
										horizontalAlignment: HorizontalAlignment.Right

										defaultImageSource: "asset:///Image/icon_search.png"
										pressedImageSource: "asset:///Image/icon_search.png"

										onClicked: {
											findLocation();
										}
									}
								} //end of search box

								ScrollView {
									scrollViewProperties {
										scrollMode: ScrollMode.Vertical
										overScrollEffectMode: OverScrollEffectMode.OnScroll
										pinchToZoomEnabled: false
									}

									horizontalAlignment: HorizontalAlignment.Center
									verticalAlignment: VerticalAlignment.Top

									id: candListScrollView
									objectName: "candListScrollView"
									visible: true
									maxHeight: 450.0
									preferredWidth: 768.0

									Container {
										id: candListContainer

										layout: StackLayout {
											orientation: LayoutOrientation.TopToBottom
										}

										objectName: "candListContainer"
									}
								}
							}

						}

						Container {
							id: dateTimeContainer

							visible: false
							horizontalAlignment: HorizontalAlignment.Center
							verticalAlignment: VerticalAlignment.Bottom

							preferredWidth: 768.0
							bottomPadding: 10.0
							background: Color.create("#7c000000")

							minWidth: 768.0
							Container {
								background: Color.create("#90000000")
								leftPadding: 30.0
								rightPadding: 20.0
								topPadding: 5.0
								bottomPadding: 5.0

								layout: DockLayout {

								}

								preferredWidth: 768.0

								Label {
									text: "Set date & time for pickup"
									textFormat: TextFormat.Plain
									verticalAlignment: VerticalAlignment.Center
									horizontalAlignment: HorizontalAlignment.Left
								}

								Button {
									text: "Done"

									onClicked: {
										dateTimeContainer.visible = false
										changeTime();
									}

									horizontalAlignment: HorizontalAlignment.Right
									verticalAlignment: VerticalAlignment.Center
									preferredWidth: 160.0
								}
							}

							DateTimePicker {
								id: timePicker
								expanded: false
								property bool bByCode: false
								objectName: "timePicker"
//                                title: "Set date & time for pickup"
								mode: DateTimePickerMode.DateTime
								value: {
									new Date();
								}

								horizontalAlignment: HorizontalAlignment.Center
								onValueChanged: {
									if (bByCode) {
										bByCode = false;
										return;
									}

									setCorrectTime();
								}
							}
						}
					}

					attachedObjects: [
						PositionSource {
							id: positionSource
							updateInterval: 1000
							active: false

							onPositionChanged: {
								fitCenter();
								positionSource.active = false;
								eventHandler.setPickupMarker(mapview.latitude, mapview.longitude);
							}

							function fitCenter() {
								mapview.latitude = positionSource.position.coordinate.latitude;
								mapview.longitude = positionSource.position.coordinate.longitude;
							}
						},

						ImagePaintDefinition {
							id: descriptionBackground
							imageSource: "asset:///Image/bubble.png"
						},
						ImagePaintDefinition {
							id: infoBackground
							imageSource: "asset:///Image/bar_insert_middle_profile.png"
						}
					]

					onCreationCompleted: {

					}
				} //end of page
			}
			onPopTransitionEnded: {
				page.destroy();
			}

		}//end of navigation pane

		imageSource: "asset:///Image/icon_bookacab.png"
	} // end of first Tab

	Tab {
		id: mybookingTab
		objectName: "myBookingTab"
		title: "My Booking"

		content: Page {
			Container { //my booking container
				id: myBookingContainer
				preferredWidth: 768.0
				preferredHeight: 1280.0

				layout: StackLayout {

				}

				background: Color.create("#dcddde")
				animations: [
					FadeTransition {
						id: showBookingPageEffect
						duration: 500
						toOpacity: 1.0
						fromOpacity: 0.0
					}
				]

				//Top Contents
				Container
				{
					id: topBookingBar
					preferredHeight: 112.0

					layout: DockLayout {

					}

					ImageView {
						id: topBookingBarBackground
						imageSource: "asset:///Image/topbar_gradient.png"
						scalingMethod: ScalingMethod.Fill
						preferredHeight: 112.0
						preferredWidth: 768.0

						horizontalAlignment: HorizontalAlignment.Center
					}

					Label {
						text: "My Booking"
						textStyle.fontWeight: FontWeight.Bold
						textStyle.color: Color.White
						textStyle.textAlign: TextAlign.Center
						textStyle.fontFamily: "Consolas"
						textStyle.fontSizeValue: 9.0
						verticalAlignment: VerticalAlignment.Center
						horizontalAlignment: HorizontalAlignment.Center
						textStyle.fontSize: FontSize.PointValue
					}

					minHeight: 112.0

				}//end of topbar

				//tab Contents
				Container {
					id: tabBar

					minHeight: 112.0
					preferredWidth: 768.0

					horizontalAlignment: HorizontalAlignment.Center

					layout: DockLayout {

					}

					background: tabBarBackground.imagePaint

					attachedObjects: [
						ImagePaintDefinition {
							id: tabBarBackground
							imageSource: "asset:///Image/bar_insert_middle_profile.png"
						}
					]

					Container {
						id: buttonContainer
						objectName: "buttonContainer"

						layout: StackLayout {
							orientation: LayoutOrientation.LeftToRight
						}

						horizontalAlignment: HorizontalAlignment.Left
						verticalAlignment: VerticalAlignment.Bottom

						leftPadding: 30.0

						Container {
							layout: DockLayout {

							}

							ImageButton {
								id: currentButton
								objectName: "currentButton"
								defaultImageSource: "asset:///Image/bar_top_grey_my-booking.png"

								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Center

								minWidth: 188.0
							}

							Label {
								id: currentLabel

								text: "Current"
								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Center

								textStyle.color: Color.Black
								textStyle.fontSize: FontSize.PointValue
								textStyle.fontSizeValue: 7.0
							}

							onTouch: {
								if (event.isDown()) {
									currentButton.defaultImageSource = "asset:///Image/bar_top_grey_my-booking.png"
									currentLabel.textStyle.color = Color.Black

									advancedButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
									advancedLabel.textStyle.color = Color.White

									historyButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
									historyLabel.textStyle.color = Color.White


									if (currentContainer.count() == 0)
									{
										currentContainer.visible = false;
										noRecordContainer.visible = true;
									}
									else
									{
										currentContainer.visible = true;
										noRecordContainer.visible = false;
									}

									advancedContainer.visible = false;
									historyContainer.visible = false;
								}
							}
						}

						Container {

							layout: DockLayout {

							}

							leftMargin: 8.0
							ImageButton {
								id: advancedButton
								objectName: "advancedButton"
								leftMargin: 8.0
								defaultImageSource: "asset:///Image/bar_top_black_my-booking.png"

								minWidth: 188.0
							}

							Label {
								id: advancedLabel

								text: "Advance"
								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Center

								textStyle.color: Color.White
								textStyle.fontSize: FontSize.PointValue
								textStyle.fontSizeValue: 7.0
							}

							onTouch: {
								if (event.isDown()) {
									currentButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
									currentLabel.textStyle.color = Color.White

									advancedButton.defaultImageSource = "asset:///Image/bar_top_grey_my-booking.png"
									advancedLabel.textStyle.color = Color.Black

									historyButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
									historyLabel.textStyle.color = Color.White

									currentContainer.visible = false;

									if (advancedContainer.count() == 0)
									{
										advancedContainer.visible = false;
										noRecordContainer.visible = true;
									}
									else
									{
										advancedContainer.visible = true;
										noRecordContainer.visible = false;
									}

									historyContainer.visible = false;
								}
							}
						}

						Container {
							layout: DockLayout {

							}

							leftMargin: 8.0
							ImageButton {
								id: historyButton
								objectName: "historyButton"
								leftMargin: 8.0
								defaultImageSource: "asset:///Image/bar_top_black_my-booking.png"

								minWidth: 188.0
							}

							Label {
								id: historyLabel

								text: "History"
								horizontalAlignment: HorizontalAlignment.Center
								verticalAlignment: VerticalAlignment.Center

								textStyle.color: Color.White
								textStyle.fontSize: FontSize.PointValue
								textStyle.fontSizeValue: 7.0
							}
							onTouch: {
								if (event.isDown()) {
									currentButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
									currentLabel.textStyle.color = Color.White

									advancedButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
									advancedLabel.textStyle.color = Color.White

									historyButton.defaultImageSource = "asset:///Image/bar_top_grey_my-booking.png"
									historyLabel.textStyle.color = Color.Black


									currentContainer.visible = false;
									advancedContainer.visible = false;

									if (historyContainer.count() == 0)
									{
										historyContainer.visible = false;
										noRecordContainer.visible = true;
									}
									else
									{
										historyContainer.visible = true;
										noRecordContainer.visible = false;
									}


								}
							}
						}
					}

				} //end of tab content bar

				ScrollView {
					preferredWidth: 768.0
					scrollViewProperties {
						scrollMode: ScrollMode.Vertical
						overScrollEffectMode: OverScrollEffectMode.OnScroll
						pinchToZoomEnabled: false
					}


					Container {
						id: mainContainer

						layout: DockLayout {

						}

						Container {
							id: noRecordContainer
							objectName: "noRecordContainer"
							preferredWidth: 768

							horizontalAlignment: HorizontalAlignment.Center
							ImageView {
								horizontalAlignment: HorizontalAlignment.Center
								imageSource: "asset:///Image/no-record@2x.png"
							}

							visible: true
							topPadding: 150.0
						}

						Container {
							id: currentContainer
							objectName: "currentContainer"
							preferredWidth: 768.0
							verticalAlignment: VerticalAlignment.Top
							horizontalAlignment: HorizontalAlignment.Center

							visible: false
						}

						Container {
							id: advancedContainer
							objectName: "advancedContainer"
							preferredWidth: 768.0
							verticalAlignment: VerticalAlignment.Top
							horizontalAlignment: HorizontalAlignment.Center

							visible: false
						}

						Container {
							id: historyContainer
							objectName: "historyContainer"
							preferredWidth: 768.0
							verticalAlignment: VerticalAlignment.Top
							horizontalAlignment: HorizontalAlignment.Center

							visible: false
						}

					}

				}


			}

			actions: [
				ActionItem {
					title: "Refresh"
					ActionBar.placement: ActionBarPlacement.OnBar

					imageSource: "asset:///Image/icon_refresh_main.png"

					onTriggered: {
						initializeMyBookingPage();
					}
				}
			]

			onCreationCompleted: {
				eventHandler.selectCurrentTab.connect(selectCurrentButton);
			}
		}

		imageSource: "asset:///Image/icon_mybooking.png"

	} // end of second Tab

	Tab {
		id: profileTab
		title: "Profile"

		content: NavigationPane {
			id: profileNavigationPane
			objectName: "profileNavigationPane"
			Page {
				Container { //profile main container
					id: profileContainer
					preferredWidth: 768.0
					preferredHeight: 1280.0

					layout: StackLayout {

					}

					background: Color.create("#f5f5f5")
					animations: [
						FadeTransition {
							id: showProfilePageEffect
							duration: 500
							toOpacity: 1.0
							fromOpacity: 0.0
						}
					]

					//Top Contents
					Container {
						id: topProfileBar

						preferredHeight: 112.0

						horizontalAlignment: HorizontalAlignment.Center

						layout: DockLayout {

						}

						minHeight: 112.0

						ImageView {
							id: topBarBackground
							imageSource: "asset:///Image/topbar_gradient.png"
							scalingMethod: ScalingMethod.Fill
							preferredHeight: 112.0
							preferredWidth: 768.0
						}
						Label {
							text: "Profile"
							textStyle.fontWeight: FontWeight.Bold
							textStyle.color: Color.White
							textStyle.textAlign: TextAlign.Center
							verticalAlignment: VerticalAlignment.Center
							horizontalAlignment: HorizontalAlignment.Center
							textStyle.fontSize: FontSize.PointValue
							textStyle.fontSizeValue: 9.0
						}

					} //end of topbar

					//Old Profile
					Container {
						id: oldProfile
						objectName: "oldProfile"
						topMargin: 40.0

						horizontalAlignment: HorizontalAlignment.Center

						//name field
						ImageTextBox {
							id: nameTextField
							objectName: "nameTextField"

							backgroundSource: "asset:///Image/bar_insert_top_profile.png"
							boxWidth: 724
							boxHeight: 112

							paddingLeft: 40
							paddingRight: 40
							paddingTop: 21
							paddingBottom: 15

							labelWidth: 112
							labelText: "Name"

							textInputMode: TextFieldInputMode.Default
							textHintText: ""
							textEnabled: true
							textValue: ""
						}

						//email field
						ImageTextBox {
							id: emailTextField
							objectName: "emailTextField"

							backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
							boxWidth: 724
							boxHeight: 106

							paddingLeft: 40
							paddingRight: 40
							paddingTop: 15
							paddingBottom: 15

							labelWidth: 112
							labelText: "Email"

							textInputMode: TextFieldInputMode.EmailAddress
							textHintText: ""
							textEnabled: true
							textValue: ""
						}

						//phone Field
						ImageTextBox {
							id: phoneTextField
							objectName: "phoneTextField"

							backgroundSource: "asset:///Image/bar_insert_btm_profile.png"
							boxWidth: 724
							boxHeight: 112

							paddingLeft: 40
							paddingRight: 40
							paddingTop: 15
							paddingBottom: 21

							labelWidth: 112
							labelText: "Phone"

							textInputMode: TextFieldInputMode.Default
							textHintText: ""
							textEnabled: false
							textValue: ""

							onTouch: {
								if (event.isUp()) {
									showRegisterWithData();
								}

							}

						}
					} //end of old profile

					//New Password Setting
					Container {
						id: newPasswordContainer
						horizontalAlignment: HorizontalAlignment.Center
						topMargin: 26.0

						//Change Password Label
						ImageTextBox {
							backgroundSource: "asset:///Image/bar_insert_top_grey_profile.png"
							boxWidth: 724
							boxHeight: 108

							paddingLeft: 40
							paddingRight: 20
							paddingTop: 15
							paddingBottom: 15

							labelWidth: 348
							labelText: "Change Password:"

							textInputMode: TextFieldInputMode.Default
							textHintText: ""
							textEnabled: false
							textValue: ""
						}

						//Current Password Field
						ImageTextBox {
							id: currentPasswordTextField
							objectName: "currentPasswordField"
							bNewProfile: true
							backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
							boxWidth: 724
							boxHeight: 108

							paddingLeft: 40
							paddingRight: 0
							paddingTop: 15
							paddingBottom: 15

							labelWidth: 316
							labelText: "Current Password"

							textInputMode: TextFieldInputMode.Password
							textHintText: "must be 6 digits"
							textEnabled: true
							textValue: ""
						}

						//New Password Field
						ImageTextBox {
							id: newPasswordTextField
							objectName: "newPasswordField"
							bNewProfile: true
							backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
							boxWidth: 724
							boxHeight: 108

							paddingLeft: 40
							paddingRight: 0
							paddingTop: 15
							paddingBottom: 15

							labelWidth: 316
							labelText: "New Password"

							textInputMode: TextFieldInputMode.Password
							textHintText: "must be 6 digits"
							textEnabled: true
							textValue: ""
						}

						//Retype Password Field
						ImageTextBox {
							id: retypeTextField
							objectName: "retypePasswordField"
							bNewProfile: true
							backgroundSource: "asset:///Image/bar_insert_btm_profile.png"
							boxWidth: 724
							boxHeight: 108

							paddingLeft: 40
							paddingRight: 0
							paddingTop: 15
							paddingBottom: 15

							labelWidth: 316
							labelText: "Retype Password"

							textInputMode: TextFieldInputMode.Password
							textHintText: "must be 6 digits"
							textEnabled: true
							textValue: ""
						}
					}

					ImageButton {
						id: saveButton

						defaultImageSource: "asset:///Image/button_greenbar_small_save.png"
						pressedImageSource: "asset:///Image/button_greenbar_small_save.png"
						preferredWidth: 298.0
						preferredHeight: 92.0
						horizontalAlignment: HorizontalAlignment.Center
						topMargin: 30.0

						onClicked: {
							updateProfile();
						}
					}

				} //end profile main container

				attachedObjects: [
					// Create the ComponentDefinition that represents the custom
					// component in myPage.qml
					ComponentDefinition {
						id: registerPageDefinition
						source: "register.qml"
					}
				]

			}
			onPopTransitionEnded: {
				page.destroy();
			}
		}

		imageSource: "asset:///Image/icon_profile.png"


	} // end of second Tab

	Tab {
		id: contactTab
		title: "Contact"


		content: Page {
			Container {
				id: contactContainer
				preferredWidth: 768.0
				preferredHeight: 1280.0

				background: Color.create("#f5f5f5")
				layout: StackLayout {

				}

				animations: [
					FadeTransition {
						id: showContactPageEffect
						duration: 500
						toOpacity: 1.0
						fromOpacity: 0.0
					}
				]

				//Main Contents
				Container {
					id: topContactBar

					preferredHeight: 112.0

					horizontalAlignment: HorizontalAlignment.Center

					layout: DockLayout {

					}

					ImageView {
						id: topContactBarBackground
						imageSource: "asset:///Image/topbar_gradient.png"
						scalingMethod: ScalingMethod.Fill
						preferredHeight: 112.0
						preferredWidth: 768.0
					}
					Label {
						text: "Contact"
						textStyle.fontWeight: FontWeight.Bold
						textStyle.color: Color.White
						textStyle.textAlign: TextAlign.Center
						textStyle.fontFamily: "Consolas"

						verticalAlignment: VerticalAlignment.Center
						horizontalAlignment: HorizontalAlignment.Center
						textStyle.fontSize: FontSize.PointValue
						textStyle.fontSizeValue: 9.0
					}

				} //end of topbar

				Container {
					topMargin: 48.0

					horizontalAlignment: HorizontalAlignment.Center

					ImageView {
						minWidth: 314.0
						minHeight: 96.0
						maxWidth: 314.0
						maxHeight: 96.0
						imageSource: "asset:///Image/icon_logo2.png"
						horizontalAlignment: HorizontalAlignment.Center

					} //end of logo of Unicablink
				} //end of logo mark

				Container {
					topMargin: 54.0
					horizontalAlignment: HorizontalAlignment.Right

					layout: DockLayout {

					}

					ImageButton {
						preferredWidth: 660.0
						preferredHeight: 314.0
						defaultImageSource: "asset:///Image/btn_contact1.png"
						pressedImageSource: "asset:///Image/btn_contact1.png"

						onClicked: {
							eventHandler.VoiceCall();
						}
					}

					Container {
						layout: StackLayout {
							orientation: LayoutOrientation.TopToBottom
						}

						verticalAlignment: VerticalAlignment.Center
						horizontalAlignment: HorizontalAlignment.Right

						rightPadding: 126.0

						Label {
							id: callusLabel

							text: "Call Us"
							textStyle.fontWeight: FontWeight.Bold
							textStyle.color: Color.Black
							textStyle.textAlign: TextAlign.Center
							textStyle.fontSize: FontSize.PointValue
							horizontalAlignment: HorizontalAlignment.Center
							textStyle.fontSizeValue: 9.0
						}

						Label {
							text: "1300-800-222"
							textStyle.fontWeight: FontWeight.Normal
							textStyle.color: Color.Black
							textStyle.textAlign: TextAlign.Center

							horizontalAlignment: HorizontalAlignment.Center
							textStyle.fontSize: FontSize.PointValue
							textStyle.fontSizeValue: 6.0
						}
					}

					onTouch: {
						if (event.isUp()) {
							eventHandler.VoiceCall();
						}
					}
				} //end of Call us

				Container {
					topMargin: 70.0
					horizontalAlignment: HorizontalAlignment.Left

					layout: DockLayout {

					}

					ImageButton {
						preferredWidth: 668.0
						preferredHeight: 314.0
						defaultImageSource: "asset:///Image/btn_contact2.png"
						pressedImageSource: "asset:///Image/btn_contact2.png"

						onClicked: {
							eventHandler.SendEmail();
						}
					}

					Container {
						layout: StackLayout {
							orientation: LayoutOrientation.TopToBottom
						}

						verticalAlignment: VerticalAlignment.Center

						leftPadding: 106.0

						horizontalAlignment: HorizontalAlignment.Left
						Label {
							id: emailusLabel

							text: "Email Us"
							textStyle.fontWeight: FontWeight.Bold
							textStyle.color: Color.Black
							textStyle.textAlign: TextAlign.Center
							horizontalAlignment: HorizontalAlignment.Center
							textStyle.fontSize: FontSize.PointValue
							textStyle.fontSizeValue: 9.0
						}

						Label {
							text: "customerservices\n@unicablink.com"
							textStyle.fontWeight: FontWeight.Normal
							textStyle.color: Color.Black
							textStyle.textAlign: TextAlign.Center
							horizontalAlignment: HorizontalAlignment.Center
							textStyle.fontSize: FontSize.PointValue
							multiline: true
							textStyle.fontSizeValue: 6.0
						}
					}
					onTouch: {
						if (event.isUp()) {
							eventHandler.SendEmail();
						}
					}
				} //end of Email us
			}
		}
		imageSource: "asset:///Image/icon_contact.png"
	} // end of fourth tab

	onActiveTabChanged: {
		if (activeTab == mainbookingTab) {
			if (mainbookingTab.bInitMainBooking == true) {
				initializeMainBookingPage();
			}
		} else if (activeTab == mybookingTab) {
			initializeMyBookingPage();
		} else if (activeTab == profileTab) {
			initializeProfilePage();
		} else if (activeTab == contactTab) {
		}
		mainbookingTab.bInitMainBooking = true;
	}

	attachedObjects: [
		ComponentDefinition {
			id: pin
			source: "pin.qml"
		},
		Dialog {
			id: confirmDialog
			objectName: "confirmDialog"

			Container {
				preferredWidth: 768
				preferredHeight: 1280

				layout: AbsoluteLayout {

				}
				horizontalAlignment: HorizontalAlignment.Center
				verticalAlignment: VerticalAlignment.Top

				background: dialogBackground.imagePaint

				ImageButton {
					id: hereButton

					layoutProperties: AbsoluteLayoutProperties {
						positionX: 192
						positionY: 744
					}
					defaultImageSource: "asset:///Image/icon_here.png"
					pressedImageSource: "asset:///Image/icon_here.png"

					onClicked: {
						eventHandler.showHomepage();
					}

				}

				Button {
					id: okButton

					text: "OK"

					onClicked: {
						confirmDialog.close();
						eventHandler.showOrderDetails();
					}

					layoutProperties: AbsoluteLayoutProperties {
						positionX: 42
						positionY: 798
					}
					preferredWidth: 688.0
				}
			}
		},

		ImagePaintDefinition {
			id: dialogBackground
			imageSource: "asset:///Image/bg_booking_dialog_z10.png"
		}
	]
	function initializeProfilePage() {
		eventHandler.initializeProfilePage();
	}

	function updateProfile() {
		eventHandler.updateProfile(nameTextField.textValue, emailTextField.textValue, phoneTextField.textValue, currentPasswordTextField.textValue, newPasswordTextField.textValue, retypeTextField.textValue);
	}

	function initializeMyBookingPage() {
		eventHandler.initializeMyBookingPage();
	}

	function changeTime() {
		eventHandler.changeTime();
	}

	function setCorrectTime() {
		eventHandler.setCorrectTime(timePicker.value);
	}

	function setPickupFlag() {
		eventHandler.setPickupFlag(pickupInfoLabel.strPickupLocation);
		findLocation();
	}

	function setDropOffFlag() {
		eventHandler.setDropOffFlag(dropoffInfoLabel.strDropOffLocation);
		findLocation();
	}

	function bookNow() {
		eventHandler.bookNow(pickupInfoLabel.strPickupLocation, pickupAtInfoText.text, dropoffInfoLabel.strDropOffLocation, timeLabel.text, budgetLabel.text);
	}

	function showRegisterWithData() {
		eventHandler.showRegisterWithData();
	}

	function findLocation() {
		eventHandler.findLocation(addressField.text);
	}

	function applyLocation() {
		eventHandler.applyLocation(addressField.text, mapViewContainer.dLat, mapViewContainer.dLon);
		candListScrollView.visible = false
	}

	function getAddress() {
		eventHandler.getAddress(mapview.latitude, mapview.longitude);
	}

	function showMainPlaces() {
		mainbookingTab.nType = 1
		eventHandler.showMainPlaces();
	}

	function initializeMainBookingPage() {
		eventHandler.initializeMainBookingPage();
		resetMainBookingSetting();
		positionSource.update();
	}

	function resetMainBookingSetting() {
		mainbookingTab.nType = 1;
		pickupInfoLabel.bSetPickup = false;
		pickupInfoLabel.strPickupLocation = "";
		pickupAtInfoText.text = "";
		dropoffInfoLabel.bSetDropOff = false;
		dropoffInfoLabel.strDropOffLocation = "";
		timePicker.bByCode = true;
		timePicker.value = new Date();
		timeLabel.text = "Now";
		budgetLabel.text = "Budget";
	}

	function selectCurrentButton()	{
		currentButton.defaultImageSource = "asset:///Image/bar_top_grey_my-booking.png"
		currentLabel.textStyle.color = Color.Black

		advancedButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
		advancedLabel.textStyle.color = Color.White

		historyButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
		historyLabel.textStyle.color = Color.White

		if (currentContainer.count() == 0) {
			currentContainer.visible = false;
			noRecordContainer.visible = true;
		} else {
			currentContainer.visible = true;
			noRecordContainer.visible = false;
		}

		advancedContainer.visible = false;
		historyContainer.visible = false;
	}
} // end of TabbedPane