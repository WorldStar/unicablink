package view.components;

import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * This button is used when the user need button with background
 * And button with caption
 * @author Passion
 *
 */

public class ImageButtonField extends Field{
	/**
	 * Members
	 */
	private Bitmap mBmpBg;
	private String strCaption;
	private String strBGPath;
	private String strDisableBGPath;

	private int nCaptionFontSize = 0;
	private int nWidth = 0;
	private int nHeight = 0;
	private int nHPadding = 0;
	private int nVPadding = 0;
	private int nId = 0;
	private float fScale = Utils.getVRatio();
	private boolean bEnabled = true;

	/**
	 * Create Image Button Object
	 * @param strBGFile: Background Image File Path
	 * @param strLabel: Text of Caption (Maybe it is empty string, only Image)
	 * @param nFontSize: Font Size of Caption
	 */
	public ImageButtonField(String strBGFile, String strLabel, int nFontSize, String strDisableBGFile)
	{
		super(Field.FOCUSABLE | Field.FIELD_HCENTER);

		//initialize
		strCaption = strLabel;
		nCaptionFontSize = nFontSize;
		strBGPath = strBGFile;
		strDisableBGPath = strDisableBGFile;

		//set background
		mBmpBg = Utils.getBitmapResource(strBGFile);
		setBackground(BackgroundFactory.createBitmapBackground(mBmpBg));

		//initialize
		nWidth = mBmpBg.getWidth();
		nHeight = mBmpBg.getHeight();

		nVPadding = (int)(nHeight - nFontSize * fScale) / 2;
		nHPadding = (int)(15 * fScale);
	}

	/**
	 * These functions are created by passion
	 * 
	 * User-Defined Functions
	 */
	public String getCaption()
	{
		return strCaption;
	}

	public void setCaption(String strNewCaption)
	{
		strCaption = strNewCaption;
	}

	public void setButtonId(int id)
	{
		nId = id;
	}

	public int getButtonId()
	{
		return nId;
	}

	public void clickButton()
	{
		//Dialog.alert("Clicked!");
		fieldChangeNotify(0);

		return;
	}

	public boolean getActionEnabled()
	{
		return bEnabled;
	}

	public void setActionEnabled(boolean bActionEnabled)
	{
		bEnabled = bActionEnabled;

		if (bEnabled)
		{
			mBmpBg = Utils.getBitmapResource(strBGPath);

		}
		else 
		{
			mBmpBg = Utils.getBitmapResource(strDisableBGPath);
		}

		setBackground(BackgroundFactory.createBitmapBackground(mBmpBg));
		invalidate();
	}
	/**
	 * overloaded functions
	 */
	protected void layout(int width, int height) {
		setExtent(Math.min(width, nWidth),
				Math.min(height, nHeight));
	}

	public int getPreferredHeight() {
		return nHeight;
	}

	public int getPreferredWidth() {
		return nWidth;
	}

	public void setBtnBackground(String strBGFile)
	{
		//set background
		mBmpBg = Utils.getBitmapResource(strBGFile);
		setBackground(BackgroundFactory.createBitmapBackground(mBmpBg));
		invalidate();
	}
	
	protected void paint(Graphics graph) {
		//Create Font
		Font fontCaption = null;
		try{
			FontFamily ff = FontFamily.forName("Times New Roman");
			fontCaption = ff.getFont(Font.BOLD, nCaptionFontSize);
		}
		catch(final ClassNotFoundException cnfe){
			UiApplication.getUiApplication().invokeLater(new Runnable()
			{
				public void run()
				{
					Dialog.alert("FontFamily.forName() threw " + cnfe.toString());
				}
			});
		}

		if (isFocus()){
			//Highlight button
			graph.setGlobalAlpha(240);
			graph.setColor(0x00FF00);
			int width = getWidth();
			int height = getHeight();
			graph.fillRoundRect(0, 0, getWidth(), getHeight(), 4, 4);
			graph.drawBitmap(0, 0, getWidth(), getHeight(), mBmpBg, 0, 0);

			graph.setFont(fontCaption);
			graph.setColor(0x000000);
			graph.drawText(strCaption, nHPadding, nVPadding);
		}
		else{
			graph.drawBitmap(0, 0, getWidth(), getHeight(), mBmpBg, 0, 0);

			graph.setFont(fontCaption);
			graph.setColor(0x000000);
			graph.drawText(strCaption, nHPadding, nVPadding);
		}

	}

	protected boolean navigationClick(int status, int time) 
	{
		if (status != 0){
			fieldChangeNotify(0);
			clickButton();
		}
		return true;
	}

	public boolean keyChar(char key, int status, int time) 
	{
		if (key == Characters.ENTER) {
			fieldChangeNotify(0);
			clickButton();
			return true;
		}
		return false;
	}
	protected boolean trackwheelClick(int status, int time)
	{        
		if (status != 0) clickButton();    
		return true;
	}

	protected boolean invokeAction(int action) 
	{
		switch( action ) {
		case ACTION_INVOKE: {
			clickButton(); 
			return true;
		}
		}
		return super.invokeAction(action);
	}    

	public boolean isFocusable() 
	{
		return true;
	}

	protected boolean touchEvent(TouchEvent message)
	{
		int x = message.getX( 1 );
		int y = message.getY( 1 );
		if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
			// Outside the field
			return false;
		}
		switch( message.getEvent() ) {
		case TouchEvent.UNCLICK:
			fieldChangeNotify(0);
			clickButton();
			return true;
		}
		return super.touchEvent( message );
	}
}
