package application.utils;

import javax.microedition.location.*;

import application.Unicablink;
import application.utils.HandleGPSNew.handleGPSListener;

import net.rim.device.api.gps.BlackBerryCriteria;
import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.gps.BlackBerryLocationProvider;
import net.rim.device.api.gps.GPSInfo;
import net.rim.device.api.ui.UiApplication;

public class HandleGPS
{
	static GPSThread gpsThread;
	public static double latitude = 0;
	public static double longitude = 0;
	public static boolean bFinished = false;

	public HandleGPS()
	{
		System.out.println("Entered! :D");
		gpsThread = new GPSThread();

		try
		{
			gpsThread.start();	
		}
		catch (IllegalThreadStateException ie)
		{
			ie.printStackTrace();
			bFinished = true;
			return;
		}
	}

	private static class GPSThread extends Thread
	{
	    static BlackBerryLocationProvider myProvider;
	    BlackBerryCriteria myCriteria;
	    static int nCount = 0;

		public void startLocationUpdate() 
		{
			/**
			 * New Method: 09/11/2013
			 */
			//Get Current Location
			System.out.println("start location update Started!");
    		
			try {
	            myCriteria = new BlackBerryCriteria();
				
	            //Set Mode
				if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_CELLSITE)){
					myCriteria.setMode(GPSInfo.GPS_MODE_CELLSITE);
				}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_ASSIST)){
					myCriteria.setMode(GPSInfo.GPS_MODE_ASSIST);
				}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_AUTONOMOUS)){
					myCriteria.setMode(GPSInfo.GPS_MODE_AUTONOMOUS);
				}else{
					myCriteria.setCostAllowed(true);
					myCriteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_LOW);
				}
	
	            try
	            {
	                myProvider = (BlackBerryLocationProvider)LocationProvider.getInstance(myCriteria);
	                myProvider.setLocationListener(new handleGPSListener(), 2, 2, 1);
	            }
	            catch (LocationException lex )
	            {
	            	System.out.println("location exception: " +lex.toString());
	            	bFinished = true;
	                return;
				} catch (Exception e) {
					System.out.println("exception: " +e.toString());
					bFinished = true;
					return;
				}
	        }
	        catch ( UnsupportedOperationException uoex )
	        {
	        	System.out.println("unsupportedoperation exception: " +uoex.toString());
	        	bFinished = true;
				return;
	        }
		}

		public void run()
		{
			System.out.println("Run!");

			/**
			 * New Method: 10/10/2013
			 */
			startLocationUpdate();
		}
		
		private void setupCriteria() {
			myCriteria = new BlackBerryCriteria();
			
            //Set Mode
			if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_CELLSITE)){
				myCriteria.setMode(GPSInfo.GPS_MODE_CELLSITE);
			}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_ASSIST)){
				myCriteria.setMode(GPSInfo.GPS_MODE_ASSIST);
			}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_AUTONOMOUS)){
				myCriteria.setMode(GPSInfo.GPS_MODE_AUTONOMOUS);
			}else{
				myCriteria.setCostAllowed(true);
				myCriteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_LOW);
			}
		}

		private void setupProvider() {
			try
            {
				try {
		            Thread.sleep(1000);
		        } catch (InterruptedException e) {
		           System.out.println(e.toString());
		           bFinished = true;
		           return;
		        }
				
                myProvider = (BlackBerryLocationProvider)LocationProvider.getInstance(myCriteria);
                myProvider.setLocationListener(new handleGPSListener(), 2, 2, 1);
            }
            catch (LocationException lex )
            {
            	System.out.println(lex.toString());
            	bFinished = true;
                return;
            	
			} catch (Exception e) {
				System.out.println(e.toString());
				bFinished = true;
				return;
			}
			
		}

		public class handleGPSListener implements LocationListener
	    {
	        public void locationUpdated(LocationProvider provider, Location location)
	        {
	        	//Get Current Location
        		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
        		
        		System.out.println("Location updated!");
	            if (location.isValid())
	            {
	                //get lat and lon
					latitude  = location.getQualifiedCoordinates().getLatitude();
					longitude = location.getQualifiedCoordinates().getLongitude();
					
					System.out.println("Location found!");
					theApp.bCheckedLocation = true;
					theApp.bNotFoundLocation = false;
					myProvider.setLocationListener(null, 0, 0, 0);
	            }
	            else
	            {
	                // invalid location
	            	nCount++;
	            	
	            	if (nCount == 3){
	            		nCount = 0;
	            		myProvider.setLocationListener(null, 0, 0, 0);
	            		
	            		System.out.println("Failed found location!");
	            		latitude  = 0;
						longitude = 0;
	            		theApp.bCheckedLocation = true;
	            		theApp.bNotFoundLocation = true;
	            	}
	            	else {
	            		setupCriteria();
			            setupProvider();
	            	}
	            	
	            }
	        }

	        public void providerStateChanged(LocationProvider provider, int newState)
	        {
	            if (newState == LocationProvider.AVAILABLE)
	            {
	                // available
	            }
	            else if (newState == LocationProvider.OUT_OF_SERVICE)
	            {
	                // GPS unavailable due to IT policy specification
	            }
	            else if (newState == LocationProvider.TEMPORARILY_UNAVAILABLE )
	            {
	                // no GPS fix
	            }
	        }
	    }
	}
}