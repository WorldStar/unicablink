import bb.cascades 1.0

Page {
    Container { //profile main container
        id: profileContainer
        preferredWidth: 720.0
        preferredHeight: 720.0

        layout: StackLayout {

        }

        background: Color.create("#f5f5f5")
        animations: [
            FadeTransition {
                id: showProfilePageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        //Top Contents
        Container {
            id: topProfileBar

            preferredHeight: 64.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            minHeight: 64.0

            ImageView {
                id: topBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 64.0
                preferredWidth: 720.0
            }
            Label {
                text: "Profile"
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 8.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
            }

        } //end of topbar

        //        //Main Container
        //        ScrollView {
        //            id: mainScrollView
        //            verticalAlignment: VerticalAlignment.Fill
        //            horizontalAlignment: HorizontalAlignment.Fill
        //
        //            scrollViewProperties {
        //                scrollMode: ScrollMode.Vertical
        //                overScrollEffectMode: OverScrollEffectMode.OnScroll
        //                pinchToZoomEnabled: false
        //            }
        //
        //        }

		ScrollContainer {
            horizontalAlignment: HorizontalAlignment.Center
            
            Container {
                //Old Profile
                Container {
                    id: oldProfile

                    topPadding: 40

                    horizontalAlignment: HorizontalAlignment.Center

                    //name field
                    ImageTextBox {
                        objectName: "nameTextField"

                        backgroundSource: "asset:///Image/bar_insert_top_profile.png"
                        boxWidth: 684
                        
                        paddingLeft: 40
                        paddingRight: 40

                        labelWidth: 152
                        labelText: "Name"

                        textInputMode: TextFieldInputMode.Default
                        textHintText: ""
                        textEnabled: false
                        textValue: "Nicole"
                    }

                    //email field
                    ImageTextBox {
                        objectName: "emailTextField"

                        backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
                        boxWidth: 684
                        
                        paddingLeft: 40
                        paddingRight: 40

                        labelWidth: 152
                        labelText: "Email"

                        textInputMode: TextFieldInputMode.EmailAddress
                        textHintText: ""
                        textEnabled: false
                        textValue: "Nicole@gmail.com"
                    }

                    //phone Field
                    ImageTextBox {
                        objectName: "nameTextField"

                        backgroundSource: "asset:///Image/bar_insert_btm_profile.png"
                        boxWidth: 684
                        
                        paddingLeft: 40
                        paddingRight: 40

                        labelWidth: 152
                        labelText: "Phone"

                        textInputMode: TextFieldInputMode.Default
                        textHintText: ""
                        textEnabled: false
                        textValue: "+601234567890"
                    }
                } //end of old profile

                //New Password Setting
                Container {
                    id: newPasswordContainer
                    horizontalAlignment: HorizontalAlignment.Center
                    topMargin: 26.0

                    //Change Password Label
                    ImageTextBox {
                        backgroundSource: "asset:///Image/bar_insert_top_grey_profile.png"
                        boxWidth: 684
                        boxHeight: 88

                        paddingLeft: 40
                        paddingRight: 40

                        labelWidth: 348
                        labelText: "Change Password:"

                        textInputMode: TextFieldInputMode.Default
                        textHintText: ""
                        textEnabled: false
                        textValue: ""
                    }

                    //Current Password Field
                    ImageTextBox {
                        backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
                        boxWidth: 684
                        
                        paddingLeft: 40
                        paddingRight: 40

                        labelWidth: 338
                        labelText: "Current Password"

                        textInputMode: TextFieldInputMode.Password
                        textHintText: ""
                        textEnabled: true
                        textValue: ""
                    }

                    //New Password Field
                    ImageTextBox {
                        backgroundSource: "asset:///Image/bar_insert_middle_profile.png"
                        boxWidth: 684

                        paddingLeft: 40
                        paddingRight: 40

                        labelWidth: 338
                        labelText: "New Password"

                        textInputMode: TextFieldInputMode.Password
                        textHintText: ""
                        textEnabled: true
                        textValue: ""
                    }

                    //Retype Password Field
                    ImageTextBox {
                        backgroundSource: "asset:///Image/bar_insert_btm_profile.png"
                        boxWidth: 684

                        paddingLeft: 40
                        paddingRight: 40

                        labelWidth: 338
                        labelText: "Retype"

                        textInputMode: TextFieldInputMode.Password
                        textHintText: ""
                        textEnabled: true
                        textValue: ""
                    }
                }

                ImageButton {
                    id: saveButton

                    defaultImageSource: "asset:///Image/button_greenbar_small_save.png"
                    preferredWidth: 208.0
                    preferredHeight: 62.0
                    horizontalAlignment: HorizontalAlignment.Center
                    topMargin: 30.0

                    onClicked: {

                    }
                }
            }

        }
    }//end profile main container
}