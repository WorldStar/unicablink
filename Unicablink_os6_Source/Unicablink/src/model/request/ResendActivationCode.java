package model.request;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.utils.DateUtils;


public class ResendActivationCode {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strPhoneNo = "";
	private String m_strEmail = "";
	
	public ResendActivationCode(){

	}

	/*
	 * Request type:
	 * 
	 * {
	 *			"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 *			"MOBILE":"+1126253547",
	 *			"EMAIL":"wang198904@gmail.com", 
	 *			"REQUESTTIME": "2013-08-15 12:05:00"			
     * }
     * 
	 */
	public void setPhoneNo(String strPhoneNo){
		m_strPhoneNo = strPhoneNo;
	}

	public void setEmail(String strEmailAddress){
		m_strEmail = strEmailAddress;
	}

	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();

		//Get current time
		String strCurrentTime = DateUtils.getCurrentTimeString();
		
		try 
		{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("MOBILE", m_strPhoneNo);
			jsonResult.put("EMAIL", m_strEmail);
			jsonResult.put("REQUESTTIME", strCurrentTime);
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonResult;
	}
}
