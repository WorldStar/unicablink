import bb.cascades 1.0

Page {

    id: registerPage
    Container {
        id: registerScreenZ10

        background: Color.create("#f5f5f5")
        layout: StackLayout {

        }

        horizontalAlignment: HorizontalAlignment.Center

        animations: [
            FadeTransition {
                id: showPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        Container {
            id: topBar

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            preferredHeight: 112.0
            ImageView {
                id: topBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 112.0
                preferredWidth: 768.0
            }
            Label {
                text: "Register"
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 9.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
            }

        }

        Container {
            id: registerInfoContainer
            topMargin: 24.0
            preferredWidth: 662.0

            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }

            horizontalAlignment: HorizontalAlignment.Center
            Container {
                id: nameInfo

                layout: DockLayout {

                }

                preferredWidth: 662.0
                maxHeight: 110.0

                ImageView {
                    imageSource: "asset:///Image/bar_insert_top_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    maxHeight: 112.0
                    minHeight: 112.0
                    preferredWidth: 662.0
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 20.0
                    rightPadding: 10.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Name"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 8.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 164.0
                        textStyle.fontSize: FontSize.PointValue
                    }

                    TextField {
                        id: nameText
                        objectName: "nameText"
                        
			            clearButtonVisible: true
                        input.submitKey: SubmitKey.Done
                        textFormat: TextFormat.Plain
                        textStyle.textAlign: TextAlign.Left
                        input.masking: TextInputMasking.Masked
                        verticalAlignment: VerticalAlignment.Center
                        inputMode: TextFieldInputMode.Default
                        backgroundVisible: false
                        textStyle.color: Color.Black
                        text: ""
                        focusHighlightEnabled: false
                        hintText: ""
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 8.0

                    }

                }
            }

            Container {
                id: emailInfo

                layout: DockLayout {

                }

                preferredWidth: 662.0
                maxHeight: 110.0

                ImageView {
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    maxHeight: 112.0
                    minHeight: 112.0
                    preferredWidth: 662.0
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 20.0
                    rightPadding: 10.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Email"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 8.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 164.0
                        textStyle.fontSize: FontSize.PointValue
                    }

                    TextField {
                        id: emailText
                        objectName: "emailText"
                        
			            clearButtonVisible: true
                        input.submitKey: SubmitKey.Done
                        textFormat: TextFormat.Plain
                        textStyle.textAlign: TextAlign.Left
                        input.masking: TextInputMasking.Masked
                        verticalAlignment: VerticalAlignment.Center
                        inputMode: TextFieldInputMode.EmailAddress
                        backgroundVisible: false
                        textStyle.color: Color.Black
                        text: ""
                        focusHighlightEnabled: false
                        hintText: ""
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 8.0
                    }
                }
            }

            Container {
                id: phoneInfo

                layout: DockLayout {

                }

                preferredWidth: 662.0
                maxHeight: 110.0

                ImageView {
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    maxHeight: 112.0
                    minHeight: 112.0
                    preferredWidth: 662.0
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 20.0
                    rightPadding: 10.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Phone"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 8.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 164.0
                        textStyle.fontSize: FontSize.PointValue
                    }

                    TextField {
                        id: phoneText

                        clearButtonVisible: true
                        input.submitKey: SubmitKey.Done
                        textFormat: TextFormat.Plain
                        textStyle.textAlign: TextAlign.Left
                        input.masking: TextInputMasking.Masked
                        verticalAlignment: VerticalAlignment.Center
                        inputMode: TextFieldInputMode.PhoneNumber
                        backgroundVisible: false
                        textStyle.color: Color.Black
                        text: ""
                        focusHighlightEnabled: false
                        hintText: "e.g +60123456789"
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 8.0
                        
                        onTextChanging: {
                            eventHandler.correctTextField(phoneText, text);
                        }
                    }
                }
            }

            Container {
                id: passwordInfo

                layout: DockLayout {

                }

                preferredWidth: 662.0
                maxHeight: 110.0

                ImageView {
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    maxHeight: 112.0
                    minHeight: 112.0
                    preferredWidth: 662.0
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 20.0
                    rightPadding: 10.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Password"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 8
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 164.0
                        textStyle.fontSize: FontSize.PointValue
                    }

                    TextField {
                        id: passwordText

                        input.submitKey: SubmitKey.Done
                        textFormat: TextFormat.Plain
                        textStyle.textAlign: TextAlign.Left
                        input.masking: TextInputMasking.Masked
                        verticalAlignment: VerticalAlignment.Center
                        inputMode: TextFieldInputMode.Password
                        backgroundVisible: false
                        textStyle.color: Color.Black
                        text: ""
                        focusHighlightEnabled: false
                        hintText: "min. 6 (alphanumeric)"
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 8.0
                    }
                }
            }
            Container {
                id: retypeInfo

                layout: DockLayout {

                }

                preferredWidth: 662.0
                maxHeight: 110.0

                ImageView {
                    imageSource: "asset:///Image/bar_insert_btm_profile.png"
                    scalingMethod: ScalingMethod.Fill
                    maxHeight: 110.0
                    minHeight: 110.0
                    preferredWidth: 662.0
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    leftPadding: 20.0
                    rightPadding: 10.0
                    topPadding: 10.0
                    bottomPadding: 10.0

                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: "Retype"
                        textStyle.fontWeight: FontWeight.Default
                        textStyle.color: Color.Black
                        textStyle.textAlign: TextAlign.Left
                        textStyle.fontFamily: "Consolas"
                        textStyle.fontSizeValue: 8.0
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 164.0
                        textStyle.fontSize: FontSize.PointValue
                    }

                    TextField {
                        id: retypeText

                        
                        input.submitKey: SubmitKey.Done
                        textFormat: TextFormat.Plain
                        textStyle.textAlign: TextAlign.Left
                        input.masking: TextInputMasking.Masked
                        verticalAlignment: VerticalAlignment.Center
                        inputMode: TextFieldInputMode.Password
                        backgroundVisible: false
                        textStyle.color: Color.Black
                        text: ""
                        focusHighlightEnabled: false
                        hintText: "min. 6 (alphanumeric)"
                        textStyle.fontSize: FontSize.PointValue
                        textStyle.fontSizeValue: 8.0
                    }
                }

            }

            TextArea {
                text: "Activation code will be send via SMS to the above phone number or email (for non Malaysian mobile number) upon signing up"
                textStyle.fontWeight: FontWeight.Default
                textStyle.color: Color.Black
                textStyle.textAlign: TextAlign.Left
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 8.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                preferredWidth: 662.0
                topMargin: 36.0
                backgroundVisible: false
                enabled: false
                opacity: 0.6
                editable: false
                textStyle.fontSize: FontSize.PointValue
                minHeight: 220.0
                input {
                    flags: TextInputFlag.SpellCheckOff
                }
            }

            //Sign Up Button
            Container {
                topMargin: 64.0

                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center

                layout: DockLayout {

                }

                ImageButton {
                    id: signUpButton
                    defaultImageSource: "asset:///Image/button_greenbar.png"
                    pressedImageSource: "asset:///Image/button_greenbar.png"

                    horizontalAlignment: HorizontalAlignment.Center

                    preferredWidth: 600.0
                    preferredHeight: 90.0
                    
                }

                Label {
                    id: signUpLabel
                    textStyle.color: Color.Black
                    text: "Sign Up"

                    textStyle.textAlign: TextAlign.Center
                    textStyle.fontFamily: "Consolas"
                    textStyle.fontSizeValue: 8.0
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    textStyle.fontSize: FontSize.PointValue

                }

                onTouch: {
                    if (event.isDown()){
                        requestSignUp();
                    } 
                }
                

                attachedObjects: [
                    // Create the ComponentDefinition that represents the custom
                    // component in myPage.qml
                    ComponentDefinition {
                        id: pageDefinition
                        source: "activation.qml"
                    }
                ]
            }
        }
    }

    function requestSignUp() {
        //Register
        eventHandler.RegisterUser(nameText.text, emailText.text, phoneText.text, passwordText.text, retypeText.text);
    }
}


