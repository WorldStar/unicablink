package view.screens;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.components.AccountInfoPasswordField;
import view.components.AccountInfoTextField;
import view.components.ImageButtonField;
import view.fieldmanager.InfoManager;
import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.Login;
import model.request.Registeration;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import application.setting.AppSetting;
import application.setting.SettingStore;
import application.utils.StringUtils;

public class LoginScreen extends BaseScreen {

	/**
	 * private members
	 */
	AccountInfoTextField phoneInfo;
	AccountInfoPasswordField passwordInfo;

	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;

	public LoginScreen() {
		super();

		/**
		 * Create Login information  manager
		 */
		InfoManager loginManger = new InfoManager(ResouceConstant.LOGIN_PADDING_VERTICAL, 
				ResouceConstant.LOGIN_TOP_PADDING, 0, 0);

		//create logo image
		Bitmap bmpTopLogo = Utils.getBitmapResource(ResouceConstant.UNICAB_LOGO);

		//Get saved phone no
		String strPhoneNo = mAppSettingStore.getAppSetting().getAccountPhone();

		//create phone info
		phoneInfo = new AccountInfoTextField(ResouceConstant.LOGIN_BG_TEXT, null, 0, 0, 15, 15, 
				"Phone", "e.g.+60123456789", strPhoneNo, 100, EditField.FILTER_PHONE, 15, 22, false);

		int nTemp = phoneInfo.getPreferredHeight();
		nTemp = phoneInfo.getPreferredWidth();

		//Get Saved Password Info
		String strPassword = mAppSettingStore.getAppSetting().getAccountPassword();

		//create phone info
		passwordInfo = new AccountInfoPasswordField(ResouceConstant.LOGIN_BG_TEXT, null, 0, 0, 15, 15, 
				"Password", "min. 6 (alphanumeric)", strPassword, 100, 0, 6, 22, false);

		nTemp = passwordInfo.getPreferredHeight();
		nTemp = passwordInfo.getPreferredWidth();

		//create Sign in Button
		ImageButtonField signInButton = new ImageButtonField(ResouceConstant.LOGIN_BTN_SIGNIN, "", 0, "")
		{
			public void clickButton() {
				onClickSignIn();
			}
		};

		nTemp = signInButton.getPreferredHeight();
		nTemp = signInButton.getPreferredWidth();

		//add fields
		loginManger.add(new BitmapField(bmpTopLogo, Field.FIELD_HCENTER));
		loginManger.add(phoneInfo);
		loginManger.add(passwordInfo);
		loginManger.add(signInButton);

		add(loginManger);


		/**
		 * Create Register and Forgot Information manager
		 */
		InfoManager registerManager = new InfoManager(ResouceConstant.LOGIN_PADDING_VERTICAL, 
				ResouceConstant.LOGIN_VERTICAL_GUTTER, 0, 0);

		/**
		 * Signup info manager
		 */
		HorizontalFieldManager signUpInfoManager = new HorizontalFieldManager();

		//Create Label
		LabelField labelForgot = new LabelField(ResouceConstant.LOGIN_STR_FORGOT);
		//set fonts
		labelForgot.setFont(Utils.getFont(Font.BOLD, ResouceConstant.LOGIN_FORGOT_LABEL_HEIGHT));

		//Create Sign Up Button
		ImageButtonField signUpButton = new ImageButtonField(ResouceConstant.LOGIN_BTN_SIGNUP, "", 0, "")
		{
			public void clickButton() {
				onClickSignUp();
			}
		};

		//add fields to Signup info manager
		signUpInfoManager.add(labelForgot);
		signUpInfoManager.add(signUpButton);

		/**
		 * Create Forgot Password
		 */
		ImageButtonField forgotButton = new ImageButtonField(ResouceConstant.LOGIN_BTN_FORGOT, "", 0, "")
		{
			public void clickButton() {
				onClickForgot();
			}
		};

		//Add register infor
		registerManager.add(signUpInfoManager);
		registerManager.add(forgotButton);

		add(registerManager);
	}


	/**
	 * User - Interaction Processing
	 */
	public void onClickSignIn()
	{
		String strPhoneNo = phoneInfo.getText();
		String strPassword = passwordInfo.getText();

		if ((strPhoneNo.length() == 0) || (strPassword.length() == 0)) {
			Dialog.alert("Please fill all fields.");
			return;
		}		

		if (!StringUtils.isPhoneNoFormat(strPhoneNo))
		{
			return;
		}

		if(strPassword.length() != 6)
		{
			Dialog.alert("Password must be 6 digits.");
			return;
		}


		//Create JSON Request
		Login jsonLogin = new Login();

		jsonLogin.setPassword(strPassword);
		jsonLogin.setPhoneNo(strPhoneNo);
		jsonLogin.setVerificationCode("");

		/**
		 * Get Unicablink App and save members temporally
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		//Get the response
		JSONObject jsonResult = null;

		String strJSONLogin = jsonLogin.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONLogin, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_LOG_IN);

		if (jsonResult == null)
		{
			return;
		}

		/*
		 * Response type:
		 * 
		 * {
		 * CustomerID:"6364C141-9C27-ED59-1F15A0A7BB8B8FA2",
		 * Email:"yuan@obama.com",
		 * Name:"Micheal",
		 * ResponseTime:"2013-08-12 10:52:43",
		 * SecurityKey:"e92684cb325f1e4d4cce27c0207562bf",
		 * Status:4
		 * }
		 *
		 */

		int nStatus;

		try 
		{
			nStatus = jsonResult.getInt("Status");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("Failed recognition of response.");
			return;
		}

		switch (nStatus) 
		{
		case 1:
		{
			try 
			{
				String strCustomerID = jsonResult.getString("CustomerID");
				String strEmail = jsonResult.getString("Email");
				String strName = jsonResult.getString("Name");
				
				AppSetting appSetting = mAppSettingStore.getAppSetting();

				appSetting.setCustomerID(strCustomerID);
				appSetting.setName(strName);
				appSetting.setEmailAddress(strEmail);
				appSetting.setPhoneNo(strPhoneNo);
				appSetting.setPassword(strPassword);
				appSetting.setLogged(true);
				appSetting.setVerified(false);

				mAppSettingStore.saveAppSetting(appSetting);
				
				
				/**
				 * Pop log in and register screens and show activation screen.
				 */
				theApp.popLoginScreen();
				theApp.goBookingScreen();
			}
			catch (JSONException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				Dialog.alert("Failed to get response.");
			}
			break;
		}

		case 2:
		{
			Dialog.alert("Invalid User ID / Password.");
			break;
		}

		case 3:
		{
			Dialog.alert("Account Inactive.");
			break;
		}

		case 4:
		{
			Dialog.alert("You are not verified.");
			break;
		}
		default:
			break;
		}
	}

	public void onClickSignUp()
	{
		((Unicablink)UiApplication.getUiApplication()).goRegisterScreen();
	}

	public void onClickForgot()
	{
		((Unicablink)UiApplication.getUiApplication()).goForgotScreen();
	}

}




