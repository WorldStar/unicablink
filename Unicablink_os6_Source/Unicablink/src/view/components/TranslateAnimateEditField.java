package view.components;

import java.util.Timer;
import java.util.TimerTask;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.LabelField;

/**
 * EditField in which text is translate animated automatically.
 * 
 * @author Ko EntryPass Sdn Bhd.
 * @created 12/01/2014
 */

public class TranslateAnimateEditField extends LabelField
{
	private String _text = "";

	private int _fontSize = 24;
	private int _paddingSpace = 6;
	
	private Timer animatorTimer;
	private AnimateTask animateTask;
	private boolean runable;
	private int fromX;
	private int toX;
	private int currentX;
	private int intervalT;
	private int intervalX;
	private Font fontTextField;

	public TranslateAnimateEditField(String text) {
		super(text);

		if (text.length() > 0) {
			_text = text;
		}

		initialize();
	}
	
	public TranslateAnimateEditField(String text, long style)
	{
		super(text, style);

		if (text.length() > 0) {
			_text = text;
		}

		initialize();
	}

	private void initialize() {
		fontTextField = Font.getDefault().derive(Font.PLAIN, _fontSize, Ui.UNITS_px);
		setFont(fontTextField);
		initTranslateAnimator();
	}

	private void initTranslateAnimator()
	{
		fromX = 300;
		toX = -(fontTextField.getAdvance(_text, 0, _text.length()-10));
		currentX = 0;
		if(toX > fromX)
		{
			intervalX = 3; //point
			this.intervalT = 100; //millisecond
		}
		else if(toX < fromX)
		{
			intervalX = -3; //point
			this.intervalT = 100; //millisecond
		}
		else
		{
			intervalX = 0;
			this.intervalT = 0; //millisecond
		}

		System.out.println("Initialize Status Info text: from "+fromX+", to "+toX+", intervalX "+intervalX);
	}

	private void startTranslateAnimator()
	{
		if(this.intervalX != 0){
			animatorTimer = new Timer();
			animateTask = new AnimateTask();

			System.out.println("Status info start animatorTimer");

			runable = true;
			animatorTimer.schedule(animateTask, this.intervalT, this.intervalT);
		}
	}
	
	private void stopTranslateAnimator()
	{
		if(animatorTimer != null && animateTask != null){
			runable = false;
			animatorTimer.cancel();
			try{
				this.wait((long)300);
			}catch(Exception ex){
				
			}

			animatorTimer = null;
			animateTask = null;
			System.out.println("Status info stop animatorTimer");
		}
	}

	protected void paint(Graphics g) {
//		g.drawText(_text, this.currentX, 0, (int) DrawStyle.LEFT, getWidth() - _paddingSpace);
//		System.out.println("Status info height = "+getHeight()+" font size == "+_fontSize);
		g.drawText(_text, this.currentX, (getHeight()-_fontSize)/2, DrawStyle.LEFT, (_text.length() * _fontSize / 3 + 50));
	}

	protected void onDisplay()
	{
		System.out.println("Status info onDisplay");
		startTranslateAnimator();
	}
	
	protected void onUndisplay()
	{
		System.out.println("Status info onUndisplay");
		stopTranslateAnimator();
	}
	
	public void setFontSize(int fontSize) {
		stopTranslateAnimator();
		_fontSize = fontSize;
		fontTextField = Font.getDefault().derive(Font.PLAIN, _fontSize, Ui.UNITS_px);
		initTranslateAnimator();
		startTranslateAnimator();
	}

	public int getFontSize() {
		return _fontSize;
	}
	
	public String getText(){
		return _text;
	}
	
	public void setText(String text) {
		stopTranslateAnimator();
		_text = text;
//		super.setText(text);
		initTranslateAnimator();
		startTranslateAnimator();
	}
	
	public void TranslateText(){
		this.invalidate();

//		System.out.println("Status info draw text at " + this.currentX);
		this.currentX += this.intervalX;
		if(this.intervalX > 0 && this.currentX > this.toX){
			this.currentX = this.fromX;
		}else if(this.intervalX < 0 && this.currentX < this.toX){
			this.currentX = this.fromX;
		}
	}

	private class AnimateTask extends TimerTask
	{
		public void run() {
			if(runable == true)
				TranslateText();
			else
				cancel();
		}
	}
}
