// Default empty project template
import bb.cascades 1.0
import my.library 1.0

// creates one page with a label
Page {
    Container {
        id: splashScreen
        layout: DockLayout {}
        
        background: Color.Black
        
        horizontalAlignment: HorizontalAlignment.Center

        verticalAlignment: VerticalAlignment.Center
        ImageView {
            id: splashImageView
            layoutProperties: StackLayoutProperties {

            }
            imageSource: "asset:///Image/login.png"
            
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            
            attachedObjects: [
                QTimer {
                    id: goLoginTimer
                    interval: 2000
                    onTimeout: {
                        //display login screen after 2 seconds
                        goLoginTimer.stop();
                        requestDisplayLoginPage();
                        
                    }
                
                    //display Login Screen after showing Splash
                    function requestDisplayLoginPage() {
                        eventHandler.ShowLogIn();
                    }

                }
            ]

            onCreationCompleted: {
                goLoginTimer.start();
            }
        
            
        }
        

    }
    

}

