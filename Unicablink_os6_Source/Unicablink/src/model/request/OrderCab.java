package model.request;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.Utils;

public class OrderCab {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strCustomerID = "";
	private String m_strCName = "";
	private String m_strCEmail = "";
	private String m_strSPhoneNo = "";
	private String m_strDateOrder = "";
	private String m_strPickup = "";
	private double m_dPlat = 0;
	private double m_dPlon = 0;
	private String m_strDropOff = "";
	private double m_dDlat = 0;
	private double m_dDlon = 0;
	private String m_strCarType = "";
	private String m_strDatePickup = "";
	private String m_strRemark = "";
	private String m_strTip = "";
	private String m_strDeviceID = Utils.getIMEI();
	
	public OrderCab(){
		
	}
	
	/*
	 * {
			"DEVICEID":"83e462f770cc34eb", 
			"PLAT":"3.1813032",
			"TIP":"",
			"NAME":"passion",
			"PICKUPTIME":"2013-08-21 19:38:08",
			"PLON":"101.5488498",
			"REMARKS":"Test",
			"DLON":"101.5776766",
			"CABTYPE":"1",
			"DROPOFF":"Au 2, Kuala Lumpur",
			"DLAT":"3.2965233",
			"ORDERTIME":"2013-08-22 19:35",
			"CUSTOMERID":"356C43C2-9E8A-02D9-EB40A95BA5A6DFF3",
			"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
			"MOBILE":"+601126253547",
			"EMAIL":"wang198904@gmail.com",
			"PICKUP":"Bandar Damai Perdana, Kuala Lumpur"
		}
	 */
	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();
		
		try{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("CUSTOMERID", m_strCustomerID);
			jsonResult.put("NAME", m_strCName);
			jsonResult.put("EMAIL", m_strCEmail);
			jsonResult.put("ORDERTIME", m_strDateOrder);
			jsonResult.put("PICKUP", m_strPickup);
			jsonResult.put("PLAT", m_dPlat);
			jsonResult.put("PLON", m_dPlon);
			jsonResult.put("DROPOFF", m_strDropOff);
			jsonResult.put("DLAT", m_dDlat);
			jsonResult.put("DLON", m_dDlon);
			jsonResult.put("CABTYPE", m_strCarType);
			jsonResult.put("PICKUPTIME", m_strDatePickup);
			jsonResult.put("REMARKS", m_strRemark);
			jsonResult.put("MOBILE", m_strSPhoneNo);
			jsonResult.put("TIP", "");
			jsonResult.put("DEVICEID", m_strDeviceID);
		}
		catch(JSONException e){
			e.printStackTrace();
			return null;
		}
		
		return jsonResult;
	}
	
	public void setCustomerID(String strCustomerID){
		m_strCustomerID = strCustomerID;
	}
	
	public void setCName(String strCName){
		m_strCName = strCName;
	}
	
	public void setCEmail(String strCEmail){
		m_strCEmail = strCEmail;
	}
	
	public void setSPhoneNo(String strPhoneNo){
		m_strSPhoneNo = strPhoneNo;
	}
	
	public void setOrderDate(String strDateOrder){
		m_strDateOrder = strDateOrder;
	}
	
	public void setPickupLocation(String strPickup){
		m_strPickup = strPickup;
	}
	
	public void setPlat(double dPlat){
		m_dPlat = dPlat;
	}
	
	public void setPlon(double dPlon){
		m_dPlon = dPlon;
	}
	
	public void setDropOff(String strDropOff){
		m_strDropOff = strDropOff;
	}
	
	public void setDlat(double dDlat){
		m_dDlat = dDlat;
	}
	
	public void setDlon(double dDlon){
		m_dDlon = dDlon;
	}
	
	public void setCarType(String strCabType){
		m_strCarType = strCabType;
	}
	
	public void setDatePickup(String strDatePickup){
		m_strDatePickup = strDatePickup;
	}
	
	public void setRemark(String strRemark){
		m_strRemark = strRemark;
	}
	
	

}
