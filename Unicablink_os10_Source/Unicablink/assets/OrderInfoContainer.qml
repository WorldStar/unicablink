import bb.cascades 1.0

Container {
    id: viewContainer

    layout: DockLayout {

    }

    topPadding: 50.0

    preferredWidth: 768.0
    Container { //Current Info Container
        id: currentInfoContainer

        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Top

        Container {
            id: summaryInfoContainer

            preferredWidth: 686.0
            preferredHeight: 98.0

            background: infoBackground.imagePaint

            attachedObjects: [
                ImagePaintDefinition {
                    id: infoBackground
                    imageSource: "asset:///Image/bar_insert_top_grey_my-booking.png"
                }
            ]
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight

            }

            horizontalAlignment: HorizontalAlignment.Center

            leftPadding: 30.0
            rightPadding: 30.0

            Label {
                id: numInfoLabel
                objectName: "numInfoLabel"

                text: propertyMap.orderId
                
                textStyle.color: Color.Black
                textStyle.fontSize: FontSize.PointValue
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Left
                maxWidth: 140
                minWidth: 140
                textStyle.fontSizeValue: 7.0
            }

            Label {
                id: typeInfoLabel
                objectName: "typeInfoLabel"

                text: propertyMap.orderSummary
                
                textStyle.color: Color.Black
                textStyle.fontSize: FontSize.PointValue
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                minWidth: 400
                maxWidth: 400
                textStyle.fontSizeValue: 6.0

            }

            ImageButton {
                defaultImageSource: "asset:///Image/icon_arrow.png"

                preferredWidth: 38.0
                preferredHeight: 56.0
                enabled: true
                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Center
                
                onClicked: {
                    showDetails();
                }
            }
        } //End of Summary Info Container

        Container {
            id: pickupInfoContainer

            preferredHeight: 90.0
            maxWidth: 686.0
            minWidth: 686.0

            background: pickupInfoBackground.imagePaint

            attachedObjects: [
                ImagePaintDefinition {
                    id: pickupInfoBackground
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                }
            ]

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight

            }

            horizontalAlignment: HorizontalAlignment.Center

            leftPadding: 30.0

            Label {
                minWidth: 130

                text: "Pick up"
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.color: Color.create("#b4000000")
                textStyle.fontSizeValue: 7.0
            }

            Label {

                objectName: "pickupLabel"
                text: propertyMap.pickupInfo
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.color: Color.create("#b4000000")
                maxWidth: 395
                minWidth: 395
                textStyle.fontSizeValue: 7.0
            }

            ImageButton {
                preferredWidth: 68.0
                preferredHeight: 60.0
                pressedImageSource: "asset:///Image/icon_pickup_mybooking.png"
                defaultImageSource: "asset:///Image/icon_pickup_mybooking.png"

                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Center

                onClicked: {

                }
            }
        } //End of pickup container

        Container {
            id: dropOffInfoContainer

            preferredHeight: 90.0
            maxWidth: 686.0
            minWidth: 686.0

            background: dropOffInfoBackground.imagePaint

            attachedObjects: [
                ImagePaintDefinition {
                    id: dropOffInfoBackground
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                }
            ]

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            horizontalAlignment: HorizontalAlignment.Center

            leftPadding: 30.0

            Label {
                minWidth: 130.0

                text: "Drop off"
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.color: Color.create("#b4000000")
                textStyle.fontSizeValue: 7.0
            }

            Label {
                objectName: "dropoffLabel"
                text: propertyMap.dropoffInfo
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.color: Color.create("#b4000000")
                maxWidth: 395
                minWidth: 395
                textStyle.fontSizeValue: 7.0
            }

            ImageButton {
                preferredWidth: 68.0
                preferredHeight: 60.0
                pressedImageSource: "asset:///Image/icon_pickup_mybooking.png"
                defaultImageSource: "asset:///Image/icon_pickup_mybooking.png"

                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Center

                onClicked: {

                }
            }

        } //End of Drop off Info Container

        Container {
            id: statusContainer

            preferredHeight: 90.0
            maxWidth: 686.0
            minWidth: 686.0

            background: statusBackground.imagePaint

            attachedObjects: [
                ImagePaintDefinition {
                    id: statusBackground
                    imageSource: "asset:///Image/bar_insert_btm_profile.png"
                }
            ]

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight

            }

            horizontalAlignment: HorizontalAlignment.Center

            leftPadding: 30.0

            Label {
                minWidth: 130

                text: "Status"
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.color: Color.create("#b4000000")
                textStyle.fontSizeValue: 7.0
            }

            Label {
                objectName: "statusLabel"
                text: propertyMap.status
                verticalAlignment: VerticalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.color: Color.create("#b4000000")
                maxWidth: 480.0
                minWidth: 480.0
                textStyle.fontSizeValue: 7.0
            }

        } //End of Status Container

        onTouch: {
            if (event.isUp()) {
                showDetails();
            }
        }
    }
    
    function showDetails()
    {
     	eventHandler.showDetails(numInfoLabel.text);   
    }
} //End of Current Info Container