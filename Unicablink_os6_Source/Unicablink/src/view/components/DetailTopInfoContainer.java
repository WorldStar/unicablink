package view.components;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;

public class DetailTopInfoContainer extends HorizontalFieldManager{
	/**
	 * private members
	 */
	//left: 20, label: 150, id: 164, gutter: 58

	private int nLeftMargin = 0;
	private int nWidthLabel = 0;
	private int nWidthId = 0;
	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private String strOrderID;

	private Font mFontText;
	private int nFontSize = 0;

	private int nPadding = 0;

	private float fScale = Utils.getVRatio();

	ImageButtonField btnCancel;
	ImageButtonField btnHistory;
	
	public DetailTopInfoContainer(String strOrderId)
	{
		super(USE_ALL_WIDTH | Manager.HORIZONTAL_SCROLL | Manager.HORIZONTAL_SCROLLBAR);

		/**
		 * initialize
		 */
		this.strOrderID = strOrderId;
		nLeftMargin = (int)(20 * fScale);
		nWidthLabel = (int)(130 * fScale);
		nWidthId = (int)(130 * fScale);
		nPadding = (int)(58 * fScale);
		nFontSize = 30;

		/**
		 * set Background
		 */

		Bitmap bgBmp = Utils.getBitmapResource(ResouceConstant.DETAILS_BAR_TOP);
		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));

		//initialze
		nTotalWidth = bgBmp.getWidth();
		nTotalHeight = bgBmp.getHeight();

		//set fonts
		mFontText = Utils.getFont(Font.BOLD, nFontSize);

		LabelField labelDescription = new LabelField("Order ID:");
		labelDescription.setFont(mFontText);

		LabelField labelOrderId = new LabelField(strOrderId);
		labelOrderId.setFont(mFontText);

		btnHistory = new ImageButtonField(ResouceConstant.DETAILS_BTN_HISTORY, "", 0, "")
		{
			public void clickButton()
			{
				showHistoryScreen();
			}
		};

		btnCancel = new ImageButtonField(ResouceConstant.DETAILS_BTN_CANCEL_NORMAL, "", 0, ResouceConstant.DETAILS_BTN_CANCEL_DISABLE)
		{
			public void clickButton()
			{
				cancelOrder();
			}
		};

		/**
		 * Add fields to Manager
		 */
		add(labelDescription);
		add(labelOrderId);
		add(btnHistory);
		add(btnCancel);
	}

	/**
	 * overrides
	 */
	public void sublayout(int nWidth, int nHeight)
	{
		int nNumFields = getFieldCount();

		if (nNumFields == 4)
		{
			int nXPos = nLeftMargin;

			Field labelDescription = this.getField(0);
			Field labelOrderId = this.getField(1);
			Field btnHistory = this.getField(2);
			Field btnCancel = this.getField(3);

			setPositionChild(labelDescription, nXPos, (nTotalHeight - (int)(nFontSize * fScale)) / 2);
			layoutChild(labelDescription, nWidthLabel, Integer.MAX_VALUE);

			nXPos = nXPos + nWidthLabel;

			setPositionChild(labelOrderId, nXPos, (nTotalHeight - (int)(nFontSize * fScale)) / 2);
			layoutChild(labelOrderId, nWidthId, Integer.MAX_VALUE);

			nXPos = nXPos + nWidthId;

			setPositionChild(btnHistory, nXPos, (nTotalHeight - btnHistory.getPreferredHeight()) / 2);
			layoutChild(btnHistory, btnHistory.getPreferredWidth(), Integer.MAX_VALUE);

			nXPos = nXPos + btnHistory.getPreferredWidth() + nPadding;

			setPositionChild(btnCancel, nXPos, (nTotalHeight - btnCancel.getPreferredHeight()) / 2);
			layoutChild(btnCancel, btnCancel.getPreferredWidth(), Integer.MAX_VALUE);
		}

		this.setExtent(nTotalWidth, nTotalHeight);
	}

	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}

	/**
	 * User Interaction Process
	 */
	public void setCancelEnabled(boolean bEnabled)
	{
		btnCancel.setActionEnabled(bEnabled);
	}
	
	public boolean getCancelEnabled()
	{
		return btnCancel.getActionEnabled();
	}
	
	public void showHistoryScreen()
	{
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
		theApp.goHistoryScreen();
	}

	public void cancelOrder()
	{

	}

	public String getOrderId()
	{
		return this.strOrderID;
	}
	
	public void setFocus()
	{
		btnHistory.setFocus();
	}

}

