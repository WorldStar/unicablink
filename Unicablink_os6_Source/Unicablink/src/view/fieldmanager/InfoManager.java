package view.fieldmanager;

import application.Utils;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * This vertical manager is used when the components has same vertical padding.
 * The components are located center in horizontal
 * @author Passion
 *
 */
public class InfoManager extends VerticalFieldManager{
	private int nVPadding = 0;
	private int nTopPadding = 0;
	private int nBottonPadding = 0;
	private float fScale = Utils.getVRatio();
	
	/**
	 * Create LogInInfoObject for objects with same vertical padding 
	 * @param nVerticalPadding
	 */
	public InfoManager(int nVerticalPadding, int nTopPadding, int nBtmPadding, int backColor){
		super(Manager.VERTICAL_SCROLL | Manager.VERTICAL_SCROLLBAR | USE_ALL_WIDTH);
		
		if (backColor != 0)
		{
			setBackground(BackgroundFactory.createSolidBackground(backColor));	
		}

		setVPadding(nVerticalPadding);
		setTopPadding(nTopPadding);
		setBottomPadding(nBtmPadding);
	}
	

	/**
	 * set vertical padding in vertical manager
	 * @param nVerticalPadding
	 */
	private void setVPadding(int nVerticalPadding){
		nVPadding = (int)((float)nVerticalPadding * fScale);
	}
	
	private void setTopPadding(int nNewTopPadding){
		nTopPadding = (int)((float)nNewTopPadding * fScale);
	}
	
	private void setBottomPadding(int nNewBtnPadding){
		nBottonPadding = (int)((float)nNewBtnPadding * fScale);
	}
	
	/**
	 * get vertical padding
	 * @return
	 */
	public int getVPadding(){
		return nVPadding;
	}
	
	public int getTopPadding(){
		return nTopPadding;
	}
	
	public int getBottomPadding(){
		return nBottonPadding;
	}
	
    public void sublayout(int nWidth, int nHeight)
    {
        int nNumFields = getFieldCount();
        int nYPos = getTopPadding(); 
        
        for (int i = 0; i < nNumFields; i++)
        {
            Field fField = this.getField(i);
            setPositionChild(fField, (Utils.getDisplayWidth() - fField.getPreferredWidth()) / 2, nYPos);
            layoutChild(fField, this.getPreferredWidth(), Integer.MAX_VALUE);
            nYPos = nYPos + fField.getPreferredHeight() + getVPadding();
        }

    	this.setExtent(Utils.getDisplayWidth(), nYPos - getVPadding() + getBottomPadding());
    }
}
