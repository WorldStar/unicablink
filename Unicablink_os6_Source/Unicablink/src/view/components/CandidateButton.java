package view.components;

import application.ResouceConstant;
import application.Utils;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class CandidateButton extends HorizontalFieldManager{
	/**
	 * Private members
	 */
	private int nLeftPadding = 0;
	private int nRightPadding = 0;

	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private float fScale = Utils.getVRatio();

	private Font mFontText;
	private int nFontSize = 0;

	//Map Information
	private String strLocationString = "";
	private double dLat = 0;
	private double dLon = 0;

	private Bitmap bgSel = null;
	private Bitmap bgUnsel = null;

	//Location Label
	LabelField mLabelAddress;

	/**
	 * Create Object: (Location Candidate Button)
	 * @param strEnableBGFile: Background File when enable
	 * @param strDisableBGFile: Background File when disable
	 * @param leftPadding: Left Padding
	 * @param rightPadding: Right Padding
	 * @param strLabelText: Location String
	 */
	public CandidateButton(String strLabelText, double dLat, double dLon)
	{
		super(USE_ALL_WIDTH);

		/**
		 * initialize
		 */
		nLeftPadding = (int)(30 * fScale);
		nRightPadding = (int)(30 * fScale);
		this.dLat = dLat;
		this.dLon = dLon;

		nFontSize = 30;


		this.strLocationString = strLabelText;

		/**
		 * set Background
		 */
		bgUnsel = Utils.getBitmapResource(ResouceConstant.ADDRESS_BAR_UNSEL);
		bgSel = Utils.getBitmapResource(ResouceConstant.ADDRESS_BAR_SEL);
		
		//initialze
		nTotalWidth = bgSel.getWidth();
		nTotalHeight = bgSel.getHeight();

		//set fonts
		mFontText = Utils.getFont(Font.PLAIN, nFontSize);
		
		//start modified by KO at 20131128
		if(strLabelText.length() > 94)
			strLabelText = strLabelText.substring(0,  90) + " ...";
		//  end modified by Ko at 20131128
		
		//Location Label
//		mLabelAddress = new LabelField(strLocationString);
		mLabelAddress = new LabelField(strLabelText);

//start modified by KO at 20131128 
//		if (strLocationString.length() > 80)
//		{
//			mFontText = Utils.getFont(Font.PLAIN, nFontSize - 4);
//		}
//		else
//  end modified by KO at 20131128
		{
			mFontText = Utils.getFont(Font.PLAIN, nFontSize);
		}
		mLabelAddress.setFont(mFontText);

		/**
		 * Add fields to Manager
		 */
		add(mLabelAddress);
	}
	
	/**
	 * Get and Set
	 */
	public String getLocation()
	{
		return this.strLocationString;
	}
	
	public double getDLat()
	{
		return this.dLat;
	}
	
	public double getDLon()
	{
		return this.dLon;
	}
	

	/**
	 * overrides
	 */
	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}
	
	public void sublayout(int nWidth, int nHeight)
	{
		int nNumFields = getFieldCount();

		if (nNumFields == 1)
		{
			int nXPos = nLeftPadding;

			Field label = this.getField(0);
			
			layoutChild(label, nTotalWidth - nLeftPadding - nRightPadding, Integer.MAX_VALUE);
			setPositionChild(label, nXPos, (nTotalHeight - label.getHeight()) / 2);
		}

		this.setExtent(nTotalWidth, nTotalHeight);
	}
	

	
	protected void paint(Graphics graph) {
		super.paint(graph);
		if (isFocus())
		{
			if (bgSel != null)
			{
				setBackground(BackgroundFactory.createBitmapBackground(bgSel));	
			}
			
		}
		else
		{
			if (bgUnsel != null)
			{
				setBackground(BackgroundFactory.createBitmapBackground(bgUnsel));	
			}
		}
	}

	protected boolean navigationClick(int status, int time) 
	{
		if (status != 0){
			fieldChangeNotify(0);
			clickButton();
		}
		return true;
	}

	public boolean keyChar(char key, int status, int time) 
	{
		if (key == Characters.ENTER) {
			fieldChangeNotify(0);
			clickButton();
			return true;
		}
		return false;
	}
	protected boolean trackwheelClick(int status, int time)
	{        
		if (status != 0) clickButton();    
		return true;
	}

	protected boolean invokeAction(int action) 
	{
		switch( action ) {
		case ACTION_INVOKE: {
			clickButton(); 
			return true;
		}
		}
		return super.invokeAction(action);
	}    

	public boolean isFocusable() 
	{
		return true;
	}

	protected boolean touchEvent(TouchEvent message)
	{
		System.out.println("aaaaaaaaaaaaaaaaaaaaa: candidate button touched!");
		int x = message.getX( 1 );
		int y = message.getY( 1 );
		if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
			// Outside the field
			return false;
		}
		switch( message.getEvent() ) {
		case TouchEvent.UNCLICK:
			clickButton();
			return true;
		}
		return super.touchEvent( message );
	}
	
	/**
	 * User Interaction
	 * 
	 */
	public void clickButton()
	{
		
	}
	
}
