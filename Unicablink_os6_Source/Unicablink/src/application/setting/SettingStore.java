package application.setting;


import model.data.DataConstant;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;


public class SettingStore {
	private PersistentObject m_persistentObject = PersistentStore.getPersistentObject(DataConstant.APP_REGISTER_KEY);
	private AppSetting m_appSetting = new AppSetting();
	private CustomHashTable m_htSetting = new CustomHashTable();
	
	public SettingStore(){
		if (m_persistentObject.getContents() == null){
			m_htSetting.put("AppSetting", m_appSetting);
			m_persistentObject.setContents(m_htSetting);
			m_persistentObject.commit();
		}
		else{
			m_htSetting = (CustomHashTable)m_persistentObject.getContents();
			if (m_htSetting.containsKey("AppSetting")){
				m_appSetting = (AppSetting)m_htSetting.get("AppSetting");	
			}
		}
	}

	public AppSetting getAppSetting(){
		return m_appSetting;
	}
	
	public void saveAppSetting(AppSetting appSetting){
		m_appSetting = appSetting;
		save();
	}
	
	private void save(){
		m_htSetting.put("AppSetting", m_appSetting);
		m_persistentObject.setContents(m_htSetting);
		m_persistentObject.commit();
	}
}
