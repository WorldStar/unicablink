/*
 * JSONReqeust.cpp
 *
 *  Created on: Aug 7, 2013
 *      Author: Passion
 */

#include "JSONReqeust.h"
#include <bb/device/HardwareInfo>
#include <QCryptographicHash>
#include <bb/data/JsonDataAccess>

using namespace bb::cascades;
using namespace bb::device;
using namespace bb::data;

//Request type
typedef enum
{
	//User log in request
	USER_LOG_IN = 1,
	//user register request
	USER_REGISTER = 2,
	//user activate request
	USER_ACTIVATE = 3,
	//resend activation code request
	RESEND_ACTIVATION_CODE = 4,
	//retrieve password request
	RETRIEVE_PASSWORD = 5,
	//order cab request
	ORDER_CAB = 6,
	//modify profile
	MODIFY_PROFILE = 7,
	//cancel order
	CANCEL_ORDER = 8,
	//order status request
	ORDER_STATUS_UPDATE = 9,
	//query position info request
	QUERY_POSITION_INFO = 10,
	//search location
	SEARCH_LOCATION = 11
} request_type;


JSONReqeust::JSONReqeust(QObject *parent)
{
	m_pParent = parent;
}

JSONReqeust::~JSONReqeust()
{

}

//Utility
//Get IMEI
QString JSONReqeust::getIMEI()
{
	QString strIMEI = "";

	HardwareInfo hdInfo(this);


	//get IMEI
	strIMEI = hdInfo.imei();

	return strIMEI;
}

//Get MD5 Password
QString JSONReqeust::getMD5Password(QString strPassword){
	QString strMD5Password = "";

	strMD5Password = QString(QCryptographicHash::hash(strPassword.toAscii(),QCryptographicHash::Md5).toHex());

	return strMD5Password;
}

QString JSONReqeust::getJSON(QMap<QString,QVariant> &params, int nType)
{
	QString strResult = "";

	// Create QVariantMap objects to contain the data for each employee
	QVariantMap request;

	request["SecurityKey"] = "e92684cb325f1e4d4cce27c0207562bf";

	switch (nType){
	case USER_LOG_IN:
	{
		//add data
		QString strTemp;

		request["Mobile"] = params["Mobile"];

		request["Psw"] = getMD5Password(params["Psw"].toString());
		request["VCode"] = params["VCode"];

		strTemp = getIMEI();
		request["DEVICEID"] = getIMEI();
		break;
	}
	case USER_REGISTER:
	{
		/*
		 * {
					"DEVICEID":"83e462f770cc34eb",
					"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
					"NAME":"Micheal",
					"MOBILE":"+8615806131326",
					"PASSWORD":"future",
					"PHONETYPE":"3",
					"EMAIL": "yuan@obama.com"
			}
		 */

		//add data
		QString strTemp;

		request["MOBILE"] = params["MOBILE"];

		request["PASSWORD"] = params["PASSWORD"];

		request["PHONETYPE"] = params["PHONETYPE"];

		request["NAME"] = params["NAME"];

		request["EMAIL"] = params["EMAIL"];

		request["DEVICEID"] = getIMEI();

		break;
	}
	case USER_ACTIVATE:
	{
		break;
	}
	case RESEND_ACTIVATION_CODE:
	{
		/*
		 * 	�SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
			�Mobile�:�+60123456789�,
			�Email�:�abcd@xxx.com�
			�REQUESTTIME�:�2013-05-15 18:00:00�
		 *
		 */
		request["Mobile"] = params["Mobile"];
		request["Email"] = params["Email"];
		request["REQUESTTIME"] = params["REQUESTTIME"];

		break;
	}
	case RETRIEVE_PASSWORD:
	{
		request["Mobile"] = params["Mobile"];
		request["REQUESTTIME"] = params["REQUESTTIME"];
		request["Email"] = params["Email"];

		break;
	}
	case ORDER_CAB:
	{
		//add Data

		/*
		 * 				{
				"DEVICEID":"354897050506261",
				"PLAT":"3.1813032",
				"TIP":"",
				"NAME":"passion",
				"PICKUPTIME":"2013-08-21 19:38:08",
				"PLON":"101.5488498",
				"REMARKS":"Test",
				"DLON":"101.5776766",
				"CABTYPE":"1",
				"DROPOFF":"Au 2, Kuala Lumpur",
				"DLAT":"3.2965233",
				"ORDERTIME":"2013-08-22 19:35",
				"CUSTOMERID":"356C43C2-9E8A-02D9-EB40A95BA5A6DFF3",
				"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
				"MOBILE":"+601126253547",
				"EMAIL":"wang198904@gmail.com",
				"PICKUP":"Bandar Damai Perdana, Kuala Lumpur"
				}
		 *
		 */

		request["DEVICEID"] = getIMEI();
		request["PLAT"] = params["PLAT"];
		request["TIP"] = params["TIP"];
		request["NAME"] = params["NAME"];
		request["PICKUPTIME"] = params["PICKUPTIME"];
		request["PLON"] = params["PLON"];
		request["REMARKS"] = params["REMARKS"];
		request["DLON"] = params["DLON"];
		request["CABTYPE"] = params["CABTYPE"];
		request["DROPOFF"] = params["DROPOFF"];
		request["DLAT"] = params["DLAT"];
		request["ORDERTIME"] = params["ORDERTIME"];
		request["CUSTOMERID"] = params["CUSTOMERID"];
		request["MOBILE"] = params["MOBILE"];
		request["EMAIL"] = params["EMAIL"];
		request["PICKUP"] = params["PICKUP"];

		break;
	}
	case MODIFY_PROFILE:
	{
		/*
		 * {
				�SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
				�NAME�:�Micheal�,
				�CUSTOMERID�:� 8AE8BE30-D459-8565-47F75BD4134A101C�
				�MOBILE�:�+8615806131326�,
				�OLDPASSWORD�:�123456�
				�NEWPASSWORD�:�abcd12�,
				�PHONETYPE�:��,
				�EMAIL�: �yuan@obama.com�
			}
		 *
		 */
		//add data
		request["MOBILE"] = params["MOBILE"];
		request["OLDPASSWORD"] = params["OLDPASSWORD"];
		request["NEWPASSWORD"] = params["NEWPASSWORD"];
		request["NAME"] = params["NAME"];
		request["PHONETYPE"] = params["PHONETYPE"];
		request["EMAIL"] = params["EMAIL"];
		request["CUSTOMERID"] = params["CUSTOMERID"];

		break;
	}
	case CANCEL_ORDER:
	{
		/*
		 * {
				�SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
				�ORDERID�:1638827,
				�REQUESTTIME�:�2012-04-23 18:00:00�
			}
		 *
		 */
		//add data
		request["ORDERID"] = params["ORDERID"];
		request["REQUESTTIME"] = params["REQUESTTIME"];

		break;
	}
	case ORDER_STATUS_UPDATE:
	{
		/*
		 * {
				�SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
				�MOBILE�:�+60174771691�,
				�ORDERArray� : [{�OrderID� : 211569},{�OrderID� : 211570}, {�OrderID� : 211571}],
				�REQUESTTIME�:�2011-05-23 11:43:44�
		 * }
		 */
		request["MOBILE"] = params["MOBILE"];
		request["ORDERArray"] = params["ORDERArray"];
		request["REQUESTTIME"] = params["REQUESTTIME"];

		break;
	}
	case QUERY_POSITION_INFO:
	{
		break;
	}
	case SEARCH_LOCATION:
	{
		/*
			{
				"Name":"KU",
				"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
				"REQUESTTIME":"2013-08-12 11:23:26",
				"CurCoordinate":"",
				"Type":"2",
				"Coordinate":""
			}
		 */

		request["Name"] = params["Name"];
		request["REQUESTTIME"] = params["REQUESTTIME"];
		request["CurCoordinate"] = params["CurCoordinate"];
		request["Type"] = params["Type"];
		request["Coordinate"] = params["Coordinate"];

		break;
	}

	default: {
		break;
	}
	}

	//Get Variant
	m_jsonResult = QVariant(request);

	JsonDataAccess jda;

	jda.saveToBuffer(m_jsonResult, &strResult);

	return strResult;
}

QVariant JSONReqeust::getJSONVariant()
{
	return m_jsonResult;
}

void JSONReqeust::reset()
{

}
