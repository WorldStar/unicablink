package model.request;

import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.Utils;

public class Registeration {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strPhoneNo = "";
	private String m_strName = "";
	private String m_strPassword = "";
	private String m_strPhoneType = "3";
	private String m_strEmailAddress = "";
	
	/*
	 * {
	 "DEVICEID":"83e462f770cc34eb",
	 "SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 "NAME":"Micheal",
	 "MOBILE":"+8615806131326",
	 "PASSWORD":"future",
	 "PHONETYPE":"3",
	 "EMAIL": "yuan@obama.com"
	 }
	 */
	public Registeration(){
		
	}
	
	public void setPhoneNo(String strPhoneNo){
		m_strPhoneNo = strPhoneNo;
	}
	
	public void setName(String strName){
		m_strName = strName;
	}
	
	public void setPassword(String strPassword){
		m_strPassword = strPassword;
	}
	
	public void setEmailAddress(String strEmailAddress){
		m_strEmailAddress = strEmailAddress;
	}
	
	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();
		
		//Get IMEI
		String strIMEI = Utils.getIMEI();

		try{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("NAME", m_strName);
			jsonResult.put("MOBILE", m_strPhoneNo);
			jsonResult.put("PASSWORD", m_strPassword);
			jsonResult.put("PHONETYPE", m_strPhoneType);
			jsonResult.put("EMAIL", m_strEmailAddress);
			jsonResult.put("DEVICEID", strIMEI);
		}
		catch(JSONException e){
			e.printStackTrace();
			return null;
		}
		
		return jsonResult;
	}
}
