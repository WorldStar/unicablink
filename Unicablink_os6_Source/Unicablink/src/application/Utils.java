package application;

import java.util.Calendar;
import java.util.TimeZone;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.resources.Resource;


/**
 * support constants and methods for this application.
 */



public class Utils {

	/*
	 * Constants
	 */

	/** 
	 * The width of display for original resources, default value is 640px.
	 */
	private static float ORIGINAL_DISPLAY_WIDTH = 640;

	/** 
	 * The height of display for original resources, default value is 480px.
	 */ 
	private static float ORIGINAL_DISPLAY_HEIGHT = 480;

	/** 
	 * The ratio between current-device display width and original display width for resources
	 */
	public static float H_RATIO;

	/** 
	 * The ratio between current-device display height and original display width for resources
	 */
	public static float V_RATIO;	


	/**
	 * The duration of viewing splash logo-image, default value is 2 seconds.
	 */
	public final static long LOGO_SCREEN_DURATION = 2000;

	/**
	 * The time that appear logo image after, default value is 3 seconds.
	 */
	public final static long SKIPADS_APPEAR_AFTER_TIME = 3000;	

	public static final String [] arrMonthName = {"Jan" , "Feb" , "Mar" , "Apr" , "May" , "Jun" , "Jul" , "Aug", "Sep", "Oct", "Nov", "Dec"};


	/*
	 * Methods
	 */

	/**
	 * get Description from status code
	 * @param nStatus: status code
	 * @return
	 */
	public static String getDescription(int nStatus)
	{
		String strDescription = "";

		switch (nStatus) {
		case 0: //Processing
		{
			strDescription = ResouceConstant.DESCRIPTION_PROCESSING;
			break;	
		}
		case 1: //Confirmed
		{
			strDescription = ResouceConstant.DESCRIPTION_CONFIRMED;
			break;	
		}
		
		case 2: //Arrived
		{
			strDescription = ResouceConstant.DESCRIPTION_ARRIVED;
			break;	
		}
		
		case 3: //Pickup
		{
			strDescription = ResouceConstant.DESCRIPTION_PICKUP;
			break;	
		}
		
		case 4: //Completed
		case 5: //Completed
		{
			strDescription = ResouceConstant.DESCRIPTION_COMPLETE;
			break;	
		}
		
		case 6: //Passenger Cancelled
		{
			strDescription = ResouceConstant.DESCRIPTION_PASSENGER_CANCEL;
			break;
		}
		
		case 7: //Driver Cancelled
		{
			strDescription = ResouceConstant.DESCRIPTION_DRIVER_CANCEL;
			break;	
		}
		
		case 8: //No Taxi
		{
			strDescription = ResouceConstant.DESCRIPTION_NO_TAXI;
			break;	
		}
		
		case 9: //New
		{
			strDescription = ResouceConstant.DESCRIPTION_NEW;
			break;	
		}
		
		case 10: //Accepted
		{
			strDescription = ResouceConstant.DESCRIPTION_ACCEPT;
			break;	
		}

		default:
			break;
		}
		
		return strDescription;
	}

	/**
	 * get status from status code
	 * @param nStatus: status code
	 * @return
	 */
	public static String getStatus(int nStatus)
	{
		String strStatus = "";

		/*
		 *	
	public static final String STATUS_PROCESS = "We are now processing your order, please wait...";
	public static final String STATUS_TAXI_EXIST = "There is taxi for you. TQ";
	public static final String STATUS_PROCESS_PICKUP = "Your taxi has arrived. Please proceed to pick up point immediately. TQ";
	public static final String STATUS_JOURNEY = "Journey start. Have a nice trip.";
	public static final String STATUS_PICKUP = "Successfully pick up.";
	public static final String STATUS_PICKUP_COMPLETE = "Successfully pick up.";
	public static final String STATUS_JOURNEY_COMPLETE = "Journey completed.";
	public static final String STATUS_PASSENGER_CANCEL = "Passenger Canceled.";
	public static final String STATUS_DRIVER_CANCEL = "Oops, We are sorry, for some reasons driver cancel the job. Please try again / call our customer support";
	public static final String STATUS_NO_TAXI = "We are sorry, there is no taxi nearby available at the moment, please try again!";
	public static final String STATUS_ADVANCE_JOB = "You are making an advance job, we will proceed your order later";
	public static final String STATUS_ACCEPTED = "Your job has been accepted"; 
		 */
		switch (nStatus) {
		case 0: //Processing
		{
			strStatus = ResouceConstant.STATUS_PROCESS;
			break;	
		}
		case 1: //Confirmed
		{
			strStatus = ResouceConstant.STATUS_TAXI_EXIST;
			break;	
		}
		
		case 2: //Arrived
		{
			strStatus = ResouceConstant.STATUS_PROCESS_PICKUP;
			break;	
		}
		
		case 3: //Pickup
		{
			strStatus = ResouceConstant.STATUS_JOURNEY;
			break;	
		}
		
		case 4: //pickup Completed
		{
			strStatus = ResouceConstant.STATUS_PICKUP_COMPLETE;
			break;
		}
		case 5: //Completed
		{
			strStatus = ResouceConstant.STATUS_JOURNEY_COMPLETE;
			break;	
		}
		
		case 6: //Passenger Cancelled
		{
			strStatus = ResouceConstant.STATUS_PASSENGER_CANCEL;
			break;
		}
		
		case 7: //Driver Cancelled
		{
			strStatus = ResouceConstant.STATUS_DRIVER_CANCEL;
			break;	
		}
		
		case 8: //No Taxi
		{
			strStatus = ResouceConstant.STATUS_NO_TAXI;
			break;	
		}
		
		case 9: //New
		{
			strStatus = ResouceConstant.STATUS_ADVANCE_JOB;
			break;	
		}
		
		case 10: //Accepted
		{
			strStatus = ResouceConstant.STATUS_ACCEPTED;
			break;	
		}

		default:
			break;
		}
		
		return strStatus;
	}

	/**
	 * check if simulator
	 * @return
	 */
	public static boolean isSimulator()
	{
		return DeviceInfo.isSimulator();
	}

	/**
	 * get Device's IMEI
	 * @return
	 */
	public static String getIMEI()
	{
		byte[] imeiByteArray = net.rim.device.api.system.GPRSInfo.getIMEI();

		String strIMEI = net.rim.device.api.system.GPRSInfo.imeiToString(imeiByteArray);

		imeiByteArray = null;

		return strIMEI;
	}
	/**
	 * Initialize utility for this application 
	 */	
	public static void init()
	{
		H_RATIO = Utils.getDisplayWidth() / ORIGINAL_DISPLAY_WIDTH;
		V_RATIO = Utils.getDisplayHeight() / ORIGINAL_DISPLAY_HEIGHT;
	}

	public static Font getFont(int nStyle, int nFontSize)
	{
		Font font = null;
		try{
			FontFamily ff = FontFamily.forName("Times New Roman");
			font = ff.getFont(nStyle, (int)(nFontSize * getVRatio()));
		}
		catch(final ClassNotFoundException cnfe){
			UiApplication.getUiApplication().invokeLater(new Runnable()
			{
				public void run()
				{
					Dialog.alert("FontFamily.forName() threw " + cnfe.toString());
				}
			});
		}

		return font;
	}

	/**
	 * Creates a bitmap from provided name resource with H_RATIO and V_RATIO
	 * 
	 * @param filename - Name of the bitmap resource.  
	 * @return New Bitmap object scaled by H_RATIO and V_RATIO , or null if this method couldn't find your named resource. 
	 * @throws NullPointerException - If the name parameter is null.
	 */
	public static Bitmap getBitmapResource(String filename) {

		if (filename == null) {
			throw new NullPointerException();
		}

		Bitmap src = Bitmap.getBitmapResource(filename);

		if(src == null)
			return null;
		int width = (int)(H_RATIO * src.getWidth());
		int height = (int)(V_RATIO * src.getHeight());
		int length = width * height;

		int[] argbData = new int[length];
		for(int i = 0; i < length; i++)
			argbData[i] = 0; // set as transparent    

		Bitmap dst = new Bitmap(width, height);  
		dst.setARGB(argbData, 0, width, 0, 0, width, height);
		argbData = null;

		src.scaleInto(dst, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FILL);    	
		src = null;
		return dst;
	}
	//	public static Bitmap decodeScaledBitmapFromSdCard(String filePath,
	//			int reqWidth, int reqHeight) {
	//
	//		// First decode with inJustDecodeBounds=true to check dimensions
	//		final BitmapFactory.Options options = new BitmapFactory.Options();
	//		BitmapFactory.decodeFile(filePath, options);
	//
	//		// Calculate inSampleSize
	//		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
	//
	//		// Decode bitmap with inSampleSize set
	//		options.inJustDecodeBounds = false;
	//		return BitmapFactory.decodeFile(filePath, options);
	//	}
	/**
	 * Return a bitmap from provided bitmap with Ratio.
	 * 
	 * @param Bitmap - source Bitmap  
	 * @return New Bitmap object scaled by RATIO. 
	 * @throws NullPointerException - If the name parameter is null.
	 */
	public static Bitmap getScaledBitmap(Bitmap src, double ratio) {

		if (src == null || ratio <= 0) {
			throw new NullPointerException();
		}
		int width = (int)(ratio * src.getWidth());
		int height = (int)(ratio * src.getHeight());
		int length = width * height;

		int[] argbData = new int[length];
		for(int i = 0; i < length; i++)
			argbData[i] = 0; // set as transparent    

		Bitmap dst = new Bitmap(width, height);  
		dst.setARGB(argbData, 0, width, 0, 0, width, height);
		argbData = null;

		src.scaleInto(dst, Bitmap.FILTER_BILINEAR, Bitmap.SCALE_TO_FILL);    	
		src = null;
		return dst;
	}

	public static Bitmap getScaledBitmap(Bitmap src, int width, int height) {
		if (src == null || width <= 0 || height <= 0) {
			return null;
		}
		int length = width * height;

		int[] argbData = new int[length];
		for(int i = 0; i < length; i++)
			argbData[i] = 0; // set as transparent    

		Bitmap dst = new Bitmap(width, height);  
		dst.setARGB(argbData, 0, width, 0, 0, width, height);
		argbData = null;

		src.scaleInto(dst, Bitmap.FILTER_BILINEAR, Bitmap.SCALE_STRETCH);    	
		src = null;
		return dst;
	}
	/**
	 * 
	 * @return Current Device display width & height.
	 */
	public static int getDisplayWidth(){
		return Display.getWidth();
	}
	public static int getDisplayHeight(){
		return Display.getHeight();
	}

	/**
	 * 
	 * @return Current Device Display Size : Default Size (640 X 480)
	 */
	public static float getHRatio(){
		return Utils.H_RATIO;
	}
	public static float getVRatio(){
		return Utils.V_RATIO;
	}

	/**
	 * 
	 * @return fontFamily which program used.
	 * 		You can change font here....
	 */
	public static FontFamily getTypeFace(){
		FontFamily typeface;
		try {
			typeface = FontFamily.forName(FontFamily.FAMILY_SYSTEM);
			return typeface;

		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	/**
	 * return MarginX/Y with Ratio H/V 
	 */
	public static int toDPIx(int x){
		return (int)(x * Utils.getHRatio());
	}
	public static int toDPIy(int y){
		return (int)(y * Utils.getVRatio());
	}

	/**
	 * @param 
	 * 		month: index of month(start from 0)
	 * @return 
	 * 		short-Month string  
	 */
	public static String getMonthString(int month){
		String result = "";
		try{
			result = arrMonthName[month];
		}catch(Exception e){
			result = "";
		}

		return result;
	}	

	/**
	 * 
	 * @param 
	 * 		strMonth :  short month string
	 * @return
	 * 		suitable month value
	 */
	public static int getMonthFromString(String strMonth)
	{
		for(int i = 0; i < 12; i++)
		{
			if(strMonth.equals(arrMonthName[i]))
				return i;
		}
		System.out.println("Invalid Month String.");
		return -1;
	}

	/**
	 * 
	 * @param strDate : date string format as 'yyyy-mm-dd hh:mm:ss +xxxx'
	 * @return
	 * 		calendar object that is suitable for date string
	 */
	public static Calendar getDateFromString(String strDate)
	{
		if(strDate == null || strDate.equals(""))
			return null;

		try{
			int nYear = Integer.parseInt(strDate.substring(0, 4));
			int nMonth = Integer.parseInt(strDate.substring(5, 7));
			int nDay = Integer.parseInt(strDate.substring(8, 10));
			int nHour = Integer.parseInt(strDate.substring(11, 13));
			int nMinute = Integer.parseInt(strDate.substring(14, 16));
			int nSecond = Integer.parseInt(strDate.substring(17, 19));

			String nTimeZoneSignal = strDate.substring(20, 21);

			int nTimeZoneHour = Integer.parseInt(strDate.substring(21, 23));
			int nTimeZoneMinute = Integer.parseInt(strDate.substring(23, 25));

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, nYear);
			cal.set(Calendar.MONTH, nMonth);
			cal.set(Calendar.DAY_OF_MONTH, nDay);
			cal.set(Calendar.HOUR_OF_DAY, nHour);
			cal.set(Calendar.MINUTE, nMinute);
			cal.set(Calendar.SECOND, nSecond);

			cal.setTimeZone(TimeZone.getTimeZone("GMT" + nTimeZoneSignal + nTimeZoneHour + ":" + nTimeZoneMinute));

			return cal;
		}
		catch(Exception e)
		{
			System.out.println("Invalid Date String.");
			return null;
		}
	}

	/**
	 * @param src: source string
	 * @param width: display area's width
	 * @param font: display font
	 * @return
	 * 		if need to be omitted, then return omitted string which format as 'xxx...' 
	 */
	public static String getOmittedString(String src, int width, Font font)
	{
		String strResult;

		int nLen = src.length();		
		if(font.getBounds(src) < width)
		{
			strResult = src;			
		}
		else{
			// assume process
			do
			{
				nLen--;
				strResult = src.substring(0, nLen);
			}while(font.getBounds(strResult) >= width);

			nLen -= 2;
			strResult = src.substring(0, nLen).concat("...");
		}

		return strResult;
	}

	/**	  
	 * @param 
	 * 		boolStr : "Y" or "N" string as JSON response
	 * @return
	 * 		proper boolean value
	 */
	public static boolean getBooleanFromString(String boolStr)
	{
		if(boolStr.equals("Y"))
			return true;
		else if(boolStr.equals("N"))
			return false;

		System.out.println("undefined boolean string!");
		return false;
	}


	/**
	 * 
	 * @param srcStr - source string
	 * @param strTarget - substring which should be replaced
	 * @param strReplace - replace string
	 * @return
	 */
	public static String replaceString(String srcStr , String strTarget , String strReplace){
		String result = new String("");

		//search target string's index in srcString
		int nextKeyIndex = srcStr.indexOf(strTarget);  
		int beforeKeyIndex = 0;

		while(nextKeyIndex != -1){
			String addString = srcStr.substring(beforeKeyIndex, nextKeyIndex) + strReplace;
			result += addString;			
			beforeKeyIndex = nextKeyIndex + strTarget.length();

			nextKeyIndex = srcStr.indexOf(strTarget, beforeKeyIndex);
		}
		//last sub string.
		int len = srcStr.length();
		result += srcStr.substring(beforeKeyIndex , len);
		if(result.equals(""))
			result = srcStr;
		return result;
	}

	public static boolean isNetworkUrl(String url)
	{
		if(url.startsWith("http:") || url.startsWith("https:"))
			return true;
		else
			return false;
	}
}
