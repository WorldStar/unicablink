package application.utils;

import java.util.Timer;

import javax.microedition.location.Criteria;
import javax.microedition.location.Location;
import javax.microedition.location.LocationException;
import javax.microedition.location.LocationListener;
import javax.microedition.location.LocationProvider;



public class GetLatLon {
	private LocationProvider provider,_locationProvider;
	public Timer timer;
	double longitude=0.0, lattitude=0.0;
	Criteria criteria;
	
	public double getLat() {
		return lattitude;
	}
	
	public double getLon() {
		return longitude;
	}
	
	public GetLatLon()
	{   
	    startLocationUpdate();
	}

	public  class LocationListenerImpl implements LocationListener {
	    public void locationUpdated(LocationProvider provider, final Location location) {
	        System.out.println("---Location Updated-----");
	        if (location.isValid()) {
	            System.out.println("---Location Valid----");
	                    longitude = location.getQualifiedCoordinates().getLongitude();
	                    lattitude= location.getQualifiedCoordinates().getLatitude();
	                    System.out.println("Lattitude :-=============================="+lattitude);
	                    System.out.println("Longitude :-=============================="+longitude);
	                    _locationProvider.setLocationListener(null, 0, 0, 0);
	            }else{
	            System.out.println("else NOT valid--Cellsite valide=-----");
	            setupCriteria();
	            setupProvider();
	        }
	        System.out.println("---Location Not Valid----");
	    }
	    public void providerStateChanged(LocationProvider provider, int newState) {}
	}

	private void setupCriteria() {
	    criteria = new Criteria();
	    criteria.setCostAllowed(true);
	    criteria.setHorizontalAccuracy(Criteria.NO_REQUIREMENT);
	    criteria.setVerticalAccuracy(Criteria.NO_REQUIREMENT);
	    criteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_LOW);
	}


	private void setupProvider() {
	    try {
	        try {
	            Thread.sleep(5000);
	        } catch (InterruptedException e) {
	           System.out.println(e.toString());
	        }
	        provider = LocationProvider.getInstance(criteria);
	        provider.setLocationListener(
	                new LocationListenerImpl(), 1, 1, 1);
	    } catch (LocationException t) {
	        System.out.println(t.toString());
	    }
	}
	public void startLocationUpdate() 
	{
	    try{
	        _locationProvider = LocationProvider.getInstance(null);
	        if (_locationProvider != null) {
	            _locationProvider.setLocationListener(
	                    new LocationListenerImpl(), 1, 1, 1);
	        } 
	    }catch(LocationException le){
	        System.out.println("----Exception Of Location--"+le);
	    }
	}
}
