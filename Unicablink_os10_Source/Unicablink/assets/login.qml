import bb.cascades 1.0

NavigationPane {
    id: navigationPane
    objectName: "navigationPane"
    Page {
        
        Container {
            id: loginScreenZ10
            
            //z10

            preferredWidth: 768.0
            
            background: Color.create("#f5f5f5")
            layout: StackLayout {

            }

            horizontalAlignment: HorizontalAlignment.Center

            animations: [
                FadeTransition {
                    id: showPageEffect
                    duration: 500
                    toOpacity: 1.0
                    fromOpacity: 0.0
                }
            ]

            
            //Main Contents
            Container {
                id: mainContents

                horizontalAlignment: HorizontalAlignment.Center
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }

                    topPadding: 230.0

                    //logo bar
                    Container {
                        layout: DockLayout {

                        }
                        horizontalAlignment: HorizontalAlignment.Center

                        
                        ImageView {
                            preferredWidth: 336.0
                            preferredHeight: 368.0
                            
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: "asset:///Image/icon_logo.png"
                            scalingMethod: ScalingMethod.Fill
                        }
                    }

                    //Log In info
                    Container {
                        id: logInInfo

                        horizontalAlignment: HorizontalAlignment.Center

                        preferredWidth: 624.0

                        topPadding: 92.0
                        layoutProperties: AbsoluteLayoutProperties {

                        }
                        Container {
                            id: mainInfo

                            layout: StackLayout {

                            }

                            //Phone No Info
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }

                                verticalAlignment: VerticalAlignment.Center

                                preferredHeight: 86.0

                                background: phoneInfoBackground.imagePaint

                                attachedObjects: [
                                    ImagePaintDefinition {
                                        id: phoneInfoBackground
                                        imageSource: "asset:///Image/bar_insert_big.png"
                                    }
                                ]

                                leftPadding: 30.0
                                rightPadding: 20.0
                                topPadding: 10.0
                                bottomPadding: 10.0
                                Label {
                                    preferredWidth: 140.0
                                    preferredHeight: 68.0

                                    text: "Phone"
                                    textStyle.fontWeight: FontWeight.Default
                                    textStyle.color: Color.Black
                                    textStyle.textAlign: TextAlign.Left
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 7.0
                                    verticalAlignment: VerticalAlignment.Center
                                    textStyle.fontSize: FontSize.PointValue

                                }

                                TextField {
                                    id: phoneText
                                    objectName: "phoneTextField"	
                                    
                                    clearButtonVisible: true
                                    input.submitKey: SubmitKey.Done
                                    textFormat: TextFormat.Plain
                                    textStyle.textAlign: TextAlign.Left
                                    input.masking: TextInputMasking.Masked
                                    verticalAlignment: VerticalAlignment.Center
                                    inputMode: TextFieldInputMode.PhoneNumber
                                    backgroundVisible: false
                                    textStyle.color: Color.Black
                                    text: ""
                                    focusHighlightEnabled: false
                                    hintText: "e.g.+60123456789"
                                    textStyle.fontSize: FontSize.PointValue
                                    textStyle.fontSizeValue: 7.0

                                    onTextChanging: {
                                        eventHandler.correctTextField(phoneText, text);
                                    }
                                }

                            }

                            //Password Info
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }

                                verticalAlignment: VerticalAlignment.Center

                                preferredHeight: 86.0

                                background: passwordInfoBackground.imagePaint

                                attachedObjects: [
                                    ImagePaintDefinition {
                                        id: passwordInfoBackground
                                        imageSource: "asset:///Image/bar_insert_big.png"
                                    }
                                ]

                                leftPadding: 30.0

                                rightPadding: 20.0
                                topPadding: 10.0
                                bottomPadding: 10.0
                                topMargin: 20.0
                                Label {
                                    preferredWidth: 140.0
                                    preferredHeight: 68.0

                                    text: "Password"
                                    textStyle.fontWeight: FontWeight.Default
                                    textStyle.color: Color.Black
                                    textStyle.textAlign: TextAlign.Left
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 7.0
                                    verticalAlignment: VerticalAlignment.Center
                                    textStyle.fontSize: FontSize.PointValue

                                }

                                TextField {
                                    id: passwordText
                                    objectName: "passwordTextField"

                                    clearButtonVisible: true
                                    input.submitKey: SubmitKey.Done
                                    textFormat: TextFormat.Plain
                                    textStyle.textAlign: TextAlign.Left
                                    input.masking: TextInputMasking.Masked
                                    verticalAlignment: VerticalAlignment.Center
                                    inputMode: TextFieldInputMode.Password
                                    backgroundVisible: false
                                    textStyle.color: Color.Black
                                    text: ""
                                    focusHighlightEnabled: false
                                    hintText: "min. 6 (alphanumeric)"
                                    textStyle.fontSize: FontSize.PointValue
                                    textStyle.fontSizeValue: 7.0
                                }

                            }

                            //Sign In Button
                            Container {
                                topMargin: 20.0

                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center

                                layout: DockLayout {

                                }

                                ImageButton {
                                    id: signInButton
                                    defaultImageSource: "asset:///Image/button_greenbar.png"
                                    pressedImageSource: "asset:///Image/button_greenbar.png"

                                    horizontalAlignment: HorizontalAlignment.Center

                                    preferredWidth: 624.0
                                    preferredHeight: 90.0
                                    minHeight: 90.0
                                }

                                Label {
                                    id: signInLabel
                                    textStyle.color: Color.Black
                                    text: "Sign In"

                                    textStyle.textAlign: TextAlign.Center
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 8.0
                                    horizontalAlignment: HorizontalAlignment.Center
                                    verticalAlignment: VerticalAlignment.Center
                                    textStyle.fontSize: FontSize.PointValue

                                }

                                onTouch: {
                                    
                                    
									if (event.isDown()){
									    validateUser();
									    
                                        //requestDisplayTabedMainPage();
                                    }
                                }

                                
                                

                            }

                            //Sign Up Button
                            Container {
                                id: signUpContainer

                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight

                                }
                                topMargin: 70.0

                                horizontalAlignment: HorizontalAlignment.Center
                                Label {
                                    text: "Don't have an account ?"
                                    textStyle.textAlign: TextAlign.Center
                                    textStyle.fontFamily: "Consolas"
                                    textStyle.fontSizeValue: 8.0
                                    verticalAlignment: VerticalAlignment.Center
                                    textStyle.color: Color.Black
                                    textStyle.fontSize: FontSize.PointValue

                                }

                                ImageButton {
                                    id: signUpButton
                                    defaultImageSource: "asset:///Image/signup_z.png"
                                    pressedImageSource: "asset:///Image/signup_z.png"
                                    

									onClicked: {
                                        var signUpPage = signUpDefinition.createObject();
                                        navigationPane.push(signUpPage);
                                    }
									
                                    attachedObjects: [
                                        // Create the ComponentDefinition that represents the custom
                                        // component in myPage.qml
                                        ComponentDefinition {
                                            id: signUpDefinition
                                            source: "register.qml"
                                        }
                                    ]
                                    verticalAlignment: VerticalAlignment.Bottom
                                }

                            }

                            }

                            //Forgot Button
                            Container {
                                id: forgotContainer

                                layout: StackLayout {

                                }

                                topMargin: 10.0

                                horizontalAlignment: HorizontalAlignment.Center
                                ImageButton {
                                    id: forgotButton
                                    defaultImageSource: "asset:///Image/forgotpassword_z.png"
                                    pressedImageSource: "asset:///Image/forgotpassword_z.png"

                                    onClicked: {
                                    	var forgotPage = forgotpageDefine.createObject();
                                    	navigationPane.push(forgotPage);
                                    }	
                                

		                            attachedObjects: [
		                                // Create the ComponentDefinition that represents the custom
		                                // component in myPage.qml
		                                ComponentDefinition {
		                                    id: forgotpageDefine
		                                    source: "forgotpassword.qml"
		                                }
		                            ]
                                }
                            }
                        }
                    }
                }
            }

        }

    onPopTransitionEnded: {
        page.destroy();
    }

    
    //Validate User
    function validateUser() {
        eventHandler.ValidateUser(phoneText.text, passwordText.text);
    }
    
    //display Tabed Main Page
    function requestDisplayTabedMainPage() {
        eventHandler.ShowTabPage();
    }
}
