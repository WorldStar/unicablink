package application;

import javax.microedition.location.LocationException;

import org.json.me.JSONArray;

import application.location.SimpleLocationProvider;
import application.setting.SettingStore;
import application.utils.GetLatLon;
import application.utils.HandleGPS;
import application.utils.HandleGPSNew;

import view.screens.ActivateScreen;
import view.screens.AddressScreen;
import view.screens.BookingScreen;
import view.screens.ContactScreen;
import view.screens.CurrentBookingPage;
import view.screens.ForgotScreen;
import view.screens.HistoryScreen;
import view.screens.LoginScreen;
import view.screens.MainBaseScreen;
import view.screens.ProfileScreen;
import view.screens.RegisterScreen;
import view.screens.SplashScreen;
import model.data.DataConstant;
import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;

public class Unicablink extends UiApplication{
	/**
	 * public members
	 */
	public static String mStrName = "";
	public static String mStrPhoneNo = "";
	public static String mStrEmail = "";
	public static String mStrPassword = "";

	public static boolean bModified = false;
	public static JSONArray mOrderArray = new JSONArray();

	public static String strSelectedOrderId = "";
	
	public static double dPickupLat = 0;
	public static double dPickupLon = 0;
	public static double dDropOffLat = 0;
	public static double dDropOffLon = 0;
	
	public static String strPickupLocation = "";
	public static String strDropOffLocation = "";
	
	public static int nFlagScreen = DataConstant.FROM_PICKUP_CONTROL;
	
	public static double dTempLat = 0;
	public static double dTempLon = 0;
	public static String mStrTempLocation = "";
	public static boolean bNotFoundLocation = false;
	public static boolean bCheckedLocation = false;
	
	public static HandleGPS mHandleGPS = new HandleGPS();
	
	//public static HandleGPSNew mHandleGPSNew = new HandleGPSNew();

	public static double dCurrentLat = 0;
	public static double dCurrentLon = 0;
	
	/**
	 * Screens
	 */
	public SplashScreen mSplashScreen;
	public ActivateScreen mActivateScreen;
	public AddressScreen mAddressScreen;
	public BookingScreen mBookingScreen;
	public ContactScreen mContactScreen;
	public ForgotScreen mForgotScreen;
	public HistoryScreen mHistoryScreen;
	public LoginScreen mLoginScreen;
	public ProfileScreen mProfileScreen;
	public RegisterScreen mRegisterScreen;
	public CurrentBookingPage mCurrentBookingScreen;


	public MainScreen mPreviousScreen = null;

	/**
	 * Settings for app
	 */
	public static SettingStore mSettingStore = new SettingStore();

	/**
	 * Settings for Screens
	 */



	/**
	 * Entry point for application
	 * @param args Command line arguments (not used)
	 */
	public static void main(String[] args){
		Unicablink theApp = new Unicablink();       
		theApp.enterEventDispatcher();
	}

	/**
	 * Creates a new StadiumAstro object
	 */
	public Unicablink()
	{
		Utils.init();
		mSplashScreen = new SplashScreen();
		pushScreen(mSplashScreen);
	}

	/**
	 * Navigation Flows
	 */
	
	/**
	 * Go Start Screen
	 */
	public void goStartScreen()
	{
		//Pop Splash Screen
		popScreen(mSplashScreen);

		boolean bLoggedIn = mSettingStore.getAppSetting().getLoggedInfo();
		boolean bVerified = mSettingStore.getAppSetting().getVerificationInfo();

		
		if (bLoggedIn)
		{
			goBookingScreen();
		}
		else if(bVerified)
		{
			goActivateScreen();
		}
		else
		{
			goLoginScreen();
		}
	}

	/**
	 * Get Setting Store
	 */
	public SettingStore getSettingStore()
	{
		return mSettingStore;
	}

	/**
	 * Go Login Screen
	 */
	public void goLoginScreen()
	{
		mLoginScreen = new LoginScreen();
		pushScreen(mLoginScreen);
	}

	/**
	 * Go Forgot Screen
	 */
	public void goForgotScreen()
	{
		mForgotScreen = new ForgotScreen();
		pushScreen(mForgotScreen);
	}

	/**
	 * Go Register Screen
	 */
	public void goRegisterScreen()
	{
		mRegisterScreen = new RegisterScreen();
		pushScreen(mRegisterScreen);
	}

	/**
	 * Go Activation Screen
	 */
	public void goActivateScreen()
	{
		mActivateScreen = new ActivateScreen();
		pushScreen(mActivateScreen);
	}

	public void popOldScreen()
	{
		popScreen(mLoginScreen);
		popScreen(mRegisterScreen);
	}

	public void reActivate()
	{
		popScreen(mRegisterScreen);
		popScreen(mPreviousScreen);
		goActivateScreen();
	}
	/**
	 * Go Booking Screen
	 */
	public void initializeSetting(){
		dPickupLat = 0;
		dPickupLon = 0;
		dDropOffLat = 0;
		dDropOffLon = 0;
		strPickupLocation = "";
		strDropOffLocation = "";
		nFlagScreen = DataConstant.FROM_PICKUP_CONTROL;
		dTempLat = 0;
		dTempLon = 0;
		mStrTempLocation = "";
	}
	
	
	public void goBookingScreen()
	{
		if (!bCheckedLocation && !mHandleGPS.bFinished){
			try {
	            Thread.sleep(500);
	        } catch (InterruptedException e) {
	           System.out.println(e.toString());
	        }
			goBookingScreen();
			return;
		}
		
		mBookingScreen = new BookingScreen();
		
		pushScreen(mBookingScreen);
		
		if (bNotFoundLocation || mHandleGPS.bFinished){
			Dialog.alert("Location Provider failed. Please check BIS.");
		}

		mPreviousScreen = (MainScreen)mBookingScreen;
	}

	public void switchBookingScreen()
	{
		bCheckedLocation = false;
		bNotFoundLocation = true;
		
		mHandleGPS = new HandleGPS();
		
		showBookingPage();
	}
	
	private void showBookingPage(){
		if (!bCheckedLocation  && !mHandleGPS.bFinished){
			try {
	            Thread.sleep(500);
	        } catch (InterruptedException e) {
	           System.out.println(e.toString());
	        }
			showBookingPage();
			return;
		}
		
		if (mPreviousScreen != null)
		{
			popScreen(mPreviousScreen);
		}

		//Initialize all setting for booking
		initializeSetting();
		
		//Create Booking Screen
		mBookingScreen = null;
		mBookingScreen = new BookingScreen();

		
		pushScreen(mBookingScreen);
		if (bNotFoundLocation || mHandleGPS.bFinished){
			Dialog.alert("Location Provider failed. Please check BIS.");
		}
		
		mPreviousScreen = (MainScreen)mBookingScreen;
	}

	public void popLoginScreen()
	{
		popScreen(mLoginScreen);
	}

	public void goBookingForAgain()
	{
		if (mCurrentBookingScreen != null)
			popScreen(mCurrentBookingScreen);

		//Delete Current Screen
		mCurrentBookingScreen = null;

		if (mBookingScreen == null)
			mBookingScreen = new BookingScreen();
		//start modified by KO
		else{
			strPickupLocation = "";
			strDropOffLocation = "";
			
			mBookingScreen.initBookingScreen();
		}
		//  end modified by KO

		pushScreen(mBookingScreen);
		mPreviousScreen = (MainScreen)mBookingScreen;
	}
	/**
	 * Go Address Screen
	 */
	public void goAddressScreen()
	{
		if (mAddressScreen != null)
			mAddressScreen = null;
		
		mAddressScreen = new AddressScreen();
		pushScreen(mAddressScreen);
	}

	public void popAddressScreen()
	{
		popScreen(mAddressScreen);
		
		mAddressScreen = null;
	}
	/**
	 * Go History Screen
	 */
	public void switchHistoryScreen()
	{
		if (mPreviousScreen != null)
		{
			popScreen(mPreviousScreen);
		}

		if (mHistoryScreen == null)
			mHistoryScreen = new HistoryScreen();

		pushScreen(mHistoryScreen);
		mPreviousScreen = (MainScreen)mHistoryScreen;
	}

	public void goHistoryScreen()
	{
		if (mCurrentBookingScreen != null)
			popScreen(mCurrentBookingScreen);

		//Delete Current Screen
		mCurrentBookingScreen = null;

		if (mHistoryScreen != null)
		{
			mHistoryScreen = null;
		}
		if (mHistoryScreen == null)
			mHistoryScreen = new HistoryScreen();

		pushScreen(mHistoryScreen);
		mPreviousScreen = (MainScreen)mHistoryScreen;
	}

	/**
	 * Go Contact Screen
	 */
	public void switchContactScreen()
	{
		if (mPreviousScreen != null)
		{
			popScreen(mPreviousScreen);
		}

		if (mContactScreen == null)
			mContactScreen = new ContactScreen();

		pushScreen(mContactScreen);
		mPreviousScreen = (MainScreen)mContactScreen;
	}

	/**
	 * Go Profile Screen
	 */
	public void switchProfileScreen()
	{
		if (mPreviousScreen != null)
		{
			popScreen(mPreviousScreen);
		}

		if (mProfileScreen == null)
			mProfileScreen = new ProfileScreen();

		pushScreen(mProfileScreen);
		mPreviousScreen = (MainScreen)mProfileScreen;
	}

	/**
	 * Go My Booking Screen
	 */
	public void goDetailsScreen(boolean bAutoRefresh)
	{
		if (mPreviousScreen != null)
			popScreen(mPreviousScreen);

		if (mCurrentBookingScreen == null)
			mCurrentBookingScreen = new CurrentBookingPage(bAutoRefresh);

		pushScreen(mCurrentBookingScreen);
	}

}
