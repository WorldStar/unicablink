package model.request;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.utils.DateUtils;

public class OrderStatus {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strPhoneNo = "";
	private JSONArray m_aJSONOrder = new JSONArray();

	public OrderStatus(){

	}

	/*
	 * {
	 *		"ORDERArray":[{"ORDERID":"75501","REQUESTCOUNT":0}],
	 *		"MOBILE":"+8613862039617",
	 *		"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 *		"REQUESTTIME":"2013-08-12 11:50:01"
	 * }
	 */
	public void setPhoneNo(String strPhoneNo)
	{
		m_strPhoneNo = strPhoneNo;
	}

	public void appendOrder(JSONObject jsonOrder)
	{
		m_aJSONOrder.put(jsonOrder);
	}

	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();

		//Get Current time
		String strCurrentTime = DateUtils.getCurrentTimeString();

		try{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("MOBILE", m_strPhoneNo);
			jsonResult.put("ORDERArray", m_aJSONOrder);
			jsonResult.put("REQUESTTIME", strCurrentTime);
		}
		catch(JSONException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonResult;
	}


}
