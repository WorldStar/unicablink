package application.utils;

import net.rim.device.api.gps.BlackBerryCriteria;
import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.gps.BlackBerryLocationProvider;
import net.rim.device.api.gps.GPSInfo;

import javax.microedition.location.*;

public class HandleGPSNew
{
    static BlackBerryLocationProvider myProvider;

	public static double latitude = 0;
	public static double longitude = 0;
	
    public HandleGPSNew()
    {
        try
        {
        	/*
        	 * BlackBerryCriteria myCriteria = new BlackBerryCriteria();
			//myCriteria.setCostAllowed(false);
			
			try{
				if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_CELLSITE)){
					myCriteria.setMode(GPSInfo.GPS_MODE_CELLSITE);
				}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_ASSIST)){
					myCriteria.setMode(GPSInfo.GPS_MODE_ASSIST);
				}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_AUTONOMOUS)){
					myCriteria.setMode(GPSInfo.GPS_MODE_AUTONOMOUS);
				}else{
					myCriteria.setCostAllowed(true);
					myCriteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_LOW);
				}

				BlackBerryLocationProvider myLocationProvider = (BlackBerryLocationProvider) LocationProvider.getInstance(myCriteria);
				BlackBerryLocation myLocation = (BlackBerryLocation) myLocationProvider.getLocation(30); //Set time out to 30
				latitude  = myLocation.getQualifiedCoordinates().getLatitude();
				longitude = myLocation.getQualifiedCoordinates().getLongitude();
        	 */
        	
            BlackBerryCriteria myCriteria = new BlackBerryCriteria();
			
            //Set Mode
			if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_CELLSITE)){
				myCriteria.setMode(GPSInfo.GPS_MODE_CELLSITE);
			}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_ASSIST)){
				myCriteria.setMode(GPSInfo.GPS_MODE_ASSIST);
			}else if(GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_AUTONOMOUS)){
				myCriteria.setMode(GPSInfo.GPS_MODE_AUTONOMOUS);
			}else{
				myCriteria.setCostAllowed(true);
				myCriteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_LOW);
			}

            try
            {
                myProvider = (BlackBerryLocationProvider)LocationProvider.getInstance(myCriteria);
                myProvider.setLocationListener(new handleGPSListener(), 10, -1, -1);
            }
            catch (LocationException lex )
            {
            	System.out.println(lex.toString());
                return;
            	
			} catch (Exception e) {
				System.out.println(e.toString());
				return;
			}

            myProvider.pauseLocationTracking(30);
            myProvider.resumeLocationTracking();
            myProvider.stopLocationTracking();
        }
        catch ( UnsupportedOperationException uoex )
        {
        }

        return;
    }

    public static class handleGPSListener implements LocationListener
    {
        public void locationUpdated(LocationProvider provider, Location location)
        {
            if (location.isValid())
            {
                //get lat and lon
				latitude  = location.getQualifiedCoordinates().getLatitude();
				longitude = location.getQualifiedCoordinates().getLongitude();
            }
            else
            {
                // invalid location
            }
        }

        public void providerStateChanged(LocationProvider provider, int newState)
        {
            if (newState == LocationProvider.AVAILABLE)
            {
                // available
            }
            else if (newState == LocationProvider.OUT_OF_SERVICE)
            {
                // GPS unavailable due to IT policy specification
            }
            else if (newState == LocationProvider.TEMPORARILY_UNAVAILABLE )
            {
                // no GPS fix
            }
        }
    }
}

