import bb.cascades 1.0

Page {
    Container {
        id: contactContainer
        preferredWidth: 768.0
        preferredHeight: 1280.0

        background: Color.create("#f5f5f5")
        layout: StackLayout {

        }

        animations: [
            FadeTransition {
                id: showContactPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        //Main Contents
        Container {
            id: topContactBar

            preferredHeight: 112.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            ImageView {
                id: topContactBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 112.0
                preferredWidth: 768.0
            }
            Label {
                text: "Contact"
                textStyle.fontWeight: FontWeight.Bold
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"

                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.PointValue
                textStyle.fontSizeValue: 9.0
            }

        } //end of topbar

        Container {
            topMargin: 48.0

            horizontalAlignment: HorizontalAlignment.Center

            ImageView {
                minWidth: 314.0
                minHeight: 96.0
                maxWidth: 314.0
                maxHeight: 96.0
                imageSource: "asset:///Image/icon_logo2.png"
                horizontalAlignment: HorizontalAlignment.Center

            } //end of logo of Unicablink
        } //end of logo mark

        Container {
            topMargin: 54.0
            horizontalAlignment: HorizontalAlignment.Right

            layout: DockLayout {

            }

            ImageButton {
                preferredWidth: 660.0
                preferredHeight: 314.0
                defaultImageSource: "asset:///Image/btn_contact1.png"
                pressedImageSource: "asset:///Image/btn_contact1.png"

                onClicked: {
                    eventHandler.VoiceCall();
                }
            }

            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom
                }

                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Right

                rightPadding: 126.0

                Label {
                    id: callusLabel

                    text: "Call Us"
                    textStyle.fontWeight: FontWeight.Bold
                    textStyle.color: Color.Black
                    textStyle.textAlign: TextAlign.Center
                    textStyle.fontSize: FontSize.PointValue
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSizeValue: 9.0
                }

                Label {
                    text: "1300-800-222"
                    textStyle.fontWeight: FontWeight.Normal
                    textStyle.color: Color.Black
                    textStyle.textAlign: TextAlign.Center
                    
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSize: FontSize.PointValue
                    textStyle.fontSizeValue: 6.0
                }
            }

            onTouch: {
                if (event.isUp()) {
                    eventHandler.VoiceCall();
                }
            }
        } //end of Call us

        Container {
            topMargin: 70.0
            horizontalAlignment: HorizontalAlignment.Left

            layout: DockLayout {

            }

            ImageButton {
                preferredWidth: 668.0
                preferredHeight: 314.0
                defaultImageSource: "asset:///Image/btn_contact2.png"
                pressedImageSource: "asset:///Image/btn_contact2.png"

                onClicked: {
                    eventHandler.SendEmail();
                }
            }

            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom
                }

                verticalAlignment: VerticalAlignment.Center

                leftPadding: 106.0

                horizontalAlignment: HorizontalAlignment.Left
                Label {
                    id: emailusLabel

                    text: "Email Us"
                    textStyle.fontWeight: FontWeight.Bold
                    textStyle.color: Color.Black
                    textStyle.textAlign: TextAlign.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSize: FontSize.PointValue
                    textStyle.fontSizeValue: 9.0
                }

                Label {
                    text: "customerservices\n@unicablink.com"
                    textStyle.fontWeight: FontWeight.Normal
                    textStyle.color: Color.Black
                    textStyle.textAlign: TextAlign.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.fontSize: FontSize.PointValue
                    multiline: true
                    textStyle.fontSizeValue: 6.0
                }
            }
            onTouch: {
                if (event.isUp()) {
                    eventHandler.SendEmail();
                }
            }
        } //end of Email us
    }
}