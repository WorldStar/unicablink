package application.location;

import javax.microedition.location.LocationException;

import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

public class MySimpleLocationProvider extends SimpleLocationProvider{
	
	private BlackBerryLocation location = null;
	
	public MySimpleLocationProvider() throws LocationException,
			IllegalStateException {
		super();
	}

	public BlackBerryLocation getLocation(int process_time) {
		location = super.getLocation(process_time);
		
		//if (location != null || location.isValid()) {
			//navigator.displayRoute();
		//} else {
		if(location==null || (location!=null && !location.isValid()) || (location!=null && location.getQualifiedCoordinates().getLatitude()==0 && location.getQualifiedCoordinates().getLongitude()==0)){
		
			UiApplication.getUiApplication().invokeLater(new Runnable() {
				public void run() {
					Dialog.alert("Failed to locating you current location! Please try again in open area!");
				}
			});
		} else {
			System.out.println("Lat: " + location.getQualifiedCoordinates().getLatitude() + " Long: " + location.getQualifiedCoordinates().getLongitude());
		}
		return location;
	}

}
