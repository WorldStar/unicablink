package view.screens;

import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class BaseScreen extends MainScreen {

	/**
	 * 
	 */
	public BaseScreen() {
		super(MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR | USE_ALL_HEIGHT | USE_ALL_WIDTH);
		// TODO Auto-generated constructor stub
		
		//set Manager's background color
		getMainManager().setBackground(BackgroundFactory.createSolidBackground(0xf5f5f5));
		
		
	}

}
