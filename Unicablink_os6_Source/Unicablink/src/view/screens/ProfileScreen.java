package view.screens;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.ModifyProfile;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import view.components.AccountInfoPasswordField;
import view.components.AccountInfoTextField;
import view.components.ImageButtonField;
import view.fieldmanager.InfoManager;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import application.setting.AppSetting;
import application.setting.SettingStore;
import application.utils.StringUtils;

public class ProfileScreen extends MainBaseScreen {

	/**
	 * Private members
	 */
	AccountInfoTextField nameInfo;
	AccountInfoTextField emailInfo;
	AccountInfoTextField phoneInfo;
	
	AccountInfoPasswordField currentPasswordInfo;
	AccountInfoPasswordField newPasswordInfo;
	AccountInfoPasswordField retypeInfo;

	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;
	
	public ProfileScreen() {
		super();

		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.PROFILE_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));

		/**
		 * Register Info Manager
		 */
		InfoManager oldInfoManager = new InfoManager(0, ResouceConstant.PROFILE_TOP_PADDING, 0, 0);
		InfoManager passwordInfoManager = new InfoManager(0, ResouceConstant.PROFILE_CONTENT_PADDING, 0, 0);
		InfoManager buttonManager = new InfoManager(0, ResouceConstant.PROFILE_BUTTON_PADDING, ResouceConstant.PROFILE_BTM_PADDING, 0);
		
		//Get Account Info
		AppSetting appSetting = mAppSettingStore.getAppSetting();
		
		String strName = appSetting.getAccountName();
		String strEmail = appSetting.getAccountEmail();
		String strPhone = appSetting.getAccountPhone();
		
		nameInfo = new AccountInfoTextField(ResouceConstant.PROFILE_BG_TOP, ResouceConstant.PROFILE_BG_TOP_H, 0, 0, 34, 20, "Name", "", strName, 
				168, EditField.FILTER_DEFAULT, 20, 30, false);
		emailInfo = new AccountInfoTextField(ResouceConstant.PROFILE_BG_MIDDLE, ResouceConstant.PROFILE_BG_MIDDLE_H, 0, 0, 34, 20, "Email", "", strEmail, 
				168, EditField.FILTER_EMAIL, 25, 30, false);
//					AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, ResouceConstant.DETAILS_BAR_MIDDLE_SEL,0, 0, 20, 10, "Pick up", "", strPickup,
//				152, EditField.NO_NEWLINE, 200, 22, true)
//		phoneInfo = new AccountInfoTextField(ResouceConstant.PROFILE_BG_BTM, ResouceConstant.PROFILE_BG_BTM_H, 0, 0, 34, 20, "Phone", "", strPhone, 
//				168, EditField.FILTER_PHONE, 15, 30, false)
		phoneInfo = new AccountInfoTextField(ResouceConstant.PROFILE_BG_BTM, ResouceConstant.PROFILE_BG_BTM_H, 0, 0, 34, 20, "Phone", "", strPhone, 
				168, EditField.FILTER_DEFAULT, 15, 30, false)
		{
			protected boolean navigationClick(int status, int time) 
			{
				if (status != 0){
					fieldChangeNotify(0);
					clickField();
				}
				return true;
			}

			public boolean keyChar(char key, int status, int time) 
			{
				if (key == Characters.ENTER) {
					fieldChangeNotify(0);
					clickField();
					return true;
				}
				return false;
			}
			protected boolean trackwheelClick(int status, int time)
			{        
				if (status != 0) clickField();    
				return true;
			}

			protected boolean touchEvent(TouchEvent message)
			{
				int x = message.getX( 1 );
				int y = message.getY( 1 );
				if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
					// Outside the field
					return false;
				}
				switch( message.getEvent() ) {
				case TouchEvent.UNCLICK:
					clickField();
					return true;
				}
				return super.touchEvent( message );
			}
			
			public void clickField()
			{
				Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
				theApp.bModified = true;
				theApp.goRegisterScreen();
			}
		};
		phoneInfo.setEditable(false);
		
		Bitmap bmpPasswordInfo =  Utils.getBitmapResource(ResouceConstant.PROFILE_BG_GREY_TOP);
		
		currentPasswordInfo = new AccountInfoPasswordField(ResouceConstant.PROFILE_BG_MIDDLE, ResouceConstant.PROFILE_BG_MIDDLE_H, 0, 0, 34, 20, "Current Password", "Must be 6 digits", "", 
				240, EditField.FILTER_DEFAULT, 6, 30, false);
		newPasswordInfo = new AccountInfoPasswordField(ResouceConstant.PROFILE_BG_MIDDLE, ResouceConstant.PROFILE_BG_MIDDLE_H, 0, 0, 34, 20, "New Password", "Must be 6 digits", "", 
				240, EditField.FILTER_DEFAULT, 6, 30, false);
		retypeInfo = new AccountInfoPasswordField(ResouceConstant.PROFILE_BG_BTM, ResouceConstant.PROFILE_BG_BTM_H, 0, 0, 34, 20, "Retype Password", "Must be 6 digits", "", 
				240, EditField.FILTER_DEFAULT, 6, 30, false);
		
		ImageButtonField btnSave = new ImageButtonField(ResouceConstant.PROFLIE_BTN_SAVE, "", 0, "")
		{
			public void clickButton()
			{
				RegisterProfile();
			}
		};
		
		//Add fields to oldInfoManager
		oldInfoManager.add(nameInfo);
		oldInfoManager.add(emailInfo);
		oldInfoManager.add(phoneInfo);
		
		//Add fields to passwordInfoManager
		passwordInfoManager.add(new BitmapField(bmpPasswordInfo));
		passwordInfoManager.add(currentPasswordInfo);
		passwordInfoManager.add(newPasswordInfo);
		passwordInfoManager.add(retypeInfo);
		
		//Add Button to buttonManager
		buttonManager.add(btnSave);
		
		/**
		 * Add manager to MainManager
		 */
		add(oldInfoManager);
		add(passwordInfoManager);
		add(buttonManager);
	}

	/**
	 * User Interaction Process
	 */
	public void RegisterProfile()
	{
		/**
		 * Get field's value
		 */
		String strName = nameInfo.getText();
		String strEmail = emailInfo.getText();
		String strPhone = phoneInfo.getText();
		String strCurrentPassword = currentPasswordInfo.getText();
		String strNewPassword = newPasswordInfo.getText();
		String strRetype = retypeInfo.getText();
		
		/**
		 * Get old profile info
		 */
		AppSetting appSetting = mAppSettingStore.getAppSetting();
		
		String strOldPassword = appSetting.getAccountPassword();
		String strCustomerId = appSetting.getAccountCustomerID();
		String strOldEmail = appSetting.getAccountEmail();
		String strOldName = appSetting.getAccountName();
		String strOldPhone = appSetting.getAccountPhone();
		
		/**
		 * Check values
		 */
		if (!strOldPassword.equals(strCurrentPassword))
		{
			Dialog.alert("Incorrect Current Password.");
			return;
		}
		
		if (strName.length() == 0)
		{
			Dialog.alert("Name cannot be empty.");
		}
		
		if (!StringUtils.isEmailFormat(strEmail))
		{
			return;
		}
		
		if (!StringUtils.isPhoneNoFormat(strPhone))
		{
			return;
		}
		
		if (!StringUtils.checkPassword(strNewPassword, strRetype))
		{
			return;
		}
		
		//Create JSON Request
		ModifyProfile jsonModify = new ModifyProfile();
		
		/*
		 * {
				"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
				"NAME":"andy",
				"MOBILE":"+8613862039617",
				"OLDPASSWORD":"",
				"NEWPASSWORD":"",
				"EMAIL":"503488188@qq.com",
				"PHONETYPE":2,
				"CUSTOMERID":"64A8C009-BA90-8EE2-6B03D9C70C8F5D9B"
		 * }
		 */
		jsonModify.setCID(strCustomerId);
		
		if (strOldEmail.equals(strEmail))
		{
			jsonModify.setEmailAddress("");
		}
		else
		{
			jsonModify.setEmailAddress(strEmail);
		}
		
		if (strCurrentPassword.equals(strNewPassword))
		{
			jsonModify.setOldPassword("");
			jsonModify.setNewPassword("");
		}
		else
		{
			jsonModify.setOldPassword(strCurrentPassword);
			jsonModify.setNewPassword(strNewPassword);
		}
		
		if (strOldName.equals(strName))
		{
			jsonModify.setName("");
		}
		else
		{
			jsonModify.setName(strName);
		}
		
		if (strOldPhone.equals(strPhone))
		{
			jsonModify.setPhoneNo("");
		}
		else
		{
			jsonModify.setPhoneNo(strPhone);
		}
		
		//Get the response
		JSONObject jsonResult = null;
		
		String strJSONModify = jsonModify.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONModify, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_UPDATE_PROFILE);
		
		if (jsonResult == null)
		{
			return;
		}
		
		
		/*
		 * response:
		 * 
		 * {
		 * 		"SecurityKey":"e92684cb325f1e4d4cce27c0207562bf",
		 * 		"Status":"1",
		 * 		"ResponseTime":"2013-08-12 13:20:19"
		 * }
		 */
		
		int nStatus;
		
		try 
		{
			nStatus = jsonResult.getInt("Status");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			Dialog.alert("Failed recognition of response.");
			return;
		}
		
		if (nStatus == 1)
		{
			//in case of Success
			Dialog.alert("Successfully updated.");
		}
		else 
		{
			Dialog.alert("Failed update, please try again later.");
		}
	}
}
