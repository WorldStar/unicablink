package view.screens;

import view.components.ImageButtonField;
import application.ResouceConstant;
import application.Utils;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;

public class ConfirmDialog extends Dialog {
	
	private LabelField labelMessage;
	private ImageButtonField buttonHere;
	
	public ConfirmDialog()
    {
        super(Dialog.D_OK, "Booking", 1, null, Manager.FOCUSABLE);

        labelMessage = new LabelField("TQ for using Unicablink App.\nYou are making adv confirmed booking. Fare will be based on sucharge RM15 + taxi meter (On Call = RM2, Starting = RM3, Distance / Time charge, Midnight charge = Additional 50%) + Car Park Fee / Toll, if in curred, For details, please refer to");
        Font mFontLabel = Utils.getFont(Font.PLAIN, 29);
        
        labelMessage.setFont(mFontLabel);
        
        add(labelMessage);
        
       buttonHere = new ImageButtonField(ResouceConstant.BOOKING_DIALOG_BTN_HERE, "", 0, "") {
    	   public void clickButton() {
    		   Browser.getDefaultSession().displayPage("http://www.sunlighttaxi.com/thing-to-note-before-making-a-online-taxi-booking");
    	   }
       };
       add(buttonHere);
    }
}
