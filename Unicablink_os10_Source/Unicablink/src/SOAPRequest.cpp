/*
 * SOAPRequest.cpp
 *
 *  Created on: Aug 7, 2013
 *      Author: Passion
 */

#include "SOAPRequest.h"

using namespace bb::cascades;

//Request type
typedef enum
{
	//User log in request
	USER_LOG_IN = 1, //ok
	//user register request
	USER_REGISTER = 2, //ok
	//user activate request
	USER_ACTIVATE = 3, //cannot
	//resend activation code request
	RESEND_ACTIVATION_CODE = 4, //cannot
	//retrieve password request
	RETRIEVE_PASSWORD = 5, //pending
	//order cab request
	ORDER_CAB = 6, //strange
	//modify profile
	MODIFY_PROFILE = 7, //internal error
	//cancel order
	CANCEL_ORDER = 8, //cannot
	//order status request
	ORDER_STATUS_UPDATE = 9, //cannot
	//query position info request
	QUERY_POSITION_INFO = 10,
	//Search Location
	SEARCH_LOCATION = 11
} request_type;

SOAPRequest::SOAPRequest(QObject* parent) {
	// TODO Auto-generated constructor stub
	m_parent = parent;

	m_jsonRequest = new JSONReqeust(this);
}

SOAPRequest::~SOAPRequest() {


	if (m_jsonRequest != NULL)
	{
		m_jsonRequest->deleteLater();
	}
}

QString SOAPRequest::getSOAPMessage(QMap<QString, QVariant> params, int nType)
{
	QString strJSON = m_jsonRequest->getJSON(params, nType);

	QString strResult = getSOAPHeader(nType);
	strResult.append(strJSON);
	QString strFooter =getSOAPFooter(nType);
	strResult.append(strFooter);

	return strResult;

}

QString SOAPRequest::getSOAPHeader(int nType)
{
	QString strHeader;

	strHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"http://CabOrdering.WS.App\"><soap:Body>";

	switch (nType){
	case USER_LOG_IN:
	{
		strHeader = strHeader + "<user_login>";
		break;
	}
	case USER_REGISTER:
	{
		strHeader = strHeader + "<user_register>";
		break;
	}
	case USER_ACTIVATE:
	{
		strHeader = strHeader + "<user_login>";
		break;
	}
	case RESEND_ACTIVATION_CODE:
	{
		strHeader = strHeader + "<send_activation_code>";
		break;
	}
	case RETRIEVE_PASSWORD:
	{
		strHeader = strHeader + "<retrieve_password>";
		break;
	}
	case ORDER_CAB:
	{
		strHeader = strHeader + "<cab_order>";
		break;
	}
	case MODIFY_PROFILE:
	{
		strHeader = strHeader + "<update_profile>";
		break;
	}
	case CANCEL_ORDER:
	{
		strHeader = strHeader + "<cancelorder_sunlight>";
		break;
	}
	case ORDER_STATUS_UPDATE:
	{
		strHeader = strHeader + "<get_order_status>";
		break;
	}
	case QUERY_POSITION_INFO:
	{
		strHeader = strHeader + "<getvehiclelastknowntrack>";
		break;
	}
	case SEARCH_LOCATION:
	{
		strHeader = strHeader + "<search_location>";
		break;
	}

	default: {
		break;
	}
	}

	strHeader = strHeader + "<content>";
	return strHeader;
}

QString SOAPRequest::getSOAPFooter(int nType)
{
	//</content><version>AND2.0</version></user_login></soap:Body></soap:Envelope>
	QString strFooter = "</content><version>BB10_2.0</version>";

	switch (nType){
		case USER_LOG_IN:
		{
			strFooter = strFooter + "</user_login>";
			break;
		}
		case USER_REGISTER:
		{
			strFooter = strFooter + "</user_register>";
			break;
		}
		case USER_ACTIVATE:
		{
			strFooter = strFooter + "</user_login>";
			break;
		}
		case RESEND_ACTIVATION_CODE:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</send_activation_code>";
			break;
		}
		case RETRIEVE_PASSWORD:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</retrieve_password>";
			break;
		}
		case ORDER_CAB:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</cab_order>";
			break;
		}
		case MODIFY_PROFILE:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</update_profile>";
			break;
		}
		case CANCEL_ORDER:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</cancelorder_sunlight>";
			break;
		}
		case ORDER_STATUS_UPDATE:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</get_order_status>";
			break;
		}
		case QUERY_POSITION_INFO:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</getvehiclelastknowntrack>";
			break;
		}
		case SEARCH_LOCATION:
		{
			strFooter = "</content><version>AND2.2.0</version>";
			strFooter = strFooter + "</search_location>";
			break;
		}

		default: {
			break;
		}
		}

	strFooter = strFooter + "</soap:Body></soap:Envelope>";
	return strFooter;
}


