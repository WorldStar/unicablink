package model.request;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.utils.DateUtils;

public class CancelOrder {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strOrderID = "";
	
	public void setOrderID(String strOrderID){
		m_strOrderID = strOrderID;
	}
	
	/*
	 * {
					"ORDERID":"75519",
					"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
					"REQUESTTIME":"2013-08-12 13:16:54"
		}
	 */
	
	
	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();
		
		//Get Current time
		String strCurrentTime = DateUtils.getCurrentTimeString();
		
		try
		{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("ORDERID", m_strOrderID);
			jsonResult.put("REQUESTTIME", strCurrentTime);
		}
		catch(JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		return jsonResult;
	}
}
