import bb.cascades 1.0

Page {
    Container {//main booking container
        id: myBookingContainer
        preferredWidth: 768.0
        preferredHeight: 1280.0

        layout: StackLayout {

        }

        background: Color.create("#dcddde")
        animations: [
            FadeTransition {
                id: showBookingPageEffect
                duration: 500
                toOpacity: 1.0
                fromOpacity: 0.0
            }
        ]

        //Top Contents
        Container {
            id: topBookingBar

            preferredHeight: 112.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }

            minHeight: 112.0

            ImageView {
                id: topBookingBarBackground
                imageSource: "asset:///Image/topbar_gradient.png"
                scalingMethod: ScalingMethod.Fill
                preferredHeight: 112.0
                preferredWidth: 768.0
            }
            Label {
                text: "My Booking"
                textStyle.fontWeight: FontWeight.Normal
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Center
                textStyle.fontFamily: "Consolas"
                textStyle.fontSizeValue: 24.0
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
            }

        } //end of topbar

        //tab Contents
        Container {
            id: tabBar

            minHeight: 112.0
            preferredWidth: 768.0

            horizontalAlignment: HorizontalAlignment.Center

            layout: DockLayout {

            }
            
            background: tabBarBackground.imagePaint

            attachedObjects: [
                ImagePaintDefinition {
                    id: tabBarBackground
                    imageSource: "asset:///Image/bar_insert_middle_profile.png"
                    repeatPattern: RepeatPattern.Fill
                }
            ]

            Container {
                id: buttonContainer
                objectName: "buttonContainer"

                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Bottom

                leftPadding: 30.0

				Container {
				    layout: DockLayout {
				        
				    }
				    
                    ImageButton {
                        id: currentButton
                        objectName: "currentButton"
                        defaultImageSource: "asset:///Image/bar_top_grey_my-booking.png"
                        
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center

                        minWidth: 188.0
                    }
                    
                    Label {
                        id: currentLabel
                        
                        text: "Current"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.Black
                    }
                    
                    onTouch: {
                        if (event.isDown()){
                            currentButton.defaultImageSource = "asset:///Image/bar_top_grey_my-booking.png"
                            currentLabel.textStyle.color = Color.Black

                            advancedButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
                            advancedLabel.textStyle.color = Color.White

                            historyButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
                            historyLabel.textStyle.color = Color.White
                        }
                    }
                }
                
                Container {
                    
                    layout: DockLayout {
                        
                    }

                    leftMargin: 8.0
                    ImageButton {
                        id: advancedButton
                        objectName: "advancedButton"
                        leftMargin: 8.0
                        defaultImageSource: "asset:///Image/bar_top_black_my-booking.png"

                        minWidth: 188.0
                    }

                    Label {
                        id: advancedLabel

                        text: "Advanced"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.White
                    }

                    onTouch: {
                        if (event.isDown()) {
                            currentButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
                            currentLabel.textStyle.color = Color.White

                            advancedButton.defaultImageSource = "asset:///Image/bar_top_grey_my-booking.png"
                            advancedLabel.textStyle.color = Color.Black

                            historyButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
                            historyLabel.textStyle.color = Color.White
                        }
                    }
                }

                Container {
                    layout: DockLayout {

                    }

                    leftMargin: 8.0
                    ImageButton {
                        id: historyButton
                        objectName: "historyButton"
                        leftMargin: 8.0
                        defaultImageSource: "asset:///Image/bar_top_black_my-booking.png"

                        minWidth: 188.0
                    }

                    Label {
                        id: historyLabel

                        text: "History"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.White
                    }
                    onTouch: {
                        if (event.isDown()) {
                            currentButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
                            currentLabel.textStyle.color = Color.White

                            advancedButton.defaultImageSource = "asset:///Image/bar_top_black_my-booking.png"
                            advancedLabel.textStyle.color = Color.White

                            historyButton.defaultImageSource = "asset:///Image/bar_top_grey_my-booking.png"
                            historyLabel.textStyle.color = Color.Black
                        }
                    }
                }
            }
        
        } //end of tab content bar
        
        Container {
            id: viewContainer
            
            
            
            layout: DockLayout {
                
            }

            topPadding: 50.0
            
            preferredWidth: 768.0
            Container { //Current Info Container
                id: currentInfoContainer
                
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Top
                
                
                
                Container {
                    id: summaryInfoContainer

                	preferredWidth: 686.0
                    preferredHeight: 124.0

                    background: infoBackground.imagePaint

                    attachedObjects: [
                        ImagePaintDefinition {
                            id: infoBackground
                            imageSource: "asset:///Image/bar_insert_top_grey_my-booking.png"
                        }
                    ]
                    layout: DockLayout {
                        
                    }
                    
                    horizontalAlignment: HorizontalAlignment.Center

                    leftPadding: 30.0
                    rightPadding: 30.0
                    
                    Label {
                        id: numInfoLabel
                        objectName: "numInfoLabel"
                        
                        text: "56452"
                        textStyle.color: Color.Black
                        textStyle.fontSize: FontSize.Default
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                    }
                    
                    Label {
                        id: typeInfoLabel
                        objectName: "typeInfoLabel"
                        
                        text: "     Car Type/ Date/ Time"
                        textStyle.color: Color.Black
                        textStyle.fontSize: FontSize.Default
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        
                    }
                    
                    ImageButton {
                        defaultImageSource: "asset:///Image/icon_arrow.png"
                        
                        preferredWidth: 38.0
                        preferredHeight: 56.0
                        enabled: true
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                    }

					onTouch: {
         				if (event.isDown()){
         				    
         				}
         			}
                } //End of Summary Info Container
                
                Container {
                    id: pickupInfoContainer
                    
                    preferredHeight: 90.0
                    preferredWidth: 686.0

                    background: pickupInfoBackground.imagePaint

                    attachedObjects: [
                        ImagePaintDefinition {
                            id: pickupInfoBackground
                            imageSource: "asset:///Image/bar_insert_middle_profile.png"
                        }
                    ]

                    layout: DockLayout {

                    }

                    horizontalAlignment: HorizontalAlignment.Center

                    leftPadding: 30.0
                    rightPadding: 30.0
                    
                    Label {
                        preferredWidth: 150.0

						text: "Pick up"
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.create("#b4000000")
                    }
                    
                    Label {
                        
                        objectName: "pickupLabel"
                        text: "GuanQian Street"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.create("#b4000000")
                        maxWidth: 360.0
                    }
                    
                    ImageButton {
                        preferredWidth: 68.0
                        preferredHeight: 60.0
                        pressedImageSource: "asset:///Image/icon_pickup_mybooking.png"
                        defaultImageSource: "asset:///Image/icon_pickup_mybooking.png"
                        
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center

                        onClicked: {
                            
                        }
                    }
                } //End of pickup container
                
                
                Container {
                    id: dropOffInfoContainer

                    preferredHeight: 90.0
                    preferredWidth: 686.0

                    background: dropOffInfoBackground.imagePaint

                    attachedObjects: [
                        ImagePaintDefinition {
                            id: dropOffInfoBackground
                            imageSource: "asset:///Image/bar_insert_middle_profile.png"
                        }
                    ]

                    layout: DockLayout {

                    }

                    horizontalAlignment: HorizontalAlignment.Center

                    leftPadding: 30.0
                    rightPadding: 30.0

                    Label {
                        preferredWidth: 150.0

                        text: "Drop off"
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.create("#b4000000")
                    }

                    Label {
                        objectName: "dropoffLabel"
                        text: "Jinji Lake"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.create("#b4000000")
                        maxWidth: 360.0
                    }

                    ImageButton {
                        preferredWidth: 68.0
                        preferredHeight: 60.0
                        pressedImageSource: "asset:///Image/icon_pickup_mybooking.png"
                        defaultImageSource: "asset:///Image/icon_pickup_mybooking.png"

                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center

                        onClicked: {

                        }
                    }

                } //End of Drop off Info Container
                
                Container {
                    id: statusContainer

                    preferredHeight: 90.0
                    preferredWidth: 686.0

                    background: statusBackground.imagePaint

                    attachedObjects: [
                        ImagePaintDefinition {
                            id: statusBackground
                            imageSource: "asset:///Image/bar_insert_btm_profile.png"
                        }
                    ]

                    layout: DockLayout {

                    }

                    horizontalAlignment: HorizontalAlignment.Center

                    leftPadding: 30.0
                    rightPadding: 30.0

                    Label {
                        preferredWidth: 150.0

                        text: "Status"
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.create("#b4000000")
                    }

                    Label {
                        objectName: "statusLabel"
                        text: "Confirmed"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle.fontSize: FontSize.Small
                        textStyle.color: Color.create("#b4000000")
                        maxWidth: 360.0
                    }

                } //End of Status Container
            } 
        }//End of Current Info Container
    }
    actions: [
        ActionItem {
            title: "Refresh"
            ActionBar.placement: ActionBarPlacement.OnBar

            imageSource: "asset:///Image/icon_refresh.png"
            
            onTriggered: {
                
            }
        }
    ]
}
