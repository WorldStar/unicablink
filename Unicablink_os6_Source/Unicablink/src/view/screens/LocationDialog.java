package view.screens;

import view.components.ImageButtonField;
import application.ResouceConstant;
import application.Utils;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;

public class LocationDialog extends Dialog {
	
	private LabelField labelMessage;
	private ImageButtonField buttonHere;
	
	public LocationDialog(String strTitle, String strMessage)
    {
        super(Dialog.D_OK, strTitle, 1, null, Manager.FOCUSABLE);

        labelMessage = new LabelField(strMessage);
        Font mFontLabel = Utils.getFont(Font.PLAIN, 29);
        
        labelMessage.setFont(mFontLabel);
        
        add(labelMessage);
    }
}
