/*
 * JSONReqeust.h
 *
 *  Created on: Aug 7, 2013
 *      Author: Passion
 */

#ifndef JSONREQEUST_H_
#define JSONREQEUST_H_


#include <QtCore/QObject>
#include <QString>
#include <bb/cascades/GroupDataModel>

using namespace bb::cascades;

class JSONReqeust: public QObject {
	Q_OBJECT
public:
	JSONReqeust(QObject *parent = 0);
	~JSONReqeust();


public:
	QString getJSON(QMap<QString,QVariant> &params, int nType);
	QString getIMEI();
	QVariant getJSONVariant();
	QString getMD5Password(QString strPassword);

private:
	void reset();

private:
	QObject* m_pParent;
	QVariant m_jsonResult;
	QVariantMap m_jsonMap;
};

#endif /* JSONREQEUST_H_ */
