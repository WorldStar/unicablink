package view.screens;

import java.util.Date;

import javax.microedition.media.Manager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.components.ImageButtonField;
import view.components.OrderInfoContainer;
import view.fieldmanager.InfoManager;
import view.fieldmanager.TabMenuManager;
import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.Order;
import model.request.OrderStatus;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.io.http.HttpDateParser;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.image.Image;
import net.rim.device.api.ui.image.ImageFactory;
import net.rim.device.api.util.StringProvider;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import application.setting.AppSetting;
import application.setting.SettingStore;
import application.utils.DateUtils;

public class HistoryScreen extends BookingBaseScreen {

	/**
	 * Private Members
	 */
	ImageButtonField currentTab;
	ImageButtonField advanceTab;
	ImageButtonField historyTab;
	VerticalFieldManager contentManager;
	
	Bitmap bmpNoRecords = null;
	BitmapField bmpFieldNoRecords = null;
	
	private JSONArray m_aJSONOrderStatus = null;

	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;
	
	public HistoryScreen() {
		super();

		//Get App Setting
		AppSetting appSetting = mAppSettingStore.getAppSetting();
		
		String strPhoneNo = appSetting.getAccountPhone();
		
		bmpNoRecords = Utils.getBitmapResource(ResouceConstant.HISTORY_NO_RECORDS);
		bmpFieldNoRecords = new BitmapField(bmpNoRecords, Field.FIELD_HCENTER);
		
		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.HISTORY_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));

		/**
		 * Managers
		 */
		TabMenuManager tabMenuManager = new TabMenuManager(28, 0, ResouceConstant.HISTORY_TAB_PADDING, 0, 8, 0xf5f5f5);
		contentManager = new VerticalFieldManager(USE_ALL_WIDTH)
		{
			
		};

		/**
		 * create tab buttons
		 */
		currentTab = new ImageButtonField(ResouceConstant.HISTORY_TAB_CURRENT_SEL, "", 0, "")
		{
			public void clickButton()
			{
				refreshCurrent();
			}
		};

		advanceTab = new ImageButtonField(ResouceConstant.HISTORY_TAB_ADVANCE_UNSEL, "", 0, "")
		{
			public void clickButton()
			{
				refreshAdvance();
			}
		};

		historyTab = new ImageButtonField(ResouceConstant.HISTORY_TAB_HISTORY_UNSEL, "", 0, "")
		{
			public void clickButton()
			{
				refreshHistory();
			}
		};
		
		try
		{
			tabMenuManager.add(currentTab);
			tabMenuManager.add(advanceTab);
			tabMenuManager.add(historyTab);
	
			/**
			 * Add managers to MainManager
			 */
			add(tabMenuManager);
			add(contentManager);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Dialog.alert("Failed add of UI components.");
			return;
		}
		
//		//Temp
//		OrderInfoContainer currentInfo = new OrderInfoContainer("56542", "ShenYang", "PyongYang", Utils.getDescription(0));
//		contentManager.add(currentInfo);
		
		/**
		 * Add Order Informations (Current)
		 */
		int nOrderCount = appSetting.getOrderCount();
		
		if (nOrderCount > 0)
		{
			int[] aOrderArray = appSetting.getOrders();
			
			OrderStatus jsonOrderStatus = new OrderStatus();
			
			for (int i = 0; i < nOrderCount; i++)
			{
				Order jsonOrder = new Order();
				
				jsonOrder.setOrderId(aOrderArray[i]);
				jsonOrder.setRequestCount(1);
				
				jsonOrderStatus.appendOrder(jsonOrder.toJSONObject());
			}
			
			jsonOrderStatus.setPhoneNo(strPhoneNo);
			
			//Get the response
			JSONObject jsonResult = null;
			
			String strJSONStatus = jsonOrderStatus.toJSONObject().toString();
			jsonResult = HTTPConnectionHelper.requestToServer(strJSONStatus, 
					DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_UPDATE_STATUS);
			
			if (jsonResult == null)
			{
				return;
			}
			
			/**
			 * Analyze Response
			 */
			
			/*
			 *  {
					"RESPONSETIME":"2013-08-12 13:10:58",
					"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
					"ORDERARRAY":
						[
							{
								"PICKUPTIME":"2013-08-12 13:10:00",
								"REMARKS":"",
								"SHOWTAXI":"0",
								"COORDINATE":"",
								"CABTYPE":"1",
								"CABNO":"",
								"DROPOFF":"WUXI, JIANGSU, CHINA",
								"MAKER":"",
								"STATUS":"0",
								"DRIVER":"",
								"ETA":"",
								"PASSENGERNAME":"TYUY",
								"MESSAGE":"TQ for your booking. We will proceed your order now!",
								"ORDERID":"75518",
								"FIXEDFARE":"RM0",
								"EFARE":"RM70.00",
								"MOBILENO":"",
								"COLOR":"",
								"PICKUP":"CHENGWAN ROAD, WUZHONG, SUZHOU, JIANGSU, CHINA, 215121"
							}
						]
				}
			 */
			
			try 
			{
				m_aJSONOrderStatus = jsonResult.getJSONArray("ORDERARRAY");
			}
			catch (JSONException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				Dialog.alert("Failed recognition of response.");
				return;
			}
			
			/*
			 * Save JSON Array
			 */
			Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
			
			try {
				theApp.mOrderArray = new JSONArray(m_aJSONOrderStatus.toString());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				Dialog.alert("Failed initialize Orders");
			}
			
			int nCount = m_aJSONOrderStatus.length();
			
			for (int i = 0; i < nCount; i++)
			{
				try {
					JSONObject jsonStatus = m_aJSONOrderStatus.getJSONObject(i);
					
					int nStatus = jsonStatus.getInt("STATUS");
					
					
					switch (nStatus) {
					case 0:
					case 1:
					case 2:
					case 3:
					case 10:
					{
						/*
						 * Add infor to Current Tab 
						 */
						//Get Response
						String strOrderId = jsonStatus.getString("ORDERID");
						String strPickup = jsonStatus.getString("PICKUP");
						String strDropOff = jsonStatus.getString("DROPOFF");
						int nCabType = jsonStatus.getInt("CABTYPE");
						String strCabType = (nCabType == 1)? "Budget": "Premium / Executive";
						
						String strPickupTime = jsonStatus.getString("PICKUPTIME");
						strPickupTime = DateUtils.StringToDate(strPickupTime);
					    
					    String strSummary = strPickupTime + " / " + strCabType;
						
						OrderInfoContainer tempInfo = new OrderInfoContainer(strOrderId, strPickup, strDropOff, strSummary, Utils.getDescription(nStatus));
						contentManager.add(tempInfo);
						
						break;
					}

					case 9:
					{
						/*
						 * Add infor to Advanced Tab
						 */
						
						break;
					}

					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					{
						/*
						 * Add infor to History Tab
						 */
						
						break;
					}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Dialog.alert("Failed recognition of response.");
					
					return;
				}
			}
			
			

		}
		
		int nFieldCount = contentManager.getFieldCount();
		if (nFieldCount == 0)
			contentManager.add(bmpFieldNoRecords);
	}

	/**
	 * User Interaction Process
	 */
	public void refreshCurrent()
	{
		//Change Button's Background
		currentTab.setBtnBackground(ResouceConstant.HISTORY_TAB_CURRENT_SEL);
		advanceTab.setBtnBackground(ResouceConstant.HISTORY_TAB_ADVANCE_UNSEL);
		historyTab.setBtnBackground(ResouceConstant.HISTORY_TAB_HISTORY_UNSEL);

		//Remove all containers
		contentManager.deleteAll();

		//Add Containers
		if (m_aJSONOrderStatus == null){
			contentManager.add(bmpFieldNoRecords);
			return;
		}
		
		int nCount = m_aJSONOrderStatus.length();
		
		for (int i = 0; i < nCount; i++)
		{
			try {
				JSONObject jsonStatus = m_aJSONOrderStatus.getJSONObject(i);
				
				int nStatus = jsonStatus.getInt("STATUS");
				
				
				switch (nStatus) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 10:
				{
					/*
					 * Add infor to Current Tab 
					 */
					//Get Response
					String strOrderId = jsonStatus.getString("ORDERID");
					String strPickup = jsonStatus.getString("PICKUP");
					String strDropOff = jsonStatus.getString("DROPOFF");
					int nCabType = jsonStatus.getInt("CABTYPE");
					String strCabType = (nCabType == 1)? "Budget": "Premium / Executive";
					
					String strPickupTime = jsonStatus.getString("PICKUPTIME");
					strPickupTime = DateUtils.StringToDate(strPickupTime);
				    
				    String strSummary = strPickupTime + " / " + strCabType;
					
					OrderInfoContainer tempInfo = new OrderInfoContainer(strOrderId, strPickup, strDropOff, strSummary, Utils.getDescription(nStatus));
					contentManager.add(tempInfo);
					
					break;
				}

				case 9:
				{
					/*
					 * Add infor to Advanced Tab
					 */
					
					break;
				}

				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				{
					/*
					 * Add infor to History Tab
					 */
					
					break;
				}
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Dialog.alert("Failed recognition of response.");
				
				return;
			}
		}
		
		int nFieldCount = contentManager.getFieldCount();
		if (nFieldCount == 0)
			contentManager.add(bmpFieldNoRecords);

	}

	public void refreshAdvance()
	{
		//Change Button's Background
		currentTab.setBtnBackground(ResouceConstant.HISTORY_TAB_CURRENT_UNSEL);
		advanceTab.setBtnBackground(ResouceConstant.HISTORY_TAB_ADVANCE_SEL);
		historyTab.setBtnBackground(ResouceConstant.HISTORY_TAB_HISTORY_UNSEL);

		//Remove all containers
		contentManager.deleteAll();

		//Add Containers
		if (m_aJSONOrderStatus == null){
			contentManager.add(bmpFieldNoRecords);
			return;
		}
		
		int nCount = m_aJSONOrderStatus.length();
		
		for (int i = 0; i < nCount; i++)
		{
			try {
				JSONObject jsonStatus = m_aJSONOrderStatus.getJSONObject(i);
				
				int nStatus = jsonStatus.getInt("STATUS");
				
				
				switch (nStatus) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 10:
				{
					/*
					 * Add infor to Current Tab 
					 */
					
					
					break;
				}

				case 9:
				{
					/*
					 * Add infor to Advanced Tab
					 */
					//Get Response
					String strOrderId = jsonStatus.getString("ORDERID");
					String strPickup = jsonStatus.getString("PICKUP");
					String strDropOff = jsonStatus.getString("DROPOFF");
					int nCabType = jsonStatus.getInt("CABTYPE");
					String strCabType = (nCabType == 1)? "Budget": "Premium / Executive";
					
					String strPickupTime = jsonStatus.getString("PICKUPTIME");
					strPickupTime = DateUtils.StringToDate(strPickupTime);
				    
				    String strSummary = strPickupTime + " / " + strCabType;
				    
					OrderInfoContainer tempInfo = new OrderInfoContainer(strOrderId, strPickup, strDropOff, strSummary, Utils.getDescription(nStatus));
					contentManager.add(tempInfo);
					break;
				}

				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				{
					/*
					 * Add infor to History Tab
					 */
					
					break;
				}
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Dialog.alert("Failed recognition of response.");
				
				return;
			}
		}

		int nFieldCount = contentManager.getFieldCount();
		if (nFieldCount == 0)
			contentManager.add(bmpFieldNoRecords);
	}

	public void refreshHistory()
	{
		//Change Button's Background
		currentTab.setBtnBackground(ResouceConstant.HISTORY_TAB_CURRENT_UNSEL);
		advanceTab.setBtnBackground(ResouceConstant.HISTORY_TAB_ADVANCE_UNSEL);
		historyTab.setBtnBackground(ResouceConstant.HISTORY_TAB_HISTORY_SEL);

		//Remove all containers
		contentManager.deleteAll();

		//Add Containers
		if (m_aJSONOrderStatus == null){
			contentManager.add(bmpFieldNoRecords);
			return;
		}
		
		int nCount = m_aJSONOrderStatus.length();
		
		for (int i = 0; i < nCount; i++)
		{
			try {
				JSONObject jsonStatus = m_aJSONOrderStatus.getJSONObject(i);
				
				int nStatus = jsonStatus.getInt("STATUS");
				
				
				switch (nStatus) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 10:
				{
					/*
					 * Add infor to Current Tab 
					 */
					
					
					break;
				}

				case 9:
				{
					/*
					 * Add infor to Advanced Tab
					 */
					
					break;
				}

				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				{
					/*
					 * Add infor to History Tab
					 */
					//Get Response
					String strOrderId = jsonStatus.getString("ORDERID");
					String strPickup = jsonStatus.getString("PICKUP");
					String strDropOff = jsonStatus.getString("DROPOFF");
					int nCabType = jsonStatus.getInt("CABTYPE");
					String strCabType = (nCabType == 1)? "Budget": "Premium / Executive";
					
					String strPickupTime = jsonStatus.getString("PICKUPTIME");
					strPickupTime = DateUtils.StringToDate(strPickupTime);
				    
				    String strSummary = strPickupTime + " / " + strCabType;
				    
					OrderInfoContainer tempInfo = new OrderInfoContainer(strOrderId, strPickup, strDropOff, strSummary, Utils.getDescription(nStatus));
					contentManager.add(tempInfo);
					
					break;
				}
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Dialog.alert("Failed recognition of response.");
				
				return;
			}
		}

		int nFieldCount = contentManager.getFieldCount();
		if (nFieldCount == 0)
			contentManager.add(bmpFieldNoRecords);

	}

	/**
	 * Overrides
	 */
	protected void makeMenu(Menu menu, int instance) 
	{ 
		super.makeMenu(menu, instance);

		menu.addSeparator();
		
		//Refresh Menu
		MenuItem menuItemRefresh = new MenuItem(new StringProvider("Refresh"), 10, 10) { 
			public void run() { 
				//set current tab
				currentTab.setBtnBackground(ResouceConstant.HISTORY_TAB_CURRENT_SEL);
				advanceTab.setBtnBackground(ResouceConstant.HISTORY_TAB_ADVANCE_UNSEL);
				historyTab.setBtnBackground(ResouceConstant.HISTORY_TAB_HISTORY_UNSEL);
				
				currentTab.setFocus();
				
				//Get saved data
				AppSetting appSetting = mAppSettingStore.getAppSetting();
				String strPhoneNo = appSetting.getAccountPhone();
				
				//delete all data from content container
				contentManager.deleteAll();
				
				/**
				 * Add Order Informations (Current)
				 */
				
				int nOrderCount = appSetting.getOrderCount();
				
				if (nOrderCount > 0)
				{
					int[] aOrderArray = appSetting.getOrders();
					
					OrderStatus jsonOrderStatus = new OrderStatus();
					
					for (int i = 0; i < nOrderCount; i++)
					{
						Order jsonOrder = new Order();
						
						jsonOrder.setOrderId(aOrderArray[i]);
						jsonOrder.setRequestCount(1);
						
						jsonOrderStatus.appendOrder(jsonOrder.toJSONObject());
					}
					
					jsonOrderStatus.setPhoneNo(strPhoneNo);
					
					//Get the response
					JSONObject jsonResult = null;
					
					String strJSONStatus = jsonOrderStatus.toJSONObject().toString();
					jsonResult = HTTPConnectionHelper.requestToServer(strJSONStatus, 
							DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_UPDATE_STATUS);
					
					if (jsonResult == null)
					{
						return;
					}
					
					/**
					 * Analyze Response
					 */
					
					/*
					 *  {
							"RESPONSETIME":"2013-08-12 13:10:58",
							"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
							"ORDERARRAY":
								[
									{
										"PICKUPTIME":"2013-08-12 13:10:00",
										"REMARKS":"",
										"SHOWTAXI":"0",
										"COORDINATE":"",
										"CABTYPE":"1",
										"CABNO":"",
										"DROPOFF":"WUXI, JIANGSU, CHINA",
										"MAKER":"",
										"STATUS":"0",
										"DRIVER":"",
										"ETA":"",
										"PASSENGERNAME":"TYUY",
										"MESSAGE":"TQ for your booking. We will proceed your order now!",
										"ORDERID":"75518",
										"FIXEDFARE":"RM0",
										"EFARE":"RM70.00",
										"MOBILENO":"",
										"COLOR":"",
										"PICKUP":"CHENGWAN ROAD, WUZHONG, SUZHOU, JIANGSU, CHINA, 215121"
									}
								]
						}
					 */
					
					try 
					{
						m_aJSONOrderStatus = jsonResult.getJSONArray("ORDERARRAY");
					}
					catch (JSONException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
						
						Dialog.alert("Failed recognition of response.");
						return;
					}
					
					/*
					 * Save JSON Array
					 */
					Unicablink theApp = (Unicablink)UiApplication.getUiApplication();
					
					try {
						theApp.mOrderArray = new JSONArray(m_aJSONOrderStatus.toString());
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						Dialog.alert("Failed initialize Orders");
					}
					
					int nCount = m_aJSONOrderStatus.length();
					
					for (int i = 0; i < nCount; i++)
					{
						try {
							JSONObject jsonStatus = m_aJSONOrderStatus.getJSONObject(i);
							
							int nStatus = jsonStatus.getInt("STATUS");
							
							
							switch (nStatus) {
							case 0:
							case 1:
							case 2:
							case 3:
							case 10:
							{
								/*
								 * Add infor to Current Tab 
								 */
								//Get Response
								String strOrderId = jsonStatus.getString("ORDERID");
								String strPickup = jsonStatus.getString("PICKUP");
								String strDropOff = jsonStatus.getString("DROPOFF");
								int nCabType = jsonStatus.getInt("CABTYPE");
								String strCabType = (nCabType == 1)? "Budget": "Premium / Executive";
								
								String strPickupTime = jsonStatus.getString("PICKUPTIME");
							    strPickupTime = DateUtils.StringToDate(strPickupTime);
							    
							    String strSummary = strPickupTime + " / " + strCabType;
							    
								OrderInfoContainer tempInfo = new OrderInfoContainer(strOrderId, strPickup, strDropOff, strSummary, Utils.getDescription(nStatus));
								contentManager.add(tempInfo);
								
								break;
							}

							case 9:
							{
								/*
								 * Add infor to Advanced Tab
								 */
								
								break;
							}

							case 4:
							case 5:
							case 6:
							case 7:
							case 8:
							{
								/*
								 * Add infor to History Tab
								 */
								
								break;
							}
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							Dialog.alert("Failed recognition of response.");
							
							return;
						}
					}
					
					
				}
				int nFieldCount = contentManager.getFieldCount();
				if (nFieldCount == 0)
					contentManager.add(bmpFieldNoRecords);
			
				
			} 
		};
		
		//Set icon to menu item (Book a cab)
		Image menuIconRefresh = ImageFactory.createImage(Bitmap.getBitmapResource(ResouceConstant.HISTORY_MENU_ITEM_REFRESH));
		menuItemRefresh.setIcon(menuIconRefresh);
		
		menu.add(menuItemRefresh);
	}
}
