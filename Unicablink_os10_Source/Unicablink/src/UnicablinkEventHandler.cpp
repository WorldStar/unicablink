/*
 * UnicablinkEventHandler.cpp
 *
 *  Created on: Jul 5, 2013
 *      Author: Passion
 */
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/DateTimePicker>
#include <bb/cascades/Label>
#include <bb/cascades/maps/DataProvider>
#include <bb/cascades/maps/MapData>
#include <bb/cascades/maps/DataProvider>
#include <bb/cascades/ActivityIndicator>
#include <bb/cascades/Container>
#include <bb/cascades/DockLayout>
#include <bb/cascades/Color>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/ImageButton>
#include <bb/cascades/ScrollView>
#include <bb/cascades/TextField>
#include <bb/cascades/Label>
#include <bb/cascades/Tab>
#include <bb/cascades/TabbedPane>
#include <bb/cascades/ImageView>

#include <bb/platform/geo/Point>
#include <bb/platform/geo/GeoLocation>
#include <bb/platform/geo/Marker>

#include <bb/UIToolkitSupport>

#include <bb/data/JsonDataAccess>

#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>
#include <bb/system/SystemDialog>

#include <bb/device/DisplayInfo>
#include <bb/device/HardwareInfo>

#include <QtXml/QXmlStreamReader>
#include <bb/PpsObject>

#include <QtXml/QXmlReader>
#include <QtNetwork/QNetworkReply>
#include <QtCore/QDateTime>
#include <QtCore/QStringList>

#include <QtLocationSubset/QGeoServiceProvider>
#include <QtLocationSubset/QGeoSearchManager>
#include <QtLocationSubset/QGeoSearchReply>
#include <QtLocationSubset/QGeoCoordinate>
#include <QtLocationSubset/QGeoPlace>
#include <QtLocationSubset/QGeoAddress>

#include <QCryptographicHash>
#include <QSettings>
#include <QDateTime>

#include "UnicablinkEventHandler.h"

using namespace bb;
using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;
using namespace bb::device;
using namespace bb::platform::geo;

//Request type
typedef enum {
	//User log in request
	USER_LOG_IN = 1,
	//user register request
	USER_REGISTER = 2,
	//user activate request
	USER_ACTIVATE = 3,
	//resend activation code request
	RESEND_ACTIVATION_CODE = 4,
	//retrieve password request
	RETRIEVE_PASSWORD = 5,
	//order cab request
	ORDER_CAB = 6,
	//modify profile
	MODIFY_PROFILE = 7,
	//cancel order
	CANCEL_ORDER = 8,
	//order status request
	ORDER_STATUS_UPDATE = 9,
	//query position info request
	QUERY_POSITION_INFO = 10,
	//Search Location
	SEARCH_LOCATION = 11
} request_type;

enum {
	//pickup location
	PICKUP_LOCATION = 1,
	//dropoff location
	DROPOFF_LOCATION = 2,
	//address
	ADDRESS_LOCATION = 3
};

//Page Type
typedef enum {
	//User log in request
	PICK_UP_PAGE = 2,
	//user register request
	DROP_OFF_PAGE = 3,
} page_type;

UnicablinkEventHandler::UnicablinkEventHandler(QObject *parent = 0) {
	// TODO Auto-generated constructor stub
	m_pMainInstance = (UnicablinkApp*) parent;
	m_paneLogIn = NULL;
	m_paneRegister = NULL;
	m_paneBooking = NULL;
	m_paneOrder = NULL;
	m_paneStatus = NULL;
	m_paneCheckOrder = NULL;
	m_paneProfile = NULL;
	m_paneNewNumber = NULL;
	m_paneInfo = NULL;
	m_paneTrackTaxi = NULL;
	m_panePickupLocation = NULL;
	m_paneTabedMain = NULL;

	m_pageCurrentBooking = NULL;
	m_pageActivation = NULL;

	m_jsonConnect = new JSONConnect(this);
	m_soapRequest = new SOAPRequest(this);

	m_bFlagCurrentBooking = false;
	m_bLoggedIn = false;
	m_bFirstPickup = true;

	//Set Locations
	m_dPickupLat = 0.0;
	m_dPickupLon = 0.0;
	m_dDropOffLat = 0.0;
	m_dDropOffLon = 0.0;

	//Set flag
	m_bPickupSet = false;
	m_bDropOffSet = false;

	//Set status message
	m_strStatusMessage[0] = "We are now processing your order, please wait, the process may take up to 5 min!";
	m_strStatusMessage[1] = "There is taxi for you. TQ";
	m_strStatusMessage[2] = "Your taxi has arrived. Please proceed to pickup point immediately. TQ";
	m_strStatusMessage[3] = "Journey start. Have a nice trip. TQ";
	m_strStatusMessage[4] = "Successfully pickup. TQ for using Unicablink";
	m_strStatusMessage[5] = "Journey completed. TQ for Using Unicablink";
	m_strStatusMessage[6] = "Passenger canceled. TQ for Using Unicablink";
	m_strStatusMessage[7] = "Opp, We are sorry, for some reasons driver cancel the job. Please try again / call our customer support.";
	m_strStatusMessage[8] = "We are sorry, there is no taxi nearby available at the moment, please try again!, TQ for using Unicablink";
	m_strStatusMessage[9] = "You are making an advance job, we will proceed your order later.";
	m_strStatusMessage[10] = "Your job has been accepted";

	//Set description message
	m_strDescription[0] = "Processing";
	m_strDescription[1] = "Confirmed";
	m_strDescription[2] = "Arrived";
	m_strDescription[3] = "Pickup";
	m_strDescription[4] = "Completed";
	m_strDescription[5] = "Completed";
	m_strDescription[6] = "Passenger Canceled";
	m_strDescription[7] = "Driver Canceled";
	m_strDescription[8] = "No Taxi";
	m_strDescription[9] = "New";
	m_strDescription[10] = "Accepted";

	QSettings appSetting("UnicablinkSoft", "Unicablink");

	m_varOrderList = appSetting.value("Orders").toList();

	m_nOrderCount = m_varOrderList.count();

	// Initialize QGeoSearchManager
	QStringList serviceProviders = QtMobilitySubset::QGeoServiceProvider::availableServiceProviders();

	m_geoServiceProvider = NULL;
	m_geoSearchManager = NULL;

	if ( serviceProviders.size() ) {
		// serviceProvider is a QGeoServiceProvider *
		m_geoServiceProvider = new QtMobilitySubset::QGeoServiceProvider(
				serviceProviders.at(0) );
		// searchManager is a QGeoSearchManager *
		m_geoSearchManager = m_geoServiceProvider->searchManager();
	}

	//Get screenWidth and Height
	bb::device::DisplayInfo display;
	m_nWinWidth = display.pixelSize().width();
	m_nWinHeight = display.pixelSize().height();

	//create auto refresh timer for order info.
	m_orderTimer = new QTimer(this);
	connect(m_orderTimer, SIGNAL(timeout()), this, SLOT(refreshOrderInfo()));

	createActivityDialog();
}

UnicablinkEventHandler::~UnicablinkEventHandler() {
	// TODO Auto-generated destructor stub
	//save Setting
	QSettings appSetting("UnicablinkSoft", "Unicablink");
	appSetting.setValue("Orders", m_varOrderList);

	if (m_jsonConnect)
		m_jsonConnect->deleteLater();

	if (m_soapRequest)
		m_soapRequest->deleteLater();

	if (m_activityDialog)
		m_activityDialog->deleteLater();

	//destroy all view
	if (m_paneBooking) {
		delete m_paneBooking;
		m_paneBooking = NULL;
	}

	if (m_paneCheckOrder) {
		delete m_paneCheckOrder;
		m_paneCheckOrder = NULL;
	}

	if (m_paneInfo) {
		delete m_paneInfo;
		m_paneInfo = NULL;
	}

	if (m_paneLogIn) {
		delete m_paneLogIn;
		m_paneLogIn = NULL;
	}

	if (m_paneNewNumber) {
		delete m_paneNewNumber;
		m_paneNewNumber = NULL;
	}

	if (m_paneOrder) {
		delete m_paneOrder;
		m_paneOrder = NULL;
	}

	if (m_panePickupLocation) {
		delete m_panePickupLocation;
		m_panePickupLocation = NULL;
	}

	if (m_paneProfile) {
		delete m_paneProfile;
		m_paneProfile = NULL;
	}

	if (m_paneRegister) {
		delete m_paneRegister;
		m_paneRegister = NULL;
	}

	if (m_paneStatus) {
		delete m_paneStatus;
		m_paneStatus = NULL;
	}

	if (m_paneTrackTaxi) {
		delete m_paneTrackTaxi;
		m_paneTrackTaxi = NULL;
	}

	if (m_paneTabedMain) {
		delete m_paneTabedMain;
		m_paneTabedMain = NULL;
	}

	if (m_pageCurrentBooking){
		delete m_pageCurrentBooking;
		m_pageCurrentBooking = NULL;
	}

	if (m_pageActivation) {
		delete m_pageActivation;
		m_pageActivation = NULL;
	}


}

////////////////////////////////////////  Map Implementation  //////////////////////////////////////////////
void UnicablinkEventHandler::getAddress(double dLat, double dLon)
{
	setAddress(ADDRESS_LOCATION, dLat, dLon);
}

void UnicablinkEventHandler::setAddress(int nType, double dLat, double dLon)
{
	m_nType = nType;

	if (m_nType == ADDRESS_LOCATION)
	{
		//Set the description label text
		Container* descriptionContainer = m_paneTabedMain->findChild<Container*>("descriptionContainer");

		descriptionContainer->setVisible(false);
	}

	//Using Google API
	QMap<QString, QString> params;


	QString strLatlng = double2String(dLat) + "," + double2String(dLon);
	params.insert("latlng", strLatlng);
	params.insert("sensor", "true");

	bool bConnect = m_jsonConnect->SendGetRequest(params, SLOT(readReverseGeocode()));
	Q_UNUSED(bConnect);
	return;
}

//get string
void UnicablinkEventHandler::readReverseGeocode()
{
	/*
	 * Using google api
	 */

	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

	if (reply)
	{
		if (reply->error() == QNetworkReply::NoError)
		{
			int available = reply->bytesAvailable();

			if (available > 0)
			{
				QByteArray buffer(reply->readAll());
				QVariant varJSON = m_jsonDataAccess.loadFromBuffer(buffer);
				if (m_jsonDataAccess.hasError()) {
					//Get Null string, return
					//showMessage("Failed get address");
					return;
				} else {
					m_jsonMap = varJSON.value<QVariantMap>();
				}
			}
		}
	}

	QString strStatus = m_jsonMap.value("status").toString();

	if (strStatus != "OK")
	{
		showMessage("Failed get address");
		return;
	}

	QVariant varOrder = m_jsonMap.value("results");

	QVariantList placeList = varOrder.value<QVariantList>();
	int nCount = placeList.count();

	if (nCount == 0)
	{
		//showMessage("Failed get address");
		return;
	}

	QVariantMap currentAddressMap = placeList.at(0).value<QVariantMap>();
	QString strAddress = currentAddressMap.value("formatted_address").toString();

	switch (m_nType) {
	case PICKUP_LOCATION:
	{
		Label* pickupInfo = m_paneTabedMain->findChild<Label*>("pickupInfoLabel");
		pickupInfo->setProperty("strPickupLocation", strAddress);
		pickupInfo->setProperty("bSetPickup", true);

		break;
	}
	case DROPOFF_LOCATION:
	{
		Label* dropoffInfo = m_paneTabedMain->findChild<Label*>("dropoffInfoLabel");
		dropoffInfo->setProperty("strDropOffLocation", strAddress);
		dropoffInfo->setProperty("bSetDropOff", true);

		break;
	}
	case ADDRESS_LOCATION:
	{
		TextField* addressTextField = m_paneTabedMain->findChild<TextField*>("addressField");
		addressTextField->setText(strAddress);

		//Set the description label text
		Label* descriptionLabel = m_paneTabedMain->findChild<Label*>("descriptionLabel");
		descriptionLabel->setProperty("text", strAddress);

		Container* descriptionContainer = m_paneTabedMain->findChild<Container*>("descriptionContainer");
		descriptionContainer->setVisible(true);

		break;
	}

	default:
		break;
	}

	return;
}

void UnicablinkEventHandler::showHomepage() {
	//Invoke Email
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.browser");
	request.setAction("bb.action.OPEN");
	request.setUri("http://www.sunlighttaxi.com/thing-to-note-before-making-a-online-taxi-booking");
	invokeManager.invoke(request);
}

void UnicablinkEventHandler::setPickupMarker(double dLat, double dLon)
{
	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");

	GeoLocation* newDrop = new GeoLocation();
	newDrop->setLatitude(dLat);
	newDrop->setLongitude(dLon);

	m_dPickupLat = dLat;
	m_dPickupLon = dLon;
	// use the marker in the assets, as opposed to the default marker
	Marker flag = Marker(UIToolkitSupport::absolutePathFromUrl(
					 QUrl("asset:///Image/icon_pickup.png")), QSize(56, 70),
					 QPoint(20, 69), QPoint(20, 1));


	newDrop->setMarker(flag);
	m_bPickupSet = true;

	mapView->mapData()->add(newDrop);


	setAddress(PICKUP_LOCATION, dLat, dLon);
}

void UnicablinkEventHandler::updateMarkers(QObject* mapObject, QObject* containerObject) const
{
//	int x, y;
    MapView* mapview = qobject_cast<MapView*>(mapObject);
    Container* container = qobject_cast<Container*>(containerObject);

    for (int i = 0; i < container->count(); i++) {
        const QPoint xy = worldToPixel(mapview,
        			container->at(i)->property("lat").value<double>(),
        			container->at(i)->property("lon").value<double>());
        container->at(i)->setProperty("x", xy.x());
        container->at(i)->setProperty("y", xy.y());
    }
}

QPoint UnicablinkEventHandler::worldToPixel(QObject* mapObject, double latitude, double longitude) const
{
    MapView* mapview = qobject_cast<MapView*>(mapObject);
    const Point worldCoordinates = Point(latitude, longitude);

    return mapview->worldToWindow(worldCoordinates);
}

QVariantList UnicablinkEventHandler::worldToPixelInvokable(QObject* mapObject, double latitude, double longitude) const
{
    MapView* mapview = qobject_cast<MapView*>(mapObject);
    const Point worldCoordinates = Point(latitude, longitude);
    const QPoint pixels = mapview->worldToWindow(worldCoordinates);

    return QVariantList() << pixels.x() << pixels.y();
}

void UnicablinkEventHandler::setProvider() {
	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");
	if (mapView) {
		mapView->setCaptionGoButtonVisible(true);
		if (mapView) {
			// creating a data provider just for the device location object. that way, when the clear function is call, this object is not removed.
			DataProvider* deviceLocDataProv = new DataProvider(
					"device-location-data-provider");
			mapView->mapData()->addProvider(deviceLocDataProv);

			// create a geolocation just for the device's location
			deviceLocation = new GeoLocation("device-location-id");
			deviceLocation->setName("Current Device Location");
			deviceLocation->setDescription(
					"<html><a href=\"http://www.blackberry.com\">Hyperlinks</a> are super useful in bubbles.</html>");

			// for that location, replace the standard default pin with the provided bulls eye asset
			Marker bullseye = Marker("asset:///Image/icon_pickup.png",
					QSize(60, 60), QPoint(29, 29), QPoint(29, 1));
			deviceLocation->setMarker(bullseye);

			deviceLocDataProv->add(deviceLocation);
		}
	}

}

void UnicablinkEventHandler::addPinToDevicePosition(QObject* mapObject, double latitude, double longitude)
{
	MapView* mapView = qobject_cast<MapView*>(mapObject);

	if (mapView) {
		GeoLocation* newDrop = new GeoLocation();
		newDrop->setLatitude(latitude);
		newDrop->setLongitude(longitude);
		// use the marker in the assets, as opposed to the default marker
		Marker flag;

		if (m_nFlag == PICK_UP_PAGE)
		{
			// use the marker in the assets, as opposed to the default marker
			flag = Marker(UIToolkitSupport::absolutePathFromUrl(
			                 QUrl("asset:///Image/icon_pickup.png")), QSize(56, 70),
			                 QPoint(20, 69), QPoint(20, 1));
		}
		else
		{
			// use the marker in the assets, as opposed to the default marker
			Marker flag = Marker(UIToolkitSupport::absolutePathFromUrl(
			                 QUrl("asset:///Image/icon_dropoff.png")), QSize(56, 70),
			                 QPoint(20, 69), QPoint(20, 1));
		}

		newDrop->setMarker(flag);

		mapView->mapData()->add(newDrop);
	}
}

//Apply Location
void UnicablinkEventHandler::applyLocation(QString strLocation, double dLat, double dLon)
{
	//Old method, currently not used
	//First, get address for setting address
	//Using Google API

//	QMap<QString, QString> params;
//
//	QString strLatlng = double2String(dLat) + "," + double2String(dLon);
//	params.insert("latlng", strLatlng);
//	params.insert("sensor", "true");
//	bool bConnect = m_jsonConnect->SendGetRequest(params, SLOT(setTargetLocation()));

	if (strLocation == "")
	{
		showMessage("Address", "Search Location cannot be empty.");
		return;
	}

	m_dTempLat = dLat;
	m_dTempLon = dLon;

	/*
	 * Set target Location
	 */

	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");

	if (m_bDropOffSet && m_bPickupSet)
	{
		mapView->setAltitude(5000);
	}

	if (m_nFlag == PICK_UP_PAGE)
	{
		m_bPickupSet = true;
		m_dPickupLat = m_dTempLat;
		m_dPickupLon = m_dTempLon;
		Label* pickupInfo = m_paneTabedMain->findChild<Label*>("pickupInfoLabel");
		pickupInfo->setProperty("bSetPickup", true);
		pickupInfo->setProperty("strPickupLocation", strLocation);

		//Set map center
		mapView->setLatitude(m_dTempLat);
		mapView->setLongitude(m_dTempLon);

		//Remove All pins
		mapView->mapData()->defaultProvider()->clear();


		//Pickup Pin
		GeoLocation* newPickupDrop = new GeoLocation();
		newPickupDrop->setLatitude(m_dPickupLat);
		newPickupDrop->setLongitude(m_dPickupLon);

		Marker flagPickup = Marker(UIToolkitSupport::absolutePathFromUrl(
							 QUrl("asset:///Image/icon_pickup.png")), QSize(56, 70),
							 QPoint(20, 69), QPoint(20, 1));
		newPickupDrop->setMarker(flagPickup);

		mapView->mapData()->add(newPickupDrop);

		//Drop off Pin
		if (m_bDropOffSet)
		{
			GeoLocation* newDropOffDrop = new GeoLocation();
			newDropOffDrop->setLatitude(m_dDropOffLat);
			newDropOffDrop->setLongitude(m_dDropOffLon);

			Marker flagDropoff = Marker(UIToolkitSupport::absolutePathFromUrl(
										 QUrl("asset:///Image/icon_dropoff.png")), QSize(56, 70),
										 QPoint(20, 69), QPoint(20, 1));
			newDropOffDrop->setMarker(flagDropoff);

			mapView->mapData()->add(newDropOffDrop);
		}
	}
	else if (m_nFlag == DROP_OFF_PAGE)
	{
		m_bDropOffSet = true;
		m_dDropOffLat = m_dTempLat;
		m_dDropOffLon = m_dTempLon;

		Label* dropoffInfo = m_paneTabedMain->findChild<Label*>("dropoffInfoLabel");
		dropoffInfo->setProperty("strDropOffLocation", QVariant(strLocation));
		dropoffInfo->setProperty("bSetDropOff", true);

		//Set map center
		mapView->setLatitude(m_dTempLat);
		mapView->setLongitude(m_dTempLon);

		//Remove All pins
		mapView->mapData()->defaultProvider()->clear();

		//Pickup off Pin
		if (m_bPickupSet)
		{
			GeoLocation* newPickupDrop = new GeoLocation();
			newPickupDrop->setLatitude(m_dPickupLat);
			newPickupDrop->setLongitude(m_dPickupLon);

			Marker flagPickup = Marker(UIToolkitSupport::absolutePathFromUrl(
										 QUrl("asset:///Image/icon_pickup.png")), QSize(56, 70),
										 QPoint(20, 69), QPoint(20, 1));

			newPickupDrop->setMarker(flagPickup);

			mapView->mapData()->add(newPickupDrop);
		}

		GeoLocation* newDropOffDrop = new GeoLocation();
		newDropOffDrop->setLatitude(m_dDropOffLat);
		newDropOffDrop->setLongitude(m_dDropOffLon);

		Marker flagDropoff = Marker(UIToolkitSupport::absolutePathFromUrl(
												 QUrl("asset:///Image/icon_dropoff.png")), QSize(56, 70),
												 QPoint(20, 69), QPoint(20, 1));
		newDropOffDrop->setMarker(flagDropoff);

		mapView->mapData()->add(newDropOffDrop);
	}

	Tab* mainTab = m_paneTabedMain->findChild<Tab*>("mainBookingTab");
	mainTab->setProperty("nType", 1);
	return;
}

//Set target location
void UnicablinkEventHandler::setTargetLocation()
{
	/*
	 * Get Address from google api
	 */

	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

	if (reply)
	{
		if (reply->error() == QNetworkReply::NoError)
		{
			int available = reply->bytesAvailable();

			if (available > 0)
			{
				QByteArray buffer(reply->readAll());
				QVariant varJSON = m_jsonDataAccess.loadFromBuffer(buffer);
				if (m_jsonDataAccess.hasError()) {
					//Get Null string, return
					showMessage("Please check network connection, Try again later.");
					return;
				} else {
					m_jsonMap = varJSON.value<QVariantMap>();
				}
			}
		}
	}

	QString strStatus = m_jsonMap.value("status").toString();

	if (strStatus != "OK")
	{
		showMessage("Service Failed, Please try again later.");
		return;
	}

	QVariant varOrder = m_jsonMap.value("results");

	QVariantList placeList = varOrder.value<QVariantList>();
	int nCount = placeList.count();

	if (nCount == 0)
	{
		showMessage("This location is not appropriate location.");
		return;
	}

	QVariantMap currentAddressMap = placeList.at(0).value<QVariantMap>();
	QString strLocation = currentAddressMap.value("formatted_address").toString();

	/*
	 * Set target Location
	 */

	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");

	if (m_bDropOffSet && m_bPickupSet)
	{
		mapView->setAltitude(5000);
	}

	if (m_nFlag == PICK_UP_PAGE)
	{
		m_bPickupSet = true;
		m_dPickupLat = m_dTempLat;
		m_dPickupLon = m_dTempLon;
		Label* pickupInfo = m_paneTabedMain->findChild<Label*>("pickupInfoLabel");
		pickupInfo->setProperty("strPickupLocation", strLocation);
		pickupInfo->setProperty("bSetPickup", true);


		//Set map center
		mapView->setLatitude(m_dTempLat);
		mapView->setLongitude(m_dTempLon);

		//Remove All pins
		mapView->mapData()->defaultProvider()->clear();


		//Pickup Pin
		GeoLocation* newPickupDrop = new GeoLocation();
		newPickupDrop->setLatitude(m_dPickupLat);
		newPickupDrop->setLongitude(m_dPickupLon);

		Marker flagPickup = Marker(UIToolkitSupport::absolutePathFromUrl(
							 QUrl("asset:///Image/icon_pickup.png")), QSize(56, 70),
							 QPoint(20, 69), QPoint(20, 1));
		newPickupDrop->setMarker(flagPickup);

		mapView->mapData()->add(newPickupDrop);

		//For Z10

		//Drop off Pin
		if (m_bDropOffSet)
		{
			GeoLocation* newDropOffDrop = new GeoLocation();
			newDropOffDrop->setLatitude(m_dDropOffLat);
			newDropOffDrop->setLongitude(m_dDropOffLon);

			Marker flagDropoff = Marker(UIToolkitSupport::absolutePathFromUrl(
										 QUrl("asset:///Image/icon_dropoff.png")), QSize(56, 70),
										 QPoint(20, 69), QPoint(20, 1));
			newDropOffDrop->setMarker(flagDropoff);

			mapView->mapData()->add(newDropOffDrop);
		}
	}
	else if (m_nFlag == DROP_OFF_PAGE)
	{
		m_bDropOffSet = true;
		m_dDropOffLat = m_dTempLat;
		m_dDropOffLon = m_dTempLon;
		Label* dropoffInfo = m_paneTabedMain->findChild<Label*>("dropoffInfoLabel");
		dropoffInfo->setProperty("strDropOffLocation", strLocation);
		dropoffInfo->setProperty("bSetDropOff", true);


		//Set map center
		mapView->setLatitude(m_dTempLat);
		mapView->setLongitude(m_dTempLon);

		//Remove All pins
		mapView->mapData()->defaultProvider()->clear();

		//Pickup off Pin
		if (m_bPickupSet)
		{
			GeoLocation* newPickupDrop = new GeoLocation();
			newPickupDrop->setLatitude(m_dPickupLat);
			newPickupDrop->setLongitude(m_dPickupLon);

			Marker flagPickup = Marker(UIToolkitSupport::absolutePathFromUrl(
										 QUrl("asset:///Image/icon_pickup.png")), QSize(56, 70),
										 QPoint(20, 69), QPoint(20, 1));

			newPickupDrop->setMarker(flagPickup);

			mapView->mapData()->add(newPickupDrop);
		}

		GeoLocation* newDropOffDrop = new GeoLocation();
		newDropOffDrop->setLatitude(m_dDropOffLat);
		newDropOffDrop->setLongitude(m_dDropOffLon);

		Marker flagDropoff = Marker(UIToolkitSupport::absolutePathFromUrl(
												 QUrl("asset:///Image/icon_dropoff.png")), QSize(56, 70),
												 QPoint(20, 69), QPoint(20, 1));
		newDropOffDrop->setMarker(flagDropoff);

		mapView->mapData()->add(newDropOffDrop);
	}

	Tab* mainTab = m_paneTabedMain->findChild<Tab*>("mainBookingTab");
	mainTab->setProperty("nType", 1);
	return;
}

void UnicablinkEventHandler::showMainPlaces()
{
	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");

	//Drop off Pin
	if (m_bPickupSet)
	{
		GeoLocation* newPickupDrop = new GeoLocation();
		newPickupDrop->setLatitude(m_dPickupLat);
		newPickupDrop->setLongitude(m_dPickupLon);

		Marker flagPickup = Marker(UIToolkitSupport::absolutePathFromUrl(
												 QUrl("asset:///Image/icon_pickup.png")), QSize(56, 70),
												 QPoint(20, 69), QPoint(20, 1));
		newPickupDrop->setMarker(flagPickup);

		mapView->mapData()->add(newPickupDrop);
	}

	if (m_bDropOffSet)
	{
		GeoLocation* newDropOffDrop = new GeoLocation();
		newDropOffDrop->setLatitude(m_dDropOffLat);
		newDropOffDrop->setLongitude(m_dDropOffLon);

		Marker flagDropoff = Marker(UIToolkitSupport::absolutePathFromUrl(
												 QUrl("asset:///Image/icon_dropoff.png")), QSize(56, 70),
												 QPoint(20, 69), QPoint(20, 1));
		newDropOffDrop->setMarker(flagDropoff);

		mapView->mapData()->add(newDropOffDrop);
	}

	//Set center of Map
	if (m_bDropOffSet)
	{
		//Set map center with Drop off place
		mapView->setLatitude(m_dDropOffLat);
		mapView->setLongitude(m_dDropOffLon);
	}
	else if(m_bPickupSet)
	{
		//Set map center with pickup place
		mapView->setLatitude(m_dPickupLat);
		mapView->setLongitude(m_dPickupLon);
	}

	if (m_bDropOffSet && m_bPickupSet)
	{
		mapView->setAltitude(5000);
	}
}

void UnicablinkEventHandler::setMapPosition(QString strLat, QString strLon, QString strAddress)
{
	Q_UNUSED(strAddress);

	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");

	if (!mapView)
		return;

	double dLat = strLat.toDouble();
	double dLon = strLon.toDouble();

	mapView->setLatitude(dLat);
	mapView->setLongitude(dLon);

	QMap<QString, QString> params;

	QString strLatlng = double2String(dLat) + "," + double2String(dLon);
	params.insert("latlng", strLatlng);
	params.insert("sensor", "true");
	bool bConnect = m_jsonConnect->SendGetRequest(params, SLOT(setBubbleDescription()));
	Q_UNUSED(bConnect);
}

void UnicablinkEventHandler::setBubbleDescription() {
	/*
	 * Get Address from google api
	 */

	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

	if (reply)
	{
		if (reply->error() == QNetworkReply::NoError)
		{
			int available = reply->bytesAvailable();

			if (available > 0)
			{
				QByteArray buffer(reply->readAll());
				QVariant varJSON = m_jsonDataAccess.loadFromBuffer(buffer);
				if (m_jsonDataAccess.hasError()) {
					//Get Null string, return
					showMessage("Please check network connection, Try again later.");
					return;
				} else {
					m_jsonMap = varJSON.value<QVariantMap>();
				}
			}
		}
	}

	QString strStatus = m_jsonMap.value("status").toString();

	if (strStatus != "OK")
	{
		showMessage("Service Failed, Please try again later.");
		return;
	}

	QVariant varOrder = m_jsonMap.value("results");

	QVariantList placeList = varOrder.value<QVariantList>();
	int nCount = placeList.count();

	if (nCount == 0)
	{
		showMessage("This location is not appropriate location.");
		return;
	}

	QVariantMap currentAddressMap = placeList.at(0).value<QVariantMap>();
	QString strLocation = currentAddressMap.value("formatted_address").toString();

	TextField* addressTextField = m_paneTabedMain->findChild<TextField*>("addressField");
	addressTextField->setProperty("text", QVariant(strLocation));

	ScrollView* candListScrollView = m_paneTabedMain->findChild<ScrollView*>(
			"candListScrollView");
	candListScrollView->setVisible(false);

	//Set the description label text
	Label* descriptionLabel = m_paneTabedMain->findChild<Label*>("descriptionLabel");
	descriptionLabel->setProperty("text", strLocation);

	Container* descriptionContainer = m_paneTabedMain->findChild<Container*>("descriptionContainer");
	descriptionContainer->setVisible(true);
}

////////////////////////////////////////   UI implementation   /////////////////////////////////////////////
//Show and hide Above Container
void UnicablinkEventHandler::showAboveContainer()
{
	Container* profileContainer = m_paneTabedMain->findChild<Container*>("oldProfile");
	profileContainer->setProperty("visible", true);

}

void UnicablinkEventHandler::hideAboveContainer()
{
	Container* profileContainer = m_paneTabedMain->findChild<Container*>("oldProfile");
	profileContainer->setProperty("visible", false);
}

//Set up Pickup and Dropoff Flags
void UnicablinkEventHandler::setPickupFlag(QString strPickupLocation) {
	m_nFlag = PICK_UP_PAGE;

	TextField* addressText = m_paneTabedMain->findChild<TextField*>("addressField");
	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");

	if (strPickupLocation != "")
	{
		addressText->setText(strPickupLocation);

		//Set the description label text
		Label* descriptionLabel = m_paneTabedMain->findChild<Label*>("descriptionLabel");
		descriptionLabel->setProperty("text", strPickupLocation);

		mapView->setLatitude(m_dPickupLat);
		mapView->setLongitude(m_dPickupLon);
	}
	else
	{
		addressText->setText("");
		Container* descriptionContainer = m_paneTabedMain->findChild<Container*>("descriptionContainer");
		descriptionContainer->setVisible(false);
		mapView->setLatitude(3.1581084);
		mapView->setLongitude(101.7118345);
	}

	//Remove All pins
	mapView->mapData()->defaultProvider()->clear();
}

void UnicablinkEventHandler::setDropOffFlag(QString strDropoffLocation) {
	m_nFlag = DROP_OFF_PAGE;

	TextField* addressText = m_paneTabedMain->findChild<TextField*>("addressField");
	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");

	if (strDropoffLocation != "")
	{
		addressText->setText(strDropoffLocation);

		//Set the description label text
		Label* descriptionLabel = m_paneTabedMain->findChild<Label*>("descriptionLabel");
		descriptionLabel->setProperty("text", strDropoffLocation);

		mapView->setLatitude(m_dDropOffLat);
		mapView->setLongitude(m_dDropOffLon);
	}
	else
	{
		addressText->setText("");
		Container* descriptionContainer = m_paneTabedMain->findChild<Container*>("descriptionContainer");
		descriptionContainer->setVisible(false);
		mapView->setLatitude(3.1581084);
		mapView->setLongitude(101.7118345);
	}

	//Remove All pins
	mapView->mapData()->defaultProvider()->clear();

}

void UnicablinkEventHandler::changeTime() {
	if (m_paneTabedMain) {
		DateTimePicker* dateTimePicker = m_paneTabedMain->findChild<
				DateTimePicker*>("timePicker");
		QString strTime = dateTimePicker->value().toString("yyyy-MM-dd hh:mm");

		Label* timeLabel = m_paneTabedMain->findChild<Label*>("timeLabel");

		timeLabel->setText(strTime);
	}
}

void UnicablinkEventHandler::createActivityDialog() {
	m_activityDialog = Dialog::create();

	Container* mainContainer = Container::create();

	DockLayout *pDockLayout = new DockLayout();
	mainContainer->setLayout(pDockLayout);

	//set main width and height
	DisplayInfo display;

	int nWidth = display.pixelSize().width();
	int nHeight = display.pixelSize().height();
	mainContainer->setMinWidth(nWidth);
	mainContainer->setMinHeight(nHeight);

	ActivityIndicator* activityIndicator = ActivityIndicator::create();
	activityIndicator->setPreferredWidth(200);
	activityIndicator->setHorizontalAlignment(HorizontalAlignment::Center);
	activityIndicator->setVerticalAlignment(VerticalAlignment::Center);

	mainContainer->add(activityIndicator);

	m_activityDialog->setContent(mainContainer);
}

void UnicablinkEventHandler::startActivity() {
	Container* mainContainer = (Container*) m_activityDialog->content();
	ActivityIndicator* activityIndicator =
			(ActivityIndicator*) mainContainer->at(0);
	activityIndicator->start();

	m_activityDialog->open();
}

void UnicablinkEventHandler::stopActivity() {
	Container* mainContainer = (Container*) m_activityDialog->content();
	ActivityIndicator* activityIndicator =
			(ActivityIndicator*) mainContainer->at(0);
	activityIndicator->stop();

	m_activityDialog->close();
}

void UnicablinkEventHandler::showMessage(QString strMessage) {
	SystemDialog *dialog = new SystemDialog("OK");
	dialog->setTitle("Unicablink");
	dialog->setBody(strMessage);
	dialog->show();
}

void UnicablinkEventHandler::showMessage(QString strTitle, QString strMessage) {
	SystemDialog *dialog = new SystemDialog("OK");
	dialog->setTitle(strTitle);
	dialog->setBody(strMessage);
	dialog->show();
}

void UnicablinkEventHandler::setCorrectTime(QDateTime changedTime) {
	if (m_paneTabedMain) {
		if (changedTime < QDateTime::currentDateTime()) {
			DateTimePicker* dateTimePicker = m_paneTabedMain->findChild<
					DateTimePicker*>("timePicker");
			dateTimePicker->setProperty("bByCode", true);
			dateTimePicker->setValue(QDateTime::currentDateTime());
		}
	}
}
//////////////////////////////////////////////// Utility ////////////////////////////////////////////////////
//Convert double to String
void UnicablinkEventHandler::correctTextField(QObject* textFieldObject, QString strText)
{
//	showMessage("changing", strText);

	TextField* textField = qobject_cast<TextField*>(textFieldObject);

	int nLength = strText.length();

	if (nLength == 1)
	{
		if (strText != "+")
		{
			textField->setText("");
		}
	}
	else if (nLength > 1)
	{
		bool bchanged = false;
		for(int c = nLength - 1; c > 0; c--)
		{
			QString strRemain = strText.left(c);
			if(c != nLength - 1)
			{
				strRemain += strText.right(nLength - c - 1);
			}
			QChar ch = strText.at(c);

			if ((ch != '0')
				&& (ch != '1')
				&& (ch != '2')
				&& (ch != '3')
				&& (ch != '4')
				&& (ch != '5')
				&& (ch != '6')
				&& (ch != '7')
				&& (ch != '8')
				&& (ch != '9'))
			{
				strText = strRemain;
				nLength = strText.length();
				bchanged = true;
			}
		}

		if(bchanged)
		{
			textField->setText(strText);
		}
	}
}

QString UnicablinkEventHandler::double2String(double dNum)
{
	QString strResult;

	if (dNum < 10)
	{
		strResult = QString::number(dNum, 'g', 8);
	}
	else if (dNum < 100)
	{
		strResult = QString::number(dNum, 'g', 9);
	}
	else if (dNum >= 100)
	{
		strResult = QString::number(dNum, 'g', 10);
	}

	return strResult;
}

double UnicablinkEventHandler::string2double(QString strNumber)
{
	//102.1231231
	int nIndex = strNumber.indexOf(".");
	QString strTemp = strNumber.mid(0, nIndex);

	int nIntegerValue = strTemp.toInt();

	strTemp = strNumber.mid(nIndex + 1, strNumber.length() - nIndex - 1);

	int nLength = strTemp.length();

	double dArray[] = {0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001};
	double temp = nIntegerValue;
	for (int i = 0; i < nLength; i++)
	{
		temp += dArray[i] * strTemp.mid(i, 1).toInt();
	}

	return temp;
}

//Get IMEI
QString UnicablinkEventHandler::getIMEI() {
	QString strIMEI = "";

	HardwareInfo hdInfo(this);

	//get IMEI
	strIMEI = hdInfo.imei();

	return strIMEI;
}

//Get MD5 Password
QString UnicablinkEventHandler::getMD5Password(QString strPassword) {
	QString strMD5Password = "";

	strMD5Password = QString(
			QCryptographicHash::hash(strPassword.toAscii(),
					QCryptographicHash::Md5).toHex());

	return strMD5Password;
}

//Get reponse type string
QString UnicablinkEventHandler::getResponseType(int nType) {
	QString strResponseType = "";

	switch (nType) {
	case USER_LOG_IN: {
		strResponseType = "user_login";
		break;
	}

	case USER_REGISTER: {
		strResponseType = "user_register";
		break;
	}

	case USER_ACTIVATE: {
		strResponseType = "user_login";
		break;
	}

	case RESEND_ACTIVATION_CODE: {
		strResponseType = "send_activation_code";
		break;
	}

	case RETRIEVE_PASSWORD: {
		strResponseType = "retrieve_password";
		break;
	}

	case ORDER_CAB: {
		strResponseType = "cab_order";
		break;
	}

	case ORDER_STATUS_UPDATE: {
		strResponseType = "get_order_status";
		break;
	}

	case MODIFY_PROFILE: {
		strResponseType = "update_profile";
		break;
	}

	case CANCEL_ORDER: {
		strResponseType = "cancelorder_sunlight";
		break;
	}

	case QUERY_POSITION_INFO: {
		strResponseType = "getvehiclelastknowntrack";
		break;
	}

	default: {
		break;
	}
	}

	return strResponseType;
}

/////////////////////////////////////// Utility functions for User Information validation //////////////////////////////
void UnicablinkEventHandler::showFullLocation(QString strLableName, QString strLocation)
{
	if (strLableName == "Pick up")
	{
		showMessage("Pick up Location", strLocation);
	}
	else
	{
		showMessage("Drop off Location", strLocation);
	}
//	static int num = 0;
//	Container* statusContainer = m_pageCurrentBooking->findChild<Container*>("statusItem");
//	statusContainer->setProperty("valueText", m_strStatusMessage[num++]);
//	if(num >= 11) num = 0;
}

bool UnicablinkEventHandler::checkName(QString strName) {
	if (strName.length() == 0)
		return false;
	return true;
}

bool UnicablinkEventHandler::checkEmailAddress(QString strEmail) {
	/*
	 * 	//check "*.com"
	 if (!strText.endsWith(".com")) return false;

	 boolean bCheckAmp = false;
	 for (int i = 0; i < strText.length(); i++){
	 String strTemp = String.valueOf(strText.charAt(i));
	 if (strTemp.equals("@")) {
	 bCheckAmp = true;
	 break;
	 }
	 }

	 //check containing of "@"
	 if (!bCheckAmp) return false;

	 if ((strText.indexOf("@") == 0) || (strText.indexOf("@") == strText.length() - 5)) return false;
	 return true;
	 *
	 */

	bool bCheck = true;
	if (!strEmail.endsWith(".com")) {
		bCheck = false;
	}

	if (!strEmail.contains("@")) {
		bCheck = false;
	}

	int nIndex = strEmail.indexOf("@");
	if ((nIndex == 0) || (nIndex == strEmail.length() - 5)) {
		bCheck = false;
	}

	if (!bCheck)
		showMessage("Please input valid Email Address.");

	return bCheck;
}

bool UnicablinkEventHandler::checkPhoneNo(QString strPhone) {
	if (!strPhone.startsWith("+")) {
		showMessage("Phone number should start with +");
		return false;
	}

	if ((strPhone.length() < 11) || (strPhone.length() > 15)) {
		showMessage("Please input valid phone number");
		return false;
	}

	//check containing only number ('0' ~ '9')
	for (int i = 1; i < strPhone.length(); i++) {
		if ((strPhone.at(i) < '0') || (strPhone.at(i) > '9')) {
			showMessage("Please input valid phone number");
			return false;
		}
	}

	return true;
}

bool UnicablinkEventHandler::checkPassword(QString strPassword,
		QString strRetype) {
	if (strPassword.length() != 6) {
		showMessage("Password must be 6 digits.");
		return false;
	}

	if (strPassword != strRetype) {
		showMessage("Please retype password correctly.");
		return false;
	}

	return true;
}

//Parse XML part
QString UnicablinkEventHandler::getJSONString(QXmlStreamReader& xml) {
	/* We need a start element, like <foo> */
	if (xml.tokenType() != QXmlStreamReader::StartElement) {
		return "";
	}

	/* Let's read the name... */
	QString elementName = xml.name().toString();
	/* ...go to the next. */
	xml.readNext();
	/*
	 * This elements needs to contain Characters so we know it's
	 * actually data, if it's not we'll leave.
	 */
	if (xml.tokenType() != QXmlStreamReader::Characters) {
		return "";
	}
	/* Now we can add it to the map.*/
	QString elementValue = xml.text().toString();
	return elementValue;
}

//Get response
bool UnicablinkEventHandler::getResponse() {
	//Create QXmlStreamReader for parsing XML
	QXmlStreamReader xml;

	QString jsonString = "";
	//Set XML namespace Porcessing: true
	xml.setNamespaceProcessing(true);
	xml.addData(responseArray);

	QString strTagName = "";
	while (!xml.atEnd() && !xml.hasError()) {
		//Read Next element
		QXmlStreamReader::TokenType token = xml.readNext();

		//If token is just StartDocument, go to Next
		if (token == QXmlStreamReader::StartDocument) {
			continue;
		}

		strTagName = xml.name().toString();
		//If token is StartElement, see if can read it
		if (token == QXmlStreamReader::StartElement) {
			//If its name is not entry, then go Next
			if (strTagName == "Envelope") {
				token = xml.readNext();

				if (token == QXmlStreamReader::Characters) {
					strTagName = xml.name().toString();
					token = xml.readNext();
					if (token == QXmlStreamReader::StartElement) {
						token = xml.readNext();
						if (token == QXmlStreamReader::Characters) {
							token = xml.readNext();
							if (token == QXmlStreamReader::StartElement) {
								token = xml.readNext();
								if (token == QXmlStreamReader::Characters) {
									token = xml.readNext();
									if (token
											== QXmlStreamReader::StartElement) {
										token = xml.readNext();
										if (token
												== QXmlStreamReader::Characters) {
											strTagName = xml.name().toString();
											QString strJSON =
													xml.text().toString();

											QVariant varJSON =
													m_jsonDataAccess.loadFromBuffer(
															strJSON);

											if (m_jsonDataAccess.hasError()) {
												return false;
											} else {
												m_jsonMap = varJSON.value<
														QVariantMap>();
												return true;
											}
										}
									}
								}
							}
						}

					}
				}
			} else //If entry tag
			{
				continue;
			}
		}

		xml.readNext();
	}

	if (jsonString == "")
		return false;

	//Error Handling
	if (xml.hasError()) {
		qDebug() << "XML Parse Error Occured!";
	}

	xml.clear();

	return true;
}

//////////////////////////////////////////// Main Activity //////////////////////////////////////////////////////////////////
//Book now
void UnicablinkEventHandler::bookNow(QString strPickupLocation, QString strPickupAtLocation, QString strDropoffLocation, QString strTime, QString strType)
{
	//Check Fields
	if (strPickupLocation == "")
	{
		showMessage("Please choose pick up location.");//////////
		return;
	}

	if (strPickupAtLocation == "")
	{
		showMessage("Please fill 'Pick up at' field.");
		return;
	}

	if (strDropoffLocation == "")
	{
		showMessage("Please choose drop off location.");
		return;
	}

	QString strCurrentTime = "";
	QDateTime currentTime = QDateTime::currentDateTime();
	strCurrentTime = currentTime.toString("yyyy-MM-dd hh:mm");

	if (strTime == "Now")
	{
		strTime = strCurrentTime;
	}

	int nCabType = 0;
	if (strType == "Budget")
	{
		nCabType = 1;
	}
	else
	{
		nCabType = 2;
	}

	//Make soap message for request
	/*
	 * 				{
				"DEVICEID":"83e462f770cc34eb",
				"PLAT":"31.3707702",
				"TIP":"",
				"NAME":"tyuy",
				"PICKUPTIME":"2013-08-13 8:45",
				"PLON":"120.7345502",
				"REMARKS":"asdfasdf",
				"DLON":"120.311143",
				"CABTYPE":1,
				"DROPOFF":"Wuxi, Jiangsu, China",
				"DLAT":"31.490637",
				"ORDERTIME":"2013-08-13 11:49",
				"CUSTOMERID":"E22D2289-FFBE-6FCD-2B66B86F715C0695",
				"SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
				"MOBILE":"+601126253547",
				"EMAIL":"503488188@qq.com",
				"PICKUP":"Chengwan Road, Wuzhong, Suzhou, Jiangsu, China, 215121"
				}
	 *
	 */
	QMap<QString, QVariant> params;

	//get User Information
	QSettings appSetting("UnicablinkSoft", "Unicablink");
	QString strName = appSetting.value("Name").toString();
	QString strEmail = appSetting.value("Email").toString();
	QString strPassword = appSetting.value("Password").toString();
	QString strCustomerID = appSetting.value("CustomerID").toString();
	QString strMobile = appSetting.value("Phone").toString();

	QString strIMEI = getIMEI();
	params.insert("DEVICEID", getIMEI());
	QString strTemp = double2String(m_dPickupLat);
	params.insert("PLAT", double2String(m_dPickupLat));
	params.insert("TIP", "");
	params.insert("NAME", strName);
	params.insert("PICKUPTIME", strTime);

	strTemp = double2String(m_dPickupLon);
	params.insert("PLON", double2String(m_dPickupLon));
	params.insert("REMARKS", strPickupAtLocation);

	strTemp = double2String(m_dDropOffLon);
	params.insert("DLON", double2String(m_dDropOffLon));

	strTemp = QString::number(nCabType);
	params.insert("CABTYPE", QString::number(nCabType));
	params.insert("DROPOFF", strDropoffLocation);

	strTemp = double2String(m_dDropOffLat);
	params.insert("DLAT", double2String(m_dDropOffLat));
	params.insert("ORDERTIME", strCurrentTime);
	params.insert("CUSTOMERID", strCustomerID);
	params.insert("MOBILE", strMobile);
	params.insert("EMAIL", strEmail);
	params.insert("PICKUP", strPickupLocation);

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			ORDER_CAB);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(checkOrderResult()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;
}

void UnicablinkEventHandler::checkOrderResult()
{
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			showMessage("Service Failed, Please try again later.");
			return;
		}
	} else {
		stopActivity();
		showMessage(
				"Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	//Analyze Response
	QString strMessage = m_jsonMap.value("Message").toString();
	m_nCurrentOrderID = m_jsonMap.value("OrderID").toInt();

	if (m_nCurrentOrderID == 0)
	{
		showMessage(strMessage);
		return;
	}

	//Show Confirm Dialog
	Dialog* dialogConfirm = m_paneTabedMain->findChild<Dialog*>("confirmDialog");

	dialogConfirm->open();

	return;
}

void UnicablinkEventHandler::showOrderDetails() {
	//Create current Booking page
	if (!m_pageCurrentBooking) {
		QmlDocument *qml =
				QmlDocument::create("asset:///currentBooking.qml").parent(this);

		if (!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);
			m_pageCurrentBooking = qml->createRootObject<Page>();
		}
	}

	//Initialize My Booking Page
	QSettings appSetting("UnicablinkSoft", "Unicablink");

	QString strPhone = appSetting.value("Phone").toString();
	QDateTime currentTime = QDateTime::currentDateTime();
	QString strCurrentTime = currentTime.toString("yyyy-MM-dd hh:mm:ss");

	QVariantMap currentOrder;
	currentOrder["OrderID"] = m_nCurrentOrderID;
	currentOrder["REQUESTCOUNT"] = 0;

	QVariant currentOrderData = QVariant(QVariantList() << QVariant(currentOrder));

	JsonDataAccess jda;

	QString strOrderArray;
	jda.saveToBuffer(currentOrderData, &strOrderArray);

	m_varOrderList.removeAt(m_varOrderList.indexOf(QVariant(m_nCurrentOrderID), 0));
	m_varOrderList.append(QVariant(m_nCurrentOrderID));
	m_nOrderCount = m_varOrderList.count();

	appSetting.setValue("Orders", m_varOrderList);

	//Make Request
	/*
	 * {
	 �SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
	 �MOBILE�:�+60174771691�,
	 �ORDERArray� : [{�OrderID� : 211569},{�OrderID� : 211570}, {�OrderID� : 211571}],
	 �REQUESTTIME�:�2011-05-23 11:43:44�
	 *	}
	 */
	QMap<QString, QVariant> params;
	params.insert("MOBILE", strPhone);
	params.insert("REQUESTTIME", strCurrentTime);
	params.insert("ORDERArray", currentOrderData);

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			ORDER_STATUS_UPDATE);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(resetCurrentBookingPage()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;
}

//Reset Current Booking Page
void UnicablinkEventHandler::resetCurrentBookingPage()
{
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}
	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	/*
	 * {
	 �SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
	 �ORDERARRAY�  : [ {OrderID : 2114567, �Status� : 2, �Message�: �On The Way�, Please Wait�, �CabNo� : �HWC1234�, �CabType� : 1, �Driver� : �Nicole Cheng (+60162038962)�,
	 �PickupTime� : �2013-02-09 11:24:55�, �Pickup� : �Jalan Maharajalela�, Dropoff : �Berjaya Times Square�, �ETA� : �0.03KM from Pickup Place�,
	 �EFare� : �RM10 ~ RM13�, �ShowTaxi� : 1},
	 {OrderID : 2114568, �Status� : 3, �Message�: �Arrived, Please go to Pickup Point�, �CabNo� : �HWC1234�, �CabType� : 1, �Driver� : �Nicole Cheng (+60162038962)�, �PickupTime� : �2013-02-09 11:24:55�, �Pickup� : �Jalan Maharajalela�, Dropoff : �Berjaya Times Square�, �ETA� : �0.03KM from Pickup Place�, �EFare� : �RM10 ~ RM13�, �ShowTaxi� : 1}  ],
	 �RESPONSETIME�:�2011-05-20 10:36:44�
	 * }
	 */
	QVariant varOrder = m_jsonMap.value("ORDERARRAY");

	QVariantList orderList = varOrder.value<QVariantList>();

	int nSize = orderList.count();

	if (nSize == 0) return;

	QVariantMap currentOrderMap = orderList.at(0).value<QVariantMap>();

	//Get Response
	QString strOrderId = currentOrderMap.value("ORDERID").toString();
	QString strMessage = currentOrderMap.value("MESSAGE").toString();
	QString strCabNo = currentOrderMap.value("CABNO").toString();

	int nCabType = currentOrderMap.value("CABTYPE").toInt();
	QString strCabType = (nCabType == 1)? "Budget": "Premium / Executive";

	QString strDriver = currentOrderMap.value("DRIVER").toString();
	QString strPickupTime = currentOrderMap.value("PICKUPTIME").toString();
	QString strPickup = currentOrderMap.value("PICKUP").toString();
	QString strDropOff = currentOrderMap.value("DROPOFF").toString();
	QString strETA = currentOrderMap.value("ETA").toString();
	QString strEFare = currentOrderMap.value("EFARE").toString();
	QString strRemarks = currentOrderMap.value("REMARKS").toString();
	int nStatus = currentOrderMap.value("STATUS").toInt();

	QString strStatus = m_strStatusMessage[nStatus];

	//Create current Booking page
	if (!m_pageCurrentBooking) {
		QmlDocument *qml =
				QmlDocument::create("asset:///currentBooking.qml").parent(this);

		if (!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);
			m_pageCurrentBooking = qml->createRootObject<Page>();
		}
	}

	ImageButton* cancelButton = m_pageCurrentBooking->findChild<ImageButton*>(
			"cancelButton");

	cancelButton->setEnabled(true);


	//Setting Value of current booking page
	Label* orderIDLabel = m_pageCurrentBooking->findChild<Label*>("orderIdLabel");
	orderIDLabel->setText(strOrderId);

	/*
		dateTimeContainer
		pickupContainer
		pickupatContainer
		dropoffContainer
		carNoContainer
		carTypeContainer
		driverContainer
		etaContainer
		estContainer
	 */
	Container* dateTimeContainer = m_pageCurrentBooking->findChild<Container*>("dateTimeContainer");
	dateTimeContainer->setProperty("valueText", strPickupTime);

	Container* pickupContainer = m_pageCurrentBooking->findChild<Container*>("pickupContainer");
	pickupContainer->setProperty("valueText", strPickup);

	Container* pickupatContainer = m_pageCurrentBooking->findChild<Container*>("pickupatContainer");
	pickupatContainer->setProperty("valueText", strRemarks);

	Container* dropoffContainer = m_pageCurrentBooking->findChild<Container*>("dropoffContainer");
	dropoffContainer->setProperty("valueText", strDropOff);

	Container* carNoContainer = m_pageCurrentBooking->findChild<Container*>("carNoContainer");
	carNoContainer->setProperty("valueText", strCabNo);

	Container* carTypeContainer = m_pageCurrentBooking->findChild<Container*>("carTypeContainer");
	carTypeContainer->setProperty("valueText", strCabType);

	Container* driverContainer = m_pageCurrentBooking->findChild<Container*>("driverContainer");
	driverContainer->setProperty("valueText", strDriver);

	Container* etaContainer = m_pageCurrentBooking->findChild<Container*>("etaContainer");
	etaContainer->setProperty("valueText", strETA);

	Container* estContainer = m_pageCurrentBooking->findChild<Container*>("estContainer");
	estContainer->setProperty("valueText", strEFare);

	Container* statusContainer = m_pageCurrentBooking->findChild<Container*>("statusItem");
	statusContainer->setProperty("valueText", strStatus);

	if ((nStatus == 4) || (nStatus == 5) || (nStatus == 6) || (nStatus == 7) || (nStatus == 8))
	{
		statusContainer->setProperty("textImageSource",
				"asset:///Image/bar_btm_red_order.png");

		ImageButton* cancelButton = m_pageCurrentBooking->findChild<ImageButton*>(
				"cancelButton");

		m_orderTimer->stop();

		cancelButton->setEnabled(false);
	}
	else
	{
		statusContainer->setProperty("textImageSource",
				"asset:///Image/bar_btm_purple_order.png");

		ImageButton* cancelButton = m_pageCurrentBooking->findChild<ImageButton*>(
				"cancelButton");

		if(!m_orderTimer->isActive())
			m_orderTimer->start(30000);

		cancelButton->setEnabled(true);
	}

	showCurrentBookingPage();
}

void UnicablinkEventHandler::refreshOrderInfo()
{
	showOrderDetails();
}

//Show Current Booking Page
void UnicablinkEventHandler::showCurrentBookingPage()
{
	if (!m_pageCurrentBooking) {
		QmlDocument *qml =
				QmlDocument::create("asset:///currentBooking.qml").parent(this);

		if (!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);
			m_pageCurrentBooking = qml->createRootObject<Page>();

		}
	}

	m_pageCurrentBooking->setParent(m_pMainInstance);

	// set created root object as a scene
	bb::cascades::Application::instance()->setScene(m_pageCurrentBooking);
}

//Find Location
void UnicablinkEventHandler::findLocation(QString strAddress) {
	if (strAddress.length() == 0)
		return;


	//Make Request
	QMap<QString, QVariant> params;

	/*
	 {
	 "Name":"KU",
	 "SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 "REQUESTTIME":"2013-08-12 11:23:26",
	 "CurCoordinate":"",
	 "Type":"2",
	 "Coordinate":""
	 }
	 */

	QDateTime currentTime = QDateTime::currentDateTime();
	QString strCurrentTime = currentTime.toString("yyyy-MM-dd hh:mm:ss");

	params.insert("Name", strAddress);
	params.insert("REQUESTTIME", strCurrentTime);
	params.insert("CurCoordinate", "");
	params.insert("Type", "2");
	params.insert("Coordinate", "");

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			SEARCH_LOCATION);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(checkFindResult()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;

}

//Book Again
void UnicablinkEventHandler::back2MyBooking()
{
	Tab* myBookingTab = m_paneTabedMain->findChild<Tab*>("myBookingTab");

	if (myBookingTab)
	{
		initializeMyBookingPage();
//		Tab* mainbookingTab = m_paneTabedMain->findChild<Tab*>("mainbookingTab");
//		mainbookingTab->setProperty("bInitMainBooking", true);
		((TabbedPane*)m_paneTabedMain)->setActiveTab(myBookingTab);
	}

	// set created root object as a scene
	bb::cascades::Application::instance()->setScene(m_paneTabedMain);
}

void UnicablinkEventHandler::checkFindResult() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}
	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	/*
	 * {
	 LOCATIONS:
	 [{COORDINATE:3.0637590,101.7395700,DISTANCE:,NAME:Alam Damai ,Kuala Lumpur},{COORDINATE:3.1800560,101.6734440,DISTANCE:,NAME:Antah Tower Condo, Jalan Kuching,},{COORDINATE:3.1718880,101.7509670,DISTANCE:,NAME:Au 2, Kuala Lumpur},{COORDINATE:3.2836580,101.5127120,DISTANCE:,NAME:Bandar Baru Kundang , Rawang},{COORDINATE:3.1825650,101.6968560,DISTANCE:,NAME:Bandar Baru Sentul, Kuala Lumpur},{COORDINATE:3.0441400,101.7395490,DISTANCE:,NAME:Bandar Damai Perdana, Kuala Lumpur},{COORDINATE:3.1943270,101.6129350,DISTANCE:,NAME:Bandar Sri Damansara, Kuala Lumpur},{COORDINATE:3.1009050,101.7139500,DISTANCE:,NAME:Bandar Sri Permaisuri, Kuala Lumpur},{COORDINATE:3.0935480,101.7224820,DISTANCE:,NAME:Bandar Tun Razak, Kuala Lumpur},{COORDINATE:3.1335180,101.6684030,DISTANCE:,NAME:Bangsar Baru, Kuala Lumpur}],
	 RESPONSETIME:2013-08-16 08:12:36,
	 SECURITYKEY:e92684cb325f1e4d4cce27c0207562bf
	 }
	 *
	 */

	/*
	 *
	 QVariant varJSON = m_jsonDataAccess.loadFromBuffer(strJSON);

	 if(m_jsonDataAccess.hasError())
	 {
	 return false;
	 }
	 else
	 {
	 m_jsonMap = varJSON.value<QVariantMap>();
	 return true;
	 }
	 */

	//Analyze Response
	QString strResponseTime = m_jsonMap.value("RESPONSETIME").toString();
	QVariant varLocation = m_jsonMap.value("LOCATIONS");

	m_listLocation = varLocation.value<QVariantList>();

	int nLocationCount = m_listLocation.count();

	ScrollView* candListScrollView = m_paneTabedMain->findChild<ScrollView*>(
			"candListScrollView");
	candListScrollView->setVisible(true);
	candListScrollView->scrollToPoint(0,0, ScrollAnimation::None);

	Container* candListContainer = m_paneTabedMain->findChild<Container*>(
			"candListContainer");

	candListContainer->removeAll();

	for (int i = 0; i < nLocationCount; i++) {
		m_jsonLocationMap = m_listLocation.at(i).value<QVariantMap>();

		QString strName = m_jsonLocationMap.value("NAME").toString();

		QString strDistance = m_jsonLocationMap.value("DISTANCE").toString();

		QString strCoordinate =
				m_jsonLocationMap.value("COORDINATE").toString();

		int nIndex = strCoordinate.indexOf(",");

		QString strLat = strCoordinate.mid(0, nIndex);
		QString strLon = strCoordinate.mid(nIndex + 1,
				strCoordinate.length() - nIndex - 1);

		QmlDocument *qml =
				QmlDocument::create("asset:///LocationCandBox.qml").parent(
						this);

		bb::device::DisplayInfo display;
		int nScreenWidth = display.pixelSize().width();


		if (!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);
			// create container
			Container *box = qml->createRootObject<Container>();

			box->setProperty("index", QVariant(i));
			box->setProperty("backgroundSource",
					QVariant("asset:///Image/bar_insert_middle_profile.png"));
			box->setProperty("boxWidth", QVariant(nScreenWidth));

			if (nScreenWidth == 720)
			{
				box->setProperty("boxHeight", QVariant(60));
			}
			else
			{
				box->setProperty("boxHeight", QVariant(90));
			}

			box->setProperty("paddingLeft", QVariant(10));
			box->setProperty("paddingRight", QVariant(10));
			box->setProperty("labelText", QVariant(strName));
			box->setProperty("strLon", QVariant(strLon));
			box->setProperty("strLat", QVariant(strLat));

			//showMessage("Address:" + strName + "," + "Lat: " + strLat + ", Lon: " + strLon);

			candListContainer->add(box);

		}



	}

	return;
}

//Submit Password
void UnicablinkEventHandler::submitPassword(QString strPhone) {
	//Check Phone No
	if (!checkPhoneNo(strPhone))
		return;

	//Make Request
	QMap<QString, QVariant> params;

	/*
	 * "SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 * "Mobile":"+601126253547",
	 * "Email":"",
	 * "REQUESTTIME":"2013-08-16 00:00:16"
	 */

	QDateTime currentTime = QDateTime::currentDateTime();
	QString strCurrentTime = currentTime.toString("yyyy-MM-dd hh:mm:ss");

	params.insert("Mobile", strPhone);
	params.insert("REQUESTTIME", strCurrentTime);
	params.insert("Email", "");

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			RETRIEVE_PASSWORD);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(checkSubmitResult()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;
}

void UnicablinkEventHandler::checkSubmitResult() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}
	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	/*
	 * {
	 �SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
	 �STATUS�:1,
	 �MESSAGE�: �Password have send to your mobile or mail�,
	 �RESPONSETIME�:�2012-03-14 14:32:55�
	 }
	 *
	 */

	//Analyze Response
	int nStatus = m_jsonMap.value("STATUS").toInt();
	QString strMessage = m_jsonMap.value("MESSAGE").toString();

	if (nStatus == 1) {
		showMessage(strMessage);
	} else if (nStatus == 2) {
		showMessage(strMessage);
	} else {
		showMessage(strMessage);
	}

	return;
}

//Cancel Order
void UnicablinkEventHandler::cancelOrder(QString strOrderID) {
	//Update Profile
	QMap<QString, QVariant> params;

	/*
	 * {
	 �SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
	 �ORDERID�:1638827,
	 �REQUESTTIME�:�2012-04-23 18:00:00�
	 * }
	 */

	QDateTime currentTime = QDateTime::currentDateTime();
	QString strCurrentTime = currentTime.toString("yyyy-MM-dd hh:mm:ss");

	params.insert("ORDERID", strOrderID);
	params.insert("REQUESTTIME", strCurrentTime);

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			CANCEL_ORDER);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(checkCancelResult()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;
}

void UnicablinkEventHandler::checkCancelResult() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}
	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	/*
	 * {
	 �SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
	 �ORDERID�:160165,
	 �STATUS�:1,
	 �MESSAGE�: �Passenger Cancelled�,
	 �RESPONSETIME�:�2012-03-14 14:32:55�
	 }
	 *
	 */

	//Analyze Response
	int nStatus = m_jsonMap.value("STATUS").toInt();
	QString strMessage = m_jsonMap.value("MESSAGE").toString();

	if (nStatus == 1) {
		if (strMessage.length() == 0)
			strMessage = "Passenger Cancelled.";
		showMessage(strMessage);

		//Change the status
		Container* statusItem = m_pageCurrentBooking->findChild<Container*>(
				"statusItem");

		statusItem->setProperty("textImageSource",
				"asset:///Image/bar_btm_red_order.png");
		statusItem->setProperty("valueText", "Order Cancelled.");

		ImageButton* cancelButton = m_pageCurrentBooking->findChild<ImageButton*>(
				"cancelButton");

		cancelButton->setEnabled(false);
	} else if (nStatus == 2) {
		if (strMessage.length() == 0)
			strMessage = "No Order Cancelled.";
		showMessage(strMessage);
	} else {
		if (strMessage.length() == 0)
			strMessage = "Strange response.";
		showMessage(strMessage);
	}

	return;
}

//Update Profile
//Input:  nameTextField.textValue, emailTextField.textValue, phoneTextField.textValue,
// 			currentPasswordTextField.textValue, newPasswordTextField.textValue, retypeTextField.textValue
void UnicablinkEventHandler::updateProfile(QString strName, QString strEmail,
		QString strPhone, QString strCurrentPassword, QString strNewPassword,
		QString strRetype) {
	if (!checkName(strName))
		return;
	if (!checkEmailAddress(strEmail))
		return;
	if (!checkPhoneNo(strPhone))
		return;
	if (!checkPassword(strNewPassword, strRetype))
		return;

	//Validate Password
	QSettings appSetting("UnicablinkSoft", "Unicablink");
	QString strRealPassword = appSetting.value("Password").toString();

	if (strRealPassword != strCurrentPassword) {
		showMessage("Wrong Password, Please input correct password");
		return;
	}

	//Update Profile
	QMap<QString, QVariant> params;

	m_strName = strName;
	m_strEmail = strEmail;
	m_strPhoneNo = strPhone;
	m_strPassword = strNewPassword;

	/*
	 * {
	 �SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
	 �NAME�:�Micheal�,
	 �CUSTOMERID�:� 8AE8BE30-D459-8565-47F75BD4134A101C�
	 �MOBILE�:�+8615806131326�,
	 �OLDPASSWORD�:�123456�
	 �NEWPASSWORD�:�abcd12�,
	 �PHONETYPE�:��,
	 �EMAIL�: �yuan@obama.com�
	 }
	 *
	 */

	QString strCustomerID = appSetting.value("CustomerID").toString();

	if (strName == appSetting.value("Name").toString())
		params.insert("NAME", "");
	else
		params.insert("NAME", strName);

	if (strEmail == appSetting.value("Email").toString())
		params.insert("EMAIL", "");
	else
		params.insert("EMAIL", strEmail);

	params.insert("MOBILE", "");

	params.insert("CUSTOMERID", strCustomerID);

	if (strCurrentPassword == strNewPassword) {
		params.insert("OLDPASSWORD", "");
		params.insert("NEWPASSWORD", "");
	} else {
		params.insert("OLDPASSWORD", strCurrentPassword);
		params.insert("NEWPASSWORD", strNewPassword);
	}
	params.insert("PHONETYPE", "3");

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			MODIFY_PROFILE);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(checkUpdateResult()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;
}

void UnicablinkEventHandler::checkUpdateResult() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}
	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	/*
	 * {
	 �SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
	 �STATUS�:�1�,
	 �RESPONSETIME�:�2011-05-20 11:43:44�
	 * }
	 */

	//Analyze Response
	int nStatus = m_jsonMap.value("Status").toInt();

	if (nStatus == 1) {
		QSettings appSetting("UnicablinkSoft", "Unicablink");

		appSetting.setValue("Name", m_strName);
		appSetting.setValue("Email", m_strEmail);
		appSetting.setValue("Phone", m_strPhoneNo);
		appSetting.setValue("Password", m_strPassword);

		showMessage("Updated successfully.");
	} else if (nStatus == 2) {
		showMessage("Failed Update.");
	} else {
		showMessage("Service Failed, Please try again later.");
	}

	return;
}

//Show details for order
void UnicablinkEventHandler::showDetails(QString strOrderID)
{
	/*
	 * {
	 �SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
	 �ORDERARRAY�  : [ {OrderID : 2114567, �Status� : 2, �Message�: �On The Way�, Please Wait�, �CabNo� : �HWC1234�, �CabType� : 1, �Driver� : �Nicole Cheng (+60162038962)�,
	 �PickupTime� : �2013-02-09 11:24:55�, �Pickup� : �Jalan Maharajalela�, Dropoff : �Berjaya Times Square�, �ETA� : �0.03KM from Pickup Place�,
	 �EFare� : �RM10 ~ RM13�, �ShowTaxi� : 1},
	 {OrderID : 2114568, �Status� : 3, �Message�: �Arrived, Please go to Pickup Point�, �CabNo� : �HWC1234�, �CabType� : 1, �Driver� : �Nicole Cheng (+60162038962)�, �PickupTime� : �2013-02-09 11:24:55�, �Pickup� : �Jalan Maharajalela�, Dropoff : �Berjaya Times Square�, �ETA� : �0.03KM from Pickup Place�, �EFare� : �RM10 ~ RM13�, �ShowTaxi� : 1}  ],
	 �RESPONSETIME�:�2011-05-20 10:36:44�
	 * }
	 */

	int nCount = m_orderInfoList.count();

	for (int i = 0; i < nCount; i++)
	{
		QVariantMap currentOrderMap = m_orderInfoList.at(i).value<QVariantMap>();

		//Get Response
		QString strCurrentOrderId = currentOrderMap.value("ORDERID").toString();

		if (strCurrentOrderId == strOrderID)
		{
			//Get Response
			QString strOrderId = currentOrderMap.value("ORDERID").toString();
			QString strMessage = currentOrderMap.value("MESSAGE").toString();
			QString strCabNo = currentOrderMap.value("CABNO").toString();

			int nCabType = currentOrderMap.value("CABTYPE").toInt();
			QString strCabType = (nCabType == 1)? "Budget": "Premium / Executive";

			QString strDriver = currentOrderMap.value("DRIVER").toString();
			QString strPickupTime = currentOrderMap.value("PICKUPTIME").toString();
			QString strPickup = currentOrderMap.value("PICKUP").toString();
			QString strDropOff = currentOrderMap.value("DROPOFF").toString();
			QString strETA = currentOrderMap.value("ETA").toString();
			QString strEFare = currentOrderMap.value("EFARE").toString();
			QString strRemarks = currentOrderMap.value("REMARKS").toString();
			int nStatus = currentOrderMap.value("STATUS").toInt();

			QString strStatus = m_strStatusMessage[nStatus];

			//Create current Booking page
			if (!m_pageCurrentBooking) {
				QmlDocument *qml =
						QmlDocument::create("asset:///currentBooking.qml").parent(this);

				if (!qml->hasErrors()) {
					// create a event handler object
					qml->setContextProperty("eventHandler", this);
					m_pageCurrentBooking = qml->createRootObject<Page>();
				}
			}

			//Setting Value of current booking page
			Label* orderIDLabel = m_pageCurrentBooking->findChild<Label*>("orderIdLabel");
			orderIDLabel->setText(strOrderId);

			Container* dateTimeContainer = m_pageCurrentBooking->findChild<Container*>("dateTimeContainer");
			dateTimeContainer->setProperty("valueText", strPickupTime);

			Container* pickupContainer = m_pageCurrentBooking->findChild<Container*>("pickupContainer");
			pickupContainer->setProperty("valueText", strPickup);

			Container* pickupatContainer = m_pageCurrentBooking->findChild<Container*>("pickupatContainer");
			pickupatContainer->setProperty("valueText", strRemarks);

			Container* dropoffContainer = m_pageCurrentBooking->findChild<Container*>("dropoffContainer");
			dropoffContainer->setProperty("valueText", strDropOff);

			Container* carNoContainer = m_pageCurrentBooking->findChild<Container*>("carNoContainer");
			carNoContainer->setProperty("valueText", strCabNo);

			Container* carTypeContainer = m_pageCurrentBooking->findChild<Container*>("carTypeContainer");
			carTypeContainer->setProperty("valueText", strCabType);

			Container* driverContainer = m_pageCurrentBooking->findChild<Container*>("driverContainer");
			driverContainer->setProperty("valueText", strDriver);

			Container* etaContainer = m_pageCurrentBooking->findChild<Container*>("etaContainer");
			etaContainer->setProperty("valueText", strETA);

			Container* estContainer = m_pageCurrentBooking->findChild<Container*>("estContainer");
			estContainer->setProperty("valueText", strEFare);

			Container* statusContainer = m_pageCurrentBooking->findChild<Container*>("statusItem");
			statusContainer->setProperty("valueText", strStatus);

			if ((nStatus == 6) || (nStatus == 7) || (nStatus == 8))
			{
				statusContainer->setProperty("textImageSource",
						"asset:///Image/bar_btm_red_order.png");

				ImageButton* cancelButton = m_pageCurrentBooking->findChild<ImageButton*>(
						"cancelButton");

				cancelButton->setEnabled(false);
			}
			else
			{
				statusContainer->setProperty("textImageSource",
						"asset:///Image/bar_btm_purple_order.png");

				ImageButton* cancelButton = m_pageCurrentBooking->findChild<ImageButton*>(
						"cancelButton");

				cancelButton->setEnabled(true);
			}

			showCurrentBookingPage();
		}
	}


	return;
}

//Initialize Main Booking Page
void UnicablinkEventHandler::initializeMainBookingPage()
{
	//initialize all setting for main booking page
	m_dPickupLat = 0.0;
	m_dPickupLon = 0.0;
	m_dDropOffLat = 0.0;
	m_dDropOffLon = 0.0;

	//Set flag
	m_bPickupSet = false;
	m_bDropOffSet = false;

	//Remove All pins
	MapView* mapView = m_paneTabedMain->findChild<MapView*>("mapView");
	mapView->mapData()->defaultProvider()->clear();
}

//Initialize My Booking Page
void UnicablinkEventHandler::initializeMyBookingPage() {
	if (!m_paneTabedMain)
		return;

	//Initialize My Booking Page
	QSettings appSetting("UnicablinkSoft", "Unicablink");

	QString strPhone = appSetting.value("Phone").toString();
	QDateTime currentTime = QDateTime::currentDateTime();
	QString strCurrentTime = currentTime.toString("yyyy-MM-dd hh:mm:ss");

	QVariantList tempList;

	int nOrderTempId = 0;

	m_nOrderCount = m_varOrderList.count();

	if (m_nOrderCount == 0) return;

	for (int i = 0; i < m_nOrderCount; i++)
	{
		nOrderTempId = m_varOrderList.at(i).toInt();

		QVariantMap varTempMap;
		varTempMap["OrderID"] = nOrderTempId;
		varTempMap["REQUESTCOUNT"] = 1;

		tempList << QVariant(varTempMap);
	}

	QVariant varOrderArray = QVariant(tempList);

	JsonDataAccess jda;

	QString strOrderArray;

	jda.saveToBuffer(varOrderArray, &strOrderArray);

	//Make Request
	/*
	 * {
	 �SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
	 �MOBILE�:�+60174771691�,
	 �ORDERArray� : [{�OrderID� : 211569},{�OrderID� : 211570}, {�OrderID� : 211571}],
	 �REQUESTTIME�:�2011-05-23 11:43:44�
	 *	}
	 */
	QMap<QString, QVariant> params;
	params.insert("MOBILE", strPhone);
	params.insert("REQUESTTIME", strCurrentTime);
	params.insert("ORDERArray", varOrderArray);

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			ORDER_STATUS_UPDATE);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(resetBookingPage()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;
}

//Reset Booking Page
void UnicablinkEventHandler::resetBookingPage() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}
	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	QVariant varOrder = m_jsonMap.value("ORDERARRAY");

	m_orderInfoList = varOrder.value<QVariantList>();
	int nCount = m_orderInfoList.count();

	Container* currentContainer = m_paneTabedMain->findChild<Container*>("currentContainer");
	currentContainer->removeAll();

	Container* advancedContainer = m_paneTabedMain->findChild<Container*>("advancedContainer");
	advancedContainer->removeAll();

	Container* historyContainer = m_paneTabedMain->findChild<Container*>("historyContainer");
	historyContainer->removeAll();

	Container* noRecordContainer = m_paneTabedMain->findChild<Container*>("noRecordContainer");
	noRecordContainer->setVisible(false);

	for (int i = 0; i < nCount; i++)
	{
		QVariantMap currentOrderMap = m_orderInfoList.at(i).value<QVariantMap>();

		int nStatus = currentOrderMap.value("STATUS").toInt();

		//Get Response
		QString strOrderId = currentOrderMap.value("ORDERID").toString();
		QString strStatus = m_strDescription[nStatus];
		QString strPickup = currentOrderMap.value("PICKUP").toString();
		QString strDropOff = currentOrderMap.value("DROPOFF").toString();
		int nCabType = currentOrderMap.value("CABTYPE").toInt();
		QString strCabType = (nCabType == 1)? "Budget": "Premium / Executive";

		QString strPickupTime = currentOrderMap.value("PICKUPTIME").toString();
		QDateTime timePickup = QDateTime::fromString(strPickupTime, "yyyy-MM-dd hh:mm:ss");
		strPickupTime = timePickup.toString("yyyy-MM-dd hh:mm");

		QString strSummary = strPickupTime + " / " + strCabType;

		QmlDocument *qml =
				QmlDocument::create("asset:///OrderInfoContainer.qml").parent(this);

		if (!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);

			// create root object for the UI
			Container *orderInfoContainer = qml->createRootObject<Container>();

			QDeclarativePropertyMap* propertyMap = new QDeclarativePropertyMap;

			propertyMap->insert("orderId", QVariant(strOrderId));
			propertyMap->insert("pickupInfo", QVariant(strPickup));
			propertyMap->insert("dropoffInfo", QVariant(strDropOff));
			propertyMap->insert("status", QVariant(strStatus));
			propertyMap->insert("orderSummary", QVariant(strSummary));

			qml->setContextProperty("propertyMap", propertyMap);

			switch (nStatus) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 10:
			{
				currentContainer->add(orderInfoContainer);
				break;
			}

			case 9:
			{
				advancedContainer->add(orderInfoContainer);
				break;
			}

			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			{
				historyContainer->add(orderInfoContainer);
				break;
			}
			}
		}
	}

//	if (currentContainer->count() == 0)
//	{
//		currentContainer->setVisible(false);
//		noRecordContainer->setVisible(true);
//	}
//	else
//	{
//		currentContainer->setVisible(true);
//		noRecordContainer->setVisible(false);
//	}

	emit selectCurrentTab();

}

//Initialize Profile Page
void UnicablinkEventHandler::initializeProfilePage() {
	if (!m_paneTabedMain)
		return;

	//initialize main fields
	QSettings appSetting("UnicablinkSoft", "Unicablink");

	/*
	 * 	//Save user information
	 appSetting.setValue("Password", m_strPassword);
	 appSetting.setValue("Phone", m_strPhoneNo);
	 appSetting.setValue("CustomerID", strCustomerID);
	 appSetting.setValue("Email", strEmail);
	 appSetting.setValue("Name", strName);
	 *
	 */

	QString strName = appSetting.value("Name").toString();
	QString strEmail = appSetting.value("Email").toString();
	QString strPhone = appSetting.value("Phone").toString();

	/////////////////////////////////  Set values  //////////////////////////////////////////////
	//Name
	Container* nameContainer = m_paneTabedMain->findChild<Container*>(
			"nameTextField");
	nameContainer->setProperty("textValue", strName);

	//Email
	Container* emailContainer = m_paneTabedMain->findChild<Container*>(
			"emailTextField");
	emailContainer->setProperty("textValue", strEmail);

	//Phone
	Container* phoneContainer = m_paneTabedMain->findChild<Container*>(
			"phoneTextField");
	phoneContainer->setProperty("textValue", strPhone);

}

//Resend Activation Code
void UnicablinkEventHandler::resendActivationCode() {
	/*
	 * {
	 �SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
	 �Mobile�:�+60123456789�,
	 �Email�:�abcd@xxx.com�
	 �REQUESTTIME�:�2013-05-15 18:00:00�
	 }
	 */
	QMap<QString, QVariant> params;

	QSettings appSetting("UnicablinkSoft", "Unicablink");

	QString strPhoneNo = appSetting.value("Phone").toString();
	QString strEmail = appSetting.value("Email").toString();
	QDateTime currentTime = QDateTime::currentDateTime();
	QString strCurrentTime = currentTime.toString("yyyy-MM-dd hh:mm:ss");

	//Temp Code
	//	strEmail = "wang198904@gmail.com";
	//	strPhoneNo = "+601126253547";

	params.insert("Mobile", strPhoneNo);
	params.insert("Email", strEmail);
	params.insert("REQUESTTIME", strCurrentTime);

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			RESEND_ACTIVATION_CODE);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(checkResendStatus()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

	return;
}

void UnicablinkEventHandler::checkResendStatus() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}
	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();

	/*
	 * {
	 �SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
	 �STATUS�:1,
	 �MESSAGE�: ��,
	 �RESPONSETIME�:�2012-03-14 14:32:55�
	 }
	 *
	 */
	//Analyze Response
	int nStatus = m_jsonMap.value("STATUS").toInt();
	QString strStatus = m_jsonMap.value("Status").toString();
	QString strMessage = m_jsonMap.value("MESSAGE").toString();

	if ((nStatus == 1) || (nStatus == 0)) {
		if (strMessage.length() != 0) {
			showMessage(strMessage);
		} else {
			showMessage("Sent successfully.");
		}
	} else if (nStatus == 2) {
		if (strMessage.length() != 0) {
			showMessage(strMessage);
		} else {
			showMessage("Request Failed, Please try again later.");
		}
	}

	return;
}

//Validate User(log in)
void UnicablinkEventHandler::ValidateUser(QString strPhoneNo,
		QString strPassword) {
	if ((strPhoneNo.length() == 0) || (strPassword.length() == 0)) {
		showMessage("Please fill all fields.");
		return;
	}

	if (!checkPhoneNo(strPhoneNo))
	{
		return;
	}

	//Save temp informations
	m_strPassword = strPassword;
	m_strPhoneNo = strPhoneNo;

	QMap<QString, QVariant> params;

	params.insert("Mobile", strPhoneNo);
	params.insert("Psw", strPassword);
	params.insert("VCode", "");

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params, USER_LOG_IN);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(CheckUser()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}
}

//Activate User Account
void UnicablinkEventHandler::activateUser(QString strActivationCode) {
	if (strActivationCode.length() == 0) {
		showMessage("Please fill activation code");
		return;
	}

	//Make Soap Request
	QMap<QString, QVariant> params;

	/*
	 {
	 "DEVICEID":"83e462f770cc34eb",
	 "Psw":"da907a1b8f74e6922d93b025eecfb852",
	 "SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 "VCode":"",
	 "MOBILE":"+601126253547"
	 }
	 */

	QSettings appSetting("UnicablinkSoft", "Unicablink");

	QString strPhoneNo = appSetting.value("Phone").toString();
	QString strPassword = appSetting.value("Password").toString();

	params.insert("VCode", strActivationCode);
	params.insert("Mobile", strPhoneNo);
	params.insert("Psw", strPassword);

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params, USER_LOG_IN);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(CheckUser()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}

}

//Register
void UnicablinkEventHandler::RegisterUser(QString strName, QString strEmail,
		QString strPhone, QString strPassword, QString strRetype) {
	if (!checkEmailAddress(strEmail))
		return;
	if (!checkPhoneNo(strPhone))
		return;
	if (!checkPassword(strPassword, strRetype))
		return;

	//Make Soap Request
	QMap<QString, QVariant> params;

	/*
	 * {
	 "DEVICEID":"83e462f770cc34eb",
	 "SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 "NAME":"Micheal",
	 "MOBILE":"+8615806131326",
	 "PASSWORD":"future",
	 "PHONETYPE":"3",
	 "EMAIL": "yuan@obama.com"
	 }
	 */

	params.insert("NAME", strName);
	params.insert("MOBILE", strPhone);
	params.insert("PASSWORD", strPassword);
	params.insert("PHONETYPE", "3");
	params.insert("EMAIL", strEmail);

	m_strPhoneNo = strPhone;
	m_strPassword = strPassword;
	m_strEmail = strEmail;

	QString strSOAPMessage = m_soapRequest->getSOAPMessage(params,
			USER_REGISTER);

	//Show activity
	startActivity();

	bool bConnect = m_jsonConnect->SendSoapRequest(strSOAPMessage,
			SLOT(CheckRegisterResult()));

	if (!bConnect) {
		stopActivity();
		//showMessage("Connection Failed, Please check your network connection.");
	}
}

//Check Register Result
void UnicablinkEventHandler::CheckRegisterResult() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}

	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	stopActivity();
	//Analyze Response
	int nStatus = m_jsonMap.value("Status").toInt();

	if (nStatus == 1) {
		showMessage("Successfully registered.");

		QSettings appSetting("UnicablinkSoft", "Unicablink");

		m_bFlagNeedVerification = true;
		m_bLoggedIn = false;

		//Save user information
		appSetting.setValue("Password", m_strPassword);
		appSetting.setValue("Phone", m_strPhoneNo);
		appSetting.setValue("Email", m_strEmail);
		appSetting.setValue("NeedVerification", m_bFlagNeedVerification);
		appSetting.setValue("LoggedIn", m_bLoggedIn);

		if (!m_pageActivation)
		{
			QmlDocument *qml =
					QmlDocument::create("asset:///activation.qml").parent(this);

			if (!qml->hasErrors()) {
				// create a event handler object
				qml->setContextProperty("eventHandler", this);

				// create root object for the UI
				m_pageActivation = qml->createRootObject<Page>();
				m_pageActivation->setParent(m_pMainInstance);
			}
		}

		bb::cascades::Application::instance()->setScene(m_pageActivation);

	} else if (nStatus == 2) {
		showMessage("Failed registration, please try again later.");
	}

	return;
}

// User Log In
void UnicablinkEventHandler::CheckUser() {
	//Get Network Reply
	QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
	if (reply) {
		int nError = reply->error();
		if (nError == QNetworkReply::NoError) {
			responseArray = reply->readAll();

			int nSize = responseArray.count();
			if (nSize == 0)
				return;
		} else {
			stopActivity();
			//showMessage("Disabled Function.");
			return;
		}

	} else {
		stopActivity();
		showMessage("Connection Failed, Please check your network connection.");
		return;
	}

	bool bResult = getResponse();

	if (!bResult) {
		stopActivity();
		showMessage("Service Failed, Please try again later.");
		return;
	}

	/*
	 * {"CustomerID":"253A5F06-9FF9-ED2F-3FE354DA5D57EE0A",
	 * "Email":"future.syg108@gmail.com",
	 * "Name":"future",
	 * "ResponseTime":"2013-08-12 16:14:10",
	 * "SecurityKey":"e92684cb325f1e4d4cce27c0207562bf",
	 * "Status":"1"}
	 */

	//Analyze Response.
	int nStatus = m_jsonMap.value("Status").toInt();

	QSettings appSetting("UnicablinkSoft", "Unicablink");

	/*
	 *      settings.beginGroup("MainWindow");
	 *		settings.setValue("size", size());
	 *		settings.setValue("pos", pos());
	 *		settings.endGroup();
	 */

	/*
	 *		QSettings settings("Moose Soft", "Clipper");
	 * 		settings.beginGroup("MainWindow");
	 *		resize(settings.value("size", QSize(400, 400)).toSize());
	 * 		move(settings.value("pos", QPoint(200, 200)).toPoint());
	 *		settings.endGroup();
	 */

	stopActivity();
	switch (nStatus) {
	case 1: {
		QString strCustomerID = m_jsonMap.value("CustomerID").toString();
		QString strEmail = m_jsonMap.value("Email").toString();
		QString strName = m_jsonMap.value("Name").toString();
		m_bLoggedIn = true;
		m_bFlagNeedVerification = false;

		//Save user information
		appSetting.setValue("Password", m_strPassword);
		appSetting.setValue("Phone", m_strPhoneNo);
		appSetting.setValue("CustomerID", strCustomerID);
		appSetting.setValue("Email", strEmail);
		appSetting.setValue("Name", strName);
		appSetting.setValue("LoggedIn", m_bLoggedIn);
		appSetting.setValue("NeedVerification", m_bFlagNeedVerification);

		m_bFlagNeedVerification = false;
		onShowTabPage();
		break;
	}
	case 2: {
		showMessage("Invalid User ID / Password.");
		break;
	}
	case 3: {
		showMessage("Account Inactive.");
		break;
	}
	case 4: {
		showMessage("You are not verified.");
		break;
	}
	default:
		break;
	}

	//delete reply
	reply->deleteLater();
	return;
}

//Send Email
void UnicablinkEventHandler::SendEmail() {
	//Invoke Email
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.pim.uib.email.hybridcomposer");
	request.setAction("bb.action.OPEN, bb.action.SENDEMAIL");
	request.setUri("mailto:customerservices@unicablink.com");
	invokeManager.invoke(request);
}

//Voice Call
void UnicablinkEventHandler::VoiceCall() {
	//Voice Call
	InvokeManager invokeManager;
	InvokeRequest request;

	QVariantMap map;
	map.insert("number", "1300800222"); // required
	QByteArray requestData = bb::PpsObject::encode(map, NULL);

	request.setAction("bb.action.DIAL");
	request.setMimeType("application/vnd.blackberry.phone.startcall");
	request.setData(requestData);

	invokeManager.invoke(request);
}

//Log In Page
void UnicablinkEventHandler::ShowLogIn() {
	QTimer::singleShot(250, this, SLOT(onShowLogIn()));
}

void UnicablinkEventHandler::onShowLogIn() {
	QSettings appSetting("UnicablinkSoft", "Unicablink");
	m_bFlagNeedVerification = appSetting.value("NeedVerification").toBool();
	m_bLoggedIn = appSetting.value("LoggedIn").toBool();


	if (m_bLoggedIn)
	{
		onShowTabPage();
	}
	else if (m_bFlagNeedVerification)
	{
		if (!m_pageActivation)
		{
			QmlDocument *qml =
					QmlDocument::create("asset:///activation.qml").parent(this);

			if (!qml->hasErrors()) {
				// create a event handler object
				qml->setContextProperty("eventHandler", this);

				// create root object for the UI
				m_pageActivation = qml->createRootObject<Page>();
				m_pageActivation->setParent(m_pMainInstance);
			}
		}

		// set created root object as a scene
		bb::cascades::Application::instance()->setScene(m_pageActivation);
	}
	else
	{
		if (!m_paneLogIn) {
			QmlDocument *qml = QmlDocument::create("asset:///login.qml").parent(
					this);

			if (!qml->hasErrors()) {
				// create a event handler object
				qml->setContextProperty("eventHandler", this);

				// create root object for the UI
				AbstractPane *paneLogIn = qml->createRootObject<AbstractPane>();

				if (paneLogIn) {
					// save to member variable and set its lifecycle property
					m_paneLogIn = paneLogIn;
					m_paneLogIn->setParent(m_pMainInstance);

					QSettings appSetting("UnicablinkSoft", "Unicablink");
					QString strPhoneNo = appSetting.value("Phone").toString();
					QString strPassword = appSetting.value("Password").toString();

					TextField* phoneTextField = m_paneLogIn->findChild<TextField*>("phoneTextField");
					phoneTextField->setProperty("text", strPhoneNo);

					TextField* passwordTextField = m_paneLogIn->findChild<TextField*>("passwordTextField");
					passwordTextField->setProperty("text", strPassword);

				}
			}
		}

		// set created root object as a scene
		bb::cascades::Application::instance()->setScene(m_paneLogIn);
	}


}

//Tab Page
void UnicablinkEventHandler::ShowTabPage() {
	QTimer::singleShot(250, this, SLOT(onShowTabPage()));
}

void UnicablinkEventHandler::onShowTabPage() {
	if (!m_paneTabedMain) {
		QmlDocument *qml =
				QmlDocument::create("asset:///maintabedpage.qml").parent(this);

		if (!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);

			// create root object for the UI
			AbstractPane *paneTabedMain = qml->createRootObject<AbstractPane>();

			if (paneTabedMain) {
				// save to member variable and set its lifecycle property
				m_paneTabedMain = paneTabedMain;
				m_paneTabedMain->setParent(m_pMainInstance);
			}
		}
		else
		{
			int nErrorCount = qml->errors().count();

			for (int i = 0; i < nErrorCount; i++)
			{
				showMessage(qml->errors().at(i).description());
			}
		}
	}

	// set created root object as a scene
	bb::cascades::Application::instance()->setScene(m_paneTabedMain);

	// save current displayed page info
	m_panePrev = m_paneTabedMain;
}

//Book Again
void UnicablinkEventHandler::BookAgain()
{
	if(!m_paneTabedMain) return;

	Tab* mainBookingTab = m_paneTabedMain->findChild<Tab*>("mainBookingTab");

	if (mainBookingTab)
	{
//		initializeMainBookingPage();
		mainBookingTab->setProperty("bInitMainBooking", false);
		((TabbedPane*)m_paneTabedMain)->setActiveTab(mainBookingTab);
	}

	// set created root object as a scene
	bb::cascades::Application::instance()->setScene(m_paneTabedMain);
}

//Show register Page with initial data
void UnicablinkEventHandler::showRegisterWithData()
{
	NavigationPane* profileNavigationPane = m_paneTabedMain->findChild<NavigationPane*>("profileNavigationPane");

	QmlDocument *qml = QmlDocument::create("asset:///register.qml").parent(
			this);

	if (!qml->hasErrors()) {
		// create a event handler object
		qml->setContextProperty("eventHandler", this);
		// create root object for the UI
		Page *pageRegister = qml->createRootObject<Page>();

		if (pageRegister) {
			TextField* nameText = pageRegister->findChild<TextField*>("nameText");
			TextField* emailText = pageRegister->findChild<TextField*>("emailText");

			QSettings appSetting("UnicablinkSoft", "Unicablink");
			QString strName = appSetting.value("Name").toString();
			QString strEmail = appSetting.value("Email").toString();

			nameText->setProperty("text", strName);
			emailText->setProperty("text", strEmail);

			profileNavigationPane->push(pageRegister);
		}
	}
}

//Register Page
void UnicablinkEventHandler::ShowRegister() {
	QTimer::singleShot(250, this, SLOT(onShowRegister()));
}

void UnicablinkEventHandler::onShowRegister() {
	if (!m_paneRegister) {
		QmlDocument *qml = QmlDocument::create("asset:///register.qml").parent(
				this);

		if (!qml->hasErrors()) {
			// create a event handler object
			qml->setContextProperty("eventHandler", this);

			// create root object for the UI
			AbstractPane *paneRegister = qml->createRootObject<AbstractPane>();
			paneRegister->setProperty("screenWidth", m_nWinWidth);
			//homePane->setProperty("screenHeight", m_nWinHeight);

			if (paneRegister) {
				// save to member variable and set its lifecycle property
				m_paneRegister = paneRegister;
				m_paneRegister->setParent(m_pMainInstance);

				// prepare data models
				//RequestMainPageData();
			}
		}
	}

	// set created root object as a scene
	bb::cascades::Application::instance()->setScene(m_paneRegister);
	initializePane();

	// save current displayed page info
	m_panePrev = m_paneRegister;
}

void UnicablinkEventHandler::initializePane() {

}

//start added by my team at 20130928

void UnicablinkEventHandler::showMapViewWithDelay(int msec, int sender)
{
	Q_UNUSED(sender);
	QTimer::singleShot(msec, this, SLOT(setMapViewVisible()));
}

void UnicablinkEventHandler::setMapViewVisible()
{
	Container* mapViewContainer = m_paneTabedMain->findChild<Container*>("mapViewContainer");
	mapViewContainer->setVisible(true);
}

void UnicablinkEventHandler::Message(int w)
{
	showMessage(QString::number(w));
}

void UnicablinkEventHandler::Message(QString s)
{
	showMessage(s);
}

//  end added by my team at 20130928
