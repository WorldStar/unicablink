package view.screens;

import java.util.Timer;
import java.util.TimerTask;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.components.AccountInfoTextField;
import view.components.DetailTopInfoContainer;
import view.components.ImageButtonField;
import view.components.StatusInfoField;
import view.fieldmanager.InfoManager;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import application.setting.AppSetting;
import application.setting.SettingStore;
import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.CancelOrder;
import model.request.Order;
import model.request.OrderStatus;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;

public class CurrentBookingPage extends BaseScreen {

	String strProcessingOrderId = "";
	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;

	private Timer autoRefreshTimer;
	private AutoRefreshTask autoRefreshTask;
	private boolean bAutoRefresh = false;

	/**
	 * Private Members
	 */
	//OrderID
	DetailTopInfoContainer topInfo;

	//Date / Time
	AccountInfoTextField dateTimeInfo;

	//Pickup 
	AccountInfoTextField pickupInfo;

	//Pickup At
	AccountInfoTextField pickupAtInfo;

	//Drop off
	AccountInfoTextField dropOffInfo;

	//Car No
	AccountInfoTextField carNoInfo;

	//Car Type
	AccountInfoTextField carTypeInfo;

	//Driver
	AccountInfoTextField driverInfo;

	//ETA/EDA
	AccountInfoTextField etaInfo;

	//Est.Fare
	AccountInfoTextField estInfo;

	//Status
	StatusInfoField statusInfo;

	//Book again Button
	ImageButtonField _btnBookAgain;
	
//	int nnn = 0;

	public CurrentBookingPage(boolean bAutoRefresh) {
		super();

		this.bAutoRefresh = bAutoRefresh;
		
		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.DETAILS_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));

		/**
		 * Detail Info Manager and Button Manager
		 */
		InfoManager detailsInfoManager = new InfoManager(0, ResouceConstant.DETAILS_TOP_PADDING, 0, 0);
		InfoManager buttonManager = new InfoManager(0, ResouceConstant.DETAILS_BTN_PADDING, 40, 0);

		/**
		 * Create informations
		 */
		//Get information of this order
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		String strCurrentOrderId = theApp.strSelectedOrderId;

		//set member variable
		strProcessingOrderId = strCurrentOrderId;

		JSONArray arrayOrderStatus = RequestToServer();

		String strPickupTime = "";
		String strPickup = "";
		int nCabType = 0;
		String strCabType = "";
		String strCarNo = "";
		String strDropOff = "";
		int nStatus = 0;
		String strDriver= "";
		String strETA = "";
		String strFare = "";
		String strRemark = "";

		try {
			JSONObject jsonStatus = arrayOrderStatus.getJSONObject(0);

			/*
			 * {
							"PICKUPTIME":"2013-08-12 13:10:00",
							"REMARKS":"",
							"SHOWTAXI":"0",
							"COORDINATE":"",
							"CABTYPE":"1",
							"CABNO":"",
							"DROPOFF":"WUXI, JIANGSU, CHINA",
							"MAKER":"",
							"STATUS":"0",
							"DRIVER":"",
							"ETA":"",
							"PASSENGERNAME":"TYUY",
							"MESSAGE":"TQ for your booking. We will proceed your order now!",
							"ORDERID":"75518",
							"FIXEDFARE":"RM0",
							"EFARE":"RM70.00",
							"MOBILENO":"",
							"COLOR":"",
							"PICKUP":"CHENGWAN ROAD, WUZHONG, SUZHOU, JIANGSU, CHINA, 215121"
				}
			 */

			//when find the current order
			strPickupTime = jsonStatus.getString("PICKUPTIME");
			strRemark = jsonStatus.getString("REMARKS");
			strPickup = jsonStatus.getString("PICKUP");
			nCabType = jsonStatus.getInt("CABTYPE");
			strCabType = (nCabType == 1) ? "Budget": "Premium / Executive";
			strCarNo = jsonStatus.getString("CABNO");

			strDropOff = jsonStatus.getString("DROPOFF");
			strDriver = jsonStatus.getString("DRIVER");
			nStatus = jsonStatus.getInt("STATUS");
			strETA = jsonStatus.getString("ETA");

			strFare = jsonStatus.getString("EFARE");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Dialog.alert("Failed recognition of response.");

			return;
		}

		//OrderID
		topInfo = new DetailTopInfoContainer(strCurrentOrderId)
		{
			public void cancelOrder()
			{
				boolean bEnabled = this.getCancelEnabled();
				if (bEnabled)
				{
					cancelCurrentOrder();
				}
			}
		};

		//Date / Time
		dateTimeInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, null,0, 0, 20, 10, "Date / Time", "", strPickupTime, 152, EditField.NO_NEWLINE, 100, 22, true);
		dateTimeInfo.setEditable(false);

		//Pickup 
		pickupInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, ResouceConstant.DETAILS_BAR_MIDDLE_SEL,0, 0, 20, 10, "Pick up", "", strPickup, 152, EditField.NO_NEWLINE, 200, 22, true)
		{
			protected boolean navigationClick(int status, int time) 
			{
				if (status != 0){
					fieldChangeNotify(0);
					clickField();
				}
				return true;
			}

			public boolean keyChar(char key, int status, int time) 
			{
				if (key == Characters.ENTER) {
					fieldChangeNotify(0);
					clickField();
					return true;
				}
				return false;
			}

			protected boolean trackwheelClick(int status, int time)
			{        
				if (status != 0) clickField();    
				return true;
			}

			protected boolean touchEvent(TouchEvent message)
			{
				int x = message.getX( 1 );
				int y = message.getY( 1 );
				if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
					// Outside the field
					return false;
				}
				switch( message.getEvent() ) {
				case TouchEvent.UNCLICK:
					clickField();
					return true;
				}
				return super.touchEvent( message );
			}
			
			public void clickField()
			{
				String strTitle = "";
				if (getLabelText().equals("Pick up")){
					strTitle = "Pick up Location";
				}
				else if (getLabelText().equals("Drop off")){
					strTitle = "Drop off Location";
				}
				
				//String strMessage = getText();
				String strMessage = getFullText();//modified by KO at 20131128
				
				LocationDialog locationDialog = new LocationDialog(strTitle, strMessage);
				locationDialog.show();
			}
		};
		pickupInfo.setEditable(false);
		//pickupInfo.setPopupable(true);

		//Pickup At
		pickupAtInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, null, 0, 0, 20, 10, "Pick up at", "", strRemark, 152, EditField.NO_NEWLINE, 100, 22, true);
		pickupAtInfo.setEditable(false);

		//Drop off
		dropOffInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, ResouceConstant.DETAILS_BAR_MIDDLE_SEL, 0, 0, 20, 10, "Drop off", "", strDropOff, 152, EditField.NO_NEWLINE, 200, 22, true)
		{
			protected boolean navigationClick(int status, int time) 
			{
				if (status != 0){
					fieldChangeNotify(0);
					clickField();
				}
				return true;
			}

			public boolean keyChar(char key, int status, int time) 
			{
				if (key == Characters.ENTER) {
					fieldChangeNotify(0);
					clickField();
					return true;
				}
				return false;
			}

			protected boolean trackwheelClick(int status, int time)
			{        
				if (status != 0) clickField();    
				return true;
			}

			protected boolean touchEvent(TouchEvent message)
			{
				int x = message.getX( 1 );
				int y = message.getY( 1 );
				if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
					// Outside the field
					return false;
				}
				switch( message.getEvent() ) {
				case TouchEvent.UNCLICK:
					clickField();
					return true;
				}
				return super.touchEvent( message );
			}
			
			public void clickField()
			{
				String strTitle = "";
				if (getLabelText().equals("Pick up")){
					strTitle = "Pick up Location";
				}
				else if (getLabelText().equals("Drop off")){
					strTitle = "Drop off Location";
				}
				
				//String strMessage = getText();
				String strMessage = getFullText();//modified by KO at 20131128
				
				LocationDialog locationDialog = new LocationDialog(strTitle, strMessage);
				locationDialog.show();
			}
		};
		dropOffInfo.setEditable(false);
		//dropOffInfo.setPopupable(true);

		//Car No
		carNoInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, null, 0, 0, 20, 10, "Car No", "", strCarNo, 152, EditField.NO_NEWLINE, 100, 22, true);
		carNoInfo.setEditable(false);

		//Car Type
		carTypeInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, null, 0, 0, 20, 10, "Car Type", "", strCabType, 152, EditField.NO_NEWLINE, 100, 22, true);
		carTypeInfo.setEditable(false);

		//Driver
		driverInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, null, 0, 0, 20, 10, "Driver", "", strDriver, 152, EditField.NO_NEWLINE, 100, 22, true);
		driverInfo.setEditable(false);

		//ETA/EDA
		etaInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, null, 0, 0, 20, 10, "ETA/EDA", "", strETA, 152, EditField.NO_NEWLINE, 100, 22, true);
		etaInfo.setEditable(false);

		//Est.Fare
		estInfo = new AccountInfoTextField(ResouceConstant.DETAILS_BAR_MIDDLE, null, 0, 0, 20, 10, "Est.Fare", "", strFare, 152, EditField.NO_NEWLINE, 100, 22, true);
		estInfo.setEditable(false);

		//Status
		statusInfo = new StatusInfoField(ResouceConstant.DETAILS_BAR_DOWN_CANCEL, 0, 0, 20, 10, "Status", "", Utils.getStatus(nStatus), 152, EditField.NO_NEWLINE, 200, 22);//modified by KO
		statusInfo.setEditable(false);

		//Book again Button
		ImageButtonField btnBookAgain = new ImageButtonField(ResouceConstant.DETAILS_BTN_BOOK_AGAIN, "", 0, "")
		{
			public void clickButton()
			{
				bookAgain();
			}
		};

		/**
		 * Refresh Status		
		 */
		refreshStatus(nStatus);

		/**
		 * Add Fields to Manager
		 */
		try
		{
			detailsInfoManager.add(topInfo);
			detailsInfoManager.add(dateTimeInfo);
			detailsInfoManager.add(pickupInfo);
			detailsInfoManager.add(pickupAtInfo);
			detailsInfoManager.add(dropOffInfo);
			detailsInfoManager.add(carNoInfo);
			detailsInfoManager.add(carTypeInfo);
			detailsInfoManager.add(driverInfo);
			detailsInfoManager.add(etaInfo);
			detailsInfoManager.add(estInfo);
			detailsInfoManager.add(statusInfo);
	
			buttonManager.add(btnBookAgain);

			/**
			 * Add Manager to MainManger
			 */
			add(detailsInfoManager);
			add(buttonManager);
		}
		catch(Exception ex)
		{
			// TODO Auto-generated catch block
			ex.printStackTrace();
			Dialog.alert("Failed add of UI components.");

			return;
		}
		
		topInfo.setFocus();
	}
	
	private JSONArray RequestToServer()
	{
		OrderStatus jsonOrderStatus = new OrderStatus();
		Order jsonOrder = new Order();

		jsonOrder.setOrderId(Integer.parseInt(strProcessingOrderId));
		jsonOrder.setRequestCount(1);

		jsonOrderStatus.appendOrder(jsonOrder.toJSONObject());

		//Get App Setting
		AppSetting appSetting = mAppSettingStore.getAppSetting();

		String strPhoneNo = appSetting.getAccountPhone();
		jsonOrderStatus.setPhoneNo(strPhoneNo);

		//Get the response
		JSONObject jsonResult = null;
		String strJSONStatus = jsonOrderStatus.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONStatus, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_UPDATE_STATUS);
		if (jsonResult == null)
		{
			return null;
		}

		/**
		 * Analyze Response
		 */
		JSONArray arrayOrderStatus;
		try 
		{
			arrayOrderStatus = jsonResult.getJSONArray("ORDERARRAY");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("Failed recognition of response.");
			return null;
		}

		return arrayOrderStatus;
	}

	/**
	 * User Interaction Process
	 */
	public void bookAgain()
	{
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		theApp.goBookingForAgain();
	}

	public void refreshStatus(int nStatus)
	{
		/**
		 * set the status
		 */
		if ((nStatus == 4) || (nStatus == 5) || (nStatus == 6) || (nStatus == 7) || (nStatus == 8)) //Completed, cann't cancel
		{
			statusInfo.setBackground(ResouceConstant.DETAILS_BAR_DOWN_CANCEL);
			topInfo.setCancelEnabled(false);
			stopAutoRefresh();
			bAutoRefresh = false;
		}
		else //Not cancelled
		{
			statusInfo.setBackground(ResouceConstant.DETAILS_BAR_DOWN_NORMAL);
			topInfo.setCancelEnabled(true);
		}
	}

	public void cancelCurrentOrder()
	{
		if (strProcessingOrderId.equals(""))
			return;

		//Create JSON Request
		CancelOrder jsonCancel = new CancelOrder();


		jsonCancel.setOrderID(strProcessingOrderId);

		//Get the response
		JSONObject jsonResult = null;

		String strJSONCancel = jsonCancel.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONCancel, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_CANCEL_ORDER);

		if (jsonResult == null)
		{
			return;
		}

		/*
		 * {
				 �SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
				 �ORDERID�:160165,
				 �STATUS�:1,
				 �MESSAGE�: �Passenger Cancelled�,
				 �RESPONSETIME�:�2012-03-14 14:32:55�
		   }
		 *
		 */

		int nStatus;
		String strMessage;
		try 
		{
			nStatus = jsonResult.getInt("STATUS");
			strMessage = jsonResult.getString("MESSAGE");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("Failed recognition of response.");
			return;
		}


		if (nStatus == 1) {
			if (strMessage.length() == 0)
				strMessage = "Passenger Cancelled.";
			Dialog.alert(strMessage);

			int nChangedStatus = 6; //Passenger Cancelled

			statusInfo.setText(strMessage);
			refreshStatus(nChangedStatus);
		} 
		else if (nStatus == 2) 
		{
			if (strMessage.length() == 0)
				strMessage = "No Order Cancelled.";
			Dialog.alert(strMessage);
		} 
		else 
		{
			if (strMessage.length() == 0)
				strMessage = "Strange response.";
			Dialog.alert(strMessage);
		}

	}

	protected void onDisplay()
	{
		System.out.println("$$$$$$$$$$$$$$$ CurrentBookingPage onDisplay $$$$$$$$$$$$$$$");
		startAutoRefresh();
	}

	protected void onUndisplay()
	{
		System.out.println("$$$$$$$$$$$$$$$ CurrentBookingPage onUndisplay $$$$$$$$$$$$$$$");
		stopAutoRefresh();
	}

	private void startAutoRefresh()
	{
//		bAutoRefresh = true;
		if(bAutoRefresh == true){
			if(autoRefreshTimer != null) autoRefreshTimer = null;
			autoRefreshTimer = new Timer();
			if(autoRefreshTask != null) autoRefreshTask = null;
			autoRefreshTask = new AutoRefreshTask();
			autoRefreshTimer.schedule(autoRefreshTask, 30000, 30000);
		}
	}
	
	private void stopAutoRefresh()
	{
		if(bAutoRefresh == true && autoRefreshTimer != null && autoRefreshTask != null){
			autoRefreshTimer.cancel();
			try{
				this.wait((long)300);
			}catch(Exception ex){
				
			}

			autoRefreshTimer = null;
			autoRefreshTask = null;
		}
//		bAutoRefresh = false;
	}

	private void AutoRefresh()
	{
		System.out.println("$$$$$$$$$$$$$$$  Status info auto refresh  $$$$$$$$$$$$$$$");
		JSONArray arrayOrderStatus = RequestToServer();

		String strPickupTime = "";
		String strPickup = "";
		int nCabType = 0;
		String strCabType = "";
		String strCarNo = "";
		String strDropOff = "";
		int nStatus = 0;
		String strDriver= "";
		String strETA = "";
		String strFare = "";
		String strRemark = "";

		try {
			JSONObject jsonStatus = arrayOrderStatus.getJSONObject(0);

			/*
			 * {
							"PICKUPTIME":"2013-08-12 13:10:00",
							"REMARKS":"",
							"SHOWTAXI":"0",
							"COORDINATE":"",
							"CABTYPE":"1",
							"CABNO":"",
							"DROPOFF":"WUXI, JIANGSU, CHINA",
							"MAKER":"",
							"STATUS":"0",
							"DRIVER":"",
							"ETA":"",
							"PASSENGERNAME":"TYUY",
							"MESSAGE":"TQ for your booking. We will proceed your order now!",
							"ORDERID":"75518",
							"FIXEDFARE":"RM0",
							"EFARE":"RM70.00",
							"MOBILENO":"",
							"COLOR":"",
							"PICKUP":"CHENGWAN ROAD, WUZHONG, SUZHOU, JIANGSU, CHINA, 215121"
				}
			 */

			//when find the current order
			strPickupTime = jsonStatus.getString("PICKUPTIME");
			strRemark = jsonStatus.getString("REMARKS");
			strPickup = jsonStatus.getString("PICKUP");
			nCabType = jsonStatus.getInt("CABTYPE");
			strCabType = (nCabType == 1) ? "Budget": "Premium / Executive";
			strCarNo = jsonStatus.getString("CABNO");

			strDropOff = jsonStatus.getString("DROPOFF");
			strDriver = jsonStatus.getString("DRIVER");
			nStatus = jsonStatus.getInt("STATUS");
			strETA = jsonStatus.getString("ETA");

			strFare = jsonStatus.getString("EFARE");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Dialog.alert("Failed recognition of response.");

			return;
		}

		//Date / Time
		try{
			dateTimeInfo.setText(strPickupTime);
		}catch(Exception ex){}

		//Pickup 
		try{
			pickupInfo.setText(strPickup);
		}catch(Exception ex){}

		//Pickup At
		try{
			pickupAtInfo.setText(strRemark);
		}catch(Exception ex){}

		//Drop off
		try{
			dropOffInfo.setText(strDropOff);
		}catch(Exception ex){}

		//Car No
		try{
			carNoInfo.setText(strCarNo);
		}catch(Exception ex){}

		//Car Type
		try{
			carTypeInfo.setText(strCabType);
		}catch(Exception ex){}

		//Driver
		try{
			driverInfo.setText(strDriver);
		}catch(Exception ex){}

		//ETA/EDA
		try{
			etaInfo.setText(strETA);
		}catch(Exception ex){}

		//Est.Fare
		try{
			estInfo.setText(strFare);
		}catch(Exception ex){}

		//Status
		try{
			statusInfo.setText(Utils.getStatus(nStatus));
//			statusInfo.setText(Utils.getStatus(nnn++));
		}catch(Exception ex){}

		/**
		 * Refresh Status
		 */
		refreshStatus(nStatus);
//		if(nnn >= 11) nnn = 0;
	}

	private class AutoRefreshTask extends TimerTask
	{
		public void run() {
			if(bAutoRefresh == true)
				AutoRefresh();
			else
				cancel();
		}
	}
}
