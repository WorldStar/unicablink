package model.data;

public class DataConstant {
	public static final String PRODUCTION_SERVER_URL = "http://202.9.98.42/App/WS/CabOrderingV2/ws.cfc?wsdl";
	public static final String DEVELOP_SERVER_URL = "http://202.9.98.143/App/WS/CabOrderingV2/ws.cfc?wsdl";
	
	//In Booking Screen
	public static final int FROM_PICKUP_CONTROL = 1;
	public static final int FROM_DROPOFF_CONTROL = 2;
	
	//Keys of screens 
	public static final long APP_REGISTER_KEY = 0x327c3c12bf8977bfL;
	
	//Request type
	public static final String API_LOG_IN = "user_login";
	public static final String API_REGISTER = "user_register";
	public static final String API_ACTIVATE = "user_login";
	public static final String API_RESEND_CODE = "send_activation_code";
	public static final String API_RETRIEVE_PASSWORD = "retrieve_password";
	public static final String API_CAB_ORDER = "cab_order";
	public static final String API_UPDATE_PROFILE = "update_profile";
	public static final String API_CANCEL_ORDER = "cancelorder_sunlight";
	public static final String API_UPDATE_STATUS = "get_order_status";
	public static final String API_QUERY_POSITION = "getvehiclelastknowntrack";
	public static final String API_SEARCH_LOCATION = "search_location";
	
	//Contact Information
	public static final String SERVICE_EMAIL_ADDRESS = "customerservices@unicablink.com";
	public static final String SERVICE_VOICE_CALL = "1300800222";
	
	
	
}
