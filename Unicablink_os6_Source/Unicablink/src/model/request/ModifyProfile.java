package model.request;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class ModifyProfile {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private String m_strCID = "";
	private String m_strPhoneNo = "";
	private String m_strName = "";
	private String m_strOldPassword = "";
	private String m_strNewPassword = "";
	private String m_strEmailAddress = "";
	private String m_strPhoneType = "3";

	/*
	 * request type: 
	 * 
	 * {
	 *		�SECURITYKEY�:�e92684cb325f1e4d4cce27c0207562bf�,
	 *		�NAME�:�Micheal�,
	 *		�CUSTOMERID�:� 8AE8BE30-D459-8565-47F75BD4134A101C�
	 *		�MOBILE�:�+8615806131326�,
	 *		�OLDPASSWORD�:�123456�
	 *		�NEWPASSWORD�:�abcd12�,
	 *		�PHONETYPE�:��,
	 *		�EMAIL�: �yuan@obama.com�
	 * }
	 *
	 */

	public void setCID(String strCID){
		m_strCID = strCID;
	}

	public void setPhoneNo(String strPhoneNo){
		m_strPhoneNo = strPhoneNo;
	}

	public void setName(String strName){
		m_strName = strName;
	}

	public void setOldPassword(String strOldPsw){
		m_strOldPassword = strOldPsw;
	}

	public void setNewPassword(String strNewPsw){
		m_strNewPassword = strNewPsw;
	}

	public void setEmailAddress(String strEmail){
		m_strEmailAddress = strEmail;
	}

	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();

		try{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("NAME", m_strName);
			jsonResult.put("CUSTOMERID", m_strCID);
			jsonResult.put("MOBILE", m_strPhoneNo);
			jsonResult.put("OLDPASSWORD", m_strOldPassword);
			jsonResult.put("NEWPASSWORD", m_strNewPassword);
			jsonResult.put("PHONETYPE", m_strPhoneType);
			jsonResult.put("EMAIL", m_strEmailAddress);
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonResult;
	}

}
