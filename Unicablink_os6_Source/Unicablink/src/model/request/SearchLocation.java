package model.request;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import application.utils.DateUtils;

public class SearchLocation {
	private String m_strSecurityKey = "e92684cb325f1e4d4cce27c0207562bf";
	private int m_nSearchType = 2;
	private String m_strLocation = "";
	private String m_strCoord = "";
	private String m_strCurCoord = "";


	/*
	 {
	 "Name":"KU",
	 "SECURITYKEY":"e92684cb325f1e4d4cce27c0207562bf",
	 "REQUESTTIME":"2013-08-12 11:23:26",
	 "CurCoordinate":"",
	 "Type":"2",
	 "Coordinate":""
	 }
	 */

	public void setSearchType(int nSearchType)
	{
		m_nSearchType = nSearchType;
	}

	public void setLocation(String strLocation)
	{
		m_strLocation = strLocation;
	}

	public void setCoord(String strCoord)
	{
		m_strCoord = strCoord;
	}

	public void setCurCoord(String strCurCoord)
	{
		m_strCurCoord = strCurCoord;
	}

	public JSONObject toJSONObject(){
		JSONObject jsonResult = new JSONObject();

		//Get Current time
		String strCurrentTime = DateUtils.getCurrentTimeString();

		try{
			jsonResult.put("SECURITYKEY", m_strSecurityKey);
			jsonResult.put("Type", 2);
			jsonResult.put("REQUESTTIME", strCurrentTime);
			jsonResult.put("Coordinate", m_strCoord);
			jsonResult.put("CurCoordinate", m_strCurCoord);
			jsonResult.put("Name", m_strLocation);
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonResult;
	}

}
