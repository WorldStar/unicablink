{
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "Ejin",
               "short_name" : "Ejin",
               "types" : [ "sublocality", "political" ]
            },
            {
               "long_name" : "Alxa",
               "short_name" : "Alxa",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Nei Mongol",
               "short_name" : "Nei Mongol",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "China",
               "short_name" : "CN",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Ejin, Alxa, Inner Mongolia, China",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 42.7951629,
                  "lng" : 103.103721
               },
               "southwest" : {
                  "lat" : 40.4054075,
                  "lng" : 97.1727628
               }
            },
            "location" : {
               "lat" : 41.958542,
               "lng" : 101.068934
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 42.7951629,
                  "lng" : 103.103721
               },
               "southwest" : {
                  "lat" : 40.4054075,
                  "lng" : 97.1727628
               }
            }
         },
         "types" : [ "sublocality", "political" ]
      },
      {
         "address_components" : [
            {
               "long_name" : "Alxa",
               "short_name" : "Alxa",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Nei Mongol",
               "short_name" : "Nei Mongol",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "China",
               "short_name" : "CN",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Alxa, Inner Mongolia, China",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 42.7951629,
                  "lng" : 106.8668544
               },
               "southwest" : {
                  "lat" : 37.4067797,
                  "lng" : 97.1727628
               }
            },
            "location" : {
               "lat" : 38.851892,
               "lng" : 105.728969
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 39.0635801,
                  "lng" : 106.105957
               },
               "southwest" : {
                  "lat" : 38.5715395,
                  "lng" : 105.3643799
               }
            }
         },
         "types" : [ "locality", "political" ]
      },
      {
         "address_components" : [
            {
               "long_name" : "Nei Mongol",
               "short_name" : "Nei Mongol",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "China",
               "short_name" : "CN",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Inner Mongolia, China",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 53.33717799999999,
                  "lng" : 126.0755856
               },
               "southwest" : {
                  "lat" : 37.4067797,
                  "lng" : 97.1727628
               }
            },
            "location" : {
               "lat" : 40.817498,
               "lng" : 111.765618
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 53.33717799999999,
                  "lng" : 126.0755856
               },
               "southwest" : {
                  "lat" : 37.4067797,
                  "lng" : 97.1727628
               }
            }
         },
         "types" : [ "administrative_area_level_1", "political" ]
      },
      {
         "address_components" : [
            {
               "long_name" : "China",
               "short_name" : "CN",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "China",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 53.56097399999999,
                  "lng" : 134.7728099
               },
               "southwest" : {
                  "lat" : 18.1535216,
                  "lng" : 73.4994136
               }
            },
            "location" : {
               "lat" : 35.86166,
               "lng" : 104.195397
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 53.56097399999999,
                  "lng" : 134.7728099
               },
               "southwest" : {
                  "lat" : 18.1535216,
                  "lng" : 73.4994136
               }
            }
         },
         "types" : [ "country", "political" ]
      }
   ],
   "status" : "OK"
}
