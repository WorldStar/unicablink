import bb.cascades 1.0

Container {
    id: root

    property alias itemWidth: root.preferredWidth
    property alias itemHeight: root.preferredHeight

    property alias labelImageSource: labelBackground.imageSource
    property alias textImageSource: textBackground.imageSource

    property alias nameLabelWidth: itemNameContainer.preferredWidth

    property int bStatus: 0
    property alias labelText: itemNameLabel.text
    property alias valueText: itemValueLabel.text
    property bool bPopupable: false
    property bool bAnimate: false

    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }

    Container {
        id: itemNameContainer

        background: labelBackground.imagePaint
        minHeight: itemHeight

        leftPadding: 30.0
        rightPadding: 30.0

        layout: DockLayout {

        }
        Label {
            id: itemNameLabel

            preferredWidth: itemNameContainer.preferredWidth - 60

            verticalAlignment: VerticalAlignment.Center

            textStyle.color: Color.create("#ff606060")
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 7.0
        }
    }

    Container {
        id: itemValueContainer
        minHeight: itemHeight
        
        background: textBackground.imagePaint
        
        horizontalAlignment: HorizontalAlignment.Left
        verticalAlignment: VerticalAlignment.Center
        
        leftPadding: 20.0
        rightPadding: 20.0
        
        layout: DockLayout {
        
        }

        ScrollView {
            scrollViewProperties {
                scrollMode: ScrollMode.Horizontal
                overScrollEffectMode: OverScrollEffectMode.OnScroll
                pinchToZoomEnabled: false
            }

			verticalAlignment: VerticalAlignment.Center
			preferredWidth: root.preferredWidth - itemNameContainer.preferredWidth

            Label {
            	id: itemValueLabel
                
                verticalAlignment: VerticalAlignment.Center
                
                textStyle.color: (bStatus == 0)? Color.create("#ff606060"): Color.White
                textStyle.fontSize: FontSize.PointValue
                textStyle.fontSizeValue: 7.0

                animations: [
                    TranslateTransition {
                        id: translateAnimation
                        fromX: 400
                        toX: 0
                        duration: 0
                        repeatCount: AnimationRepeatCount.Forever
                        easingCurve: StockCurve.Linear
                        
                    }
                ]
                
                onTextChanged: {
                    translateAnimation.stop();
                    translateAnimation.toX = -(itemValueLabel.text.length * 7 * 2);
                    var distance = translateAnimation.fromX - translateAnimation.toX;
                    if(distance < 0) distance = -distance;
                    translateAnimation.duration = distance * 10;
                    translateAnimation.play();
                }
            }
            
//            onTouch: {
//                if (event.isUp()) {
//                	eventHandler.showFullLocation(itemNameLabel.text, itemValueLabel.text);
//                }
//            }
            
            onCreationCompleted: {
                if(bAnimate == true){
                    translateAnimation.play();
                }
            }
        }
    }
    
    attachedObjects: [
        ImagePaintDefinition {
            id: labelBackground 
        },
        
        ImagePaintDefinition {
            id: textBackground
        }
    ]
}
