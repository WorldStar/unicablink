package view.components;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResouceConstant;
import application.Utils;

public class OrderTopInfoManager extends HorizontalFieldManager{
	/**
	 * private members
	 */
	//left: 36, orderid: 130, label: 340, imagebutton

	private int nLeftMargin = 0;
	private int nWidthOrderId = 0;
	private int nWidthDescription = 0;
	private int nTotalWidth = 0;
	private int nTotalHeight = 0;

	private String strOrderID;
	private String strSummary;

	private Font mFontText;
	private int nFontSize = 0;

	private int nPadding = 0;

	private float fScale = Utils.getVRatio();
	private Bitmap mBmpBgSel = null;
	private Bitmap mBmpBgUnsel = null;
	private boolean focusflg = false;

	public OrderTopInfoManager(String strOrderId, String strSummary)
	{
		super(USE_ALL_WIDTH | Manager.HORIZONTAL_SCROLL | Manager.HORIZONTAL_SCROLLBAR);

		/**
		 * initialize
		 */
		this.strOrderID = strOrderId;
		nLeftMargin = (int)(36 * fScale);
		nWidthOrderId = (int)(130 * fScale);
		nWidthDescription = (int)(340 * fScale);
		nFontSize = 30;

		/**
		 * set Background
		 */

		mBmpBgUnsel = Utils.getBitmapResource(ResouceConstant.HISTORY_CONTENT_TOP);
		mBmpBgSel = Utils.getBitmapResource(ResouceConstant.HISTORY_CONTENT_TOP_SEL);
		
		//initialze
		nTotalWidth = mBmpBgSel.getWidth();
		nTotalHeight = mBmpBgSel.getHeight();

		//set fonts
		mFontText = Utils.getFont(Font.BOLD, nFontSize);

		LabelField labelOrder = new LabelField(strOrderId);
		labelOrder.setFont(mFontText);
		
		LabelField labelDescription = new LabelField(strSummary);
		labelDescription.setFont(mFontText);

		Bitmap bmpArrow = Utils.getBitmapResource(ResouceConstant.HISTORY_BTN_ARROW);

		/**
		 * Add fields to Manager
		 */
		add(labelOrder);
		add(labelDescription);
		add(new BitmapField(bmpArrow));

		add(new NullField(NullField.FOCUSABLE));
	}

	/**
	 * overrides
	 */
	public void sublayout(int nWidth, int nHeight)
	{
		int nNumFields = getFieldCount();

		if (nNumFields == 4)
		{
			int nXPos = nLeftMargin;

			Field labelOrder = this.getField(0);
			Field labelDescription = this.getField(1);
			Field btnArrow = this.getField(2);

			setPositionChild(labelOrder, nXPos, (nTotalHeight - (int)(nFontSize * fScale)) / 2);
			layoutChild(labelOrder, nWidthOrderId, Integer.MAX_VALUE);

			nXPos = nXPos + nWidthOrderId;

			layoutChild(labelDescription, nWidthDescription, Integer.MAX_VALUE);
			setPositionChild(labelDescription, nXPos, (nTotalHeight - labelDescription.getHeight()) / 2);
			

			nXPos = nXPos + nWidthDescription;

			setPositionChild(btnArrow, nXPos, (nTotalHeight - btnArrow.getPreferredHeight()) / 2);
			layoutChild(btnArrow, btnArrow.getPreferredWidth(), Integer.MAX_VALUE);
		}

		this.setExtent(nTotalWidth, nTotalHeight);
	}
	
	public int getPreferredHeight() {
		return nTotalHeight;
	}
	
	public int getPreferredWidth() {
		return nTotalWidth;
	}

	/**
	 * User Interaction Process
	 */
	public void showOrderDetails()
	{
		
	}
	
//	public void setBtnBackground(String strBGFile)
//	{
//		//set background
//		mBmpBg = Utils.getBitmapResource(strBGFile);
//		setBackground(BackgroundFactory.createBitmapBackground(mBmpBg));
//		invalidate();
//	}
	
	public String getOrderId()
	{
		return this.strOrderID;
	}
	
	protected void paint(Graphics graph) {
		super.paint(graph);
		if (focusflg){
			setBackground(BackgroundFactory.createBitmapBackground(mBmpBgSel));
		}
		else{
			setBackground(BackgroundFactory.createBitmapBackground(mBmpBgUnsel));
		}

	}
	protected void onFocus(int direction){
		focusflg = true;
	}
	protected void onUnfocus(){
		focusflg = false;
	}
	protected boolean navigationClick(int status, int time) 
	{
		if (status != 0){
			fieldChangeNotify(0);
			showOrderDetails();
		}
		return true;
	}

	public boolean keyChar(char key, int status, int time) 
	{
		if (key == Characters.ENTER) {
			fieldChangeNotify(0);
			showOrderDetails();
			return true;
		}
		return false;
	}
	protected boolean trackwheelClick(int status, int time)
	{        
		if (status != 0) showOrderDetails();    
		return true;
	}

	protected boolean invokeAction(int action) 
	{
		switch( action ) {
		case ACTION_INVOKE: {
			showOrderDetails(); 
			return true;
		}
		}
		return super.invokeAction(action);
	}    


	protected boolean touchEvent(TouchEvent message)
	{
		int x = message.getX( 1 );
		int y = message.getY( 1 );
		if( x < 0 || y < 0 || x > getExtent().width || y > getExtent().height ) {
			// Outside the field
			return false;
		}
		switch( message.getEvent() ) {
		case TouchEvent.UNCLICK:
			showOrderDetails();
			return true;
		}
		return super.touchEvent( message );
	}

}

