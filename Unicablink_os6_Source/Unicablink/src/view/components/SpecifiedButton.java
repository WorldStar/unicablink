package view.components;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResouceConstant;
import application.Utils;

public class SpecifiedButton extends HorizontalFieldManager{
	/**
	 * private members
	 */
	//left: 30, label: 150, location: 440, right: 20, paddingButton: 370

	private int nLeftMargin = 0;
	private int nRightMargin = 0; 
	
	private int nTotalWidth = 0;
	private int nTotalHeight = 0;
	private int nPadding = 0;

	private float fScale = Utils.getVRatio();
	
	String strBtnFilePath = "";
	
	private Font mFontText;
	private int nFontSize = 0;

	LabelField mLable;
	ImageButtonField mButton;
	
	public SpecifiedButton(String strLabel, String strBtnPath, String strBgPath)
	{
		super(USE_ALL_WIDTH | Manager.HORIZONTAL_SCROLL | Manager.HORIZONTAL_SCROLLBAR);

		/**
		 * initialize
		 */
		nLeftMargin = (int)(30 * fScale);
		nRightMargin = (int)(20 * fScale);
		nPadding = (int)(20 * fScale);
		
		this.strBtnFilePath = strBtnPath;
		
		nFontSize = 30;

		/**
		 * set Background
		 */
		Bitmap bgBmp = Utils.getBitmapResource(strBgPath);
		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));

		//initialze
		nTotalWidth = bgBmp.getWidth();
		nTotalHeight = bgBmp.getHeight();
		
		//Image Button
		mButton = new ImageButtonField(this.strBtnFilePath, "", 0, "")
		{
			public void clickButton()
			{
				doAction();
			}
		};

		//set fonts
		mFontText = Utils.getFont(Font.PLAIN, nFontSize);

		mLable = new LabelField(strLabel);
		mLable.setFont(mFontText);

		/**
		 * Add fields to Manager
		 */
		add(mButton);
		add(mLable);
		
	}

	/**
	 * overrides
	 */
	public void sublayout(int nWidth, int nHeight)
	{
		int nNumFields = getFieldCount();

		if (nNumFields == 2)
		{
			int nXPos = nLeftMargin;

			Field button = this.getField(0);
			Field label = this.getField(1);
			
			setPositionChild(button, nXPos, (nTotalHeight - (int)(button.getPreferredHeight())) / 2);
			layoutChild(button, button.getPreferredWidth(), Integer.MAX_VALUE);

			nXPos = nXPos + button.getPreferredWidth() + nPadding;

			layoutChild(label, nTotalWidth - nLeftMargin - nRightMargin - button.getPreferredWidth(), Integer.MAX_VALUE);
			setPositionChild(label, nXPos, (nTotalHeight - label.getHeight()) / 2);
		}

		this.setExtent(nTotalWidth, nTotalHeight);
	}

	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}
	
	public void setLableText(String strText)
	{
		mLable.setText(strText);
		invalidate();
	}
	
	public String getLabelText()
	{
		return mLable.getText();
	}
	
	/**
	 * User Interation Process
	 */
	public void doAction()
	{
		
	}
}
