package view.screens;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.ForgotPassword;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import application.ResouceConstant;
import application.Utils;
import application.setting.SettingStore;
import application.utils.StringUtils;
import view.components.AccountInfoTextField;
import view.components.ImageButtonField;
import view.fieldmanager.InfoManager;



public class ForgotScreen extends BaseScreen {

	/**
	 * Private members
	 */
	AccountInfoTextField phoneInfo;
	
	private SettingStore mAppSettingStore = application.Unicablink.mSettingStore;
	
	public ForgotScreen() {
		super();
		
		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.FORGOT_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));
		
		/**
		 * Phone Info Manager
		 */
		InfoManager forgotInfoManager = new InfoManager(ResouceConstant.FORGOT_GUTTER_DESCRIPTION, ResouceConstant.FORGOT_GUTTER_BUTTON, 0, 0);
		
		//Phone No Info
		phoneInfo = new AccountInfoTextField(ResouceConstant.FORGOT_BG_TEXT, null, 0, 0, 30, 30, 
				"Phone No", "e.g. +60123456789", "", 140, EditField.FILTER_PHONE, 15, 34, false);
		
		Bitmap bmpDescription = Utils.getBitmapResource(ResouceConstant.FORGOT_DESCRIPTION);
		
		//add fields
		forgotInfoManager.add(phoneInfo);
		forgotInfoManager.add(new BitmapField(bmpDescription));
		
		/**
		 * Button Manager
		 */
		InfoManager buttonManager = new InfoManager(0, ResouceConstant.FORGOT_GUTTER_BUTTON, 0, 0);
		
		//Submission Button
		ImageButtonField submissionButton = new ImageButtonField(ResouceConstant.FORGOT_BTN_FORGOT, "", 0, "")
		{
			public void clickButton()
			{
				onClickSubmission();
			}
		};
		
		//add button
		buttonManager.add(submissionButton);
		
		/**
		 * Add all field managers
		 */
		add(forgotInfoManager);
		add(buttonManager);
	}
	
	/**
	 * User interactions
	 */
	public void onClickSubmission()
	{
		String strPhoneNo = phoneInfo.getText();
		
		//check phone no
		if (!StringUtils.isPhoneNoFormat(strPhoneNo))
			return;
		
		//Create JSON Request
		ForgotPassword jsonForgot = new ForgotPassword();
		
		jsonForgot.setEmail("");
		jsonForgot.setPhoneNo(strPhoneNo);
		
		//Get the response
		JSONObject jsonResult = null;
		
		String strJSONForgot = jsonForgot.toJSONObject().toString();
		jsonResult = HTTPConnectionHelper.requestToServer(strJSONForgot, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_RETRIEVE_PASSWORD);
		
		if (jsonResult == null)
		{
			return;
		}
		
		
		/*
		 * Response type:
		 * 
		 * {
		 * �SECURITYKEY�:� a5ab307e0cec2e0d17e523e62298f0c1�,
		 * �STATUS�:1,
		 * �MESSAGE�: �Password have send to your mobile or mail�,
		 * �RESPONSETIME�:�2012-03-14 14:32:55�
		 * }
		 *
		 */
		
		int nStatus;
		String strMessage;
		try 
		{
			nStatus = jsonResult.getInt("STATUS");
			strMessage = jsonResult.getString("MESSAGE");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			Dialog.alert("Failed recognition of response.");
			return;
		}
		
		Dialog.alert(strMessage);
	}

}
