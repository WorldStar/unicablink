package view.screens;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import view.components.AddressTopInfoContainer;
import view.components.BubbleDescription;
import view.components.CandidateButton;
import view.components.ImageButtonField;
import view.fieldmanager.CandidateManager;
import application.ResouceConstant;
import application.Unicablink;
import application.Utils;
import model.data.DataConstant;
import model.networkconnection.HTTPConnectionHelper;
import model.request.SearchLocation;
import net.rim.device.api.lbs.maps.model.MapPoint;
import net.rim.device.api.lbs.maps.ui.MapAction;
import net.rim.device.api.lbs.maps.ui.MapField;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.AbsoluteFieldManager;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class AddressScreen extends BaseScreen implements FieldChangeListener{

	/**
	 * private members
	 */
	private double dLat = 0;
	private double dLon = 0;
	private boolean locationOffset = false;//modified by KO

	private CandidateManager candManager = null;
	private AddressTopInfoContainer addressInfo;

	private MapField mMapField;
//	private HorizontalFieldManager contentManager;
//	private VerticalFieldManager contentManager;//modified by KO
	private AbsoluteFieldManager contentManager;//modified by KO
	BubbleDescription bubble;

	public AddressScreen() {
		super();

		//set top bar
		Bitmap bmpTopBar = Utils.getBitmapResource(ResouceConstant.ADDRESS_TOP_BAR);
		add(new BitmapField(bmpTopBar, Field.FIELD_HCENTER));

		//Get App's saved data
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		String strAddress = theApp.mStrTempLocation;

		int nFromControl = theApp.nFlagScreen;
		locationOffset = false;//modified by KO
		//Get Latitude and Longitude
		if (nFromControl == DataConstant.FROM_PICKUP_CONTROL)
		{
			dLat = theApp.dPickupLat;
			dLon = theApp.dPickupLon;
		}
		else
		{
			dLat = theApp.dDropOffLat;
			dLon = theApp.dDropOffLon;
		}

		//Process Exception
		if ((dLat == 0) && (dLon == 0))
		{
			//set Malaysian coordinates
			dLat = 3.1581084;
			dLon = 101.7118345;
		}else{
			locationOffset = true;//modified by KO
		}



		//AddressTopInfo
		addressInfo = new AddressTopInfoContainer(strAddress)
		{
			public void searchAddress()
			{
				String strSearchString = this.getAddress();

				if (!strSearchString.equals(""))
				{
					getSearchResultFromSever(strSearchString);
				}
			}
		};
		
		//Bubble //start modified by KO
		String strCurrentLocation = "";
		if(locationOffset)//modified by KO
			strCurrentLocation = getCurrentAddress(dLat, dLon);
		bubble = new BubbleDescription(strCurrentLocation)
		{
			public boolean isFocusable() {
				// TODO Auto-generated method stub
				return false;
			}
		};
		
		if (bubble != null)
		{
			if (strCurrentLocation.length() == 0)
			{
				bubble.setVisualState(Field.VISUAL_STATE_DISABLED);
			}
		}
		//end modified by KO

		//MapField
		mMapField = new MapField(Utils.getDisplayWidth(), (int)(ResouceConstant.ADDRESS_MAP_HEIGHT * Utils.getVRatio()))
		{
			public boolean isFocusable() {
				// TODO Auto-generated method stub
				boolean rt = true;
				if(contentManager != null && contentManager.getFieldCount() == 5)
					rt = false;
				return rt;
			}
		};
		mMapField.setAutoUpdate(true);
		mMapField.setChangeListener(this);
		
		
		
		MapAction action = mMapField.getAction();
		action.setCentreAndZoom(new MapPoint(dLat, dLon), 4);


		//Apply Location Button
		ImageButtonField btnApply = new ImageButtonField(ResouceConstant.ADDRESS_BTN_APPLY, "", 0, "")
		{
			public void clickButton()
			{
				applyLocation();
			}
		};

		String strPinPath = (nFromControl == DataConstant.FROM_PICKUP_CONTROL)? ResouceConstant.ADDRESS_ICON_PICKUP: ResouceConstant.ADDRESS_ICON_DROPOFF;

		Bitmap bmpPin = Utils.getBitmapResource(strPinPath);
		BitmapField btnPin = new BitmapField(bmpPin, Field.FIELD_HCENTER)
		{
			public boolean isFocusable() {
				// TODO Auto-generated method stub
				return false;
			}
		};
/* modified by KO		
		String strCurrentLocation = "";
		if(locationOffset)//modified by KO
			strCurrentLocation = getCurrentAddress(dLat, dLon);
		bubble = new BubbleDescription(strCurrentLocation)
		{
			public boolean isFocusable() {
				// TODO Auto-generated method stub
				return false;
			}
		};
		
		if (bubble != null)
		{
			if (strCurrentLocation.length() == 0)
			{
				bubble.setVisualState(Field.VISUAL_STATE_DISABLED);
			}	
		}
*/		


//		contentManager = new HorizontalFieldManager()
//		contentManager = new VerticalFieldManager()//modified by KO
		contentManager = new AbsoluteFieldManager()//modified by KO
		{
			/**
			 * overrides
			 */
			public void sublayout(int nWidth, int nHeight)
			{
				int nNumFields = getFieldCount();


				if (nNumFields == 5)
				{
					Field mapField = this.getField(0);
					Field pin = this.getField(1);
					Field bubble = this.getField(2);
					Field candContainer = this.getField(3);//modified by KO
					Field button = this.getField(4);//modified by KO

					layoutChild(mapField, mapField.getPreferredWidth(), mapField.getPreferredHeight());
					setPositionChild(mapField, 0, 0);
					
					layoutChild(pin, pin.getPreferredWidth(), pin.getPreferredHeight());
					setPositionChild(pin, (Utils.getDisplayWidth() - pin.getPreferredWidth()) / 2, (int)(Utils.getVRatio() * ResouceConstant.ADDRESS_MAP_HEIGHT) / 2 - pin.getPreferredHeight());

					layoutChild(bubble, bubble.getPreferredWidth(), bubble.getPreferredHeight());
					setPositionChild(bubble, 0, (int)(Utils.getVRatio() * ResouceConstant.ADDRESS_MAP_HEIGHT) / 2 - pin.getPreferredHeight() - bubble.getPreferredHeight());

					layoutChild(button, button.getPreferredWidth(), button.getPreferredHeight());
					setPositionChild(button, (Utils.getDisplayWidth() - button.getPreferredWidth()) / 2, (int)(Utils.getVRatio() * ResouceConstant.ADDRESS_BTN_PADDING));
					
					layoutChild(candContainer, candContainer.getPreferredWidth(), candContainer.getPreferredHeight());
					setPositionChild(candContainer, 0, 0);
				}

				if (nNumFields == 4)
				{
					Field mapField = this.getField(0);
					Field pin = this.getField(1);
					Field bubble = this.getField(2);
					Field button = this.getField(3);

					layoutChild(mapField, mapField.getPreferredWidth(), mapField.getPreferredHeight());
					setPositionChild(mapField, 0, 0);
					
					layoutChild(pin, pin.getPreferredWidth(), pin.getPreferredHeight());
					setPositionChild(pin, (Utils.getDisplayWidth() - pin.getPreferredWidth()) / 2, (int)(Utils.getVRatio() * ResouceConstant.ADDRESS_MAP_HEIGHT) / 2 - pin.getPreferredHeight());

					layoutChild(bubble, bubble.getPreferredWidth(), bubble.getPreferredHeight());
					setPositionChild(bubble, 0, (int)(Utils.getVRatio() * ResouceConstant.ADDRESS_MAP_HEIGHT) / 2 - pin.getPreferredHeight() - bubble.getPreferredHeight());

					layoutChild(button, button.getPreferredWidth(), button.getPreferredHeight());
					setPositionChild(button, (Utils.getDisplayWidth() - button.getPreferredWidth()) / 2, (int)(Utils.getVRatio() * ResouceConstant.ADDRESS_BTN_PADDING));
				}

				this.setExtent(Utils.getDisplayWidth(), (int)(ResouceConstant.ADDRESS_MAP_HEIGHT * Utils.getVRatio()));
			}
			
//			protected boolean touchEvent(TouchEvent te){
//				if(te.getEvent()==TouchEvent.CANCEL)
//					return true;
//				int x = te.getX(1);
//				int y = te.getY(1);
//				if (y < 0 || y > this.getHeight()) 
//					return super.touchEvent(te);
//				if((y >= 0) && (y <= this.getHeight())){
//					int index = this.getFieldAtLocation(x,y);
//					if(index < 0) return true;
//					
//					System.out.println("index::"+index);
//					Field f = this.getField(index);
//					f.get
//					if(f==null) return true;
//
//					this.invalidate();
//					System.out.println("invalidate called");
//					if(te.getEvent()==TouchEvent.UP)
//						return true;
//					if(te.getEvent()==TouchEvent.CLICK)
//						return false;
//				}
//				return true;
//			}
		};

		/**
		 * Add Containers to Main Manager
		 */
		add(addressInfo);


		contentManager.add(mMapField);
		
		contentManager.add(btnPin);
		contentManager.add(bubble);
		contentManager.add(btnApply);
		

		add(contentManager);
		
		if (strAddress.length() > 0){
			addressInfo.searchAddress();
		}
	}

	/**
	 * Change Listener for map field
	 */
	public void fieldChanged( Field field, int actionId )
	{
		switch ( actionId )
		{
		case MapAction.ACTION_CENTRE_CHANGE:
		{
			if (candManager != null)
			{
				candManager.deleteAll();
				contentManager.delete(candManager);
				candManager = null;
				
			}
			
			MapPoint mPoint = (MapPoint)mMapField.getFocusedMappable();
			double dTempLat = mPoint.getLat();
			double dTempLon = mPoint.getLon();

			setCandidateInfo("", dTempLat, dTempLon);
			break;
		}
		case MapAction.ACTION_ZOOM_CHANGE:
			break;
		}
	}

	/**
	 * Utils Function
	 * 
	 * Get Candidates from Server
	 * @param strSearchString
	 */
	/**
	 * Reverse Geocoding
	 */
	public String getCurrentAddress(double dLat, double dLon)
	{
		if ((dLat == 0) && (dLon == 0)) return "";

		//Get Address and set pickup field text
		JSONObject jsonResult = null;

		jsonResult = HTTPConnectionHelper.requestAddress(dLat, dLon);

		if (jsonResult == null)
		{
			return "";
		}


		/*
		 {
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "Ejin",
               "short_name" : "Ejin",
               "types" : [ "sublocality", "political" ]
            },
            {
               "long_name" : "Alxa",
               "short_name" : "Alxa",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Nei Mongol",
               "short_name" : "Nei Mongol",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "China",
               "short_name" : "CN",
               "types" : [ "country", "political" ]
            }
         ],
         "formatted_address" : "Ejin, Alxa, Inner Mongolia, China",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 42.7951629,
                  "lng" : 103.103721
               },
               "southwest" : {
                  "lat" : 40.4054075,
                  "lng" : 97.1727628
               }
            },
            "location" : {
               "lat" : 41.958542,
               "lng" : 101.068934
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 42.7951629,
                  "lng" : 103.103721
               },
               "southwest" : {
                  "lat" : 40.4054075,
                  "lng" : 97.1727628
               }
            }
         },
         "types" : [ "sublocality", "political" ]
      }],
   "status" : "OK"
		 */

		String strStatus;
		String strResults;

		try 
		{
			strStatus = jsonResult.getString("status");
			strResults = jsonResult.getString("results");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

			Dialog.alert("Service Failed.");
			return "";
		}

		if (!strStatus.equals("OK"))
			return "";

		JSONArray aJSONArrayResults;
		try {
			aJSONArrayResults = new JSONArray(strResults);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

		int nCount = aJSONArrayResults.length();

		if (nCount > 0)
		{
			try {
				JSONObject jsonAddress = aJSONArrayResults.getJSONObject(0);
				String strAddress = jsonAddress.getString("formatted_address");

				return strAddress;

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}

		}

		return "";
	}

	public void getSearchResultFromSever(String strSearchString)
	{
		//Create JSON Request
		SearchLocation jsonSearch = new SearchLocation();

		jsonSearch.setLocation(strSearchString);

		//Get the response
		JSONObject jsonResult = null;

		String strJSONSearch = jsonSearch.toJSONObject().toString();


		jsonResult = HTTPConnectionHelper.requestToServer(strJSONSearch, 
				DataConstant.PRODUCTION_SERVER_URL, DataConstant.API_SEARCH_LOCATION);

		if (jsonResult == null)
		{
			return;
		}

		/*
		 * Response type:
		 * 
		 * /*
		 * {
			 LOCATIONS:
			 [
				 {COORDINATE:3.0637590,101.7395700,DISTANCE:,NAME:Alam Damai ,Kuala Lumpur},
				 {COORDINATE:3.1800560,101.6734440,DISTANCE:,NAME:Antah Tower Condo, Jalan Kuching,},
				 {COORDINATE:3.1718880,101.7509670,DISTANCE:,NAME:Au 2, Kuala Lumpur},
				 {COORDINATE:3.2836580,101.5127120,DISTANCE:,NAME:Bandar Baru Kundang , Rawang},
				 {COORDINATE:3.1825650,101.6968560,DISTANCE:,NAME:Bandar Baru Sentul, Kuala Lumpur},
				 {COORDINATE:3.0441400,101.7395490,DISTANCE:,NAME:Bandar Damai Perdana, Kuala Lumpur},
				 {COORDINATE:3.1943270,101.6129350,DISTANCE:,NAME:Bandar Sri Damansara, Kuala Lumpur},
				 {COORDINATE:3.1009050,101.7139500,DISTANCE:,NAME:Bandar Sri Permaisuri, Kuala Lumpur},
				 {COORDINATE:3.0935480,101.7224820,DISTANCE:,NAME:Bandar Tun Razak, Kuala Lumpur},
				 {COORDINATE:3.1335180,101.6684030,DISTANCE:,NAME:Bangsar Baru, Kuala Lumpur}
			 ],
			 RESPONSETIME:2013-08-16 08:12:36,
			 SECURITYKEY:e92684cb325f1e4d4cce27c0207562bf
           }
		 *
		 */

		JSONArray aJSONLocations;
		try 
		{
			aJSONLocations = jsonResult.getJSONArray("LOCATIONS");
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		int nLocationCount = aJSONLocations.length();
		
		if (nLocationCount > 3)
			nLocationCount = 3;

		if (nLocationCount > 0){
			if (candManager != null)
			{
				candManager.deleteAll();
				contentManager.delete(candManager);
				candManager = null;
			}
			candManager = new CandidateManager();
			candManager.setBackground(BackgroundFactory.createSolidBackground(0xffff00));

			for (int i = 0; i < nLocationCount; i++)
			{
				JSONObject jsonLocation;
				try {
					jsonLocation = aJSONLocations.getJSONObject(i);

					/**
					 * Get Location Information
					 */
					//Get Location
					String strFindedLocation = jsonLocation.getString("NAME");
					String strCoordinate = jsonLocation.getString("COORDINATE");

					//Get Lat and Lon
					int nIdx = strCoordinate.indexOf(",");
					String strLat = strCoordinate.substring(0, nIdx - 1);
					String strLon = strCoordinate.substring(nIdx + 1);

					double dFindedLat = Double.parseDouble(strLat);
					double dFindedLon = Double.parseDouble(strLon);



					CandidateButton candButton = new CandidateButton(strFindedLocation, dFindedLat, dFindedLon)
					{
						public void clickButton()
						{
							setCandidateInfo(this.getLocation(), this.getDLat(), this.getDLon());
						}
					};

					candManager.add(candButton);


				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}

			}
			contentManager.insert(candManager, 3);//modified by KO
//			contentManager.add(candManager);//modified by KO
		}
	}

	/**
	 * User Interaction
	 */
	public void setCandidateInfo(String strLocation, double dLat, double dLon)
	{
		this.dLat = dLat;
		this.dLon = dLon;
		
		if (bubble != null && locationOffset)//modified by KO
		{
			bubble.setVisualState(Field.VISUAL_STATE_NORMAL);
		}
		

		MapAction action = mMapField.getAction();
		action.setCentreAndZoom(new MapPoint(dLat, dLon), 3);

		String strSelectedLocation = getCurrentAddress(dLat, dLon);
		
		if (bubble != null && locationOffset)//modified by KO
		{
			bubble.setLocation(strSelectedLocation);
			if (strSelectedLocation.length() == 0)
			{
				bubble.setVisualState(Field.VISUAL_STATE_DISABLED);
			}
		}


		if (addressInfo != null && locationOffset)//modified by KO
			addressInfo.setAddress(strSelectedLocation);
		
		if(!locationOffset) locationOffset = true;//modified by KO
		
		if (candManager != null)
		{
			candManager.deleteAll();
			contentManager.delete(candManager);
			candManager = null;
		}
			
	}

	public void applyLocation()
	{
		/**
		 * Get and Set app's data
		 */
		Unicablink theApp = (Unicablink)UiApplication.getUiApplication();

		int nFlagFrom = theApp.nFlagScreen;


		if (nFlagFrom == DataConstant.FROM_PICKUP_CONTROL)
		{
			theApp.dPickupLat = dLat;
			theApp.dPickupLon = dLon;

			theApp.strPickupLocation = getCurrentAddress(dLat, dLon);

			theApp.mBookingScreen.setPickupLocation(theApp.strPickupLocation);
		}
		else
		{
			theApp.dDropOffLat = dLat;
			theApp.dDropOffLon = dLon;

			theApp.strDropOffLocation = getCurrentAddress(dLat, dLon);

			theApp.mBookingScreen.setDropOffLocation(theApp.strDropOffLocation);
		}


		theApp.mBookingScreen.setAllPin();

		//pop this Screen
		theApp.popAddressScreen();
	}
}
