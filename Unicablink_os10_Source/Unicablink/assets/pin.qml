import bb.cascades 1.0

//! [0]
Container {
    // File path of the pin image
    property string pinImageSource: (index == 0) ? "asset:///Image/icon_pickup.png": "asset:///Image/icon_dropoff.png"
    // pointerOffsetX, pointerOffsetY is the position of the pixel in pin image that should point to the location. Change these to match your pin image.
    property int pointerOffsetX: 26
    property int pointerOffsetY: 70
    /////////////////////////////////////////////////////////
    id: root
    property int index: 0
    property int x: 0
    property int y: 0
    property double lat:0
    property double lon:0
    
    
    clipContentToBounds: false
    overlapTouchPolicy: OverlapTouchPolicy.Allow
    layoutProperties: AbsoluteLayoutProperties {
        id: position
        positionX: x - pointerOffsetX
        positionY: y - pointerOffsetY
    }
    layout: DockLayout {

    }
    
    ImageView {
        id: pinImage

        imageSource: pinImageSource
        overlapTouchPolicy: OverlapTouchPolicy.Allow
    }
    
}
//! [0]
