package view.components;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import application.ResouceConstant;
import application.Utils;

public class LocationContainer extends HorizontalFieldManager{
	/**
	 * private members
	 */
	//left: 30, label: 150, location: 440, right: 20, paddingButton: 370

	private int nLeftMargin = 0;
	private int nRightMargin = 0; 
	private int nWidthLabel = 0;
	private int nWidthLocation = 0;
	private int nTopMargin = 0;

	private int nTotalWidth = 0;
	private int nTotalHeight = 0;
	private int nPadding = 0;

	private float fScale = Utils.getVRatio();
	private String mstrLocation;
	
	String strBtnFilePath = "";
	
	private Font mFontText, mFontLocation;
	private int nFontSize = 0;

	LabelField mLabelDescription;
	InsideLabelEditField mLableLocation;
	ImageButtonField mBtnLocation;
	
	public LocationContainer(String strLabel, String strLocation, String strBtnPath, String strHintText)
	{
		super(USE_ALL_WIDTH | Manager.HORIZONTAL_SCROLL | Manager.HORIZONTAL_SCROLLBAR);

		/**
		 * initialize
		 */
		nLeftMargin = (int)(30 * fScale);
		nWidthLabel = (int)(120 * fScale);
		nRightMargin = (int)(20 * fScale);
		nTopMargin = (int)(12 * fScale);
		nWidthLocation = (int)(420 * fScale);
		nPadding = (int)(370 * fScale);
		
		this.strBtnFilePath = strBtnPath;
		
		nPadding = (int)(0 * fScale);
		nFontSize = 26;

		/**
		 * set Background
		 */

		Bitmap bgBmp = Utils.getBitmapResource(ResouceConstant.BOOKING_BAR_LONG);
		setBackground(BackgroundFactory.createBitmapBackground(bgBmp));

		//initialze
		nTotalWidth = bgBmp.getWidth();
		nTotalHeight = bgBmp.getHeight();

		//set fonts
		mFontText = Utils.getFont(Font.PLAIN, nFontSize);
		mFontLocation = Utils.getFont(Font.PLAIN, nFontSize);

		mLabelDescription = new LabelField(strLabel);
		mLabelDescription.setFont(mFontText);

		mstrLocation = strLocation;
		if(strLocation.length() > 110){
			strLocation = strLocation.substring(0,  105) + " ...";
		}
		mLableLocation = new InsideLabelEditField(strHintText, strLocation, 300, 0);
		mLableLocation.setEditable(false);
		mLableLocation.setFont(mFontLocation);

		mBtnLocation = new ImageButtonField(strBtnFilePath, "", 0, "")
		{
			public void clickButton()
			{
				showLocation();
			}
		};

		/**
		 * Add fields to Manager
		 */
		add(mLabelDescription);
		add(mLableLocation);
		add(mBtnLocation);
	}

	/**
	 * overrides
	 */
	public void sublayout(int nWidth, int nHeight)
	{
		int nNumFields = getFieldCount();

		if (nNumFields == 3)
		{
			int nXPos = nLeftMargin;

			Field labelDescription = this.getField(0);
			Field labelLocation = this.getField(1);
			Field btnLocation = this.getField(2);
			
			setPositionChild(labelDescription, nXPos, (nTotalHeight - (int)(nFontSize * fScale)) / 2);
			layoutChild(labelDescription, nWidthLabel, Integer.MAX_VALUE);

			nXPos = nXPos + nWidthLabel;

			layoutChild(labelLocation, nWidthLocation, Integer.MAX_VALUE);
			setPositionChild(labelLocation, nXPos, (nTotalHeight - labelLocation.getHeight()) / 2);
			

			nXPos = nXPos + nWidthLocation;

			setPositionChild(btnLocation, nXPos, (nTotalHeight - btnLocation.getPreferredHeight()) / 2);
			layoutChild(btnLocation, btnLocation.getPreferredWidth(), Integer.MAX_VALUE);
		}

		this.setExtent(nTotalWidth, nTotalHeight);
	}

	public int getPreferredHeight() {
		return nTotalHeight;
	}

	public int getPreferredWidth() {
		return nTotalWidth;
	}
	
	public void setLocation(String strLocation)
	{
		mstrLocation = strLocation;
		if(strLocation.length() > 110){
			strLocation = strLocation.substring(0, 105) + " ...";
		}
		mLableLocation.setText(strLocation);
		invalidate();
	}
	
	public String getLocation()
	{
		return mstrLocation;
//		return mLableLocation.getText();
	}
	
	/**
	 * User Interation Process
	 */
	public void showLocation()
	{
		
	}
	
	
	
}
