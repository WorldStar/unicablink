APP_NAME = Unicablink

CONFIG += qt warn_on cascades10
LIBS += -lbbplatform -lbbcascadesmaps
LIBS += -lQtLocationSubset
LIBS += -lbb
LIBS += -lbbdata
LIBS += -lbbdevice
LIBS += -lbbutility
LIBS += -lbbsystem
LIBS += -lGLESv1_CM
LIBS += -lGLESv1_CM
QT += xml

include(config.pri)

device {
    CONFIG(debug, debug|release) {
        # Device-Debug custom configuration
    }

    CONFIG(release, debug|release) {
        # Device-Release custom configuration
    }
}

simulator {
    CONFIG(debug, debug|release) {
        # Simulator-Debug custom configuration
    }
}
