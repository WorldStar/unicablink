package view.fieldmanager;

import application.Utils;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class CandidateManager extends VerticalFieldManager{

	public CandidateManager()
	{
		super(USE_ALL_WIDTH | Manager.VERTICAL_SCROLL | Manager.VERTICAL_SCROLLBAR);
	}
	
    public void sublayout(int nWidth, int nHeight)
    {
        int nNumFields = getFieldCount();
        int nYPos = 0; 
        
        for (int i = 0; i < nNumFields; i++)
        {
            Field fField = this.getField(i);
            
            layoutChild(fField, this.getPreferredWidth(), Integer.MAX_VALUE);
            setPositionChild(fField, (Utils.getDisplayWidth() - fField.getPreferredWidth()) / 2, nYPos);
            
            nYPos = nYPos + fField.getPreferredHeight();
        }

        if (nNumFields > 0)
        {
        	if (nNumFields >= 3)
        	{
        		this.setExtent(Utils.getDisplayWidth(), (int)(240 * Utils.getVRatio()));
        	}
        	else
        	{
        		this.setExtent(Utils.getDisplayWidth(), (int)(nNumFields * 80 * Utils.getVRatio()));
        	}
        }
        else
        {
        	this.setExtent(Utils.getDisplayWidth(), 0);
        }
    }
    
	protected boolean touchEvent(TouchEvent message)
	{
		System.out.println("aaaaaaaaaaaaaaa: candidateManager touched");
		return super.touchEvent( message );
	}
}
