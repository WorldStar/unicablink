import bb.cascades 1.0

ScrollView {
    scrollViewProperties {
        scrollMode: ScrollMode.Vertical
        overScrollEffectMode: OverScrollEffectMode.OnScroll
        pinchToZoomEnabled: false
    }
}